var duration = moment.duration({
    'seconds': $(':input[name="unixtime"]').val()
});
var timestamp = new Date(0, 0, 0, 2, 10, 30);
var interval = 1;
var timer = setInterval(function() {
    timestamp = new Date(timestamp.getTime() + interval * 1000);
    duration = moment.duration(duration.asSeconds() - interval, 'seconds');
    var min = duration.minutes();
    var sec = duration.seconds();
    sec -= 1;
    if (min < 0) return clearInterval(timer);
    if (min < 10 && min.length != 2) min = '0' + min;
    if (sec < 0 && min != 0) {
        min -= 1;
        sec = 59;
    } else if (sec < 10 && length.sec != 2) sec = '0' + sec;
    $('#countdown').text(min + ':' + sec);
    if (min == 0 && sec == 0) {
        clearInterval(timer);
        $('#countdown').parent().hide();
        $('#taskAnswerForm').submit();
    }
}, 1000);