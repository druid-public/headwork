# Cool-In

## Cas d'usages

- Acheter du pain
- Organiser l'anniversaire de mon fils
    - Demander les copains qu'ils veut
    - Faire les cartes d'invitation
    - Acheter les cadeaux
    - Récupérer la participation des parents
    - Faire la déco
    - Faire les courses
    - Commander le gâteau
    - Ranger
    - Charger l'appareil photo
    - Charger le camescope
    - Envoyer des photos aux parents
- urgent, payer impot
- pour le 5/12, review BDA
- écrire le rapport
    - 1 faire le plan
    - 1 récupérer le modele
    - 2 valider le plan 


- rester en forme
    - chaque jour
        - faire des pompes
        - faire des squats
- entretenir la voiture


## Idées

- Proposer Heisenowher
- Mettre un temps par défaut, basé sur historique
- Echelle 5mn, 1/2 journée, journée, si plus il faudra splitter
- proposer un style (tutoiement, ...)
- s'inspirer de la syntaxe MD ou org-mod
- vue jour / semaine / mois / année
- capacité à ingurgiter des todos manuscripts (tesseract?)
- montrer la proposition de calendrier
- vue jour/semaine/mois
- donner le rapport entre estimation du temps et réalité
- indiquer qu'on commence et qu'on fini un truc
- demander si une tâche s'est bien passée (génial, content, pas content)
- proposition visible en ics
- proposer de déléguer ?
- si c'est court, le faire tout de suite
- urgent/important d'abord
- donner des rôles (travail, famille, personnel)
- equilibrer des rôles
- avoir un score de stress cumulé (trouver un modèle de stress)
- proposer des choses, en relation avec des agenda thématiques (et si vous alliez au cinéma voir ...)
- vue "Et maintenant", qui permet de dire "pas maintenant"
- faire un design avec fluidui
- comparer avec Google Next et autres
