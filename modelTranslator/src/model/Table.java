package model;

import java.util.Map;

public class Table {
// translation to David's format (SQL ?)
    private Map<String, String> attributes;
    private String name;

    public Table(Map<String, String> attributes, String name) {
        this.name = name;
        this.attributes = attributes;
    }

    public String createTable(){
        String sqlreq = "DROP TABLE IF EXISTS `"+ name +"`;\n" +
                "CREATE TABLE IF NOT EXISTS `"+ name +"` (\n";
        for (String attribute:attributes.keySet()){
            sqlreq += "`"+attribute+"` "+attributes.get(attribute)+",\n";
        }
        return sqlreq;
    }
}
