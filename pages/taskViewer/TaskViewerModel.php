<?php

/**
 * Tasks Viewer Model.
 * 
 * @author Projet M2 VIZ
 * @see tasks-viewer.php
 */


/**
 * Class NodeData.
 */
class NodeData
{
    public $id;
    public $label;
    public $type;

    /**
     * Class constructor
     */
    public function __construct(string $id, string $label, string $type)
    {
        $this->id = $id;
        $this->label = $label;
        $this->type = $type;
    }
}

/**
 * Class EdgeData.
 */
class EdgeData
{
    public $source;
    public $target;
    public $type;
    public $label;
    public $state;

    /**
     * Class constructor
     */
    public function __construct(string $source, string $target, string $type, string $label, string $state)
    {
        $this->source = $source;
        $this->target = $target;
        $this->type = $type;
        $this->label = $label;
        $this->state = $state;
    }
}
