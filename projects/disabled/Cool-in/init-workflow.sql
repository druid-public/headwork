DELETE from Artifact where classid in (select id from ArtifactClass where project = 'Cool-in');
DELETE from ArtifactClass where project = 'Cool-in';
-- To do task automaton
insert into ArtifactClass(id,description, definition, tablename, project, autostart)
values(9000, 'Donnez-nous vos TODOs !', 'Cool-in/coolin.sca', 'Artifact', 'Cool-in', true);

insert into ArtifactClass(id,description, definition, tablename, project, autostart)
values(9001, 'Diviser pour régner : décomposez des gros TODOs', 'Cool-in/task-decomposition.sca', 'Artifact', 'Cool-in', true);

DROP TABLE IF EXISTS CoolInAnswerButton;
CREATE TABLE CoolInAnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO CoolInAnswerButton values (1,'Personnelle','danger');
INSERT INTO CoolInAnswerButton values (2,'Profesionnelle','warning');
INSERT INTO CoolInAnswerButton values (3,'Familiale','success');
INSERT INTO CoolInAnswerButton values (4,'1 - aucun stress, voire relaxant','');
INSERT INTO CoolInAnswerButton values (5,'2 - aucun stress, neutre','');
INSERT INTO CoolInAnswerButton values (6,'3 - stress léger, motivant','');
INSERT INTO CoolInAnswerButton values (7,'4 - stress, désagréable, fatiguant','');
INSERT INTO CoolInAnswerButton values (8,'5 - stress très élevé, ne peut durer trop longtemps','');
INSERT INTO CoolInAnswerButton values (9,'30mn ou moins','');
INSERT INTO CoolInAnswerButton values (10,'1h','');
INSERT INTO CoolInAnswerButton values (11,'2h','');
INSERT INTO CoolInAnswerButton values (12,'1 demi-journée','');
INSERT INTO CoolInAnswerButton values (13,'1 jour','');
INSERT INTO CoolInAnswerButton values (14,'2 jours','');
INSERT INTO CoolInAnswerButton values (15,'3 jours','');
INSERT INTO CoolInAnswerButton values (16,'1 semaine','');
INSERT INTO CoolInAnswerButton values (17,'Plus de 1 semaine','');
