<?php

function starti18n(){
    if(!array_key_exists("lang",$_SESSION)){
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
        $usedLang = ['fr', 'en','ka'];
        $lang = in_array($lang, $usedLang) ? $lang : 'en';
        $_SESSION['lang']=$lang;
    }

    $fr=array(
            // template-main.tmpl
            "Projects"=>"Projets",
            "Contribute!"=>"Contribuez !",
            "Embark others"=>"Recrutez",
            "Progress"=>"Progressez",
            "Forum"=>"Forum",
            "View feedbacks"=>"Retours",
            "Tools"=>"Outils",
            "Monitor"=>"Tableau de bord",
            "Reset"=>"Réinitialisation",
            "Logged as"=>"Connecté comme",
            "Edit profile"=>"Edition du profil",
            "Logout"=>"Déconnexion",
            "About"=>"A propos",
            "Credits"=>"Crédits",
            "Our project"=>"Le projet",
            "Source"=>"A propos",
            "Wiki"=>"Wiki",
            "Documentation"=>"Documentation",
            "Thank you for your feedback ! We will take it into consideration."=>"Merci pour votre retour, nous allons le prendre en compte.",
            "Any feedback"=>"Une remarque ?",
            "What do you think we should improve on this page?"=>"Que devrions-nous améliorer sur cette page ?",
            "Cancel"=>"Annuler",
            "Send feedback"=>"Envoyer",
            // showtask.php
            "Then Contribute!"=>"Puis contribuez !",
            "Choose your activity..."=>"Choisissez votre activité...",
            "Task Description"=>"Description de la tâche",
            "Relevance Score"=>"Pertinence",
            "Start!"=>"Démarrer !",
            "Error: skill not found task or user skill identify"=>"Erreur: Compétence non trouvé pour la tâche ou l'utilisateur",
            // profile.php
            "'s profile"=>" : profil",
            "Availability:" => "Disponibilité : ",
            "Indicate how often you plan on coming on the platform:"=>"Indiquez avec quelle fréquence vous aimeriez participer sur la plateforme :",
            "As often as possible"=>"Aussi souvent que possible",
            "Once per week or so" =>"Environ une fois par semaine",
            "Some times per month"=>"Quelques fois par mois",
            "Some times per year" =>"Quelques fois par an",
            "Accept conditions below and save changes"=>"Accepter les conditions ci-dessous et enregistrer",
            // skills.php
            "Manage you skills" =>"Gérez vos compétences !",
            "Please indicate/update your skills on this dashboard. " =>"Merci d'indiquer vos compétences sur ce tableau",
            "Rookie......Fair........Expert"=>"Débutant....Amateur.....Expert",
            "Common sense"=>"Savoir général",
            "Browse skills"=>"Rechercher une compétence",
            // Forum
            "Welcome on the forum"=>"Bienvenue sur le forum",
            "There are currently "=>"Il y a actuellement ",
            "There is currently "=>"Il y a actuellement ",
            " topics in the forum."=>" sujets dans le forum",
            " topic in the forum."=>" sujet dans le forum",
            "Search a topic"=>"Rechercher un sujet",
            "Create a topic"=>"Créer un sujet",
            "Last change: "=>"Dernière mise à jour : ",
            // Monitor
            "Monitor Board"=>"Tableau de bord",
            // Headache
            "Ouuuch! ... Headache!"=>"Ouuuch ! Gros mal de tête !",
            "Sorry, we got an internal error. We are on it. Try going back to the "=>"Désolé, nous rencontrons un problème. On travaille dessus. Essayez de retourner à cette ",
            "main page"=>"page"
    );

    $ka=array(
                // template-main.tmpl
            "Projects"=>"პროექტები",
            "Contribute!"=>"შეიტანე შენი წვლილი!",
            "Embark others"=>"ჩართე სხვაც",
            "Progress"=>"წინ წადი",
            "Forum"=>"ფორუმი",
            "View feedbacks"=>"უკუკავშირი",
            "Tools"=>"ხელსაწყოები",
            "Monitor"=>"მონიტორინგის დაფა",
            "Reset"=>"გადატვირთვა",
            "Logged as"=>"შემოსულია, ",
            "Edit profile"=>"პროფილის რედაქტირება",
            "Logout"=>"გასვლა",
            "About"=>"შესახებ",
            "Credits"=>"კრედიტები",
            "Our project"=>"ჩვენი პროექტი",
            "Source"=>"წყარო",
            "Wiki"=>"ვიკი",
            "Documentation"=>"დოკუმენტაცია",
            "Thank you for your feedback ! We will take it into consideration."=>"გმადლობთ უკუკავშირისთვის. გავითვალისწინებთ",
            "Any feedback"=>"გააქვთ შენიშვნა",
            "What do you think we should improve on this page?"=>"რისი გაუმჯობესება შეიძლება ამ გვერდზე",
            "Cancel"=>"გაუქმება",
            "Send feedback"=>"გაგზავნა",
                // showtask.php
                "Then Contribute!"=>"შეიტანეთ თქვენი წვლილი!",
                "Choose your activity..."=>"აირჩიეთ თქვენი აქტივობა...",
                "Task Description"=>"დაავალების აღწერა",
                "Relevance Score"=>"შესაბამისობის ქულა",
                "Start!"=>"დაწყება",
                "Error: skill not found task or user skill identify"=>"შეცდომა: უნარი ვერ მოიძებნა ამ დავალებისთვის ან მომხმარებლისთვის",
                // profile.php
                "'s profile"=>" : პროფილი",
                "Availability:" => "ხელმისაწვდომობა: ",
                "Indicate how often you plan on coming on the platform:"=>"რამდენად ხშირად აპირებთ პლატფორმაზე შემოსვლას:",
                "As often as possible"=>"რაც შეიძლება ხშირად",
                "Once per week or so" =>"დაახლოებით კვირაში ერთხელ",
                "Some times per month"=>"თვეში რამდენჯერმე",
                "Some times per year" =>"წელიწადში რამდენჯერმე",
                "Accept conditions below and save changes"=>"დაეთანხმეთ ქვემოთ მოცემულ პირობებს და შეინახეთ ცვლილებები",
                // skills.php
                "Manage you skills" =>"მართეთ თქვენი უნარები",
                "Please indicate/update your skills on this dashboard. " =>"გთხოვთ, მიუთითოთ/განაახლოთ თქვენი უნარები ამ დაფაზე",
                "Rookie.......Fair.......Expert"=>"ახალბედა.......მოყვარული........ექსპერტი",
                "Common sense"=>"ზოგადი ცოდნა",
                "Browse skills"=>"მოძებნეთ უნარი",
                // Forum
                "Welcome on the forum"=>"კეთილი იყოს თქვენი მობრძანება ფორუმზე",
                "There are currently "=>"ამჟამად არსებობს ",
                "There is currently "=>"ამჟამად არსებობს ",
                " topics in the forum."=>" თემები ფორუმზე",
                " topic in the forum."=>" თემა ფორუმზე",
                "Search a topic"=>"მოძებნეთ თემა",
                "Create a topic"=>"შექმენით თემა",
                "Last change: "=>"Ბოლო განახლება: ",
                // Monitor
                "Monitor Board"=>"მონიტორინგის დაფა",
                // Headache
                "Ouuuch! ... Headache!"=>"აუუუუ! დიდი თავის ტკივილი!",
                "Sorry, we got an internal error. We are on it. Try going back to the "=>"უკაცრავად, დაფიქსირდა პრობლემა. ჩვენ უკვე ვმუშაობთ მის აღმოსაფხვრელად. შეეცადეთ დაუბრუნდეთ ამ ",
                "main page"=>"მთავარი გვერდი"
    );

    $GLOBALS['T']=array(
                        "fr"=>$fr,
                        "ka"=>$ka
                  );
}

function t($string){
    if ($_SESSION['lang']!="en")
        if(array_key_exists($string,$GLOBALS['T'][$_SESSION['lang']]))
            return $GLOBALS['T'][$_SESSION['lang']][$string];
    return $string;
}

/*
* return the pathname $prefix$suffix with the correct language extension
*/
function tpath($prefix,$suffix){
    $langExt="";
    if (isset($_SESSION))
        if (@$_SESSION['lang']!="en")
            $langExt="-".$_SESSION['lang'];
    return $prefix.$langExt.$suffix;
}
?>
