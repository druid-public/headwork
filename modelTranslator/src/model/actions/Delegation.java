package model.actions;

import model.Parameters;
import model.SqlCommands;
import model.classConstructors.JsonClassArtifact;
import model.connections.DBConnector;

import java.util.ArrayList;
import java.util.List;

public class Delegation extends Action {

    private Parameters<String> map;
    private String stopCond;
    private String reduce;
    private String parentArtifact;
    private DBConnector connector;
    private String nameAction;

    /**
     * @param guard Boolean Sql request
     * @param map {SubArtifact, Peers}
     * @param stopCond Boolean Sql request
     * @param reduce Sql command
     * @param info "parentArtifact" and info for the platform
     * @param connector necessary to fetch peers
     */
    public Delegation(String guard, Parameters<String> map, String stopCond, String reduce, Parameters<Object> info, DBConnector connector) {
        super(guard, info);
        this.map = map;
        this.parentArtifact = (String) info.get("parentArtifact");
        this.stopCond = stopCond;
        this.map = map;
        this.reduce = reduce;
        this.connector = connector;
        this.nameAction = (String) info.get("title");
    }

    /*
    ne pas chercher les peers ici, mais insérer
     */
    public String writeAction() {
        StringBuilder actions = new StringBuilder();
        for (String subArtifact : map.keySet()) {
            String request = map.get(subArtifact);
            List<Parameters> peers = connector.getPeers(request);
            for (Parameters peer : peers) {
                List<String> attributes = (List<String>) peer.get("attributes");
                List<String> values = (List<String>) peer.get("values");
                actions.append(SqlCommands.insert(subArtifact+"Artifact", attributes, values));
                actions.append(SqlCommands.insert("ARTIFACT",
                        "classid,ownerid,node,state,awaited, attributes",
                        "'"+Math.abs(subArtifact.hashCode()) + "', " +
                                peer.get("idPeer")+", " +
                                "1, " +
                                "'running', " +
                                "null, " +
                                "(select max(id) from " + subArtifact + "Artifact)"));
                actions.append(SqlCommands.insert("subArtifacts", "idParent, idChild, active", "(select ID from CURRENTARTIFACT where USER=SESSION_USER), (select max(id) from ARTIFACT), 1"));
            }
        }
        actions.append(SqlCommands.update("ARTIFACT", "state", "'waiting'", "ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER)"));
        //actions.append(SqlCommands.update("ARTIFACT", "awaited", "'idTask'", "ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER)"));

        actions.append("delete from profile where idtask in (select id from task where artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER));");
        actions.append("delete from task where artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER);");

        //actions.append(SqlCommands.insert("task","title,description,body,checker,checkermsg",
        //        "'Finish"+nameAction+"', 'finish the delegation', 'finish the delegation', null, 'nothing to write, just click below'"));
        //String selectIdTask = "select id from task where title='Finish" + nameAction +"'";
        //actions.append("insert into profile values (("+selectIdTask+"),1)");
        //System.out.println("\tactions:\t"+actions.toString());
        return actions.toString();
    }

    public String writeFinishTask() {
        String selectIdChild = "select idChild from subArtifacts where idParent in(";
        String currentId = "select ID from CURRENTARTIFACT where USER=SESSION_USER";
        StringBuilder allChildren = new StringBuilder().append(selectIdChild).append(currentId).append(")");
        for (int deep=1 ; deep <= (int) this.info.get("deep"); deep++){
            allChildren.append(" or idParent in(");
            allChildren.append(selectIdChild.repeat(deep));
            allChildren.append(currentId);
            allChildren.append(")".repeat(deep+1));
        }
        allChildren.append(")");
        //System.out.println(allChildren);
        List<String> actions = new ArrayList<>();
        actions.add(reduce);
        actions.add(SqlCommands.update("ARTIFACT", "state", "'finished'",
                "ID in (" + allChildren.toString()));
        actions.add(SqlCommands.update("subArtifacts", "active", "0",
                "idChild in (select * from ("+allChildren.toString()+" as tmp )")); //tmp necessary for updating subArt while querying it in the where clause, but maybe there is a better implem (performance ?)
        actions.add(SqlCommands.update("ARTIFACT","state", "'running'",
                "ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER)"));
        StringBuilder finishTask = new StringBuilder();
        for (String action : actions){
            finishTask.append("                \"").append(action).append("\",\n");
        }
        System.out.println(this.info.get("monitoringMessage"));
        return  "    },\n" +
                "    \"" + this.getId() + "\": {\n" +
                "        \"1\": {\n" +
                "            \"guard\": \"select true from dual\",\n" +
                "            \"actions\": []\n" +
                "        }\n" +
                "    },\n" +
                "    \"" + (this.getId()+1) + "\": {\n" +
                "        \"1\": {\n" +
                //"            \"guard\": \"" + stopCond + "\",\n" +
                "            \"guard\": \"select count(*)>=1 from answer where " +
                                        "idtask=(select id from task where title='Finish " + nameAction +"' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER))\",\n" +
                "            \"actions\": [\n" +
                                    finishTask.toString() +
                "                \"delete from profile where idtask in (select id from task where artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER))\",\n" +
                "                \"delete from task where artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)\"\n" +
                "            ]\n" +
                "        },\n" +
                "        \"" + (this.getId()+2) + "\": {\n" +
                "            \"guard\": \"select count(*)<1 from task where " +
                                        "title='Monitor " + nameAction +"' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)\",\n" +
                "            \"actions\": [\n" +
                "                \""+SqlCommands.insert("task","title,description,body,checker,checkermsg, artifact",
                                                    "'Monitor "+nameAction+"', 'monitor the delegation', "+this.info.get("monitoringMessage")+", " +
                                                            "null, 'nothing to write, just click below', (select ID from CURRENTARTIFACT where USER=SESSION_USER)")+"\",\n"+
                "                \"insert into profile values " +
                                    "((select id from task where title='Monitor "+nameAction+"' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER))," +
                                    "1)\"\n" +
                "            ]\n" +
                "        },\n" +
                "        \"" + (this.getId()+3) + "\": {\n" +
                JsonClassArtifact.WRITE_ACTION_CHANGE_ARTIFACT +
                "        },\n" +
                "        \"" + (this.getId()+4) + "\": {\n" +
                "            \"guard\": " +
                                "\"select if(" +
                                    "(select count(*)>0 from ARTIFACT " +
                                        "where ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER))" +//TODO : inutile, à enlever
                                    ",if(" +
                                        "(select count(*)<1 from task where title='Finish " + nameAction + "' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)), " +
                                        "("+ stopCond +")" +
                                        ", 0)" +
                                    ", 0)" +
                                "\",\n" +
                "            \"actions\": [\n" +
                "                \"" + SqlCommands.insert("task","title,description,body,checker,checkermsg, artifact",
                                    "'Finish "+nameAction+"', 'finish the delegation', 'finish the delegation', " +
                                            "null, 'nothing to write, just click below', (select ID from CURRENTARTIFACT where user=SESSION_USER)") + "\",\n" +
                "                \"insert into profile values ((select id from task where title='Finish " + nameAction + "' " +
                                    "and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)),1)\"\n" +
                "            ]\n" +
                "        }\n" +
                "    },\n" +
                "    \"" + (this.getId()+2) + "\": {\n" +
                "        \""+ (this.getId() +1) +"\": {\n" +
                "            \"guard\": \"select true from dual\",\n" +
                "            \"actions\": []\n" +
                "        }\n" +
                "    },\n" +
                "    \"" + (this.getId()+3) + "\": {\n" +
                "        \""+ (this.getId() +1) +"\": {\n" +
                "            \"guard\": \"select true from dual\",\n" +
                "            \"actions\": []\n" +
                "        }\n" +
                "    },\n" +
                "    \"" + (this.getId()+4) + "\": {\n" +
                "        \""+ (this.getId() +1) +"\": {\n" +
                "            \"guard\": \"select true from dual\",\n" +
                "            \"actions\": []\n" +
                "        }\n";
    }


    public String writeMethod() {
        return "";
    }
}
