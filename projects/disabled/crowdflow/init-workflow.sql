DELETE from currentartifact ;
-- where NAME in (select TABLENAME from artifactclass where project = 'CROWDFLOW'); DON'T KNOW WHY WE DO NOT DELETE
DELETE from artifact where classid in (select id from artifactclass where project = 'CROWDFLOW');
DELETE from artifactclass where project = 'CROWDFLOW';
DELETE from profile;
DELETE from task;
DELETE from answer;
-- DELETE from result;
DELETE from subartifacts;

--ALTER TABLE ARTIFACT AUTO_INCREMENT = 1; SEEMS NOT TO WORK WITH INNODB, USING LAST_INSERT_ID() INSTEAD AFTER (David)
ALTER TABLE task AUTO_INCREMENT = 2;

drop table if exists UserArtifact;
create table UserArtifact (location varchar(100),id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into artifactclass(id,description,definition,tablename, project) values (2645995,'The User Artifact','crowdflow/User.sca','UserArtifact', 'CROWDFLOW');

drop table if exists PhotoArtifact;
create table PhotoArtifact (image varchar(500),taxon varchar(100),id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into artifactclass(id,description,definition,tablename, project) values (77090322,'The Photo Artifact','crowdflow/Photo.sca','PhotoArtifact', 'CROWDFLOW');

drop table if exists IdentificationArtifact;
create table IdentificationArtifact (image varchar(500),taxon varchar(100),id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into artifactclass(id,description,definition,tablename, project) values (591130994,'The Identification Artifact','crowdflow/Identification.sca','IdentificationArtifact', 'CROWDFLOW');
insert into UserArtifact(location) values ('Somewhere');

insert into artifact(ownerid,classid,state,node,attributes) values ('1','2645995','running',1,(select max(id) from UserArtifact));
insert into currentartifact(ID,NAME,USER) values (LAST_INSERT_ID(),'UserArtifact', 1);
insert into UserArtifact(location) values ('Somewhere');
insert into artifact(ownerid,classid,state,node,attributes) values ('2','2645995','running',1,(select max(id) from UserArtifact));
insert into currentartifact(ID,NAME,USER) values (LAST_INSERT_ID(),'UserArtifact', 2);
insert into UserArtifact(location) values ('Somewhere');
insert into artifact(ownerid,classid,state,node,attributes) values ('3','2645995','running',1,(select max(id) from UserArtifact));
insert into currentartifact(ID,NAME,USER) values (LAST_INSERT_ID(),'UserArtifact', 3);
insert into UserArtifact(location) values ('Somewhere');
insert into artifact(ownerid,classid,state,node,attributes) values ('4','2645995','running',1,(select max(id) from UserArtifact));
insert into currentartifact(ID,NAME,USER) values (LAST_INSERT_ID(),'UserArtifact', 4);
