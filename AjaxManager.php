<?php
// TODO Had replaced / scratched the old AjaxManager ??!!
session_start();

require_once "HWlib.php";
require_once "pages/taskViewer/TaskViewerModel.php";

$manager = new AjaxManager();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $manager->$action();
}

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    $manager->$action();
}

/**
 * This class goal is to compute data to send back to the ajax call
 */
class AjaxManager
{

    private $db;
    // Constants : node types
    private const ARTIFACT_CLASS = 'artifactClass';
    private const ARTIFACT = 'artifact';
    private const USER = 'user';
    // Constants : node prefix
    private const ARTIFACT_CLASS_PREFIX = 'artifactClass_';
    private const ARTIFACT_PREFIX = 'artifact_';
    private const USER_PREFIX = 'user_';
    // Constants : edge types
    private const EDGE_ARTIFACT_TO_ARTIFACT_CLASS = 'artifact-to-artifactClass';
    private const EDGE_ARTIFACT_TO_USER = 'artifact-to-user';
    private const EMPTY_PARAMETER = '';

    /**
     * Class constructor
     */
    function __construct()
    {
        $this->db = HWdbconnect();
    }

    /**
     * Retrieve the latest enabled task id and send its value through JSON
     */
    function getLastestTaskId()
    {
        if (isset($_POST['task'])) {
            $taskId = $_POST['task'];
            $stmt = $this->db->prepare("SELECT idtask FROM profile WHERE idtask = (
                SELECT t.id
                FROM task t, profile p, skills s
                WHERE s.iduser = :user
                AND s.idskill = p.idskill
                AND t.id = p.idtask
                AND t.artifact = (SELECT artifact FROM task WHERE id = :task ORDER BY id DESC LIMIT 1)
                ORDER BY t.id DESC LIMIT 1
            )");
            $stmt->bindParam(':task', $taskId, PDO::PARAM_INT);
            $stmt->bindParam(':user', $_SESSION['id'], PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            $json = array("lastTask" => intval($result));
            echo json_encode($json);
        }
    }

    /**
     * get_nodes_data
     *
     * Returns all nodes data required to build the graph.
     */
    function getNodesData()
    {
        $t_data = array();

        // ArtifactClass nodes
        $stmt = $this->db->prepare("SELECT * FROM ArtifactClass;");
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        while ($line = ($stmt->fetch())) {
            $t_data[] = new NodeData(
                self::ARTIFACT_CLASS_PREFIX . $line['id'], // id
                $line['description'], // label
                self::ARTIFACT_CLASS // type
            );
        }

        // Artifact nodes
        $stmt = $this->db->prepare("SELECT * FROM Artifact;");
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        while ($line = ($stmt->fetch())) {
            $t_data[] = new NodeData(
                self::ARTIFACT_PREFIX . $line['id'], // id
                self::EMPTY_PARAMETER, // label
                self::ARTIFACT // type
            );
        }

        // Users nodes, retrieve users who answered tasks
        $stmt = $this->db->prepare("SELECT * FROM Users WHERE id IN (SELECT user FROM Answer);");
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        while ($line = ($stmt->fetch())) {
            $t_data[] = new NodeData(
                self::USER_PREFIX . $line['id'], // id
                $line['name'], // label
                self::USER // type
            );
        }

        echo json_encode($t_data, JSON_FORCE_OBJECT);
    }

    /**
     * get_edges_data
     *
     * Returns all edges data required to build the graph.
     */
    function getEdgesData()
    {
        $t_data = array();

        // Edges from Artifact nodes to ArtifactClass nodes
        $stmt = $this->db->prepare("SELECT id, classid FROM Artifact;");
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        while ($line = ($stmt->fetch())) {
            $t_data[] = new EdgeData(
                self::ARTIFACT_PREFIX . $line['id'], // source
                self::ARTIFACT_CLASS_PREFIX . $line['classid'], // target
                self::EDGE_ARTIFACT_TO_ARTIFACT_CLASS, // type
                self::EMPTY_PARAMETER, // label
                self::EMPTY_PARAMETER // state
            );
        }

        // Edges from Artifact nodes to User nodes
        $stmt = $this->db->prepare("SELECT DISTINCT Answer.artifact, Answer.user, ArtifactClass.description, Artifact.state FROM Answered JOIN Answer ON Answered.modified = Answer.modified JOIN Artifact ON Answer.artifact = Artifact.id JOIN ArtifactClass ON Artifact.classid = ArtifactClass.id;");
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        while ($line = ($stmt->fetch())) {
            $t_data[] = new EdgeData(
                self::ARTIFACT_PREFIX . $line['artifact'], // source
                self::USER_PREFIX . $line['user'], // target
                self::EDGE_ARTIFACT_TO_USER, // type
                $line['description'], // label
                $line['state'] // stage
            );
        }

        echo json_encode($t_data, JSON_FORCE_OBJECT);
    }
}
