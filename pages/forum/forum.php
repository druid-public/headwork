<?php

require_once "ForumController.php";

/**
 * Show the correct forum level depending of parameters
 *
 * @param array $getArray
 *            the array of $_GET elements
 */
function showForum($getArray)
{
    global $VIEW;
    $forum = new ForumController();
    if (count($getArray) == 1) {
        $categories = $forum->retrieveCategories();
        $VIEW['MAIN'] .= $categories;
    } else {
        if (isset($getArray['category'])) {
            $pageNo = isset($getArray['page']) && $getArray['page'] > 0 ? $getArray['page'] : 1;
            $topics = $forum->retrieveTopicsFromCateg($getArray['category'], $pageNo);
            $VIEW['MAIN'] .= $topics;
        } elseif (isset($getArray['topic'])) {
            $pageNo = isset($getArray['page']) && $getArray['page'] > 0 ? $getArray['page'] : 1;
            $posts = $forum->retrievePostsFromTopic($getArray['topic'], $pageNo);
            $VIEW['MAIN'] .= $posts;
        }
    }
}

/**
 * Show the topic creation page, with some parameters already filled in depending of the function parameters
 *
 * @param array $getArray
 *            the array of $_GET elements
 */
function showTopicCreation($getArray)
{
    global $VIEW;
    $forum = new ForumController();
    $categ = (isset($getArray['category']) ? $getArray['category'] : null);
    $creationForm = $forum->retrieveTopicCreationForm($categ);
    $VIEW['MAIN'] .= $creationForm;
}

function searchForum($getArray)
{
    global $VIEW;
    $forum = new ForumController();
    $pageNo = isset($getArray['page']) && $getArray['page'] > 0 ? $getArray['page'] : 1;
    if (isset($getArray['forumQuery'])) {
        $category = null;
        if (isset($getArray['category'])) {
            $category = $getArray['category'];
        }
        $topics = $forum->searchForTopic($getArray['forumQuery'], $pageNo, $category);
        $VIEW['MAIN'] .= $topics;
    }
}


/**
 * Store the user post in database
 * @param int $topicId the topic id in which the post will be added
 * @param string $content the content of the post
 */
function insertPostForum($topicId, $content)
{
    $forum = new ForumController();
    $forum->addPostToTopic($topicId, $content);
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    header('Location: http://'.$host.$uri.'/index.php?mode=showForum&topic='.$topicId);
}

/**
 * Create a new topic
 * @param string $title the topic title
 * @param int $category the category's id of the topic
 * @param int $task the task's id (only used if the category is 'task')
 * @param string $content
 */
function insertNewTopic($title, $category, $task, $content)
{
    $forum = new ForumController();
    $topicId = $forum->createNewTopic($title, $category, $task, $content);
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    header('Location: http://'.$host.$uri.'/index.php?mode=showForum&topic='.$topicId);
}


?>
