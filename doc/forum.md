# HEADWORK's forum

## New database schema
3 new tables have been created in the database : `forum_category`, `forum_topic` and `forum_post`.  
As the forum can be accessed by newer members who are looking for an answer, please do not erase the content in `init-workflow.sql`.

### `headwork`.`forum_category` table
| Field name  | Type          | Purpose                     |
| ----------- | ------------- | --------------------------- |
| id_category | int(11)       | Store the category ID       |
| title       | varchar(200)  | Category's title to display |
| description | varchar(1000) | Detail the category's goal  |

The forum has only 2 categories : `HEADWORK Platform` and `Tasks` whose goals are the same as the chat's ones.

### `headwork`.`forum_topic` table
| Field name    | Type         | Purpose                                 | Target table          |
| ------------- | ------------ | --------------------------------------- | --------------------- |
| id_topic      | int(11)      | Store the topic ID                      |                       |
| title         | varchar(200) | Topic's title to display                |                       |
| content       | text         | Store the topic content (HTML enabled)  |                       |
| creation_date | datetime     | Store the topic creation date           |                       |
| #creator_id   | int(11)      | Store the topic creator ID              | `users`.`id`          |
| #id_task      | int(11)      | Store the related task                  | `task`.`id`           |
| #id_category  | int(11)      | Store the parent category               | `forum_category`.`id` |

To create a topic, please use the dedicated page on the forum.

### `headwork`.`forum_post` table
| Field name | Type     | Purpose                                | Target table       |
| ---------- | -------- | -------------------------------------- | ------------------ |
| id_post    | int(11)  | Store the post ID                      |                    |
| content    | text     | Store the topic content (HTML enabled) |                    |
| #user_id   | int(11)  | Store the post submitter ID            | `users`.`id`       |
| date       | datetime | Store the post submit date             |                    |
| #id_topic  | int(11)  | Store the parent topic ID              | `forum_topic`.`id` |

## Plugin used
We use [TinyMCE](https://www.tiny.cloud/get-tiny/self-hosted/) as an enhanced textarea in order to use HTML formatting in replies or topics.