package demo;

import model.Artifact;
import model.Parameters;
import model.actions.*;
import model.connections.*;

import java.util.ArrayList;
import java.util.List;

public class WeGarden {

    //TODO : mettre autre part où ça a plus de sens
    public static Artifact certif = new Artifact("certif")
            .addAttribute("artifactID", "varchar(20)") //certfified..
            .addAttribute("attribute","varchar(50)")
            .addAttribute("value","varchar(100)")
            .addAttribute("time", "timestamp")
            .addAttribute("type", "ENUM('basic','induced')");

    public static void main(String[] args) {

        PlatformConnector connector = new HeadworkPlatformConnector("WeGarden");

        // Subartifact for the plant delegation "ask others what they think of your plant"
        Artifact looks = new Artifact("looks")
                .addAttribute("picture", "varchar(100)")
                .addAttribute("aspect", "varchar(100)")
                .addAction(new UpdateAction((new Parameters<>()
                        .addParam("title", "Does this plant looks fine or sick?")
                        .addParam("description", "Does this plant looks fine or sick?")
                        .addParam("attribute", "aspect")
                        .addParam("artifact", "Looks")
                        .addParam("SQLtype", "varchar(100)"))));

        Artifact plant = new Artifact("plant")
                .addAttribute("taxon", "varchar(100)")
                .addAttribute("aspect", "varchar(100)")
                .addAttribute("picture", "varchar(100)")
                .addAction(new UpdateAction((new Parameters<>()
                        .addParam("title", "Indicate the taxon")
                        .addParam("description", "Indicate the taxon")
                        .addParam("attribute", "taxon")
                        .addParam("artifact", "Plant")
                        .addParam("SQLtype", "varchar(100)"))))
                .addAction(new UpdateAction((new Parameters<>()
                        .addParam("title", "Does this plant looks fine or sick?")
                        .addParam("description", "Does this plant looks fine or sick?")
                        .addParam("attribute", "aspect")
                        .addParam("artifact", "Plant")
                        .addParam("SQLtype", "varchar(100)"))))
                .addAction(new Delegation(
                        looks,
                        new Parameters<String>().addParam("picture","select picture from PlantArtifact"),//"select picture from PlantArtifact" //AnswerForm
                        "select count(*)>0 from LooksArtifact where aspect IS NOT NULL",
                        Delegation.maxCountAnswer("aspect", "Plant","aspect","Looks"),
                        new Parameters<>()
                                .addParam("title", "Ask others if your plant looks good")
                                .addParam("description", "Ask others if your plant looks good")
                                .addParam("body", "(select body from Template where id='wegarden-default')")
                                //"Send a picture of your plant to have others advices on what your plant looks like")
                                .addParam("parentArtifact", "Plant")
                                //.addParam("monitoringMessage", "BlaBla")
                                .addParam("type", "0"),
                        connector.getDBConnection()))
                .addAction(new Certification(
                        "select count(*)>0 from certifArtifact where attribute=taxon and value=[value]",
                        "taxon","Plant"))
                ;

        Artifact garden = new Artifact("garden")
                .addAttribute("location", "varchar(500)")
                .addAttribute("name", "varchar(100)")
                .addAttribute("weather", "varchar(100)")
                .addAction(new CreationAction(new Parameters<>()
                        .addParam("title", "Add a new plant to your garden")
                        .addParam("newArtifact", plant)
                        .addParam("initAttribute", "picture")
                        .addParam("description","Give a picture of your plant")
                        ))
                .addAction(new UpdateAction("select count(location)=0 from gardenArtifact where " +
                        "id=(select attributes from Artifact where id=CURRENT_ARTIFACT)",
                        new Parameters<>()
                                .addParam("title", "Where is your garden?")
                                .addParam("attribute", "location")
                                .addParam("artifact", "Garden")
                                .addParam("SQLtype", "varchar(100)")
                                .addParam("description", "Indicate the location of your garden")
                ))
                .addAction(new UpdateAndStore(
                        "select count(*)=0 from Gardensweather where " +
                                "Garden=CURRENT_ARTIFACT and " +
                                "date >= NOW() - INTERVAL 1 MINUTE",
                        new Parameters<>()
                                .addParam("title", "Describe the weather?")
                                .addParam("attribute", "weather")
                                .addParam("artifact", "Garden")
                                .addParam("SQLtype", "varchar(100)")
                                .addParam("description", "Indicate todays weather")
                                .addParam("type", 2)
                                .addParam("arg1", "select value, text from EnumWeather")
                                .addParam("checkermsg", "''")
                                .addParam("certification", "true")
                        ,connector))
                .addAction(new Certification("location","Garden"))
                ;

        Artifact user = new Artifact("user")
                .addAction(new CreationAction(new Parameters<>()
                        .addParam("title", "Add a garden")
                        .addParam("newArtifact", garden)
                        .addParam("initAttribute", "name")
                        .addParam("description", "Enter a new garden")
                ))
                .addAction(new CreationAction("select name='Pluvio' from Users where id=SESSION_USER",
                        new Parameters<>()
                                .addParam("title","Add a sensor")
                                .addParam("newArtifact",sensor)
                                .addParam("initAttribute", "location")));
                //.addAction(new CertifAction());

        connector.CreateArtifactClass(user);
        connector.CreateArtifactClass(garden);
        connector.CreateArtifactClass(plant);
        connector.CreateArtifactClass(looks);
        connector.CreateArtifactClass(certif);



        // définir ce qui est là dès le début ----------------------------------------------------------------------
        List<Parameters> listArtifacts = new ArrayList<>();
        Parameters<Object> firstArtifacts = new Parameters<>();
        firstArtifacts.addParam("artifact", "User")
                .addParam("request",
                        "select 0 as idPeer");
        listArtifacts.add(firstArtifacts);

        connector.instantiate(listArtifacts);
    }
}