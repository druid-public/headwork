<?php


/**
 * showSkillsRec
 *
 * Show the skill tree and the form to pick up a skill and select an associated level
 *
 * @param  $conn : A valid database PDO connection
 * @param  $skill :the text of the current skill
 * @param  $level :the level of the current skill
 * @param  $skillid : the id of the current skill
 * @param  $userid : the id of user we are showing the skills
 * @param  $parentid : the id of the parent skill of the current skill (null if current skill is the toplevel skill)
 * @param  $skill_link : the conceptUri of a skill
 * @param  $indicateur : array that contain 0 for debutant 1 for amateur 2 for expert
 */

function showSkillsRec($conn, $skill, $level, $skillid, $userid, $parentid = 0, $skill_link, $indicateur, $borneMin, $borneMax)
{

    global $VIEW;
    $table = $conn->query("select id,skill,IFNULL(level,0) as level,parent_skill from SkillTree left outer join Skills on (Skills.idskill=SkillTree.id and iduser=$userid) where parent_skill=$skillid");
    $wikipediaQuery = HTMLlinkNewPage(HTMLWikipediaPage($skill), "<sup>?</sup>");
    $slider = "<input type='range' class='slider' id='$skillid' name='$skill' min='0' max='100' step='20' value='$level' parent='$parentid'>";

    $stmt2 = $conn->prepare("SELECT distinct idskill, count(iduser) as expert
                                FROM Skills
                                where LEVEL>=$borneMax
                                and idskill=:id_skill
                                GROUP BY idskill;");

    $stmt3 = $conn->prepare("SELECT distinct idskill, count(iduser) as amateur
                                    FROM Skills
                                    where LEVEL>$borneMin and LEVEL<$borneMax
                                    and idskill=:id_skill
                                    GROUP BY idskill;");

    $stmt4 = $conn->prepare("SELECT distinct idskill, count(iduser) as debutant
                                        FROM Skills
                                        where LEVEL<=$borneMin
                                        and idskill=:id_skill
                                        GROUP BY idskill;");

    //$wikipediaQuery = HTMLlinkNewPage(HTMLWikipediaPage($skill), "<sup>?</sup>");
    $escoQuery = "";//HTMLlinkNewPage(HTMLEscoPage($skill_link), "<sup>?</sup>");

    $slider = "<input type='range' class='slider' id='$skillid' name='$skill' min='0' max='100' step='20' value='$level' parent='$parentid'>";


    $total = 0;
    if (is_numeric($indicateur[0]) && is_numeric($indicateur[1]) && is_numeric($indicateur[2])) {
        $total = ($indicateur[0] + $indicateur[1] + $indicateur[2]);
    }

    if ($table->rowCount() > 0) {


        $VIEW['MAIN'] .= "<li><span class='caret'>" . $skill . " </span>" . $escoQuery;
        if ($total > 0) {
            $VIEW['MAIN'] .= "<div class='progress'> <div class='progress-bar bg-info' role='progressbar'
                               style='width: " . ($indicateur[0] * 100) / $total . "%'" . " aria-valuenow='" . $indicateur[0] . "' aria-valuemin='0'
                               aria-valuemax='" . $total . "'>" . $indicateur[0] . "</div>
                               <div class='progress-bar bg-warning' role='progressbar'
                               style='width: " . ($indicateur[1] * 100) / $total . "%'" . " aria-valuenow='" . $indicateur[1] . "' aria-valuemin='0' aria-valuemax='" . $total . "'>" . $indicateur[1] . "</div>
                               <div class='progress-bar bg-danger' role='progressbar' style='width: " . ($indicateur[2] * 100) / $total . "%'" . " aria-valuenow='" . $indicateur[2] . "'aria-valuemin='0'
                               aria-valuemax='" . $total . "'>" . $indicateur[2] . "</div></div>";
        }
        $VIEW['MAIN'] .= $slider;
        // $VIEW['MAIN'] .= $row['$skillid'];
        $VIEW['MAIN'] .= "<ul id='myUL' class='nested'>";

        foreach ($table as $tuple) {
            $stmt2->execute(array('id_skill' => $tuple['id']));
            $res = $stmt2->setFetchMode(PDO::FETCH_ASSOC);
            $stmt3->execute(array('id_skill' => $tuple['id']));
            $res3 = $stmt3->setFetchMode(PDO::FETCH_ASSOC);
            $stmt4->execute(array('id_skill' => $tuple['id']));
            $res4 = $stmt4->setFetchMode(PDO::FETCH_ASSOC);
            $rowcount = 0;
            $expert = 0;
            $amateur = 0;
            $debutant = 0;

            while ($line = $stmt2->fetch()) {
                $rowcount++;
                $expert = $line['expert'];
            }
            while ($line = $stmt3->fetch()) {
                $rowcount++;
                $amateur = $line['amateur'];
            }
            while ($line = $stmt4->fetch()) {
                $rowcount++;
                $debutant = $line['debutant'];
            }
            $indicateur_form = array($debutant, $amateur, $expert);
            $VIEW['MAIN'] .= "<li>";
            showSkillsRec($conn, $tuple['skill'], $tuple['level'], $tuple['id'], $userid, $tuple['parent_skill'], $skill_link, $indicateur_form, $borneMin, $borneMax);
            $VIEW['MAIN'] .= "</li>";
        }
        $VIEW['MAIN'] .= "</ul>";
        $VIEW['MAIN'] .= "</li>";
    } else {
        $VIEW['MAIN'] .= "<li>" . $skill . $escoQuery;
        if ($total > 0) {
            $VIEW['MAIN'] .= "<div class='progress'> 
                                    <div class='progress-bar bg-info' role='progressbar'
                                             style='width: " . ($indicateur[0] * 100) / $total . "%'" . " aria-valuenow='" . $indicateur[0] . "' aria-valuemin='0'
                                             aria-valuemax='" . $total . "'>" . $indicateur[0] . "</div>
                                    <div class='progress-bar bg-warning' role='progressbar'
                                             style='width: " . ($indicateur[1] * 100) / $total . "%'" . " aria-valuenow='" . $indicateur[1] . "' aria-valuemin='0' aria-valuemax='" . $total . "'>
                                             " . $indicateur[1] . "</div>
                                     <div class='progress-bar bg-danger' role='progressbar' style='width: " . ($indicateur[2] * 100) / $total . "%'" . " aria-valuenow='" . $indicateur[2] . "'aria-valuemin='0'
                                             aria-valuemax='" . $total . "'>" . $indicateur[2] . "</div> 
                                 </div>";
        }
        $VIEW['MAIN'] .= $slider;
        $VIEW['MAIN'] .= "</li>";
    }

    $VIEW['MAIN'] .= <<<EOL
<!--Scipt permettant de filtrer les balise li critère simple uppercase du début du mot 
    à améliorer si on veut plus de précision sur les mots composé
-->
<script>
function myFunction() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('myInput');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('li');

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}
</script>
EOL;

}

/**
 * skills
 *
 * Show the skill tree and the form to pick up a skill and select an associated level
 *
 * @param  $conn : A valid database PDO connection
 */

function skills($conn)
{
    global $VIEW;

    $VIEW['MAIN'] .= HTMLh1(t("Manage you skills"));
    $VIEW['MAIN'] .= HTMLp(t("Please indicate/update your skills on this dashboard. "));
    $VIEW['MAIN'] .= "<label for='search' id='lbl_search'></label>";
    $VIEW['MAIN'] .= "<input type='text' id='myInput' onkeyup='myFunction()' placeholder='".t("Browse skills")."'>";

    //$VIEW['MAIN'] .= "<div id='skillLegend'>Rookie...Fair...Expert</div>";
    $VIEW['MAIN'] .= "<ul>";
    showSkillsRec($conn, "Common sense", 100, 1, $_SESSION['id'], 0, 'null', "na", 50, 80);
    $VIEW['MAIN'] .= "</ul>";
    $VIEW['MAIN'] .= <<<EOL

    <style type='text/css'>
    #skillLegend {
      color:black;
      // me padding: 30px;
      // me margin-left: 920px;
      //display: grid;
      width: initial;
      font-size: 18px;
      // me
      position: fixed;
      left: 1220px;
    }

    .slider {
        height: 0px;
      display: block;
      position: absolute;
      left: 1220px;
      margin: -22px;
      margin-bottom: -500px;
      padding: 7px;
      width: 120px;
      
    }
   #myInput {
    width: 100%; /* Ajout de largeur */
    font-size: 17px; /* augmenter la police */
    padding: 10px 10px 10px 10px; /* Ajouter du padding */
    margin-bottom: 12px; /* ajout d'espace avant l'input */
  }
  
  
  #myUL li {
    //box-sizing: content-box;
    width: 130%;
    /* border: 1px solid #ddd; /* ajout de bordure pour chaque lien */
    margin-top: 0px; /* empecher les double bordures */
    /* background-color: #f6f6f6; /* couleur d'arriere plan gris */
    padding-left: 7%; /* ajout de padding */
    text-decoration: none; /* retirer les interlignes, souligner*/
    font-size: 17px; /* augmenter la police */
    color: black; /* ajout de couleur noir */
    /* display: block; /* lister les li dans un bloc  */
    /* margin-rigth: 50%;*/
   

  }
  #skillid {
    box-sizing: 100%;
    border: 1px solid #ddd; /* ajout de bordure pour chaque lien */
    margin-top: 5px; /* empecher les double bordures */
    /* background-color: #f6f6f6; /* couleur d'arriere plan gris */
    padding-left: 10%; /* ajout de padding */
    text-decoration: none; /* retirer les interlignes, souligner*/
    font-size: 17px; /* augmenter la police */
    color: black; /* ajout de couleur noir */
    display: block; /* lister les li dans un bloc  */
    /*margin-rigth: 50%; */
    
 
  }
  .caret{
    -webkit-transition: color 2s;
    transition: color 2s;
    margin-left: -40px;
}
  .caret:hover {
  /* background-color: rgb(215, 215, 215); */
  color: blue;
  }

  .progress{
    width: 7%;
    margin: -22px;
    /*width: 10%;*/
    display: flex;
    position: absolute;
    left: 1020px; 
    //background-color: green;
  }


}
</style>


<script>
var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("caret-down");
  });
}

var slider=document.getElementsByClassName("slider");
for (i = 0; i < slider.length; i++) {
  slider[i].addEventListener("change", function() {
    var parent=document.getElementById(this.getAttribute('parent'));
    if (parent!=null){
        var currentValue=parseInt(this.value);
        var parentValue=parseInt(parent.value);
        console.log("index.php?mode=addSkill&submit="+this.id+"&level="+this.value);
        var client = new XMLHttpRequest();
        client.open('GET', "index.php?mode=addSkill&submit="+this.id+"&level="+this.value);
        client.send();          
    }
  });
}

</script>

EOL;
}


/**
 * addSkill
 *
 * Add a new skill,level couple to a user in the db
 *
 * @param  $conn : A valid database PDO connection
 */

function addSkill($conn)
{
    $stmt = $conn->prepare("replace into Skills values(:iduser,:idskill,:level)");
    $stmt->bindParam(':iduser', $_SESSION["id"], PDO::PARAM_INT);
    $stmt->bindParam(':idskill', $_GET["submit"], PDO::PARAM_INT);
    $stmt->bindParam(':level', $_GET["level"], PDO::PARAM_INT);
    $stmt->execute() or die(mysql_error());
    header('Location: index.php?mode=skills');
}

/**
 * saveSkill
 *
 * Launch the Skyline depending of the user preferences
 *
 * @param  $conn : A valid database PDO connection
 */

function saveSkill($conn)
{
    global $VIEW;
    if (file_exists("Skyline/Skyline.jar")) {
        $query = "SELECT skill from preferences where iduser = :iduser";
        $stmt2 = $conn->prepare($query);
        $stmt2->bindParam(':iduser', $_SESSION['id'], PDO::PARAM_STR);
        $stmt2->execute();
        if ($stmt2->rowCount() < 2) {
            $VIEW['MAIN'] .= HTMLbr() . "<p><i class=\"glyphicon glyphicon-warning-sign\"></i> You need to select yours skills before launching the algorithm. (2 skills minimum)</p>";
            showSkills($conn);
        } else {
            execInBackground("java -jar Skyline/Skyline.jar " . $_SESSION['id']);
            header('Location: index.php?mode=showTasks');
        }
    } else {
        $VIEW['MAIN'] .= HTMLbr() . "<p><i class=\"glyphicon glyphicon-warning-sign\"></i> Skyline algorithm not found !</p>";
        showSkills($conn);
    }
}

/**
 * generateJson
 *
 * Generate the taxonomy with the skillTree saved in the Db
 *
 * @param  $conn : A valid database PDO connection
 */

function generateJson($conn)
{
    global $skill;
    global $sons;
    $query = "SET CHARACTER SET utf8";
    $stmt2 = $conn->prepare($query);
    $stmt2->execute();
    $query = "SELECT * from skilltree";
    $stmt2 = $conn->prepare($query);
    $stmt2->execute();
    $stmt2->setFetchMode(PDO::FETCH_ASSOC);
    while ($line = $stmt2->fetch()) {
        $skill[$line["id"]] = $line["skill"];
        if (isset($sons[$line["parent_skill"]])) {
            array_push($sons[$line["parent_skill"]], $line["skill"]);
        } else {
            $sons[$line["parent_skill"]] = array($line["skill"]);
        }
    }
    $json = array("name" => $skill[1], "parent" => "null", "children" => getArray($skill[1]));
    $fichier = fopen('skill.json', 'w+');
    fwrite($fichier, json_encode($json, JSON_UNESCAPED_UNICODE));
    fclose($fichier);
    header('Location: index.php?mode=skills');
}

/**
 * getArray
 *
 * Recursive function to create the json
 *
 * @param  $conn : A valid database PDO connection
 */

function getArray($parent)
{
    global $skill;
    global $sons;
    $res = array();
    foreach ($sons[array_search($parent, $skill)] as $son) {
        if (isset($sons[array_search($son, $skill)])) {
            array_push($res, array("name" => $son, "parent" => $parent, "children" => getArray($son)));
        } else {
            array_push($res, array("name" => $son, "parent" => $parent));
        }
    }
    return $res;
}


?>
