# Headwork

Crowdsourcing relies on potentially huge numbers of on-line participants to resolve data acquisition or analysis tasks. It is an exploding area that impacts various domains, ranging from scientific knowledge enrichment to market analysis support. But currently, existing crowd platforms rely mostly on low level programming paradigms, rigid data models and poor participant profiles, which yields severe limitations. The low- level nature of existing solutions prevents the design of complex data acquisition workflows, that could be executed, composed, searched and even be proposed by participants them- selves. Taking into account the quality, uncertainty, inconsistency and representativeness of participant contributions is still an open problem. Methods for assigning a task to the correct participant according to his trust, motivation and expertise, automatically improving crowd execution time, computing optimal participant rewards, are missing. Similarly, usual crowd campaigns produce isolated and rigid data sets: A flexible and common data model for the produced knowledge about data and participants could allow participative knowledge acquisition. To overcome these challenges, Headwork will define:

* Rich workflow, participant, data and knowledge models to capture various kind of crowd applications with complex data acquisition tasks and human specificities
* Methods for deploying, verifying, optimizing, but also monitoring and adapting crowd- based workflow executions at run time.

# Partners

To reach its goals, Headwork relies on two experts of large participative knowledge acquisition platforms

* Cesco (Musée National d'Histoire Naturelle)
* FouleFactory
* Valda (INRIA Paris)
* Druid (Rennes 1)
* Links (Inria-Lille)
* Sumo (Inria-Bretagne)
* Spirals (Inria-Lille)

# Notes

* See the Wiki for documentation (installation, crowdsourcing project developpment, etc.).
* Beware that Headwork uses PHP sessions, so is not directly compliant for multi-server deployment.
* Feel free to contribute!



