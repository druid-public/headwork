$('#taskAnswerForm').on('submit.multicards', function(e) {
    // We prevent the default action of the from in order to add a custom answer input
    e.preventDefault();
    answer = [];
    $(':input[id^="answer-"]:checked').each(function() {
        answer.push({"idtask": $(this).attr('data-task-id'), "value": $(this).val() });
    });
    // Creation of a hidden input containing the JSON as string
    let toAdd = $("<input/>").attr("type", "hidden").attr("name", "answer").val(JSON.stringify(answer));
    $(this).append(toAdd);
    // We unbind this event handler to avoid infinite loop
    $(this).off("submit.multicards");
    // We submit the form ONLY is no ajax is involved, as it would cause double submit with it
    if ($(':input[name="ajax"]').length == 0) {
        $(this).submit();
    }
})

$(':input[id^="answer-"]').change(function() {
    if ($(this).is(':checked')) {
        $(this).parent().attr('class', 'btn btn-info btn-sm');
        $(':input[data-task-id^="' + $(this).attr('data-task-id') + '"]').not(this).parent().attr('class', 'btn btn-secondary btn-sm');
    }
});