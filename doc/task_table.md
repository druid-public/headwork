# Task table explained

## Fields

| Field name  | Type           | Purpose                                                                 | More info                             |
| ----------- | -------------- | ----------------------------------------------------------------------- | ------------------------------------- |
| id          | int(11)        | Store the task ID                                                       |                                       |
| artifact    | int(11)        | Store the artifact's owner ID                                           |                                       |
| title       | varchar(100)   | Display the task's title                                                |                                       |
| description | varchar(100)   | Detail the task's goal                                                  |                                       |
| type        | int(11)        | Specify the answer type                                                 | [Answer types](page-answer-types)     |
| body        | varchar(10000) | Store the whole HTML to display                                         |                                       |
| deadline    | datetime       | ?                                                                       |                                       |
| timer       | bigint(20)     | Store the unix timestamp limit to complete the task                     | Force the submit when time is reached |
| duration    | int(11)        | ?                                                                       |                                       |
| reward      | int(11)        | The number of points earned when completing the task ?                  |                                       |
| checker     | varchar(1000)  | The condition to be met by the answer in order to be accepted           |                                       |
| checkermsg  | varchar(1000)  | The text detailling the answer requirements                             |                                       |
| ajax        | tinyint(1)     | Enable Ajax processing for the answer submit and displaying next task   | FALSE by default                      |
| help        | tinyint(1)     | Enable resource linking by the user                                     | FALSE by default                      |
| argN        | text           | Store an additional SQL query to be used, must be used with type field  | [Answer types](page-answer-types)     |