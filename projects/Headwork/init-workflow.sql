DROP TABLE IF EXISTS Headwork;
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'Headwork');
DELETE from ArtifactClass where project = 'Headwork';

DROP TABLE IF EXISTS AnswerButton;
CREATE TABLE AnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO AnswerButton values (1,'Mauvaise','danger');
INSERT INTO AnswerButton values (2,'À revoir','warning');
INSERT INTO AnswerButton values (3,'Parfaite','success');

DROP TABLE IF EXISTS ExpertiseValue;
CREATE TABLE ExpertiseValue(value varchar(100),level int);
insert into ExpertiseValue values ("Newbie",0),("Fair",30),("Skilled",60),("Expert",100);

DROP TABLE IF EXISTS POI;
CREATE TABLE POI(value varchar(100),category varchar(100),categorycode int);
insert into POI values ("Felis Sylvestris","Cat",1),("Felis Uncia","Cat",1),("Schist","Rock",2),("Migmatite","Rock",2),("Phyllite","Rock",2),("Gneiss","Rock",2);

DROP TABLE IF EXISTS Job;
CREATE TABLE Job(text varchar(100),value int);
insert into Job values ("Academic",1),("Engineer",2),("PhD Candidate",3),("Postdoc",4),("Student",5),("Other",6);

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (6000,'Crowdsourcing for beginners','Headwork/crowdsourcing.sca', 'Artifact', 'Headwork',true);

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (6001,'Discover HEADWORK','Headwork/discover.sca', null, 'Headwork',true);

-- insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (6002,'Voting','Headwork/headworkVoting.sca', null, 'Headwork',true);

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (6003,'Task interface capabilities','Headwork/gui.sca', null, 'Headwork',true);

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (6004,'Cats and Rocks and branching questions','Headwork/cats.sca', null, 'Headwork',true);

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (6005,'I want to believe!: Uncertain answers','Headwork/belief.sca', null, 'Headwork',true);
