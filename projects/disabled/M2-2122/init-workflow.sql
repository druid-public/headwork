DELETE from Artifact where classid in (select id from ArtifactClass where project = 'M2-2122');
DELETE from ArtifactClass where project = 'M2-2122';
-- To do task automaton
insert into ArtifactClass(id,description, definition, tablename, project, autostart)
values(8000, 'Base de données de tâches personnelles', 'M2-2122/m2Todo.sca', 'Artifact', 'M2-2122', true);

DROP TABLE IF EXISTS AnswerButton;
CREATE TABLE AnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO AnswerButton values (1,'Personnelle','danger');
INSERT INTO AnswerButton values (2,'Profesionnelle','warning');
INSERT INTO AnswerButton values (3,'Familiale','success');
INSERT INTO AnswerButton values (4,'1','');
INSERT INTO AnswerButton values (5,'2','');
INSERT INTO AnswerButton values (6,'3','');
INSERT INTO AnswerButton values (7,'4','');
INSERT INTO AnswerButton values (8,'5','');
