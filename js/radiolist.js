$('#taskAnswerForm').on('submit.radiolist', function(e) {
    // We prevent the default action of the from in order to add a custom answer input
    e.preventDefault();
    answer = [];
    $(':input[id^="answer-"]:checked').each(function() {
        var idTask = $(this).attr('data-task-id');
        // If the feedback is enabled when the user chose an answer requiring explanations
        if ($(this).attr('data-feedback') == 1) {
            $.ajax({
                type: "POST",
                url: "index.php",
                data: {
                    mode: 'insertTaskFeedback',
                    id: idTask,
                    feedback: $('#feedbackAnswerTextarea-' + idTask).val(),
                    targetTable: $(this).attr('data-target-feedback')
                }
            });
        }
        answer.push({"idtask": idTask, "value": $(this).val() });
    });
    // Creation of a hidden input containing the JSON as string
    let toAdd = $("<input/>").attr("type", "hidden").attr("name", "answer").val(JSON.stringify(answer));
    $(this).append(toAdd);
    // We unbind this event handler to avoid infinite loop
    $(this).off('submit.radiolist');
    // We submit the form ONLY is no ajax is involved, as it would cause double submit with it
    if ($(':input[name="ajax"]').length == 0) {
        $(this).submit();
    }
});

$(':input[id^="answer-"]').change(function() {
    var id = $(this).attr('data-task-id');
    if ($(this).attr('data-feedback') == 1) {
        $('#feedbackAnswer-' + id).modal();
    }
});

$(':input[id^="feedbackAnswerSubmit-"]').click(function() {
    var id = $(this).attr('data-task-id');

});