<?php


require "ForumModel.php";

$forum = new ForumController();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $forum->$action();
}

class ForumController {

    private $model;
    private $paginationLimit; // The number of topics we want on one page


    /**
     * Class constructor
     */
    function __construct() {
        $conn = HWdbconnect();
        $this->model = new ForumModel($conn);
        $this->paginationLimit = 10;
    }


    /**
     * Retrieve all the forum categories
     * @return string the generated HTML of the forum
     */
    function retrieveCategories() {
        $categories = $this->model->getCategoriesWithEdit();
        $totalTopics = $this->model->getTotalTopics();
        $result = array(
            "categories" => $categories,
            "total_topics" => $totalTopics
        );
        return $this->getForumHtml($result);
    }

    /**
     * Retrieve the topics (limited by the pagination limit) from a given category
     * @param int $category the category id
     * @param int $pageNo the page number to determine the starting point of the query
     * @return string the generated HTML of the forum
     */
    function retrieveTopicsFromCateg($category, $pageNo) {
        $start = ($pageNo - 1) * $this->paginationLimit;
        $categoryInfos = $this->model->getCategory($category);
        $topics = $this->model->getTopicsFromCateg($category, $this->paginationLimit, $start);
        $totalTopics = $this->model->getTotalTopicsFromCateg($category);
        $result = array(
            "category" => $categoryInfos,
            "topics" => $topics,
            "total_topics" => $totalTopics
        );
        $html = $this->getForumCategoryHtml($result);
        $html .= $this->generatePagination($pageNo, array("mode" => "showForum", "category" => $category), $totalTopics);
        return $html;
    }

    /**
     * Retrieve the posts (limited by the pagination limit) from a given topic
     * @param int $topicId the topic id
     * @param int $pageNo the page number to determine the starting point of the query
     * @return string the generated HTML of the forum
     */
    function retrievePostsFromTopic($topicId, $pageNo) {
        $start = ($pageNo - 1) * $this->paginationLimit;
        $topicInfos = $this->model->getTopic($topicId);
        $taskInfos = array();
        if (!is_null($topicInfos['id_task'])) {
            $taskInfos = $this->model->getTask($topicInfos['id_task']);
        }
        $categoryInfos = $this->model->getCategory($topicInfos['id_category']);
        $posts = $this->model->getPostsFromTopic($topicId, $this->paginationLimit, $start);
        $totalPosts = $this->model->getTotalPostsFromTopic($topicId);
        $thankedPosts = $this->model->retrieveUserThankedPosts($topicId, $_SESSION['id']);
        $result = array(
            "category" => $categoryInfos,
            "topic" => $topicInfos,
            "posts" => $posts,
            "total_posts" => $totalPosts,
            "thanked_posts" => $thankedPosts
        );
        if (!is_null($topicInfos['id_task'])) {
            $result['task'] = $taskInfos;
        }
        $html = $this->generateWarning();
        $html .= $this->getForumTopicHtml($result, $pageNo);
        $html .= $this->generatePagination($pageNo, array("mode" => "showForum", "topic" => $topicId), $totalPosts);
        $html .= HTMLform(
            "index.php",
            "POST",
            HTMLinput("hidden", "topicId", $topicId)
            .HTMLinput("hidden", "mode", "insertPostForum")
            .$this->generatePostTextarea()
            .HTMLtagblock("button", array("type" => "submit", "class" => "btn btn-dark"), "Submit")
        );
        return $html;
    }

    /**
     * Retrieve the topic creation form
     * @param int $category the pre-selected category
     * @return string the generated HTML of the creation form
     */
    function retrieveTopicCreationForm($category = NULL) {
        $categories = $this->model->getCategories();
        $tasks = $this->model->getTasks();
        $creationElements = $this->getTopicCreationHtml($categories, $category, $tasks);
        $html = $this->generateWarning();
        $html .= HTMLform(
            "index.php",
            "POST",
            HTMLinput("hidden", "mode", "insertNewTopic")
            .HTMLdiv($creationElements.$this->generateTopicTextarea(), array("class" => "forumTopicCreation"))
            .HTMLtagblock("button", array("type" => "submit", "class" => "btn btn-dark"), "Submit")
        );
        return $html;
    }


    /**
     * Add the post submitted by the user
     * @param int $topicId the topic id in which post will be added
     * @param string $content the post content
     */
    function addPostToTopic($topicId, $content) {
        $this->model->addPostToTopic($topicId, $content, $_SESSION['id']);
    }

    /**
     * Create a new topic in the forum
     * @param string $title the topic title
     * @param int $category the topic category
     * @param int $task the topic task, only if the category is 'task'
     * @param string $content the topic content
     * @return int the id of the new topic
     */
    function createNewTopic($title, $category, $task, $content) {
        if ($category == 2) {
            return $this->model->createNewTopicTask($title, $category, $task, $content, $_SESSION['id']);
        }
        return $this->model->createNewTopic($title, $category, $content, $_SESSION['id']);
    }

    
    /**
     * Retrieve topics whose title or content contains the user input from all the forum
     * @param string $searchString the user input to search for
     * @param int $pageNo the page of results to display
     */
    function searchForTopic($searchString, $pageNo, $category = NULL) {
        $start = ($pageNo - 1) * $this->paginationLimit;
        $topics = "";
        $categoryInfos = array();
        if (is_null($category)) {
            $topics = $this->model->searchTopics($searchString, $this->paginationLimit, $start);
        } else {
            $topics = $this->model->searchTopicsFromCateg($searchString, $category, $this->paginationLimit, $start);
            $categoryInfos = $this->model->getCategory($category);
        }
        $totalTopics = $this->model->getTotalFromSearch($searchString, $category);
        $result = array(
            "topics" => $topics,
            "total_topics" => $totalTopics,
            "category" => $categoryInfos
        );
        $html = $this->getForumSearchHtml($searchString, $result);
        $html .= $this->generatePagination($pageNo, array("mode" => "searchForum", "forumQuery" => $searchString), $totalTopics);
        return $html;
    }


    /**
     * Increase or decrease the thanks number of a post
     * All the parameters are retrieved from POST as this function is meant to be called by Ajax
     */
    function managePostThanks() {
        // TODO - TEMPORARY : NEEDS TO FIND AN ALTERNATE SOLUTION (only function called by Ajax)
        session_start();
        // END TODO
        if (!isset($_POST['post_id']) || !isset($_SESSION['id'])) {
            echo json_encode(array("success" => false));
        } else {
            $postId = $_POST['post_id'];
            $userId = $_SESSION['id'];
            $thanked = $this->model->getPostThanksFromUser($postId, $userId);
            if ($thanked) {
                $removing = $this->model->removeThanksRecord($postId, $userId);
                $decreasing = $this->model->decreasePostThanks($postId);
                if ($removing && $decreasing) {
                    echo json_encode(array("success" => true, "action" => "decrease"));
                } else {
                    echo json_encode(array("success" => false));
                }
            } else {
                $adding = $this->model->addThanksRecord($postId, $userId);
                $increasing = $this->model->increasePostThanks($postId);
                if ($adding && $increasing) {
                    echo json_encode(array("success" => true, "action" => "increase"));
                } else {
                    echo json_encode(array("success" => false));
                }
            }
        }
    }



    /**
     * Create the forum HTML for the categories
     * @param array $categories an array of the forum categories
     * @return string the generated HTML of the forum
     */
    function getForumHtml($infos) {
        // Creating the navigation bar
        $navigationArray = array(
            array("title" => "Forum")
        );
        $html = "";
        $html .= HTMLdiv(
            HTMLh1(t("Welcome on the forum"))
            .$this->generateNavigation($navigationArray),
            array("class" => "forumPosition")
        );
        // Displaying the number of topics in the forum
        $topicSentence = "";
        if ($infos['total_topics'] > 1) {
            $topicSentence = t("There are currently ").$infos['total_topics'].t(" topics in the forum.");
        } else {
            $topicSentence = t("There is currently ").$infos['total_topics'].t(" topic in the forum.");
        }
        $content = HTMLtagblock(
            "li",
            array("class" => "list-group-item"),
            HTMLdiv(
                HTMLtagblock("strong", array("class" => "text-muted"), $topicSentence)
                .HTMLform(
                    "index.php",
                    "GET",
                    HTMLinput("text", "forumQuery", "", t("Search a topic"), false, "form-control form-control-sm text-left")
                    .HTMLinput("hidden", "mode", "searchForum")
                ).HTMLtagblock("a", array("href" => "index.php?mode=showTopicCreation", "class" => "btn btn-primary btn-sm text-white"), t("Create a topic")),
                array("class" => "d-flex justify-content-between")
            )
        );
        // Creating the list of categories
        foreach ($infos['categories'] as $cat) {
            $date = date_create($cat['last_modification']);
            $content .= HTMLtagblock(
                "li",
                array("class" => "list-group-item"),
                HTMLtagblock(
                    "a",
                    array("href" => "index.php?mode=showForum&category=".$cat['id_category']),
                    HTMLdiv(
                        HTMLdiv(
                            HTMLtagblock("strong", array(), $cat['title'])
                            .HTMLtagblock("small", array("class" => "float-right"), t("Last change: ").$date->format('jS F Y').", ".$date->format("H:i")),
                            array("class" => "cardHeader")
                        )
                        .HTMLdiv($cat['description'], array())
                    )
                )
            );
        }
        $contentList = HTMLtagblock("ul", array("class" => "list-group list-group-flush"), $content);
        $contentDiv = HTMLdiv($contentList, array("class" => "card forumCard"));
        $html .= $contentDiv;
        return $html;
    }

    /**
     * Create the forum HTML for the topics of a category
     * @param array $infos an array containing multiple informations such as the category and the topics to display
     * @return string the generated HTML of the forum
     */
    function getForumCategoryHtml($infos) {
        // Creating the navigation bar
        $navigationArray = array(
            array("title" => "Forum", "link" => "index.php?mode=showForum"),
            array("title" => $infos['category']['title'])
        );
        $html = "";
        $html .= HTMLdiv(
            HTMLh1("Welcome in the ".$infos['category']['title']." category of the forum")
            .HTMLh3($infos['category']['description'])
            .$this->generateNavigation($navigationArray),
            array("class" => "forumPosition")
        );
        // Displaying the number of topics in the category
        $topicSentence = "";
        if ($infos['total_topics'] > 1) {
            $topicSentence = "There are currently ".$infos['total_topics']." topics in this category.";
        } else {
            $topicSentence = "There is currently ".$infos['total_topics']." topic in this category.";
        }
        $content = HTMLtagblock(
            "li",
            array("class" => "list-group-item"),
            HTMLdiv(
                HTMLtagblock("strong", array("class" => "text-muted"), $topicSentence)
                .HTMLform(
                    "index.php",
                    "GET",
                    HTMLinput("text", "forumQuery", "", "Search a topic", false, "form-control form-control-sm text-left")
                    .HTMLinput("hidden", "mode", "searchForum")
                    .HTMLinput("hidden", "category", $infos['category']['id_category'])
                ).HTMLtagblock("a", array("href" => "index.php?mode=showTopicCreation&category=".$infos['category']['id_category'], "class" => "btn btn-primary btn-sm text-white"), t("Create a topic")),
                array("class" => "d-flex justify-content-between")
            )
        );
        // Creating the list of topics
        foreach ($infos['topics'] as $topic) {
            $editDate = date_create($topic['last_modification']);
            $creationDate = date_create($topic['creation_date']);
            $content .= HTMLtagblock(
                "li",
                array("class" => "list-group-item"),
                HTMLtagblock(
                    "a",
                    array("href" => "index.php?mode=showForum&topic=".$topic['id_topic']),
                    HTMLdiv(
                        HTMLdiv(
                            HTMLtagblock("strong", array(), $topic['title'])
                            .HTMLtagblock("small", array("class" => "float-right"), "Last change : ".$editDate->format('jS F Y').", ".$editDate->format("H:i")),
                            array("class" => "cardHeader")
                        )
                        .HTMLdiv(
                            "by <strong>".$topic['creator_name']."</strong>&nbsp;<small>the ".$creationDate->format("jS F Y")." at ".$creationDate->format("H:i")."</small>",
                            array()
                        )
                    )
                )
            );
        }
        $contentList = HTMLtagblock("ul", array("class" => "list-group list-group-flush"), $content);
        $contentDiv = HTMLdiv($contentList, array("class" => "card forumCard"));
        $html .= $contentDiv;
        return $html;
    }
    
    /**
     * Create the forum HTML for a topic
     * @param array $infos an array of the forum categories
     * @param int $pageNo the page number, used to decide if the topic content is displayed or not
     * @return string the generated HTML of the forum
     */
    function getForumTopicHtml($infos, $pageNo) {
        $html = "";
        $topicDate = date_create($infos['topic']['creation_date']);
        // Displays parents in the forum structure
        $navigationArray = array(
            array("title" => "Forum", "link" => "index.php?mode=showForum"),
            array("title" => $infos['category']['title'], "link" => "index.php?mode=showForum&category=".$infos['category']['id_category']),
            array("title" => $infos['topic']['title'])
        );
        // Sticky title of the topic
        $html .= HTMLdiv(
            HTMLh1($infos['topic']['title'])
            .HTMLh3("Posted by <strong>".$infos['topic']['name']."</strong>&nbsp;<small>on the ".$topicDate->format("jS F Y")."</small>")
            .$this->generateNavigation($navigationArray),
            array("class" => "forumPosition")
        );
        // Content of the topic
        if ($pageNo == 1) {
            // If the topic is about a task
            $taskHtml = "";
            if (isset($infos['task'])) {
                $task = $infos['task'];
                $additionnalContent = (is_array($task) ? " &mdash; ".$task['title']." &mdash; ".$task['description'] : "");
                $taskHtml = HTMLdiv(
                    "Task n&deg;".$infos['topic']['id_task'].$additionnalContent,
                    array("class" => "card-footer text-muted")
                );
            }
            $html .= HTMLdiv(
                HTMLdiv(
                    HTMLtagblock("h5", array("class" => "card-title"), $infos['topic']['title'])
                    .HTMLtagblock("h6", array("class" => "card-subtitle mb-2 text-muted"), $infos['topic']['name']." - ".$topicDate->format('jS F Y')." at ".$topicDate->format('H:i'))
                    .HTMLdiv($infos['topic']['content'], array("class" => "card-text")),
                    array("class" => "card-body")
                )
                .$taskHtml,
                array("class" => "card forumTopicContent")
            );
        }
        // Posts of the topic (limited number)
        $content = "";
        foreach ($infos['posts'] as $post) {
            $creationDate = date_create($post['date']);
            $content .= HTMLtagblock(
                "li",
                array("class" => "list-group-item"),
                HTMLdiv(
                    HTMLdiv(
                        HTMLtagblock("strong", array(), $post['name'])
                        .HTMLtagblock("small", array("class" => "float-right"), $creationDate->format('jS F Y').", ".$creationDate->format('H:i')),
                        array("class" => "cardHeader", "id" => "forumPostHeader-".$post['id_post'])
                    )
                    .HTMLdiv(
                        $post['content'],
                        array("id" => "forumPost-".$post['id_post'])
                    )
                    .HTMLdiv(
                        HTMLtagblock(
                            "button",
                            array(
                                "id" => "forumThanksPost-".$post['id_post'],
                                "data-post-id" => $post['id_post'],
                                "class" => "btn btn-outline-success btn-sm".(in_array($post['id_post'], $infos['thanked_posts']) ? " active" : ""),
                                "data-toggle" => "button",
                                "aria-pressed" => in_array($post['id_post'], $infos['thanked_posts']) ? "true" : "false"
                            ), HTMLtagblock("span", array("id" => "forumThanksPostNumber-".$post['id_post'], "class" => "badge badge-light"), $post['thanks'])." Thanks")
                        .HTMLtagblock("button", array("id" => "forumQuotePost-".$post['id_post'], "data-post-id" => $post['id_post'], "class" => "btn btn-outline-primary btn-sm"), "Quote"),
                        array("class" => "text-right")
                    )
                )
            );
        }
        $contentList = "";
        $contentList = HTMLtagblock("ul", array("class" => "list-group list-group-flush"), $content);
        if (count($infos['posts']) > 0) {
            $contentDiv = HTMLdiv($contentList, array("class" => "card forumCard"));
            $html .= $contentDiv;
        }
        $html .= "<script src='js/forum.js'></script>";
        return $html;
    }

    /**
     * Create the creation topic form content
     * @param array $categories all the categories from the database
     * @param int $selectedCat the pre-selected category id
     * @param array all the tasks from the database
     * @return string the generated HTML of the topic creation elements
     */
    function getTopicCreationHtml($categories, $selectedCat, $tasks) {
        $titleInputDiv = HTMLdiv(
            HTMLtagblock("label", array("for" => "topicTitle"), "Topic title")
            .HTMLtag("input", array("id" => "topicTitle", "name" => "topicTitle", "placeholder" => "Enter title", "required" => "true", "class" => "form-control")),
            array("class" => "form-group")
        );
        // Creating category form element
        $categorySelectContent = "";
        foreach ($categories as $cat) {
            $attributes = array("value" => $cat['id_category']);
            if ($cat['id_category'] == $selectedCat) {
                $attributes['selected'] = true;
            }
            $categorySelectContent .= HTMLtagblock("option", $attributes, $cat['title']);
        }
        $categorySelectDiv = HTMLdiv(
            HTMLtagblock("label", array("for" => "topicCategory"), "Choose a category")
            .HTMLtagblock("select", array("id" => "topicCategory", "name" => "topicCategory", "class" => "form-control", "required" => "true"), $categorySelectContent),
            array("class" => "form-group")
        );
        // Creating task form element
        $taskSelectContent = "";
        $lastTaskArtifact = 0;
        $lastArtifactTitle = "";
        $tastTempContent = "";
        foreach ($tasks as $task) {
            if ($lastTaskArtifact != $task['artifact']) {
                if ($lastTaskArtifact != 0) {
                    $taskSelectContent .= HTMLtagblock("optgroup", array("label" => $lastArtifactTitle), $tastTempContent);
                }
                $lastArtifactTitle = $task['title'];
                $lastTaskArtifact = $task['artifact'];
            }
            $tastTempContent .= HTMLtagblock("option", array("value" => $task['id']), $task['id']." - ".$task['description']);
        }
        $taskSelectContent .= HTMLtagblock("optgroup", array("label" => $lastArtifactTitle), $tastTempContent);
        $taskAttributes = array("class" => "form-group");
        if ($selectedCat != 2) {
            $taskAttributes['style'] = "display: none;";
        }
        $taskSelectDiv = HTMLdiv(
            HTMLtagblock("label", array("for" => "taskSelect"), "Choose a task")
            .HTMLtagblock("select", array("id" => "taskSelect", "name" => "taskSelect", "class" => "form-control"), $taskSelectContent),
            $taskAttributes
        );
        $html = $titleInputDiv.$categorySelectDiv.$taskSelectDiv;
        $html .= "<script src='js/forum.js'></script>";
        return $html;
    }

    /**
     * Create the forum search screen
     * @param string $search the query searched by the user
     * @param array $infos an array of the retrieved topics and category infos
     * @return string the generated HTML of the results of the query
     */
    function getForumSearchHtml($search, $infos) {
        // Creating the navigation bar
        $navigationArray = array(array("title" => "Forum", "link" => "index.php?mode=showForum"));
        if (count($infos['category']) > 0) {
            array_push($navigationArray, array("title" => $infos['category']['title'], "link" => "index.php?mode=showForum&category=".$infos['category']['id_category']));
        }
        array_push($navigationArray, array("title" => "Results for '<em>".$search."</em>'"));
        $html = "";
        $html .= HTMLdiv(
            HTMLh1("Search results for <mark>".$search."</mark>")
            .$this->generateNavigation($navigationArray),
            array("class" => "forumPosition")
        );
        // Displaying the number of topics found using the search
        $topicSentence = "";
        if ($infos['total_topics'] > 1) {
            $topicSentence = "There are ".$infos['total_topics']." results for <mark>".$search."</mark>";
        } else {
            $topicSentence = "There is ".$infos['total_topics']." result for <mark>".$search."</mark>";
        }
        if (count($infos['category']) > 0) {
            $topicSentence .= " in category <mark>".$infos['category']['title']."</mark>";
        }
        $content = HTMLtagblock(
            "li",
            array("class" => "list-group-item"),
            HTMLdiv(HTMLtagblock("strong", array("class" => "text-muted"), $topicSentence))
        );
        // Creating the list of topics
        foreach ($infos['topics'] as $topic) {
            $editDate = date_create($topic['last_modification']);
            $creationDate = date_create($topic['creation_date']);
            $content .= HTMLtagblock(
                "li",
                array("class" => "list-group-item"),
                HTMLtagblock(
                    "a",
                    array("href" => "index.php?mode=showForum&topic=".$topic['id_topic']),
                    HTMLdiv(
                        HTMLdiv(
                            HTMLtagblock("strong", array(), $topic['title'])
                            .HTMLtagblock("small", array("class" => "float-right"), "Last change : ".$editDate->format('jS F Y').", ".$editDate->format("H:i")),
                            array("class" => "cardHeader")
                        )
                        .HTMLdiv(
                            "by <strong>".$topic['creator_name']."</strong>&nbsp;<small>the ".$creationDate->format("jS F Y")." at ".$creationDate->format("H:i")."</small>",
                            array()
                        )
                    )
                )
            );
        }
        $contentList = HTMLtagblock("ul", array("class" => "list-group list-group-flush"), $content);
        $contentDiv = HTMLdiv($contentList, array("class" => "card forumCard"));
        $html .= $contentDiv;
        return $html;
    }

    /**
     * Generate a textarea which is used in topic pages to submit new posts
     * @return string a portion of HTML which contains both the textarea and its TinyMCE initialisation 
     */
    private function generatePostTextarea() {
        $html = $this->generateTextarea("forumPostTextarea", "Reply to the topic");
        $html .= "<script>tinymce.init({ selector: 'textarea#forumPostTextarea', statusbar: false, height: 240 });</script>";
        return $html;
    }

    /**
     * Generate a textarea which is used in topic creation page
     * @return string a portion of HTML which contains both the textarea and its TinyMCE initialisation 
     */
    private function generateTopicTextarea() {
        $html = $this->generateTextarea("topicTextarea", "Topic description");
        $html .= "<script>tinymce.init({ selector: 'textarea#topicTextarea', statusbar: false, height: 340 });</script>";
        return $html;
    }

    /**
     * Generate the textarea required by the other 2 functions
     * @return string the textarea and the script integration
     */
    private function generateTextarea($name, $label) {
        $html = HTMLdiv(
            HTMLtagblock("label", array("for" => $name), $label)
            .HTMLtagblock("textarea", array("id" => $name, "name" => $name), ""), array("class" => "forumTopicContent")
        );
        $html .= "<script src='/headwork-master/js/tinymce/tinymce.min.js'></script>";
        return $html;
    }

    /**
     * Generate the forum pagination
     * @param int $pageNo the current page number
     * @param array $params an array of key/value
     * @param int $nbTotal the total number of elements, not limited by the pagination limit
     */
    private function generatePagination($pageNo, $params, $nbTotal) {
        $lastPageNo = ceil($nbTotal / $this->paginationLimit);
        if ($lastPageNo == 0) {
            $lastPageNo = 1;
        }
        $parametersString = "";
        foreach ($params as $key => $value) {
            $parametersString .= $key."=".$value."&";
        }
        $firstPageBtn = HTMLtagblock("a",
            array(
                "class" => ($pageNo == 1 ? "btn btn-dark disabled" : "btn btn-dark"),
                "href" => ($pageNo == 1 ? "#" : "index.php?".$parametersString."page=1")
            ), "&#8810;");
        $leftBtn = HTMLtagblock("a",
            array(
                "class" => ($pageNo == 1 ? "btn btn-dark disabled" : "btn btn-dark"),
                "href" => ($pageNo == 1 ? "#" : "index.php?".$parametersString."page=".($pageNo - 1)),
            ), "&lt;");
        $middleBtn = HTMLtagblock("button",
            array(
                "class" => "btn btn-dark"
            ), $pageNo);
        $rightBtn = HTMLtagblock("a",
            array(
                "class" => ($pageNo * $this->paginationLimit >= $nbTotal ? "btn btn-dark disabled" : "btn btn-dark"),
                "href" => ($pageNo * $this->paginationLimit >= $nbTotal ? "#" : "index.php?".$parametersString."page=".($pageNo + 1))
            ), "&gt;");
        $lastPageBtn = HTMLtagblock("a",
            array(
                "class" => ($pageNo == $lastPageNo ? "btn btn-dark disabled" : "btn btn-dark"),
                "href" => ($pageNo == $lastPageNo ? "#" : "index.php?".$parametersString."page=".$lastPageNo)
            ), "&#8811;");
        return HTMLdiv(
            $firstPageBtn.$leftBtn.$middleBtn.$rightBtn.$lastPageBtn,
            array(
                "class" => "btn-group forumTopicContent",
                "role" => "group"
            )
        );
    }

    /**
     * Generate the top navigation path / tree view
     * @param array $array an array containing each level of the hierarchy
     */
    private function generateNavigation($array) {
        $listContent = "";
        $i = 1;
        foreach ($array as $element) {
            $attributes = array();
            $attributes['class'] = ($i == sizeof($array) ? "breadcrumb-item active" : "breadcrumb-item");
            if ($i == sizeof($array)) {
                $attributes['aria-current'] = "page";
            }
            $content = $element['title'];
            if ($i < sizeof($array)) {
                $content = HTMLtagblock("a", array("href" => $element['link']), $content);
            }
            $listContent .= HTMLtagblock("li", $attributes, $content);
            $i++;
        }
        return HTMLtagblock(
            "nav",
            array("aria-label" => "breadcrumb"),
            HTMLtagblock("ol", array("class" => "breadcrumb"), $listContent)
        );
    }

    /**
     * Generate the alert to notify users that admins can read
     */
    private function generateWarning() {
        $html = HTMLdiv(
            "Please be aware that <u>administrators</u> are able to read everything you send"
            .HTMLtagblock(
                "button",
                array("class" => "close", "data-dismiss" => "alert", "aria-label" => "Close"),
                HTMLtagblock("span", array("aria-hidden" => "true"), "&times;")
            ),
            array("role" => "alert", "class" => "alert alert-warning")
        );
        return $html;
    }

}

?>