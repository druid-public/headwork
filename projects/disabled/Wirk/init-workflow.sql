DELETE from Artifact where classid in (select id from ArtifactClass where project = 'Wirk');
DELETE from ArtifactClass where project = 'Wirk';

-- Chasser le Totoro Artifact
insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (7000,'Chasser le Totoro','Wirk/wirk.sca', 'Artifact', 'Wirk',true);

-- Wirk Service call example
insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (7001,'Wirk service call demo','Wirk/wirk-service.sca', 'Artifact', 'Wirk',true);
