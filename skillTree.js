var margin = {top: 20, right: 100, bottom: 20, left: 100};

if(type=="prefs"){
  var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    width = (w.innerWidth || e.clientWidth || g.clientWidth)-margin.top-margin.bottom,
    height = (w.innerHeight|| e.clientHeight|| g.clientHeight);
}else{
  var width = 600-margin.top-margin.bottom,
    height = 800-margin.left-margin.right;
}

var i = 0,
    duration = 750,
    root;

var depthLastChanges=0,diff=0;

var depth= {};

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

const zoom = d3.behavior.zoom()
  .scaleExtent([1/4, 9])
  .on('zoom', function () {
    d3.select('g').attr('transform', d3.event.transform)
  });

var svg = d3.select("#tree").append("svg")
    .attr("width", function(){ if(type=="prefs"){ return width; }else{ return width + margin.right + margin.left;}})
    .attr("height", height + margin.top + margin.bottom)
    .call(d3.behavior.zoom().on("zoom", function () {
      svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")")
    }))
  .append("g")
    .attr("transform",  function(){ if(type=="prefs"){ return "translate(" + margin.left + "," + margin.top + ")"; }else{return "translate( 25 ," + margin.top + ")";}})
    ;

d3.json("skill.json", function(error, flare) {
  if (error) throw error;
  root = flare;
  root.x0 = height / 2;
  root.y0 = 0;

  function collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }
  }
  
  tree.nodes(root).reverse().forEach(function(d){
    depth[d.name]=d.depth;
  });

  if(type=="prefs"){
    depthShift();
    depthLastChanges=0;
    diff=0;
    depthShiftSpe();
    document.getElementById("list").value=JSON.stringify(list['skill']);
    document.getElementById("listCouple").value=JSON.stringify(listCouple);
    document.getElementById("listSpe").value=JSON.stringify(listSpe['skill']);
  }

  expandAll();
  if(type!="prefs"){
	  root.children.forEach(function(d){
		  if(d.children) d.children.forEach(collapse);
	  }); //Collapse the tree except at the root's children

	  update(root);
  }

});

d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = depth[d.name] * 180;});

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")";});

  nodeEnter.append("circle")
      .attr("r", 1e-6)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; })
      .on("click", click);
  
  /*nodeEnter.append("foreignObject")//An "idea" for adding the checkbox
  .attr("width", 100)
  .attr("height", 40)
  .html(function(d){ return "<input type=hidden name=check value="+d.name+" /><input type=hidden name=mode value=saveSkill /><button type=submit class=btn name=submit value="+d.name+" ><i class=\"glyphicon glyphicon-check\"></i></button></form>";})
  .on("click", function(d, i){
      console.log(d.name);
  });*/
  
  if(type=="prefs"){
	  nodeEnter.append("foreignObject")
	  		.attr("width", function(d){if(!inList(d.name)){ return d.name.length+"em";}else{ return d.name.length+3+"em";}})
	  		.attr("height", 30)
	      .attr("x", function(d) { return d.children || d._children ? -d.name.length*8 : +5; })
	      .attr("y", function(d) { return d.children || d._children ? -30 : -15; })
	      .attr("dy", ".35em")
	      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
	      .html(function(d){ if(inList(d.name) ){ return "<p style=\"color:#00aced\">"+d.name+"</p>";}else if(inListSpe(d.name)){return "<p style=\"color:#19ff19\">"+d.name+"</p>";}else{ return "<p>"+d.name+"</p>";}})//.text(function(d) { return d.name; })
	      .style("fill-opacity", 1e-6)
        .on('click', function(d){
          if(typeof list['skill'] !== 'undefined'){ 
                if(document.getElementById("pref1").value!=""){
                  if(document.getElementById("pref2").value!=""){
                    if(document.getElementById("pref1").value!=d.name && document.getElementById("pref2").value!=d.name){
                      document.getElementById("pref2").value=d.name;
                    }
                  }else{
                    document.getElementById("pref2").value=d.name;
                  }
                }else{
                  document.getElementById("pref1").value=d.name;
                }
            }else{ 
              document.getElementById("tree").innerHTML+= "Select your skills before using preferences";
          }
          });
  }else{
	  nodeEnter.append("foreignObject")
		.attr("width", function(d){if(!inList(d.name)){ return d.name.length+"em";}else{ return d.name.length+3+"em";}})
		.attr("height", 30)
    .attr("x", function(d) { return d.children || d._children ? -d.name.length*8 : +5; })
    .attr("y", function(d) { return d.children || d._children ? -30 : -15; })
    .attr("dy", ".35em")
    .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
    .html(function(d){ if(!inList(d.name)){return "<input type=hidden name=mode value=addSkill /><a>"+d.name+" </a>"; }else{ if(typeof list['skill']!=='string'){ return "<p>"+d.name+" : "+list['level'][index(d.name)]+"</p>";}else{return "<p>"+d.name+" : "+list['level']+"</p>";}}})//.text(function(d) { return d.name; })
    .style("fill-opacity", 1e-6)
    .on('click', function(d){ 
      if(typeof list['skill'] !== 'undefined'){ 
            if(!inList(d.name)){ 
              var name=d.name;
              var levelmin=levelMinRec(d);
              var levelmax=levelMax(d);
              if(parseInt(document.getElementById("level").value)<levelmax && parseInt(document.getElementById("level").value)>levelmin){
                document.location.href="index.php?mode=addSkill&submit="+name+"&level="+document.getElementById("level").value;
              }else if(parseInt(document.getElementById("level").value)<=levelmin){
                document.getElementById("limit").innerHTML= " The level need to be greater than "+levelmin; 
              }else{
                document.getElementById("limit").innerHTML= " The level need to be less than "+levelmax; 
              }
            }
        }else{ 
          document.location.href="index.php?mode=addSkill&submit="+d.name+"&level="+document.getElementById("level").value;
      }
      });
  }

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
      .attr("r", 4.5)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if(type=="prefs"){
    if (d.children) {
        deleteChildCouple(d);
        listCopy =JSON.parse(JSON.stringify(list['skill']));
        for(i = 0; i < listCopy.length; i++) {
            resultat = listCouple.find(function(element) { return element[0] == listCopy[i] || element[1] == listCopy[i]; });
            if(resultat==undefined){
                var index = list['skill'].indexOf(listCopy[i]);
                if (index > -1) {
                  list['skill'].splice(index, 1);
                }
            }
        }
        document.getElementById("listCouple").value=JSON.stringify(listCouple);
        document.getElementById("list").value=JSON.stringify(list['skill']);
        node = d3.selectAll("circle"); 
        g = node.select(function(f) { if(f.name==d.name){ return this;} });
        g.on('click', function() { d3.event.stopPropagation(); });
    }
  }
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}

function expand(d) {
	var children = (d.children) ? d.children : d._children;
	if (d._children) {
	    d.children = d._children;
	    d._children = null;
	}
	if (children) children.forEach(expand);
	}

	function expandAll() {
	expand(root);
	update(root);
	}

function inList(skill){
	if(typeof list['skill']=='string'){
		if(skill==list['skill']){
			return true;
		}
	}else if(typeof list['skill'] !== 'undefined'){
		for(i = 0; i < list['skill'].length; i++) {
			if(skill==list['skill'][i]){
				return true;
			}
		}
	}
	return false;
}

function inListSpe(skill){
	if(typeof listSpe['skill']=='string'){
		if(skill==listSpe['skill']){
			return true;
		}
	}else if(typeof listSpe['skill'] !== 'undefined'){
		for(i = 0; i < listSpe['skill'].length; i++) {
			if(skill==listSpe['skill'][i]){
				return true;
			}
		}
	}
	return false;
}

function levelMin(d){
	if(d.children){
		d=d.children;
		while(!inList(d.name) && d.children){
			d=d.children;
		}
		if(inList(d.name)){
			if(typeof list['skill']!=='string'){
				return list['level'][index(d.name)];
			}else{
				return list['level'];
			}
		}
	}
	return -1;
}

function levelMinRec(d){
	max = -1;
	if(d.children){
		d=d.children;
		d.forEach(function(d){
			if(inList(d.name)){
				if(typeof list['skill']!=='string'){
					max = Math.max(max,Math.max(list['level'][index(d.name)],levelMinRec(d)));
				}else{
					max = Math.max(max,Math.max(list['level'],levelMinRec(d)));
				}
			}else{
				max = Math.max(max,levelMinRec(d));
			}
		});
	}
	return max;
}

function levelMax(d){
	if(d.parent){
		d=d.parent;
		while(!inList(d.name) && d.parent){
			d=d.parent;
		}
		if(inList(d.name)){
			if(typeof list['skill']!=='string'){
				return list['level'][index(d.name)];
			}else{
				return list['level'];
			}
		}
	}
	return 101;
}

function index(skill){
	for(i = 0; i < list['skill'].length; i++) {
		if(skill==list['skill'][i]){
			return i;
		}
	}
}

function depthShift(){
  for(var j = 0; j < list['skill'].length; j++) {
    tree.nodes(root).reverse().forEach(function(d){ 
      if(d.name==list['skill'][j]){ 
        diff=depthLastChanges-depth[d.name]; if(diff<0){ depthLastChanges=depth[d.name]; }else{ depthLastChanges=depth[d.name]+1+diff; depth[d.name]=depth[d.name]+1+diff; if(d.children){ d.children.forEach(function(d){ depth[d.name]=depth[d.name]+1+diff; updateChildren(d,diff); }); }}
      }
    });
  }
}

function depthShiftSpe(){
  for(var j = 1; j < listSpe['skill'].length; j=j+2) {
    tree.nodes(root).reverse().forEach(function(d){ 
      if(d.name==listSpe['skill'][j+1]){ 
        depth[d.name]=depth[d.name]+1; if(d.children){ d.children.forEach(function(d){ depth[d.name]=depth[d.name]+1+1; updateChildren(d,1); }); }}
      }
    );
  }
}

function updateChildren(d,diff){
  if(d.children){ d.children.forEach(function(d){ depth[d.name]=depth[d.name]+1+diff;updateChildren(d,diff); });}
}

function deleteChildCouple(d){
    var index = 0;
    listCopy =JSON.parse(JSON.stringify(listCouple));
    for(var i = 0; i < listCopy.length; i++) {
        if(isChild(d,listCopy[i][0]) && isChild(d,listCopy[i][1])){
            index = isItemInArray(listCouple,listCopy[i]);
            if (index > -1){
                listCouple.splice(index, 1);
            }
        }
    }
  //if(d.children){ d.children.forEach(function(d){ deleteChildCouple(d); });}
}

function isChild(d,fils){
    if(d.name==fils){
        return true;
    }
    if(d.children){
        return d.children.some(function(d){ return isChild(d,fils); });
    }
    return false;
}

function getWindowHeight() {
    var windowHeight=0;
    if (typeof(window.innerHeight)=='number') {
        windowHeight=window.innerHeight;
    } else {
        if (document.documentElement&& document.documentElement.clientHeight) {
            windowHeight = document.documentElement.clientHeight;
        } else {
            if (document.body&&document.body.clientHeight) {
                windowHeight=document.body.clientHeight;
            }
        }
    }
    return windowHeight;
}
function getWindowWidth() {
 var windowWidth=0;
 if (typeof(window.innerWidth)=='number') {
  windowWidth=window.innerWidth;
    } else {
  if (document.documentElement&& document.documentElement.clientWidth) {
   windowWidth = document.documentElement.clientWidth;
        } else {
   if (document.body&&document.body.clientWidth) {
    windowWidth=document.body.clientWidth;
            }
        }
    }
 return windowWidth;
}

function isItemInArray(array, item) {
    for (var i = 0; i < array.length; i++) {
        // This if statement depends on the format of your array
        if (array[i][0] == item[0] && array[i][1] == item[1]) {
            return i;   // Found it
        }
    }
    return -1;   // Not found
}