<?php

/**
 *	HEADWORK main
 *
 *	Main variables:
 *	$conn: database connection
 *	$mode: the action required by the web user
 *	$artifact: the running workflow of crowdsourcing
 *	$VIEW: global variable, associative array containing changing parts of the web interface
 *	       Template in template-main.php
 *	       MAIN: the main part of the Web page
 */

// checking proper installation

if (file_exists("config.php"))
    require("config.php");
else {
    die("Headwork is not properly installed yet. Set a config.php file (use the config.php.example template).");
}

require_once("lib/i18n.php");
require_once("lib/HWlib.php");
require_once("lib/HTML.php");
require_once("lib/artifacts.php");
require_once("pages/skills/skills.php");
require_once("pages/workflowdesign/workflowdesign.php");
require_once("pages/showCredit/showCredit.php");
require_once("pages/showState/showState.php");
require_once("pages/loginForm/loginForm.php");
require_once("pages/register/register.php");
require_once("pages/addUser/addUser.php");
require_once("pages/logout/logout.php");
require_once("pages/profile/profile.php");
require_once("pages/showTasks/showTasks.php");
require_once("pages/startup/startup.php");
require_once("pages/drawArtifact/drawArtifact.php");
require_once("pages/maintenance/maintenance.php");
require_once("pages/feedback/feedback.php");
require_once("pages/forum/forum.php");
require_once("pages/taskViewer/taskViewer.php");

// TODO faire une classe avec action($conn,$context), qui va appeler la fonction précise avec les $_GET (tout est sur ce format)

session_start();

starti18n();

try { // global exception catcher

$conn = HWdbconnect();

$VIEW = array("MAIN" => "");

$mode = (new LoginForm($conn))->login($conn);
$_SESSION['mode']=$mode;

if (isset($_SESSION['username']))
    $VIEW["ID"] = $_SESSION['username'];

$VIEW["PROJECT"]=HWlistProjects($conn);

// TODO prototype of the future $mode call system with pageList security test
$pageList=array("login"=>"LoginForm",
                "register"=>"Register");

switch ($mode) {
     case "login":
            // TODO prototype of the future $mode call system with pageList security test
            if(array_key_exists($mode,$pageList)){
                $class=$pageList[$mode];
                $obj=new $class($conn);
                $obj->render();
            }
          break;
     case "register":
            if(array_key_exists($mode,$pageList)){
                $class=$pageList[$mode];
                $obj=new $class($conn);
                $obj->setUserNameAlreadyUsed(false);
                $obj->render();
            }
          break;
     case "addUser":
          addUser($conn);
          break;
     case "logout":
          logout($conn);
          break;
     case "profile":
          profile($conn);
          break;
     case "saveProfile":
          saveProfile($conn);
          break;
     case "skills":
          skills($conn);
          break;
     case "addSkill":
          addSkill($conn);
          break;
     case "saveSkill":
          saveSkill($conn);
          break;
     case "showTasks":
          showTasks($conn);
          break;
     case "answer":
          answer($conn, $_REQUEST['choice']);
          break;
     case "insertAnswer":
            @insertJsonAnswer($conn, $_POST['id'], $_SESSION['activity_id'],$_POST['answer'], @$_POST['mass']);
            break;
     case "restart":
         restart($conn);
         initialize($conn);
         createBots($conn,NBBOTS);
         showTasks($conn);
         break;
     case "showState":
          showState($conn);
          break;
     case "showArtifact":
          showArtifact($conn,$_GET['artifactid']);
          break;
     case "drawArtifact":
          drawArtifact($conn,$_GET['artifactid']);
          break;
     case "Home":
          showTasks($conn);
          break;
     case "toggleLang":
           if(in_array(@$_GET['lang'],array("en","fr","ka")))
            $_SESSION['lang']=$_GET['lang'];
           else
            $_SESSION['lang']='en';
           showTasks($conn);
           break;
     case "showCredit":
            // prototype of the future $mode call system with pageList security test
            $pageList=array("showCredit"=>"Credits");
            if(array_key_exists($mode,$pageList)){
                $class=$pageList[$mode];
                $obj=new $class($conn);
                $obj->render();
            }
          break;
     case "maintenance":
          maintenance();
          break;
     case "submitFeedback":
          submitFeedback($conn, $_POST['page'], $_POST['feedbackText']);
          break;
     case "showFeedback":
          $pageNo = isset($_GET['page']) ? $_GET['page'] : 1;
          showFeedback($conn, $pageNo);
          break;
     case "insertTaskFeedback":
          insertTaskFeedback($conn, $_POST['id'], $_POST['feedback'], $_POST['targetTable']);
          break;
     case "showForum":
          showForum($_GET);
          break;
     case "insertPostForum":
          insertPostForum($_POST['topicId'], $_POST['forumPostTextarea']);
          break;
     case "showTopicCreation":
          showTopicCreation($_GET);
          break;
     case "insertNewTopic":
          insertNewTopic($_POST['topicTitle'], $_POST['topicCategory'], $_POST['taskSelect'], $_POST['topicTextarea']);
          break;
     case "searchForum":
          searchForum($_GET);
          break;
     case "startArtifact":
         startArtifact($conn, $_GET['artifactid'],$_SESSION['id']);
         showTasks($conn);
         break;
     case "workflowdesign":
          workflowdesign($conn);
          break;
     case "addArtifact":
         addArtifact($conn,$_POST['artifact']);
         break;
     case "showTasksViewer":
          showTasksViewer();
          break;
    case "insertUpload":
        insertUpload($conn, $_POST['id'], $_SESSION['activity_id'],@$_POST['mass']);
        break;
    case "insertReportAbuse":
        insertReportAbuse($conn, $_POST['project1'], $_POST['activity1'], $_POST['etape1'], $_POST['category1'], $_POST['comments1']);
        break;
     default:
          showHome();
          break;
}


// determining language

require("templates/template-main.php");

}
catch (Exception $e){ // global exception handler
    $message=$e->getMessage();
    $trace=$e->getTraceAsString();
    echo "
        <div class='alert alert-danger' role='alert'>
        <center><img src='images/klipartz.com.png' width='300px' alt='a person with headache image'/></center>
        <h1>".t("Ouuuch! ... Headache!")."</h1>
        <h2>".t("Sorry, we got an internal error. We are on it. Try going back to the ")."<a href='index.php'>".t("main page")."</a>.</h2>
        ";
     if(DEBUG) {
        echo "
            <code>
            <h2>Details</h2>
            <p>$message</p>
        ";
        print_r($e->getTrace());
        echo "</code>";
    }
    echo "</div>";
    exit;
}

?>
