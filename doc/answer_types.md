# Possible answer types

## New database schema
In order to allow the use of other types of answer, 3 fields have been added to the table `task` of the current database schema.  
The fields are `type`, `arg1` and `arg2`.

### `headwork.task.type`
Currently, integer is the datatype used (this may change in the future).  
Some types have been created in order to group multiple tasks in one. It enhances the user experience as she/he won't need to complete multiple look alike tasks.

#### `NULL` value
**Default value**, displays the default textarea as answer field.  
It is now possible to add a placeholder by using the `arg1` field, you only need to put text in it.

#### Type `0`
Removes any input so you can display text only. When the user will click on the submit button, an insert will be done in base in order to be able to check the guard.  
![Type 0 example : only the body is displayed](uploads/b887641e206d68bb3f4c23f30363ee95/answer-type-0.png)

#### Type `1`
Displays a select HTML element which options are retrieved using the arg1 field.  
Uses `arg1`. The SQL query must return 2 fields per row : `value` and `text`. If you plan to use the value as the displayed text, just select the value twice and use `text` as an alias on one of them.

#### Type `2`
Displays radio buttons which values are retrieved using the arg1 field.  
Uses `arg1` field. The SQL query must return 2 fields per row : `value` and `text`. If you plan to use the value as the displayed text, just select the value twice and use `text` as an alias on one of them.

#### Type `3`
Displays a textarea which has multi-lines edit enabled. This means that a line break (CR/LF) in the field will split your answers. Instead of having one chunk of text saved as your answer, each line you typed in the field will be separated.  
It is now possible to add a placeholder by using the `arg1` field, you only need to put text in it.  
If you want to display a placeholder on multiple lines, add `\n` between your 'lines' in your artifact file.

#### Type `4`
Displays radio buttons which values are retrieved using the arg1 field. A custom text input has been added to allow to set a custom answer.    
Uses `arg1` field.  
The SQL query must return 2 fields per row : `value` and `text`. If you plan to use the value as the displayed text, just select the value twice and use `text` as an alias on one of them.  
[Type 4 example](uploads/98b1cbb02ff2c9a46b743492cde148ec/answer-type-4.png)

#### Type `5`
Displays a list of tasks where the answer type is a unique choice among radio buttons. This type enables the possibility to answer multiple tasks at once, instead of doing ***n*** different tasks.  
Uses both `arg1` and `arg2` fields.  
The first SQL query's goal is to retrieve the text/question to display. It must return 2 fields named `text` and `id` for each row. `id` should contain the task ID you would have used if grouping tasks wasn't possible. The `id` is used to group radio buttons from the same line together.  
The second SQL query's goal is to retrieve the possible answers. It can return 5 fields for each row. Only 2 of them are mandatory : `value` and `text` (same goal as type `2` and `4`).  
The 3 other fields are optionnal :  
* `request_feedback` field : a boolean which enables (if true) the feedback request on user answer. It is specifically used for unexpected answers (in the picture below, it would be for the red one). It can only be true for **one row**. If the user chooses this answer, a dialog will appear and ask for feedback about the text/question. This field must be used with `target_table`.
* `target_table` field : a string which contains the database feedback table name (specific to your artifact). Must be used with `request_feedback`.
* `class` field : a string which contains the Bootstrap color to use for the choice buttons ('danger' for example). It can be different for each row. The default Bootstrap color used is 'secondary'.  

For example, this is a table structure which has been used with this type (`target_table` was added manually in the query) :

| id  | value | text          | class   | request_feedback |
| --- | ----- | ------------- | ------- | ---------------- |
| 1   | 0     | Bad           | danger  | true             |
| 2   | 1     | To be revised | warning | false            |
| 3   | 2     | Perfect       | success | false            |

![alt text](uploads/829cd590dc394614773df01fbaa05886/answer-type-5.png "Type 5 example")  
NB : a Javascript file builds a JSON containing each answer. This JSON is inserted into the hidden 'answer' input. The actions.php script has been edited in order to manage JSON answers (see the function insertAnswer()).

#### Type `6`
Displays a list which has, for each element, a bound textarea. This means that the user can answer multiple questions/requests in differents textarea into a unique task.  
Uses `arg1`.
The SQL query must return 2 fields per row : `id` and `text`. `id` should contain the task ID you would have used if grouping tasks wasn't possible.  
A new option has been enabled only for this type at the moment : `help` (but tools to implement it to other types have been created).  
It is now a field of the `task` table which stores a boolean/tinyint to enable it. The user will see 2 new inputs where he will be able to enter a useful link and the important page (in the case of a PDF for example).  
![alt text](uploads/0f7090686c545685e2d0f443cde89c2b/answer-type-6.png "Type 6 example")  
NB : a Javascript file builds a JSON containing each answer. This JSON is inserted into the hidden 'answer' input. The actions.php script has been edited in order to manage JSON answers (see the function insertAnswer()).

#### Type `7`
Displays cards showing multiple answers for one question/request. This type is better to use than type `5` **if the answers are longer**.  
Uses both `arg1` and `arg2`.
The first SQL query must return 2 fields per row : `id` and `text`. `id` should contain the task ID you would have used if grouping tasks wasn't possible.  
The second query's goals is to retrieve the possible answers. It must return 2 fields : `text` and `value`.  
An optional field can also be added to the query : `help`. This field should be JSON type that contains 2 entries : `link` and `page`. It is used as a justification by the user for his answer. This field has here only a display purpose.  
![alt text](uploads/8d05d940adf241ee8f71c9a112e33409/answer-type-7.png "Type 7 example")  
NB : a Javascript file builds a JSON containing each answer. This JSON is inserted into the hidden 'answer' input. The actions.php script has been edited in order to manage JSON answers (see the function insertAnswer()).

#### Type `8`
Displays a small image which can be clicked to display a draw.io modal (you can click it again to edit the diagram you started).
It allows the creation of diagrams. As the answer is a PNG encoded in base64, the string can be quite long so you may have to adapt some database fields to a larger type.  
`arg1` is optionnal. This time, it does not store a SQL query but a base64 image (PNG with embedded XML for draw.io) which should begin like this : `data:image/png;base64,` followed by a sequence of characters. It makes the image instantly editable, without going throught the template selection process.  
NB : if you use the `arg1` field, it **must** be a PNG with embedded XML using base64 or the load will fail.

#### Type `9`
Displays a list of images where the user can only select 1.  
Uses `arg1`.
The SQL query must return 2 fields per row : `id` and `value`. `id` should contain the unique identifier of the row containg the image to display. `value` must contain the 'source' of the image, it should be either the base64 code retrieved from type `8` or a valid url or path.  
A custom style is applied to make the whole list more readable.

## For developers
If you plan to add a new multiple answer type, please refer to other JavaScript files such as `multiplecards.js` or `radiolist.js` for the submit part.  
You'll eventually need to add a condition to submit the form as with the Ajax fonctionnality, the Ajax part will submit the form itself.  
The code to prevent double submitting should be similar to this :
```javascript
if ($(':input[name="ajax"]').length == 0) {
    $(this).submit();
}
```