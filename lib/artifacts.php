<?php

require_once("lib/wirk.php");

/**
 * sugarFree
 *
 * Remove syntactic sugar from guards and actions in artifacts
 *
 * @param  $str string: The string to unsugar
 * @param $conn A valid pdo connection (optional)
 * @param $noquote TODO HACK do not quote the result of HTMLTABLE for json export validity
 * @return stringbool string: sugar-free string
 */

function sugarFree($str,$conn,$noquote=false){
    if($str=="none")
            $str="select true from dual";
    if (preg_match("/^task ([0-9]+) is answered/",$str,$matches)){
            $str="select true from Answered where artifact=CURRENT_ARTIFACT and id=".$matches[1];
    }
    if (preg_match("/^use '(.*)' as template/",$str,$matches)){
        $str="select @TEMPLATE:=body from Template where id='".$matches[1]."'";
    }
    if (preg_match("/^use '(.*)' as title/",$str,$matches)){
            $str="select @TITLE:='".$matches[1]."'";
    }

    if (preg_match("/^use '(.*)' as description/",$str,$matches)){
        $str="select @DESCRIPTION:='".$matches[1]."'";
    }

    if (preg_match("/^use '(.*)' as checker message/",$str,$matches)){
                    $str="select @CHECKERMSG:='".$matches[1]."'";
    }

    if (preg_match("/^use '(.*)' as checker/",$str,$matches)){
        $str="select @CHECKER:='".$matches[1]."'";
    }

    if (preg_match("/^use '(.*)' as prompt/",$str,$matches)){
        $str="select @ARG1:='".$matches[1]."'";
    }
    if (preg_match("/^take majority voting of task ([0-9]+) as @(.*)/",$str,$matches)){
        $str="select @".$matches[2].":=reference_value from (select value as reference_value,count(*) as Votes from Answer where idtask=".$matches[1]." and Answer.artifact=CURRENT_ARTIFACT group by value) as votetable where Votes>all(select count(*) as Votes2 from Answer where value<>reference_value and idtask=".$matches[1]." and Answer.artifact=CURRENT_ARTIFACT group by value);";
    	$str.="select @".$matches[2].":=if(@".$matches[2]." is null,'no majority',@".$matches[2].")";
    }

    if (preg_match("/^use answer of task ([0-9]+) as @(.*)/",$str,$matches)){
        $str="select @".$matches[2].":=value from Answer where user=SESSION_USER and artifact=CURRENT_ARTIFACT and idtask=".$matches[1];
    }

    if (preg_match("/^answer task ([0-9]+) with (.*)/",$str,$matches)){
        $str="insert into Answer(idtask,user,artifact,value) values(".$matches[1].",SESSION_USER,CURRENT_ARTIFACT,".$matches[2].")";
    }


    if (preg_match("/^select @(.*):=HTMLTABLE\((.*)\)$/",$str,$matches)){
        $str="select @".$matches[1].":=";
        $res=showQueryAnswerAsString($conn, "", $matches[2]);
        if ($noquote)
            $str.='hidden';
        else
            $str.=$conn->quote($res);
    }

    if (preg_match("/^prepare task ([0-9]+) as (basic|message|homemade|multi-line input|radio|dropdown)/",$str,$matches)){
        $str="select @TITLE:='';select @DESCRIPTION:='';select @TEMPLATE:='';select @BODY:=@TEMPLATE;select @CHECKER:=NULL;select @CHECKERMSG:=NULL;select @PARAM:=NULL;";
        $str.="select @TASKID:=".$matches[1].";";
        switch ($matches[2]){
            case "basic":
                $str.="select @TYPE:=NULL";
                break;
            case "message":
                $str.="select @TYPE:=0";
                break;
            case "multi-line input":
                $str.="select @TYPE:=3";
                break;
            case "homemade":
                $str.="select @TYPE:=11";
                break;
            case "radio":
                $str.="select @TYPE:=2";
                break;
            case "dropdown":
                $str.="select @TYPE:=1";
                break;
            default:
                break;
                // TODO continue with other sugar cubes :)
        }
    }

    if (preg_match("/^install task/",$str,$matches)){
        $str="insert into Task(id,artifact,title,description,body,checker,checkermsg,type,arg1) values (@TASKID,CURRENT_ARTIFACT,@TITLE,@DESCRIPTION,@TEMPLATE,@CHECKER,@CHECKERMSG,@TYPE,@ARG1)";
    }

    if (preg_match("/^forget profile for current artifact/",$str,$matches)){
        $str="delete from Profile where idartifact=CURRENT_ARTIFACT";
    }

    if (preg_match("/^forget tasks for current artifact/",$str,$matches)){
        $str="delete from Task where artifact=CURRENT_ARTIFACT";
    }

    if (preg_match("/^forget answers for current artifact/",$str,$matches)){
        $str="delete from Answer where artifact=CURRENT_ARTIFACT";
        $str="delete from Answered where artifact=CURRENT_ARTIFACT";
    }

    if (preg_match("/^offer task to anyone/",$str,$matches)){
        $str="insert into Profile values (@TASKID,CURRENT_ARTIFACT,1)";
    }

    if (preg_match("/^skill ([0-9]+) is relevant for the task/",$str,$matches)){
        $str="insert into Profile values (@TASKID,CURRENT_ARTIFACT,".$matches[1].")";
    }

    if (preg_match("/^replace (.*) with (.*)/",$str,$matches)){
        $str="select @TEMPLATE:=replace(@TEMPLATE,'".$matches[1]."',".$matches[2].")";
    }

    if (preg_match("/^use \((.*)\) as list/",$str,$matches)){
            $str="select @ARG1:='".$matches[1]."'";
        }

    if (preg_match("/^use answers from task (.*) as list/",$str,$matches)){
            $str="select @ARG1:='select value as value,value as text from Answer where user=SESSION_USER and idtask=".$matches[1]." and artifact=CURRENT_ARTIFACT'";
        }

    if (preg_match("/^upgrade user skill ([0-9]+) by ([0-9]+)/",$str,$matches)){
//            $str="update Skills set level=level+".$matches[2]." where iduser=SESSION_USER and idskill=".$matches[1];
            $str="insert into Skills values(SESSION_USER,".$matches[1].",".$matches[2].") on duplicate key update level=level+".$matches[2];
    }

    return $str;
}


/**
 * update
 *
 * Update an artifact from its current node to the next node reachable by valid a guard
 *
 * @param  $conn : A valid database PDO connection
 * @param  $artifact : an artifact description
 * @param $artifactid: the id of the artifact
 * @param $count : the number of nodes already visited
 * @return bool : true if a guard is validated
 */
function updateonce($conn, $artifact, $artifactid, $count)
{
    debug("artifactid $artifactid, count $count");
    $node=0;
    $query="select node from Artifact where id=$artifactid";
    $node=$conn->query($query)->fetch()['node'];

    debug("updating from node $node");

    $query="select definition from ArtifactClass,Artifact where Artifact.id=$artifactid and Artifact.classid=ArtifactClass.id";
    $artifactfile=$conn->query($query)->fetch()['definition'];
    $artifact=load_artifact($artifactfile);

    if (array_key_exists($node,$artifact))
        $stateinfo=$artifact[$node];
    else throw new ErrorException("Target node $node does not exist in current artifact");

    debug("node data: ".HTMLpre(json_encode($stateinfo)));

    // checking if we have reached a final node
    if (empty($artifact[$node])||array_keys($artifact[$node])==array("nodetip")){
        debug("Reaching final state $node");
        $conn->query("update Artifact set state='finished' where ID=$artifactid");
        return false;
    }

    //Empty variable to hold guard value if found
    $guard = '';
    $actions = '';

    foreach ($stateinfo as $dest=>$v) {
        debug("testing transition: $dest");
        if($dest=="nodetip")
            debug("a nodetip");
        else {
            debug("checking transition $node -> $dest");
            debug("node data: ".json_encode($stateinfo[$dest]));

            if( isset($artifact[$node][$dest]["guard"]) ){
                $guard=$artifact[$node][$dest]["guard"];
                $guard=sugarFree($guard,$conn);
            }
            else throw new Exception("No guard in artifact");

            if($guard=="none")
                $guard="select true from dual";

            if (preg_match("/^task ([0-9]+) is answered/",$guard,$matches)){
                $guard="select true from Answered where artifact=CURRENT_ARTIFACT and id=".$matches[1];
            }
            $guard=str_replace('SESSION_USER', $_SESSION['id'], $guard);
            $guard=str_replace("CURRENT_ARTIFACT",$artifactid, $guard);
            $actions=str_replace('SESSION_USER', $_SESSION['id'], $artifact[$node][$dest]["actions"]);

            if( isset($artifact[$node][$dest]["actions"]) ){
                $actions=str_replace('SESSION_USER', $_SESSION['id'], $artifact[$node][$dest]["actions"]);
            }
            else
                throw new Exception("No action in artifact");

            if (isset($_SESSION['skills'])) {
                $actions=str_replace('SKILLS', $_SESSION['skills'], $actions);
                // TODO but actions is an array?
                // TODO what for?
            }

            $activated=step($conn, $artifactid, $node, $dest, $guard, $actions);
            debug("result:".$activated);
            if ($activated)
                return true;
        }
    }
    debug("no transition from $node");
    return false; // no guard is validated
}


/**
 * Return the json array of an artifact, stored in the artifactclasses directory
 * @param name The filename of the artifact in the artifactclasses directory
 *
 */

function load_artifact($name){
    return json_decode(file_get_contents("projects/$name"),true);
}

/**
 * Return the json array of an artifact, stored in the artifactclasses directory
 * @param The artifact's id
 *
 */

function loadArtifactFromId($artifactid,$conn){
    $stmt_artifact = $conn->query("select definition from Artifact, ArtifactClass where Artifact.id=$artifactid and Artifact.classid=ArtifactClass.id")->fetch();
    $artifact_file = $stmt_artifact['definition'];
    return load_artifact($artifact_file);
}

function DEPRECATEDupdate($conn, $artifact)
{
    $count = 0;
    $activated = true;
    while ($count < count($artifact)+1 && $activated) {
        $activated=updateonce($conn, $artifact, $artifactid, $count);

        $count += 1;
        $query = "select definition from ArtifactClass where id=(select classid from Artifact where id=(select id from CurrentArtifact where user=".$_SESSION['id']." and name = '".$_SESSION['tablename']."'))";
        $stmt=$conn->query($query);
        $artifactfile=$stmt->fetch()['definition'];
        $artifact=load_artifact($artifactfile);
    }
}


/*
 * update 
 * 
 * Given a valid connection and artifact id, update the given artifact up to saturation
 * @param $conn: a valid PDO connection
 * @param $artifactid: a valid artifact id 
 * New update function, under dev
 * We should update up to saturation all running artifact of the current user
 * for each running artifact of user USERID
 *  update until no guard is activated or we reach the number of states+1
 * */
function update($conn, $artifactid)
{
    $nbNode = 0;
    $activatedGuard = true;
   
    if (isset($artifactid)){
        
        $query = "select definition from ArtifactClass,Artifact where Artifact.id=$artifactid and ArtifactClass.id=Artifact.classid";
        $stmt=$conn->query($query);
        $artifactfile=$stmt->fetch()["definition"];
        $artifact=load_artifact($artifactfile);
        
//        while ($nbNode < count($artifact)+1 && $activatedGuard) {
          while ($activatedGuard) {
            $activatedGuard=updateonce($conn, $artifact, $artifactid, $nbNode);
            $nbNode += 1;
        }
    }

}

/**
 * step
 *
 * Given a db connection, an artifact id, a starting node, a destination node, a guard and a list of actions,
 * check the guard. If true, update the artifact to the destination node and execute the list of actions. 
 *
 * @param  $conn : a valid database PDO connection
 * @param  $from : a starting node number in the artifact
 * @param  $to : a destination node number in the artifact
 * @param  $guard : a guard for the transition
 * @param  $actions : a list of actions to perform if the guard is true
 * @return bool : the value of the guard
 */
function step($conn, $artifactid, $from, $to, $guard, $actions)
{
    $conn->beginTransaction();

    // checking guards
    $guardvalue=false;
    debug("test guard ".$guard);
    $res=$conn->query($guard);
    if ($res->rowCount()==0) {
        $guardValue=false;
    } elseif ($res->rowCount()>1) {
        debug("Warning, guard with several values, taking last");
        foreach ($res as $rows) {
            $guardvalue=$rows[0];
        }
    } else {
        foreach ($res as $rows) {
            $guardvalue=$rows[0];
        }
    }


    // if enabled, change state then run actions
    if ($guardvalue) {
        $conn->query("update Artifact set node=$to where ID=$artifactid");
        foreach ($actions as $action) {
            debug("executing $action");
            $action=sugarFree($action,$conn);
            $action=str_replace("CURRENT_ARTIFACT",$artifactid, $action);
            $action=str_replace("SESSION_USER",$_SESSION['id'], $action);

            if (preg_match("/^call wirk image annotator as task ([0-9]+) for '(.*)'/",$action,$matches)){
                $taskid=$matches[1];
                $imageUrlQuery=$matches[2];
                $imageUrls=$conn->query($imageUrlQuery);
                $imageUrl=$imageUrls->fetch()[0];

                callWirkImageAnnotator($imageUrl,$artifactid,$taskid,$_SESSION['id'],$conn);
                $action="";
            }

            if (preg_match("/^check wirk answer/",$action,$matches)){
                checkWirkImageAnnotator($conn);
                $action="";
            }

            if($action=="PIGNISTIC"){
                    pignistic($conn,$artifactid);
                    $action="";
            }

            if($action=="jsonToCoolInTaskDecomposition"){
                    jsonToCoolInTaskDecomposition($conn,$artifactid);
                    $action="";
            }

            if($action=="jsonToCoolInTask"){
                    jsonToCoolInTask($conn,$artifactid);
                    $action="";
            }

            if ($action)
                $conn->query($action);
        }

    }
    $conn->commit();
    return $guardvalue;
}

/**
 * startArtifact
 *
 * Given an artifact id and a user id, assign this artifact to the user and start it.
 *
 * @param $conn: a valid PDO connection
 * @param  $artifactid: a valid artifact id
 * @param  $userid: a valid user id
 */

function startArtifact($conn,$artifactid,$userid){
    $conn->query("update Artifact set ownerid=$userid where id=$artifactid");
    update($conn, $artifactid);
    
}

//Artifact.classid,concat('index.php?mode=startArtifact&artifactid=',min(Artifact.id)),'details' from Artifact,ArtifactClass where project='" . $_SESSION['project'] . "' and Artifact.classid=ArtifactClass.id and Artifact.ownerid=0
/**
 * startAllArtifact
 *
 * Given a user id, assign all available artifact to the user and start them.
 *
 * @param $conn: a valid PDO connection
 * @param  $userid: a valid user id
 */

function startAllArtifact($conn,$userid){
    $table=$conn->query("select id from ArtifactClass where autostart and id not in (select classid from Artifact where ownerid=$userid and state='running')");
    if($table->rowCount()>0){
        debug("ArtifactClass to instanciate available");
        while($tuple=$table->fetch()){
            $artifactclassid=$tuple['id'];
            debug("Instanciating a $artifactclassid");
            $conn->query("insert into Artifact(ownerid,classid,state,node,attributes) values ($userid,$artifactclassid,'running',1,1)");
            $artifactid=$conn->LastInsertId();
            update($conn,$artifactid);
        }
    }
}

/**
 * pignistic
 *
 * Given a valid PDO connection, computes the pignistic decision on values of table Answer for the given artifact. 
 * The decision is made on all available answers. The result appears as a new answer made by the admin user, with a task id being
 * the last task id plus one.  
 * @param $conn: a valid PDO connection
 * @param $artifactid: a valid artifact id
 */
function pignistic($conn,$artifactid){
    $table=$conn->query("select value from Answer where Artifact=$artifactid group by value order by sum(mass) desc limit 1");
    $choice=$table->fetch()['value'];
    $conn->query("insert into Answer(idtask,user,Artifact,value,mass) select MAX(idtask)+1,1,$artifactid,$choice,1 from Answer where Artifact=$artifactid");
    
}

/**
 * jsonToCoolInTask
 *
 * Inserts as many tuples into the CoolInTask table as available in a given JSON array.
 * @param $conn: a valid PDO connection
 * @param $artifactid: a valid artifact id
 */
function jsonToCoolInTask($conn,$artifactid){
    $table=$conn->query("select idtask,user,artifact, cast(replace(value,'\\\\','') as JSON) as json FROM Answer WHERE idtask=2 and artifact=$artifactid");
    while($tuple=$table->fetch()){
        $idtask=$tuple['idtask'];
        $user=$tuple['user'];
        //TODO artifact in SQL not useful

        @$json=json_decode($tuple['json']);

        foreach($json as $todo){
            $description=$todo->text;
            $idtodo=1;//$todo->id;
            $stress=$todo->stress;
            $duration=$todo->duration;
            $stressLimit=$todo->stress_limit;
            $descriptionEn=$todo->text_en;
            $query="insert into CoolInTask(idtask,user,artifact,description,duration,stress,stresslimit) values ($idtask, $user, $artifactid,'$description',$duration,$stress,$stressLimit)";
            debug("95 $query");
            $conn->query($query);
        }
    //JSON_EXTRACT(json,'$[0][1]') as description,if(CAST(JSON_EXTRACT(json,'$[0][2]') as CHAR)='null',null,null) as parentid,JSON_EXTRACT(json,'$[0][3]') as position from (select idtask,user,artifact, cast(replace(value,'\\\\','') as JSON) as json FROM Answer WHERE idtask=5) as cleaned");

    }
   // TODO In progress
}


/**
 * jsonToCoolInTaskDecomposition
 *
 * Inserts as many tuples into the CoolInTaskDecomposition table as available in a given JSON array.
 * @param $conn: a valid PDO connection
 * @param $artifactid: a valid artifact id
 */
function jsonToCoolInTaskDecomposition($conn,$artifactid){
    $table=$conn->query("select idtask,user,artifact, cast(replace(value,'\\\\','') as JSON) as json FROM Answer WHERE idtask=5 and artifact=$artifactid");
    while($tuple=$table->fetch()){
        $idtask=$tuple['idtask'];
        $user=$tuple['user'];
        //TODO artifact in SQL not useful
        $json=json_decode($tuple['json']);
        foreach($json as $todo){
            $idtodo=$todo[0];
            $description=$todo[1];
            $parentid=is_null($todo[2])?"null":$todo[2];
            $position=$todo[3];
            $query="insert into CoolInTaskDecomposition(idtask,user,artifact,idtodo,description,parentid,position) values ($idtask, $user, $artifactid,$idtodo,'$description',$parentid,$position)";
            debug("5495 $query");
            $conn->query($query);
        }
    //JSON_EXTRACT(json,'$[0][1]') as description,if(CAST(JSON_EXTRACT(json,'$[0][2]') as CHAR)='null',null,null) as parentid,JSON_EXTRACT(json,'$[0][3]') as position from (select idtask,user,artifact, cast(replace(value,'\\\\','') as JSON) as json FROM Answer WHERE idtask=5) as cleaned");

    }
   // TODO In progress
}

/**
 * Add a new artifact class to the MyOwnProject projectegory is 'task')
 * @param string $content
 */
function addArtifact($conn,$json){
    global $VIEW;
    $VIEW['MAIN'].="Did it<pre>$json</pre>";

    $file=fopen("projects/MyOwnProject/myownproject.sca", "w");
    fwrite($file,$json);
    fclose($file);
    $query="insert into ArtifactClass(id,description,definition,project,autostart) values (10000,'MyOwnProject','MyOwnProject/myownproject.sca', 'MyOwnProject',true)";
    $conn->query($query);
}



/**
 * nextTaskFromArtifact
 *
 * retrieve the next task available for the given artifact and user (i.e. task with the smallest id)
 *
 * @param $conn:        A valid database PDO connection
 * @param $user:        A user id
 * @param $artifact:    A valid artifact id
 */
function nextTaskFromArtifact($conn,$user,$artifact){
    $stmt = $conn->prepare("SELECT t.id
                                            FROM Task t, Profile p, Skills s
                                            WHERE s.iduser = :user
                                            AND s.idskill = p.idskill
                                            AND t.id = p.idtask
                                            AND t.artifact = :artifact
                                            ORDER BY t.id asc LIMIT 1");
    $stmt->bindParam(':user', $user, PDO::PARAM_INT);
    $stmt->bindParam(':artifact', $artifact, PDO::PARAM_INT);
    $res=$stmt->execute();
    $nextTaskId = $stmt->fetchColumn();
    return $nextTaskId;
}

// TODO document bots
// TODO document presentation.html


?>
