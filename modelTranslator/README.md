# Translator for the Crowdflow model

The goal of the translator is to express a crowdsourcing application in the *Crowdflow* model 
and construct the corresponding application plugged on a given platform or database.
For now the translator can be plugged on the *Headwork* platform, but the goal is to be generic and be able to plug an application on different platform. 

## Executing the demo
A example of an application is described in the `demo.BuildDemo` class. 
When running this class, it creates the Json files needed to run the application on the Headwork platform in the `artifactclasses` directory and in the `init-workflow.sql` file.
If you don't want to modify the demo, simply open the headwork home in your browser, the application should work.

## Executing an other application
If you want to try out other applications, you can construct your application in Java using the method from the `Application` class.
You can look at `demo.BuildDemo` for an example. 

For the moment, you can only use the `HeadworkPlatformConnector`, which construct Json and Sql files needed to connect to the Headwork platform.
The `HeadworkDatabaseConnector` is not yet operational, but should when it is finished create a Java application that connects directly with Headwork database, without using the platform.
