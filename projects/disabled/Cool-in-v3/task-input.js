
// nb of todos to collect (up to 10)
nbTask=2;

populate_form(nbTask)

function populate_form(tasks) {
    let trs = document.forms[0].getElementsByTagName('table')[1].getElementsByTagName('tr')
    trs[1].value="bob1";
    if (tasks - trs.length > -1) {
        for (let i = trs.length; i <= tasks; ++i) {
            let tr = trs[1].cloneNode(deep=true)
            let inputs = tr.querySelectorAll('*[name], *[id]')

            const regex = /-\d+$/
            for (let j = 0; j < inputs.length; ++j) {
                let input = inputs[j]
                let suffix = "-" + i

                if (input.hasAttribute('id'))
                    input.id = input.id.replace(regex, suffix)

                if (input.hasAttribute('name'))
                    input.name = input.name.replace(regex, suffix)
                input.value="bob"+suffix
            }

            trs[i - 1].after(tr)
        }
    }
    else if (tasks > 0) {
        for (let i = trs.length - 1; i > tasks; --i) {
            trs[i].remove()
        }
    }
}

document.forms[0].addEventListener('submit', function(event) {
    const df = new FormData(event.target);
    let dict = {}
    for (let [name, value] of df.entries()) {
        if(name.match(/-task-/)){
            prefix = name.split(/-task-\d*$/)[0]
            task = 'task-' + parseInt(name.match(/\d*$/))

            if (dict[task] == undefined) {
                dict[task] = {}
            }
            // on remplace les quotes par des espaces
            dict[task][prefix] = value.replace(/["']/g, " ");
        }
    }
    json = JSON.stringify(dict, null, '\t');
    document.querySelector("#answer").value = json;
    return false
})
