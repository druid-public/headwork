<?php

/**
 * logout
 *
 * Close and unset the session, redirection to the log in page
 *
 * @param  $conn : A valid database PDO connection
 */
function logout($conn)
{
    unset($_SESSION);
    session_destroy();
    // Suppressing automatic connection cookies
    setcookie('login', '');
    setcookie('pass_hache', '');
    (new LoginForm($conn))->render();
}
?>