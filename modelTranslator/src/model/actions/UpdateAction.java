package model.actions;

import model.Parameters;
import model.SqlCommands;

/**
 * An update modifies one or several attributes of the artifact on which it is performed.
 */
public class UpdateAction extends SimpleAction {
    private String attribute;
    private String javatype;
    private String artifact;


    public UpdateAction(String guard, Parameters<Object> parameters) {
        super(guard, parameters);
        String sqltype = (String) parameters.get("type");
        if (sqltype.startsWith("int")) javatype = "Integer";
        else javatype = "String";
        this.attribute = (String) parameters.get("attribute");
        this.artifact = (String) parameters.get("artifact");
        super.info.addParam("action",this.writeAction());
    }

    public String writeMethod() {
        return  "    public void update"+attribute+"("+ javatype+" att){\n" +
                "        this."+attribute+" = att;\n" +
                "        //update database\n" +
                "        connection.sendToDb(\"insert into \"+ this.getClass().getSimpleName()+\"("+ attribute +") values (\" + att +\")\");\n" +
                "    }\n";
    }

    /**
     * The actions are :
     *    - update the attribute in the table of the artifact's class
     * @return
     */
    public String writeAction() {
        String selectionAnswer = "select id from task where title='" + this.info.get("title")+"' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)";
        String selectionAnswerValue = "select value from answer where idtask=(" + selectionAnswer + ")";

        String changeAttribute = SqlCommands.update(artifact + "Artifact", attribute,
                "(" + selectionAnswerValue + ")",
                "id=(select ATTRIBUTES from ARTIFACT " +
                            "where ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER))");

        return changeAttribute;
    }
}
