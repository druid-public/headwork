package model.connections;

import model.Parameters;

import java.sql.ResultSet;
import java.util.List;

public interface DBConnector {
    void sendToDb(String request);
    ResultSet getFromDb(String request);
    List<Parameters> getPeers(String request);
}
