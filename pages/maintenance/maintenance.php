<?php

function maintenance()
{
    global $VIEW;

    unset($_SESSION);
    session_destroy();

    $_SESSION['maintenance'] = "maintenance";
    $VIEW['MAIN'] = file_get_contents("templates/maintenance.html");
    debug("maintenance page");
}

?>


