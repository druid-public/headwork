package model.actions;

import model.Counter;
import model.Parameters;

/**
 * An Action is the representation of a task, an action of an artifact class.
 * It can be an update of the artifact's data, the creation of a new artifact, a movement of the artifact (from one peer to an other) or a delegation.
 *
 * An action has a guard and a body. The SQL requests composing the body are executed when the guard is satisfied.
 */
public abstract class Action {

    protected String guard;
    //protected String body;
    protected Parameters<Object> info;
    private int id = this.hashCode();//Counter.nextValue("task");

    /**
     * For an always-accepting guard, give "select true from dual".
     * It does't matter wether the guard end with a semicolon ';' or not.
     *
     * The info should always contain information about the task to be performed by the action such as :
     *   - "title" the name of the action
     *   - "description" a description of the action for a human user
     *   - "body" the sql request corresponding to the action
     *   - "checker" begins with "REGEXP ", the regular expression checking the format of an answer to the task
     *   - "checkermsg" a human readable description of the format of an answer to the task
     *
     * @param guard {String} a sql request returning 1 if the action should be executed else 0.
     * @param info {Parameter} the parameters of the action and information about the action
     */
    public Action(String guard, Parameters<Object> info){//, String body
        //this.body = body;
        this.guard = guard;
        this.info = info;
    }

    public int getId() {
        return id;
    }


    //public boolean isGuardSatisfied() {return True;}

    //public String getBody() {return body;}
    public String getGuard() {return guard;}
    public Parameters<Object> getInfo(){return info;}

    /**
     * Return the character string of the java method to perform the action (used to generate the class artifact java file)
     * @return
     */
    public abstract String writeMethod();

    /**
     * Return the character string of the SQL requests needed to insert the task corresponding to the action in the 'task' table.
     * @return
     */
    public String writeTask() {
        StringBuilder task = new StringBuilder("insert into task(title,description,body,checker,checkermsg, artifact) values(");
        for (String param : new String[]{"title","description","body","checker","checkermsg"}){
            if (this.getInfo().get(param) != null)
                task.append("'").append(this.getInfo().get(param)).append("',");
            else
                task.append("null,");
        }
        task.append("(select ID from CURRENTARTIFACT where user=SESSION_USER))");
        return task.toString();
    }

    /**
     * Return the character string of the SQL requests needed to perform the action.
     * If several requests are needed, they are separated by ';'
     * @return
     */
    public abstract String writeAction();

    public abstract String writeFinishTask();
}
