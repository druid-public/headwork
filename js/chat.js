/**
 * @author Adrien W.
 * WARNING : As the HEADWORK's architecture is not final, code may need a change in the future.
 * The line I expect to change is the line n°12, as I'm currently using 2 hidden inputs instead of 1 as it exists on another page
 */

// Setting up the url here so it will be easier to edit when HEADWORK will be restructured
var ajaxUrl = "ChatController.php?action=";

// We only add the chat 'base' on load to avoid scripts not loaded
$(document).ready(function() {
    initializeChat();
});

function initializeChat() {
    var dataToSend = ($(':input[name="mode"]').length != 0 && $(':input[name="mode"]').val().startsWith("insertAnswer") ? {task_id: $(':input[name="id"]').val()} : {});
    $.ajax({
        type: "POST",
        url: ajaxUrl + "getTopic",
        data: dataToSend,
        success: function(data) {
            $('#content').append(data);
            bindEvents();
            manageFilling();
        }
    });
}

// We force the refresh of the posts every n milliseconds
var n = 5000;
setInterval(manageFilling, n);

// Binding the events, must be in a function as DOM elements don't exist on page load
function bindEvents() {
    $('#chatBtn').click(function() {
        if ($(this).attr('data-chat-display') == "false") {
            $('#chatDiv').animate({left: "0"});
            $(this).attr('data-chat-display', true);
        } else {
            $('#chatDiv').animate({left: "-=102%"});
            $(this).attr('data-chat-display', false);
        }
    });
    $('#chatSendTaskPost').click(function() {
        sendPost('Task');
    });
    $('#chatSendProjectPost').click(function() {
        sendPost('Project');
    });
}

// Send the post content to add it to the topic
function sendPost(type) {
    var text = $('#chat' + type + 'Textarea').val();
    if (text.length > 0) {
        $('#chat' + type + 'Textarea').attr('disabled', true);
        $(this).attr('disabled', true);
        $.ajax({
            type: "POST",
            url: ajaxUrl + "addPostToTopic",
            data: {
                topic_id: $('#chat' + type + 'Id').val(),
                post_content: text
            },
            success: function(data) {
                jsonData = JSON.parse(data);
                if (jsonData['success']) {
                    $('#chatConfirmation').fadeIn().delay(2000).fadeOut();
                    manageFilling();
                } else {
                    $('#chatFailure').fadeIn().delay(2000).fadeOut();
                }
                $('#chat' + type + 'Textarea').val("");
                $('#chat' + type + 'Textarea').attr('disabled', false);
                $('#chatSend' + type + 'Post').attr('disabled', false);
            }
        })
        
    }
}


/**
 * Adds new posts to the pre-generated list(s)
 */
function manageFilling() {
    fillChatPosts('Project');
    if ($('#chatTaskId').length != 0) {
        fillChatPosts('Task');
    }
}

/**
 * Fill the list with posts
 * @param {String} type The type (either 'Project' or 'Task') of list to fill
 */
function fillChatPosts(type) {
    $.ajax({
        type: "POST",
        url: ajaxUrl + "getPostsFromTopic",
        data: {topic_id: $('#chat' + type + 'Id').val()},
        success: function(data) {
            jsonData=[];
            try {
                jsonData = JSON.parse(data);
            }
            catch (e){
            }
            var i = 1;
            jsonData.forEach(element => {
                if (i > $('#chat' + type + 'PostsNumber').val()) {
                    let date = moment(new Date(element['date']));
                    $('#chat' + type + 'PostList').append(""
                        + "<li class='list-group-item p-2'>"
                        + "<div>"
                        + "<strong>" + element['name'] + "</strong>"
                        + "<small class='float-right chatDate'>" + date.format('HH:mm') + " on " + date.format('DD/MM/YYYY') + "</small>"
                        + "</div>"
                        + "<div class='text-justify'>" + element['content'] + "</div>"
                        + "</li>"
                    );
                }
                i++;
            });
            $('#chat' + type + 'PostsNumber').val(jsonData.length);
            $('#chatBtn').fadeIn();
        }
    });
}
