DROP TABLE IF EXISTS ReadMyWiki;
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'SoundRecording');
DELETE from ArtifactClass where project = 'SoundRecording';

DROP TABLE IF EXISTS AnswerButton;
CREATE TABLE AnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO AnswerButton values (1,'Mauvaise','danger');
INSERT INTO AnswerButton values (2,'À revoir','warning');
INSERT INTO AnswerButton values (3,'Parfaite','success');


insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (13000,'ReadMyWiki basics','ReadMyWiki/ReadMyWiki.sca', 'Artifact', 'ReadMyWiki',true);