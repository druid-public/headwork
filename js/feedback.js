$('#submitFeedbackBtn').click(function() {
    var currentPage = location.pathname + location.search;
    var feedback = $('#feedbackTextarea').val();
    if (feedback.length > 0) {
        $.ajax({
            type: "POST",
            url: "index.php?mode=submitFeedback",
            data: {page: currentPage, feedbackText: feedback},
            success: function() {
                $('#feedbackModal').modal('hide');
                $('#feedbackTextarea').val('');
                $('#feedbackAlert').fadeIn();
            }
        });
    }
});