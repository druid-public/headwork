# Installing Headwork

* install Apache, Php, Mysql version >= 8.0.23
* copy headwork files into apache htdocs directory
* In Mysql
    * create a "headwork" database
    * create a user "headworkadmin" with all privileges on headwork db with default password "HEAg2r4a3g2"
    * run sql commands in script/init-db-schema.sql with e.g. mysql -u headworkadmin -p headwork < init-db-schema.sql
* Open the headwork home in your browser (typically http://localhost:8888/headwork)
* The demo account is "Demo"/"Demo"
* Click "restart" to restart the workflow


## On Linux (Ubuntu 19.04) :

* install the packages needed:
```
sudo apt update
sudo apt install php
sudo apt install mysql-server
sudo apt install apache2
sudo apt install phpmyadmin
sudo apt install php-mysql
sudo apt install libapache2-mod-php7.2
```
* configure php:
    * In php.ini, uncomment the line `extension=msqli`.
* In MySql (`sudo mysql`) :
    * create a "headwork" database and a user "headworkadmin" with all privileges on headwork db with default password "HEAg2r4a3g2"
    ```
    CREATE DATABASE headwork;
    CREATE USER 'headworkadmin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'HEAg2r4a3g2';
    GRANT ALL ON headwork.* TO 'headworkadmin'@'localhost';
    FLUSH PRIVILEGES;
    ```
    * run sql commands in init-db-schema.sql 
    ```
    mysql -u headworkadmin -p headwork
    mysql> source script/init-db-schema.sql;
    ```
* start apache
```
sudo service apache2 start
```
* copy headwork files in the `/var/www/html` directory (sudo needed)
* Open the headwork home in your browser (http://localhost/headwork)
* The demo account is "Demo"/"Demo"
* Click "restart" to restart the workflow


	
