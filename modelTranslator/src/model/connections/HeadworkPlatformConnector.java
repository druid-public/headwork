package model.connections;

import com.mysql.cj.util.StringUtils;
import model.Parameters;
import model.SqlCommands;
import model.actions.Action;
import model.classConstructors.JsonClassArtifact;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HeadworkPlatformConnector extends HeadworkConnector{

    private DBConnector connector;
    private List<String> toInitWorkflow = new ArrayList<>();

    public HeadworkPlatformConnector() {
        super();
        this.connector = new WriteSql("", this.getConnection());
    }

    public void CreateArtifactClass(String nameArtifact, Parameters<String> attributes, List<Action> actions) {
        new JsonClassArtifact(nameArtifact, actions);
        attributes.addParam("id","int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT");
        toInitWorkflow.addAll(this.getSqlNewClass(nameArtifact, attributes));
    }

    public void instantiate(List<Parameters> BasicPeersArtifact) {
        for (String request : CLEAN_DATABASE)
            connector.sendToDb(request);
        for (String request : toInitWorkflow)
            connector.sendToDb(request);
        try {
            //create basic artifacts
            for (int i = 0; i < BasicPeersArtifact.size(); i++) {
                Parameters artifact = BasicPeersArtifact.get(i);
                String artifactName = (String) artifact.get("artifact");
                //faire la requête bd correspondante (à qui on envoit)
                String request = (String) artifact.get("request");
                ResultSet rs = connector.getFromDb(request);
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();

                //récupérer les résultats
                while (rs.next()) { //iterate on peers
                    int idPeer = rs.getInt("idPeer");
                    List<String> values = new ArrayList<>();
                    List<String> attributes = new ArrayList<>();
                    for (int col = 1; col <= columnCount; col++ ) {//iterate on attributes
                        String attribute = rsmd.getColumnLabel(col);
                        if (!attribute.equals("idPeer")) {
                            String value = rs.getString(col);
                            if (StringUtils.isStrictlyNumeric(value))
                                values.add(value);
                            else
                                values.add("'" + value + "'");
                            attributes.add(attribute);
                        }
                    }
                    List<String> generalAttributes = new ArrayList<>();
                    generalAttributes.add("OWNERID");
                    generalAttributes.add("CLASSID");
                    generalAttributes.add("STATE");
                    generalAttributes.add("NODE");
                    generalAttributes.add("ATTRIBUTES");
                    List<String> generalValues = new ArrayList<>();
                    generalValues.add("'"+idPeer+"'");
                    generalValues.add("'"+artifactName.hashCode()+"'");
                    generalValues.add("'running'");
                    generalValues.add("1");
                    generalValues.add("(select max(id) from "+artifactName+"Artifact)");
                    connector.sendToDb(SqlCommands.insert(artifactName+"Artifact",attributes,values));
                    connector.sendToDb(SqlCommands.insert("ARTIFACT", generalAttributes, generalValues));
                    //add currentArtifact
                    if (i ==  0){
                        connector.sendToDb(SqlCommands.insert("CURRENTARTIFACT", "ID,NAME,USER",
                                idPeer+",'" + artifactName + "Artifact', "+idPeer)); //assuming idPeers are from 1 to n as well as the first artifacts
                    }
                }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public DBConnector getDBConnection() {
        return connector;
    }
}
