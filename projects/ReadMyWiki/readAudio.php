<?php
require_once($_SERVER['DOCUMENT_ROOT']."/headwork/config.php");
require_once("../../lib/HWlib.php");

session_start();
$conn = HWdbconnect();

//we select the blob to show in the modal
if (isset($_GET['FunctionTest'])) {
    $select = $conn->query("select blobValue from upload where id = 37");
}

//the content of the modal
if (isset($select)) {
    foreach ($select as $row) {
        echo "<div class='modal-header'>
            <h5 class='modal-title' id='readAudioModalLabel' style='color:black'>Audio</h5>
            <button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
          </div>
          <div class='modal-body'>
            <audio controls>
                <source src='data:audio/mp3;base64," . base64_encode($row['blobValue']) . "'/>
            </audio>
            <div class='row mb-3'>
                <select class='form-select bs-tooltip-auto' id='rating' name='rating'>
                    <option selected>Select between 1 to 5</option>
                    <option value='1'>1</option>
                    <option value='2'>2</option>
                    <option value='3'>3</option>
                    <option value='4'>4</option>
                    <option value='5'>5</option>
                </select>
            </div>
          </div>
          <div class='modal-footer'>
                <button type='button' class='btn btn-primary' data-bs-dismiss='modal'>Close</button>
                <input id='submitRating' type='button' class='btn btn-primary' value='Send'>
          </div>";
    }
}

if (isset($_GET['insertRating'])) {
    $select = $conn->query("select rating from upload where id = 37");
    foreach ($select as $row) {
        $oldValue = $row['rating'];
        $newValue = ($oldValue + $_POST['rating1']) / 2;
        $update = $conn->query("update upload set rating = '" . $newValue . "' where id = 37");
    }
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#submitRating").click(function() {
            var rating = $("#rating").val();
            var dataString = 'rating1='+ rating;
            if(rating=='') {
                alert("Please Fill All Fields");
            } else {
                $.ajax({
                    type: "POST",
                    url: "projects/ReadMyWiki/readAudio.php?insertRating",
                    data: dataString,
                    cache: false,
                    success: function(){
                        alert("Form Submitted Succesfully");
                    }
                });
            }
            return false;
        });
    });
</script>
<?php
