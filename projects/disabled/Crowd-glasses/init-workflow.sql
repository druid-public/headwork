DROP TABLE IF EXISTS CrowdGlassesMsg;
DROP TABLE IF EXISTS CrowdGlassesGPS;

DELETE from Artifact where classid in (select id from ArtifactClass where project = 'Crowd-glasses');
DELETE from ArtifactClass where project = 'Crowd-glasses';

CREATE TABLE CrowdGlassesMsg(id int auto_increment primary key, user varchar(100),text varchar(300));
CREATE TABLE CrowdGlassesGPS(id datetime default current_timestamp primary key, longitude varchar(100),latitude varchar(100));

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (10000,'Emergency Response','Crowd-glasses/crowd-glasses.sca', null, 'Crowd-glasses',true);

// TODO Automate numbering, limit access to files in project directory
