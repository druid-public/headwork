package model;

import java.util.List;

public class SqlCommands {

    public static String insert(String table, List<String> attributes, List<String> values){
        return "insert into " + table +
                "(" + concatWith(",",attributes) +
                ") values (" + concatWith(",",values) + ");";
    }
    public static String insert(String table, String attribute, String value){
        return "insert into " + table +
                "(" + attribute +
                ") values (" + value + ");";
    }
    public static String insert(String table, List<String> attributes, List<String> values, String condition){
        return "insert into " + table +
                "(" + concatWith(",",attributes) +
                ") values (" + concatWith(",",values) + ")" +
                " where " + condition + ";";
    }
    public static String insert(String table, String attribute, String value, String condition){
        return "insert into " + table +
                "(" + attribute +
                ") values (" + value + ")" +
                " where " + condition + ";";
    }

    public static String update(String table, Parameters<String> values){
        return "update " + table + " set " +
                concatWith(" and ", "=", values)
                + ";";
    }
    public static String update(String table, String attribute, String value){
        return "update " + table + " set " + attribute + "=" + value
                + ";";
    }
    public static String update(String table, Parameters<String> values, String condition){
        return "update " + table + " set " +
                concatWith(" and ", "=", values)
                + " where " + condition + ";";
    }
    public static String update(String table, String attribute, String value, String condition){
        return "update " + table + " set " + attribute + "=" + value
                + " where " + condition + ";";
    }

    public static String create(String table, Parameters<String> attributes){
        return "create table if not exists " +
                table + " (" +
                concatWith(",", " ", attributes) +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    }

    public static String drop(String table){
        return "drop table if exists " + table + ";";
    }

    private static String concatWith(String delimiter, List<String> attributes){
        StringBuilder str = new StringBuilder();
        boolean first = true;
        for (String attribute:attributes){
            if (!first)
                str.append(delimiter);
            first = false;
            str.append(attribute);
        }
        return str.toString();
    }
    private static String concatWith(String outdelimiter, String indelimiter, Parameters<String> attributes){
        StringBuilder str = new StringBuilder();
        boolean first = true;
        for (String attribute:attributes.keySet()){
            if (!first)
                str.append(outdelimiter);
            first = false;
            str.append(attribute).append(indelimiter).append(attributes.get(attribute));
        }
        return str.toString();
    }
}
