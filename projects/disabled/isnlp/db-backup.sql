-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  jeu. 29 août 2019 à 16:36
-- Version du serveur :  8.0.16
-- Version de PHP :  7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `headwork`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `retrieveBestQueries` ()  BEGIN
    DECLARE best int;
    SET best = (SELECT COUNT(*)*0.25 from bdd_query);
    SELECT * FROM bdd_query ORDER BY total_score / voters_nb LIMIT best;
END$$

--
-- Fonctions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `retrieveBestQueriesCOUNT` () RETURNS INT(11) BEGIN
    DECLARE best int;
    SET best = (SELECT COUNT(*)*0.25 from bdd_query);
    RETURN best;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `answer`
--

CREATE TABLE `answer` (
  `idtask` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `artifact` varchar(100) NOT NULL,
  `value` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `help` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `answer`
--

INSERT INTO `answer` (`idtask`, `user`, `artifact`, `value`, `help`) VALUES
(4006, '1', '1', '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `artifact`
--

CREATE TABLE `artifact` (
  `ID` int(11) NOT NULL,
  `CLASSID` varchar(100) NOT NULL,
  `OWNERID` int(11) NOT NULL,
  `NODE` int(11) NOT NULL,
  `STATE` enum('running','waiting','finished') NOT NULL,
  `AWAITED` int(11) DEFAULT NULL,
  `ATTRIBUTES` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `artifact`
--

INSERT INTO `artifact` (`ID`, `CLASSID`, `OWNERID`, `NODE`, `STATE`, `AWAITED`, `ATTRIBUTES`) VALUES
(1, '4000', 1, 7, 'finished', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `artifactclass`
--

CREATE TABLE `artifactclass` (
  `ID` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `DEFINITION` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `artifactclass`
--

INSERT INTO `artifactclass` (`ID`, `DESCRIPTION`, `DEFINITION`) VALUES
('4000', 'IsnlpArtifact', 'isnlp-demo.sca');

-- --------------------------------------------------------

--
-- Structure de la table `availability`
--

CREATE TABLE `availability` (
  `iduser` int(11) NOT NULL,
  `idday` int(11) NOT NULL,
  `period` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bdd_feedback`
--

CREATE TABLE `bdd_feedback` (
  `idf` int(11) NOT NULL,
  `idtask` int(11) DEFAULT NULL,
  `feedback` varchar(1000) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bdd_marks`
--

CREATE TABLE `bdd_marks` (
  `idm` int(11) NOT NULL,
  `value` int(11) DEFAULT NULL,
  `text` varchar(50) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `request_feedback` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bdd_marks`
--

INSERT INTO `bdd_marks` (`idm`, `value`, `text`, `class`, `request_feedback`) VALUES
(1, 0, 'Mauvaise', 'danger', 1),
(2, 1, 'À revoir', 'warning', 0),
(3, 2, 'Parfaite', 'success', 0);

-- --------------------------------------------------------

--
-- Structure de la table `bdd_proposal`
--

CREATE TABLE `bdd_proposal` (
  `idp` int(11) NOT NULL,
  `value` text,
  `help` json DEFAULT NULL,
  `total_approved` int(11) DEFAULT '0',
  `oracle_approved` tinyint(1) DEFAULT '0',
  `idq` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bdd_proposal`
--

INSERT INTO `bdd_proposal` (`idp`, `value`, `help`, `total_approved`, `oracle_approved`, `idq`, `iduser`) VALUES
(1, 'select * from actor where birtdate < 1980', NULL, 0, 0, 4002, 2),
(2, 'select * from movie where release_date = 2010', NULL, 0, 0, 4003, 2),
(3, 'Can be the one (1)', NULL, 1, 0, 4005, 2),
(4, 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 3, 0, 4002, 3),
(5, 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 3, 0, 4003, 3),
(6, 'Can be the one (2)', NULL, 1, 0, 4005, 3),
(7, 'no', NULL, 0, 0, 4002, 1),
(8, 'yrd', NULL, 0, 0, 4003, 1),
(9, 'Lien ci-dessous', '{\"link\": \"https://sql.sh/\", \"page\": \"\"}', 1, 1, 4005, 1);

-- --------------------------------------------------------

--
-- Structure de la table `bdd_query`
--

CREATE TABLE `bdd_query` (
  `idq` int(11) NOT NULL,
  `value` text,
  `total_score` int(11) DEFAULT NULL,
  `voters_nb` int(11) DEFAULT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bdd_query`
--

INSERT INTO `bdd_query` (`idq`, `value`, `total_score`, `voters_nb`, `iduser`) VALUES
(4002, 'Tous les acteurs nés avant 1980', 6, 3, 2),
(4003, 'Tous les films sortis en 2010', 6, 3, 2),
(4004, 'Tous les acteurs ayant joué dans le film \"Mars Attacks!\"', 3, 3, 3),
(4005, 'test \"lier un cours\"', 6, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `bdd_time`
--

CREATE TABLE `bdd_time` (
  `idt` int(11) NOT NULL,
  `endtime` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bdd_time`
--

INSERT INTO `bdd_time` (`idt`, `endtime`) VALUES
(244, 1567085517);

-- --------------------------------------------------------

--
-- Structure de la table `chat_post`
--

CREATE TABLE `chat_post` (
  `id_post` int(11) NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `chat_topic`
--

CREATE TABLE `chat_topic` (
  `id_topic` int(11) NOT NULL,
  `id_task` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chat_topic`
--

INSERT INTO `chat_topic` (`id_topic`, `id_task`) VALUES
(1, NULL),
(2, 4001),
(3, 4002),
(4, 4003),
(5, 4004),
(7, 4005),
(6, 4006),
(8, 4007);

-- --------------------------------------------------------

--
-- Structure de la table `cities`
--

CREATE TABLE `cities` (
  `iduser` int(11) NOT NULL,
  `city` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `currentartifact`
--

CREATE TABLE `currentartifact` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `USER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `currentartifact`
--

INSERT INTO `currentartifact` (`ID`, `NAME`, `USER`) VALUES
(1, 'IsnlpArtifact', 1),
(1, 'IsnlpArtifact', 2),
(1, 'IsnlpArtifact', 3),
(1, 'IsnlpArtifact', 4);

-- --------------------------------------------------------

--
-- Structure de la table `feedback`
--

CREATE TABLE `feedback` (
  `idfeedback` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `page` varchar(128) NOT NULL,
  `text` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `feedback`
--

INSERT INTO `feedback` (`idfeedback`, `iduser`, `page`, `text`) VALUES
(1, 1, '/headwork/index.php?mode=answer&choice=4002', 'a long feedback in order to trigger the popover button instead of the whole text'),
(2, 1, '/headwork/index.php?mode=showFeedback', 'pagination test'),
(3, 1, '/headwork/index.php?mode=showFeedback', 'if the text to display is too long the table doesn\'t look right'),
(4, 1, '/headwork/index.php?mode=Home', 'The CSS must be bad there');

-- --------------------------------------------------------

--
-- Structure de la table `forum_category`
--

CREATE TABLE `forum_category` (
  `id_category` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `forum_category`
--

INSERT INTO `forum_category` (`id_category`, `title`, `description`) VALUES
(1, 'HEADWORK Platform', 'For all topics related to the platform'),
(2, 'Tasks', 'For all topics about specific tasks');

-- --------------------------------------------------------

--
-- Structure de la table `forum_post`
--

CREATE TABLE `forum_post` (
  `id_post` int(11) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `thanks` int(11) NOT NULL DEFAULT '0',
  `id_topic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `forum_post`
--

INSERT INTO `forum_post` (`id_post`, `content`, `user_id`, `thanks`, `id_topic`) VALUES
(13, '<p>test</p>', 1, 1, 1),
(14, '<p style=\"text-align: left;\">oui</p>', 1, 0, 3),
(15, '<blockquote>\r\n<p><strong>Demo</strong>, <small>5th August 2019, 14:05</small> said :</p>\r\n<p style=\"text-align: left;\">oui</p>\r\n</blockquote>\r\n<p style=\"text-align: left;\">Test</p>', 1, 0, 3),
(16, '<blockquote>\r\n<p><strong>Demo</strong>, <small>5th August 2019, 14:05</small> said :</p>\r\n<p style=\"text-align: left;\">oui</p>\r\n</blockquote>\r\n<p style=\"text-align: left;\">Test</p>', 1, 0, 3),
(17, '<p>test redirection</p>', 1, 0, 3),
(18, '<p><code>$t = oui;</code></p>', 1, 0, 3),
(19, '<blockquote>\r\n<p><strong>Demo</strong>, <small>23rd August 2019, 10:38</small> said :</p>\r\n<p><code>$t = oui;</code></p>\r\n</blockquote>\r\n<p><code>test</code></p>', 1, 0, 3),
(20, '<blockquote>\r\n<p><strong>Demo</strong>, <small>23rd August 2019, 10:39</small> said :</p>\r\n<blockquote>\r\n<p><strong>Demo</strong>, <small>23rd August 2019, 10:38</small> said :</p>\r\n<p><code>$t = oui;</code></p>\r\n</blockquote>\r\n<p><code>test</code></p>\r\n</blockquote>\r\n<p>fsdfsdf<code></code></p>', 1, 1, 3),
(21, '<p>one more</p>', 1, 1, 3),
(22, '<p>plus que 3 pour une nouvelle page</p>', 1, 0, 3),
(23, '<p>2</p>', 1, 0, 3),
(24, '<p>new page !</p>', 1, 0, 3),
(25, '<p>svp casse pas</p>', 1, 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `forum_post_thanks`
--

CREATE TABLE `forum_post_thanks` (
  `id_post` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `forum_post_thanks`
--

INSERT INTO `forum_post_thanks` (`id_post`, `id_user`) VALUES
(13, 1),
(20, 1),
(21, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_topic`
--

CREATE TABLE `forum_topic` (
  `id_topic` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) DEFAULT NULL,
  `id_task` int(11) DEFAULT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `forum_topic`
--

INSERT INTO `forum_topic` (`id_topic`, `title`, `content`, `creator_id`, `id_task`, `id_category`) VALUES
(1, 'Topic de test', 'Contenu', 2, NULL, 1),
(2, 'Mon premier sujet !', 'Oui', 1, 4002, 2),
(3, 'Topic creation test', '<p>Using TinyMCE to do&nbsp;<code>HTML formatting</code></p>', 1, NULL, 1),
(4, 'Les corrections', '<p>Suis-je oblig&eacute; de proposer une correction pour chaque requ&ecirc;te ?</p>', 1, 4003, 2),
(5, 'Creation test rediretion', '<p>contenu</p>', 1, 4003, 2);

-- --------------------------------------------------------

--
-- Structure de la table `identificationartifact`
--

CREATE TABLE `identificationartifact` (
  `image` varchar(500) DEFAULT NULL,
  `taxon` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `isnlpartifact`
--

CREATE TABLE `isnlpartifact` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `isnlp_query`
--

CREATE TABLE `isnlp_query` (
  `idquery` int(11) NOT NULL,
  `query` text NOT NULL,
  `correction` text NOT NULL,
  `help` json DEFAULT NULL,
  `idsession` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `isnlp_query`
--

INSERT INTO `isnlp_query` (`idquery`, `query`, `correction`, `help`, `idsession`) VALUES
(1, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 1),
(2, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 1),
(3, 'Requete oui', 'oracle', NULL, 1),
(4, 'Ma requête', 'Can be the one (1)', NULL, 2),
(5, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 2),
(6, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 2),
(7, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 3),
(8, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 3),
(9, 'ma req', 'select * from mareq', NULL, 3),
(10, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 4),
(11, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 4),
(12, 'Yes', 'test', NULL, 4),
(13, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 5),
(14, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 5),
(16, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 6),
(17, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 6),
(18, 'test', 'mine', NULL, 6),
(19, 'test', 'Can be the one (1)', NULL, 1),
(20, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 1),
(21, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 1),
(22, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 1),
(23, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 1),
(24, 'Tous les films réalisés par David GROSS-AMBLARD', 'SELECT * FROM movie WHERE director = &quot;David GROSS-AMBLARD&quot;', NULL, 1),
(25, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 1),
(26, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 1),
(27, 'Tous les films réalisés par la MIAGE', 'SELECT * FROM movie WHERE director = &amp;quot;MIAGE&amp;quot;', NULL, 1),
(28, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 1),
(29, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 1),
(30, 'fsd', 'ezae &quot;miage&quot;', NULL, 1),
(31, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 7),
(32, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 7),
(33, 'requête test', 'lalalal \"miage\"', NULL, 7),
(34, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 8),
(35, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 8),
(36, 'Tous les films réalisés par la MIAGE', 'SELECT * FROM movie WHERE director = \"MIAGE\"', NULL, 8),
(37, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 9),
(38, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 9),
(39, 'tset', 'Can be the one (2)', NULL, 9),
(40, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 10),
(41, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 10),
(42, 'Tous les films réalisés par la MIAGE', 'SELECT * FROM movie WHERE director = \"MIAGE\"', NULL, 10),
(43, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 11),
(44, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 11),
(45, 'test', 'quotes \'simples\'', NULL, 11),
(46, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 12),
(47, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 12),
(48, 'Tous les films réalisés par la MIAGE', 'SELECT * FROM movie WHERE director = \"MIAGE\"', NULL, 12),
(49, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 13),
(50, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 13),
(51, 'test', 'Can be the one (2)', NULL, 13),
(52, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 14),
(53, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 14),
(54, 'test', 'Can be the one (2)', NULL, 14),
(55, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 15),
(56, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 15),
(57, 'Tous les films créés par la MIAGE', 'SELECT * FROM movie WHERE director = \"MIAGE\"', NULL, 15),
(58, 'test', 'oui', NULL, 16),
(59, 'wouhou ajax !', 'fsdf', NULL, 16),
(60, 'yes', 'wxcvwx', NULL, 16),
(61, 'a\r', 'please', NULL, 17),
(62, 'e', 'work', NULL, 17),
(63, 'a\r', 'tes', NULL, 18),
(64, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 19),
(65, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 19),
(66, 'my only query', 'yes', NULL, 19),
(67, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 20),
(68, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 20),
(69, 'my only query', 'c', NULL, 20),
(70, 'please do work', 'Can be the one (1)', NULL, 21),
(71, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 21),
(72, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 21),
(73, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 22),
(74, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 22),
(75, 'abcdefghijklmnopqrstuvwxyz', 'Can be the one (2)', NULL, 22),
(76, 'abc', 'Can be the one (1)', NULL, 23),
(77, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 23),
(78, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 23),
(79, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 24),
(80, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 24),
(81, '&\"', 'p', NULL, 24),
(82, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 25),
(83, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 25),
(84, 'abc', 'c', NULL, 25),
(85, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 26),
(86, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 26),
(87, 'aze', '+5pts (please)', NULL, 26),
(88, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 27),
(89, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 27),
(90, 'A good query', 'please oracle\'s', NULL, 27),
(91, 'test', 'Can be the one (1)', NULL, 28),
(92, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 28),
(93, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 28),
(94, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 29),
(95, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 29),
(96, 'test \"lier un cours\"', 'Réponse justifiée par le cours ci-dessous', NULL, 29),
(97, 'Tous les acteurs nés avant 1980', 'SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"', NULL, 30),
(98, 'Tous les films sortis en 2010', 'SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"', NULL, 30),
(99, 'test \"lier un cours\"', 'Lien ci-dessous', '{\"link\": \"https://sql.sh/\", \"page\": \"\"}', 30);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `matrixskyline`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `matrixskyline` (
`duration` int(11)
,`idtask` int(11)
,`reward` int(11)
,`skill1` varchar(100)
,`skill2` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure de la table `photoartifact`
--

CREATE TABLE `photoartifact` (
  `image` varchar(500) DEFAULT NULL,
  `taxon` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `photoartifact`
--

INSERT INTO `photoartifact` (`image`, `taxon`, `id`) VALUES
('test', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `idp` int(11) NOT NULL,
  `label` int(11) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pictures`
--

INSERT INTO `pictures` (`idp`, `label`, `file`) VALUES
(1, NULL, 'http://indicia.mnhn.fr/indicia/upload/1528364634244.jpg');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `preferences`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `preferences` (
`iduser` int(11)
,`skill` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure de la table `profile`
--

CREATE TABLE `profile` (
  `idtask` int(11) NOT NULL,
  `idskill` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `profile`
--

INSERT INTO `profile` (`idtask`, `idskill`) VALUES
(4007, 1);

-- --------------------------------------------------------

--
-- Structure de la table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `result`
--

INSERT INTO `result` (`id`, `description`, `value`) VALUES
(4002, 'Requête : Tous les acteurs nés avant 1980', 'Correction : SELECT name, firstname FROM actor WHERE birthdate < \"1980-01-01\"'),
(4003, 'Requête : Tous les films sortis en 2010', 'Correction : SELECT title, length, director FROM movie WHERE release_date >= \"2010-01-01\" AND release_date <= \"2010-12-31\"'),
(4005, 'Requête : test \"lier un cours\"', 'Correction : Lien ci-dessous');

-- --------------------------------------------------------

--
-- Structure de la table `skills`
--

CREATE TABLE `skills` (
  `iduser` int(11) NOT NULL,
  `idskill` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `skills`
--

INSERT INTO `skills` (`iduser`, `idskill`, `level`) VALUES
(1, 1, 55),
(1, 1002, 20),
(2, 1, 50),
(2, 1002, 5),
(3, 1, 50),
(3, 1002, 8),
(4, 1002, 1);

-- --------------------------------------------------------

--
-- Structure de la table `skilltree`
--

CREATE TABLE `skilltree` (
  `id` int(11) NOT NULL,
  `parent_skill` int(11) DEFAULT NULL,
  `skill` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `skilltree`
--

INSERT INTO `skilltree` (`id`, `parent_skill`, `skill`) VALUES
(1, NULL, 'Any'),
(2, 1, 'Computing'),
(3, 2, 'Programming'),
(4, 3, 'Imperative'),
(5, 3, 'Functional'),
(6, 3, 'Object-oriented'),
(7, 4, 'PHP'),
(8, 6, 'Java'),
(9, 5, 'Haskell'),
(10, 8, 'J2EE'),
(11, 8, 'Spring'),
(12, 7, 'Pear'),
(13, 4, 'C'),
(14, 4, 'Go'),
(15, 5, 'Scala'),
(16, 6, 'C++'),
(17, 6, 'Python'),
(18, 1, 'Spipoll'),
(19, 18, 'Orthoptères'),
(20, 19, 'Gryllidae'),
(21, 20, 'Oecanthus'),
(22, 21, 'pellucens'),
(23, 22, 'La Volucelle transparente (Volucella pellucens)'),
(24, 22, 'Le Grillon d\'Italie (Oecanthus pellucens)'),
(25, 18, 'Coléoptères'),
(26, 25, 'Anobiidae'),
(27, 26, 'Ptinomorphus'),
(28, 27, 'imperialis'),
(29, 28, 'Le Ptinomorphe impérial (Ptinomorphus imperialis)'),
(30, 26, 'Rhagium'),
(31, 30, 'bifasciatum'),
(32, 31, 'La Rhagie à 2 bandes (Rhagium bifasciatum)'),
(33, 25, 'Buprestidae'),
(34, 33, 'Acmaeodera'),
(35, 34, 'pilosellae'),
(36, 35, 'L\'Acméodère de la Piloselle (Acmaeodera pilosellae)'),
(37, 33, 'Anthaxia'),
(38, 37, 'nitidula'),
(39, 38, 'L\'Anthaxie nitidula (femelle) (Anthaxia nitidula)'),
(40, 33, 'Ptosima'),
(41, 40, 'undecimmaculata'),
(42, 41, 'Le Bupreste à taches jaunes (Ptosima undecimmaculata)'),
(43, 25, 'Byturidae'),
(44, 43, 'Byturus'),
(45, 44, 'ochraceus'),
(46, 45, 'Le Byturide aux gros yeux (Byturus ochraceus)'),
(47, 44, 'tomentosus'),
(48, 47, 'Le Byturide aux petits yeux (Byturus tomentosus)'),
(49, 25, 'Cerambycidae'),
(50, 49, 'Agapanthia'),
(51, 50, 'cardui'),
(52, 51, 'L\'Agapanthie des Chardons (Agapanthia cardui)'),
(53, 51, 'La Belle-Dame (Vanessa cardui)'),
(54, 49, 'Aromia'),
(55, 54, 'moschata'),
(56, 55, 'L\'Aromie musquée (Aromia moschata)'),
(57, 49, 'Callimus'),
(58, 57, 'angulatus'),
(59, 58, 'Le Capricorne Callimus (Callimus angulatus)'),
(60, 49, 'Chlorophorus'),
(61, 60, 'ruficornis'),
(62, 61, 'Le Chlorophore soufré (Chlorophorus ruficornis)'),
(63, 60, 'trifasciatus'),
(64, 63, 'Le Clyte à 3 bandes (Chlorophorus trifasciatus)'),
(65, 60, 'herbstii'),
(66, 65, 'Le Clyte herbstii (Chlorophorus herbstii)'),
(67, 60, 'pilosus'),
(68, 67, 'Le Clyte poilu (Chlorophorus pilosus)'),
(69, 60, 'varius'),
(70, 69, 'Le Clyte varié (Chlorophorus varius)'),
(71, 49, 'Poecilium'),
(72, 71, 'alni'),
(73, 72, 'Le Clyte alni (Poecilium alni)'),
(74, 49, 'Anaglyptus'),
(75, 74, 'mysticus'),
(76, 75, 'Le Clyte mystique (Anaglyptus mysticus)'),
(77, 49, 'Dinoptera'),
(78, 77, 'collaris'),
(79, 78, 'Le Dinoptère (Dinoptera collaris)'),
(80, 49, 'Gaurotes'),
(81, 80, 'virginea'),
(82, 81, 'Le Gaurotes vierge (Gaurotes virginea)'),
(83, 49, 'Alosterna'),
(84, 83, 'tabacicolor'),
(85, 84, 'Le Grammoptère couleur de tabac (Alosterna tabacicolor)'),
(86, 49, 'Nustera'),
(87, 86, 'distigma'),
(88, 87, 'La Lepture à 2 points (Nustera distigma)'),
(89, 49, 'Anoplodera'),
(90, 89, 'sexguttata'),
(91, 90, 'La Lepture à 6 taches (Anoplodera sexguttata)'),
(92, 49, 'Stenurella'),
(93, 92, 'melanura'),
(94, 93, 'La Lepture à suture noire (Stenurella melanura)'),
(95, 49, 'Leptura'),
(96, 95, 'aurulenta'),
(97, 96, 'La Lepture couleur d\'or (à antennes jaunes) (Leptura aurulenta)'),
(98, 96, 'La Lepture couleur d\'or (à antennes noires) (Leptura aurulenta)'),
(99, 49, 'Stictoleptura'),
(100, 99, 'rubra'),
(101, 100, 'La Lepture rouge (femelle) (Stictoleptura rubra)'),
(102, 49, 'Rutpela'),
(103, 102, 'maculata'),
(104, 103, 'La Lepture tachetée (Rutpela maculata)'),
(105, 103, 'La Scolie des jardins (femelle) (Megascolia maculata)'),
(106, 49, 'Lepturobosca'),
(107, 106, 'virens'),
(108, 107, 'La Lepture verdâtre (Lepturobosca virens)'),
(109, 49, 'Oberea'),
(110, 109, 'erythrocephala'),
(111, 110, 'Le Longicorne Oberea (Oberea erythrocephala)'),
(112, 49, 'Cerambyx'),
(113, 112, 'scopolii'),
(114, 113, 'Le Petit Capricorne (Cerambyx scopolii)'),
(115, 49, 'Opsilia'),
(116, 115, 'coerulescens'),
(117, 116, 'La Phytoécie bleuâtre (Opsilia coerulescens)'),
(118, 49, 'Saperda'),
(119, 118, 'scalaris'),
(120, 119, 'Le Saperde à échelons (Saperda scalaris)'),
(121, 49, 'Vadonia'),
(122, 121, 'unipunctata'),
(123, 122, 'La Vadonie à 1 point (Vadonia unipunctata)'),
(124, 49, 'Stenopterus'),
(125, 124, 'sp'),
(126, 125, 'Les Sténoptères noirs (Stenopterus)'),
(127, 25, 'Cetoniidae'),
(128, 127, 'Oxythyrea'),
(129, 128, 'funesta'),
(130, 129, 'Le Drap mortuaire (Oxythyrea funesta)'),
(131, 127, 'Gnorimus'),
(132, 131, 'variabilis'),
(133, 132, 'Le Gnorimus sombre (Gnorimus variabilis)'),
(134, 131, 'nobilis'),
(135, 134, 'Le Gnorimus vert (Gnorimus nobilis)'),
(136, 127, 'Osmoderma'),
(137, 136, 'eremita'),
(138, 137, 'Le Pique-prunes (Osmoderma eremita)'),
(139, 127, 'Valgus'),
(140, 139, 'hemipterus'),
(141, 140, 'La Valgue hémiptère (Valgus hemipterus)'),
(142, 25, 'Chrysomelidae'),
(143, 142, 'Chrysolina'),
(144, 143, 'americana'),
(145, 144, 'La Chrysomèle américaine (Chrysolina americana)'),
(146, 142, 'Cryptocephalus'),
(147, 146, 'vittatus'),
(148, 147, 'Le Cryptocéphale à bandes blanches (Cryptocephalus vittatus)'),
(149, 25, 'Cleridae'),
(150, 149, 'Trichodes'),
(151, 150, 'octopunctatus'),
(152, 151, 'Le Clairon à 8 points (Trichodes octopunctatus)'),
(153, 150, 'alvearius'),
(154, 153, 'Le Clairon des ruches (Trichodes alvearius)'),
(155, 150, 'flavocinctus'),
(156, 155, 'Le Clairon espagnol (Trichodes flavocinctus)'),
(157, 150, 'apiarius'),
(158, 157, 'Le Clairon des abeilles (Trichodes apiarius)'),
(159, 150, 'leucopsideus'),
(160, 159, 'Le Clairon commun (Trichodes leucopsideus)'),
(161, 25, 'Coccinellidae'),
(162, 161, 'Coccinella'),
(163, 162, 'septempunctata'),
(164, 163, 'La Coccinelle à 7 points (Coccinella septempunctata)'),
(165, 25, 'Dascillidae'),
(166, 165, 'Dascillus'),
(167, 166, 'cervinus'),
(168, 167, 'Le Dascillide (Dascillus cervinus)'),
(169, 25, 'Dermestidae'),
(170, 169, 'Attagenus'),
(171, 170, 'punctatus'),
(172, 171, 'Le Dermeste à points blancs (Attagenus punctatus)'),
(173, 25, 'Elateridae'),
(174, 173, 'Calambus'),
(175, 174, 'bipustulatus'),
(176, 175, 'Le Taupin à 2 taches (Calambus bipustulatus)'),
(177, 25, 'Malachiidae'),
(178, 177, 'Anthocomus'),
(179, 178, 'fasciatus'),
(180, 179, 'Le Malachide à bandes rouges (Anthocomus fasciatus)'),
(181, 177, 'Clanoptilus'),
(182, 181, 'abdominalis'),
(183, 182, 'Les Malachides sombres (Clanoptilus)'),
(184, 25, 'Pyrochroidae'),
(185, 184, 'Pyrochroa'),
(186, 185, 'coccinea'),
(187, 186, 'Le Cardinal à tête noire (Pyrochroa coccinea)'),
(188, 185, 'serraticornis'),
(189, 188, 'Le Cardinal à tête rouge (Pyrochroa serraticornis)'),
(190, 25, 'Rutelidae'),
(191, 190, 'Hoplia'),
(192, 191, 'argentea'),
(193, 192, 'L\'Hoplie argentée (Hoplia argentea)'),
(194, 192, 'La Noctuelle argentée (Cucullia argentea)'),
(195, 191, 'coerulea'),
(196, 195, 'L\'Hoplie bleue (Hoplia coerulea)'),
(197, 25, 'Pentatomidae'),
(198, 197, 'Graphosoma'),
(199, 198, 'semipunctatum'),
(200, 199, 'Le Pentatome ponctué (Graphosoma semipunctatum)'),
(201, 198, 'lineatum'),
(202, 201, 'Le Pentatome rayé (Graphosoma lineatum)'),
(203, 197, 'Ancyrosoma'),
(204, 203, 'leucogrammes'),
(205, 204, 'La Punaise Ancyrosoma (Ancyrosoma leucogrammes)'),
(206, 197, 'Eurydema'),
(207, 206, 'oleracea'),
(208, 207, 'La Punaise du Chou (Eurydema oleracea)'),
(209, 18, 'Lépidoptères'),
(210, 209, 'Axiidae'),
(211, 210, 'Axia '),
(212, 211, 'napoleona'),
(213, 212, 'La Timie corse (Axia napoleona)'),
(214, 211, 'margarita'),
(215, 214, 'La Timie perle (Axia margarita)'),
(216, 209, 'Crambidae'),
(217, 216, 'Pleuroptya '),
(218, 217, 'ruralis'),
(219, 218, 'La Pyrale du Houblon (Pleuroptya ruralis)'),
(220, 216, 'Eurrhypara '),
(221, 220, 'hortulata'),
(222, 221, 'La Pyrale de l\'Ortie (Eurrhypara hortulata)'),
(223, 209, 'Geometridae'),
(224, 223, 'Idaea '),
(225, 224, 'aureolaria'),
(226, 225, 'L\'Acidalie double-ceinture (Idaea aureolaria)'),
(227, 224, 'muricata'),
(228, 227, 'La Phalène aurorale (Idaea muricata)'),
(229, 223, 'Angerona '),
(230, 229, 'prunaria'),
(231, 230, 'L\'Angéronie du Prunier (Angerona prunaria)'),
(232, 223, 'Aspitates '),
(233, 232, 'gilvaria'),
(234, 233, 'L\'Aspilate jaunâtre (Aspitates gilvaria)'),
(235, 232, 'ochrearia'),
(236, 235, 'L\'Aspilate ochracée (Aspitates ochrearia)'),
(237, 223, 'Camptogramma '),
(238, 237, 'bilineata'),
(239, 238, 'La Brocatelle d\'or (Camptogramma bilineata)'),
(240, 223, 'Cidaria '),
(241, 240, 'fulvata'),
(242, 241, 'La Cidarie fauve (Cidaria fulvata)'),
(243, 223, 'Opisthograptis '),
(244, 243, 'luteolata'),
(245, 244, 'La Citronnelle rouillée (Opisthograptis luteolata)'),
(246, 223, 'Siona '),
(247, 246, 'lineata'),
(248, 247, 'La Divisée (Siona lineata)'),
(249, 223, 'Baptria '),
(250, 249, 'tibiale'),
(251, 250, 'L\'Eubolie de l\'Actée (Baptria tibiale)'),
(252, 223, 'Chiasmia '),
(253, 252, 'clathrata'),
(254, 253, 'Le Géomètre à barreaux (Chiasmia clathrata)'),
(255, 223, 'Pseudopanthera '),
(256, 255, 'macularia'),
(257, 256, 'La Panthère (Pseudopanthera macularia)'),
(258, 223, 'Gagitodes '),
(259, 258, 'sagittata'),
(260, 259, 'La Périzome du Pigamon (Gagitodes sagittata)'),
(261, 223, 'Ourapteryx '),
(262, 261, 'sambucaria'),
(263, 262, 'La Phalène du Sureau (Ourapteryx sambucaria)'),
(264, 223, 'Odezia '),
(265, 264, 'atrata'),
(266, 265, 'Le Ramoneur (Odezia atrata)'),
(267, 223, 'Psodos '),
(268, 267, 'quadrifaria'),
(269, 268, 'Le Ruban fauve (Psodos quadrifaria)'),
(270, 223, 'Timandra '),
(271, 270, 'comae'),
(272, 271, 'La Timandre aimée (Timandra comae)'),
(273, 223, 'Abraxas '),
(274, 273, 'sylvata'),
(275, 274, 'La Zérène de l\'Orme (Abraxas sylvata)'),
(276, 273, 'grossulariata'),
(277, 276, 'La Zérène du Groseillier (Abraxas grossulariata)'),
(278, 209, 'Hesperiidae'),
(279, 278, 'Pyrgus '),
(280, 279, 'sidae'),
(281, 280, 'L\'Hespérie à bandes jaunes (Pyrgus sidae)'),
(282, 278, 'Carterocephalus '),
(283, 282, 'palaemon'),
(284, 283, 'L\'Hespérie Echiquier (Carterocephalus palaemon)'),
(285, 278, 'Heteropterus '),
(286, 285, 'morpheus'),
(287, 286, 'Le Miroir (Heteropterus morpheus)'),
(288, 278, 'Hesperia '),
(289, 288, 'comma'),
(290, 289, 'La Virgule (Hesperia comma)'),
(291, 209, 'Lycaenidae'),
(292, 291, 'Eumedonia'),
(293, 292, 'eumedon'),
(294, 293, 'L\'Argus de la Sanguinaire (Eumedonia eumedon)'),
(295, 291, 'Lycaena '),
(296, 295, 'tityrus'),
(297, 296, 'L\'Argus myope (mâle) (Lycaena tityrus)'),
(298, 295, 'phlaeas'),
(299, 298, 'Le Cuivré commun (Lycaena phlaeas)'),
(300, 295, 'helle'),
(301, 300, 'Le Cuivré de la Bistorte (mâle) (Lycaena helle)'),
(302, 295, 'virgaureae'),
(303, 302, 'Le Cuivré de la Verge d\'or (femelle) (Lycaena virgaureae)'),
(304, 302, 'Le Cuivré de la Verge d\'or (mâle) (Lycaena virgaureae)'),
(305, 295, 'dispar'),
(306, 305, 'Le Cuivré des marais (mâle) (Lycaena dispar)'),
(307, 295, 'hippothoe'),
(308, 307, 'Le Cuivré écarlate (mâle) (Lycaena hippothoe)'),
(309, 291, 'Glaucopsyche '),
(310, 309, 'melanops'),
(311, 310, 'L\'Azuré de la Badasse (Glaucopsyche melanops)'),
(312, 309, 'alexis'),
(313, 312, 'L\'Azuré des Cytises (Glaucopsyche alexis)'),
(314, 291, 'Agriades'),
(315, 314, 'optilete'),
(316, 315, 'L\'Azuré de la Canneberge (Agriades optilete)'),
(317, 314, 'orbitulus'),
(318, 317, 'L\'Azuré de la Phaque (Agriades orbitulus)'),
(319, 314, 'glandon'),
(320, 319, 'Les Azurés de haute montagne (Alpes) (Agriades glandon)'),
(321, 291, 'Polyommatus '),
(322, 321, 'daphnis'),
(323, 322, 'L\'Azuré de l\'Orobe (Polyommatus daphnis)'),
(324, 291, 'Aricia'),
(325, 324, 'nicias'),
(326, 325, 'L\'Azuré des Géraniums (Aricias nicias)'),
(327, 291, 'Celastrina '),
(328, 327, 'argiolus'),
(329, 328, 'L\'Azuré des Nerpruns (femelle) (Celastrina argiolus)'),
(330, 291, 'Scolitantides '),
(331, 330, 'orion'),
(332, 331, 'L\'Azuré des Orpins (Scolitantides orion)'),
(333, 291, 'Maculinea'),
(334, 333, 'nausithous'),
(335, 334, 'L\'Azuré des Paluds (Maculinea nausithous)'),
(336, 291, 'Pseudophilotes '),
(337, 336, 'baton'),
(338, 337, 'L\'Azuré du Thym (femelle) (Pseudophilotes baton)'),
(339, 337, 'L\'Azuré du Thym (mâle) (Pseudophilotes baton)'),
(340, 291, 'Cupido '),
(341, 340, 'argiades'),
(342, 341, 'L\'Azuré du Trèfle (Cupido argiades)'),
(343, 291, 'Cacyreus '),
(344, 343, 'marshalli'),
(345, 344, 'Le Brun des Pélargoniums (Cacyreus marshalli)'),
(346, 291, 'Tomares '),
(347, 346, 'ballus'),
(348, 347, 'Le Faux-Cuivré smaragdin (Tomares ballus)'),
(349, 291, 'Hamearis '),
(350, 349, 'lucina'),
(351, 350, 'La Lucine (Hamearis lucina)'),
(352, 291, 'Callophrys '),
(353, 352, 'rubi'),
(354, 353, 'Le Thécla de la Ronce (Callophrys rubi)'),
(355, 291, 'Satyrium '),
(356, 355, 'w- album'),
(357, 356, 'Le Thécla de l\'Orme (Satyrium w-album)'),
(358, 355, 'spini'),
(359, 358, 'Le Thécla des Nerpruns (Satyrium spini)'),
(360, 355, 'pruni'),
(361, 360, 'Le Thécla du Prunier (Satyrium pruni)'),
(362, 291, 'Thecla '),
(363, 362, 'betulae'),
(364, 363, 'Le Thécla du Bouleau (Thecla betulae)'),
(365, 291, 'Quercusia'),
(366, 365, 'quercus'),
(367, 366, 'Le Thécla du Chêne (Quercusia quercus)'),
(368, 366, 'Le Sphinx du Chêne vert (Marumba quercus)'),
(369, 291, 'Laeosopis '),
(370, 369, 'roboris'),
(371, 370, 'Le Thécla du Frêne (Laeosopis roboris)'),
(372, 209, 'Noctuidae'),
(373, 372, 'Emmelia '),
(374, 373, 'trabealis'),
(375, 374, 'L\'Arlequinette jaune (Emmelia trabealis)'),
(376, 372, 'Moma '),
(377, 376, 'alpium'),
(378, 377, 'L\'Avrilière (Moma alpium)'),
(379, 372, 'Laspeyria '),
(380, 379, 'flexula'),
(381, 380, 'Le Crochet (Laspeyria flexula)'),
(382, 372, 'Dicycla '),
(383, 382, 'oo'),
(384, 383, 'Le Double zéro (Dicycla oo)'),
(385, 372, 'Noctua '),
(386, 385, 'fimbriata'),
(387, 386, 'La Frangée (Noctua fimbriata)'),
(388, 385, 'pronuba'),
(389, 388, 'Le Hibou (Noctua pronuba)'),
(390, 372, 'Enterpia '),
(391, 390, 'laudeti'),
(392, 391, 'L\'Hadène de Laudet (Enterpia laudeti)'),
(393, 372, 'Euclidia'),
(394, 393, 'miniata'),
(395, 394, 'Le M noir (Euclidia mi)'),
(396, 394, 'La Rosette (Miltochrista miniata)'),
(397, 393, 'glyphica'),
(398, 397, 'La Doublure jaune (Euclidia glyphica)'),
(399, 372, 'Phlogophora '),
(400, 399, 'meticulosa'),
(401, 400, 'La Méticuleuse (Phlogophora meticulosa)'),
(402, 372, 'Nudaria '),
(403, 402, 'mundana'),
(404, 403, 'La Mondaine (Nudaria mundana)'),
(405, 372, 'Cucullia '),
(406, 405, 'argentea'),
(407, 406, 'L\'Hoplie argentée (Hoplia argentea)'),
(408, 406, 'La Noctuelle argentée (Cucullia argentea)'),
(409, 372, 'Deltote '),
(410, 409, 'bankiana'),
(411, 410, 'La Noctuelle argentule (Deltote bankiana)'),
(412, 372, 'Anarta '),
(413, 412, 'myrtilli'),
(414, 413, 'La Noctuelle de la Myrtille (Anarta myrtilli)'),
(415, 372, 'Gortyna '),
(416, 415, 'borelii'),
(417, 416, 'La Noctuelle des Peucédans (Gortyna borelii)'),
(418, 372, 'Hypena '),
(419, 418, 'lividalis'),
(420, 419, 'L\'Hypène livide (Hypena lividalis)'),
(421, 418, 'crassalis'),
(422, 421, 'La Noctuelle épaissie (Hypena crassalis)'),
(423, 372, 'Charanyca '),
(424, 423, 'trigrammica'),
(425, 424, 'La Noctuelle trilignée (Charanyca trigrammica)'),
(426, 372, 'Griposia'),
(427, 426, 'aprilina'),
(428, 427, 'La Runique (Griposia aprilina)'),
(429, 209, 'Erebidae'),
(430, 429, 'Setina '),
(431, 430, 'aurita'),
(432, 431, 'L\'Ecaille alpine (Setina aurita)'),
(433, 429, 'Euplagia '),
(434, 433, 'quadripunctaria'),
(435, 434, 'L\'Ecaille chinée (Euplagia quadripunctaria)'),
(436, 429, 'Utetheisa '),
(437, 436, 'pulchella'),
(438, 437, 'L\'Ecaille du Myosotis (Utetheisa pulchella)'),
(439, 429, 'Callimorpha '),
(440, 439, 'dominula'),
(441, 440, 'L\'Ecaille marbrée (Callimorpha dominula)'),
(442, 429, 'Hypena '),
(443, 442, 'lividalis'),
(444, 443, 'L\'Hypène livide (Hypena lividalis)'),
(445, 442, 'crassalis'),
(446, 445, 'La Noctuelle épaissie (Hypena crassalis)'),
(447, 429, 'Cybosia '),
(448, 447, 'mesomella'),
(449, 448, 'La Lithosie Eborine (Cybosia mesomella)'),
(450, 429, 'Lithosia '),
(451, 450, 'quadra'),
(452, 451, 'La Lithosie quadrille (femelle) (Lithosia quadra)'),
(453, 451, 'La Lithosie quadrille (mâle) (Lithosia quadra)'),
(454, 429, 'Grammodes '),
(455, 454, 'bifasciata'),
(456, 455, 'La Noctuelle de la Salsepareille (Grammodes bifasciata)'),
(457, 429, 'Dysgonia '),
(458, 457, 'algira'),
(459, 458, 'La Passagère (Dysgonia algira)'),
(460, 429, 'Miltochrista '),
(461, 460, 'miniata'),
(462, 461, 'Le M noir (Euclidia mi)'),
(463, 461, 'La Rosette (Miltochrista miniata)'),
(464, 429, 'Drasteria '),
(465, 464, 'cailino'),
(466, 465, 'La Synède de l\'Osier (Drasteria cailino)'),
(467, 429, 'Atolmis '),
(468, 467, 'rubricollis'),
(469, 468, 'La Veuve (Atolmis rubricollis)'),
(470, 209, 'Nymphalidae'),
(471, 470, 'Pyronia '),
(472, 471, 'tithonus'),
(473, 472, 'L\'Amaryllis (Pyronia tithonus)'),
(474, 471, 'bathseba'),
(475, 474, 'L\'Ocelle rubanné (Pyronia bathseba)'),
(476, 470, 'Vanessa '),
(477, 476, 'cardui'),
(478, 477, 'L\'Agapanthie des Chardons (Agapanthia cardui)'),
(479, 477, 'La Belle-Dame (Vanessa cardui)'),
(480, 476, 'atalanta'),
(481, 480, 'Le Vulcain (Vanessa atalanta)'),
(482, 470, 'Argynnis '),
(483, 482, 'pandora'),
(484, 483, 'Le Cardinal (Argynnis pandora)'),
(485, 482, 'aglaja'),
(486, 485, 'Le Grand Nacré (Argynnis aglaja)'),
(487, 482, 'elisa'),
(488, 487, 'Le Nacré tyrrhénien (Argynnis elisa)'),
(489, 482, 'paphia'),
(490, 489, 'Le Tabac d\'Espagne (Argynnis paphia)'),
(491, 470, 'Araschnia '),
(492, 491, 'levana'),
(493, 492, 'La Carte géographique forme orangé (Araschnia levana)'),
(494, 492, 'La Carte géographique forme sombre (Araschnia levana)'),
(495, 470, 'Coenonympha '),
(496, 495, 'arcania'),
(497, 496, 'Le Céphale (Coenonympha arcania)'),
(498, 495, 'dorus'),
(499, 498, 'Le Fadet des garrigues (Coenonympha dorus)'),
(500, 495, 'corinna'),
(501, 500, 'Le Fadet tyrrhénien (Coenonympha corinna)'),
(502, 470, 'Hipparchia '),
(503, 502, 'fidia'),
(504, 503, 'Le Chevron blanc (Hipparchia fidia)'),
(505, 470, 'Euphydryas '),
(506, 505, 'cynthia'),
(507, 506, 'Le Damier de l\'Alchémille (mâle) (Euphydryas cynthia)'),
(508, 505, 'aurinia'),
(509, 508, 'Le Damier de la Succise (Euphydryas aurinia)'),
(510, 505, 'desfontainii'),
(511, 510, 'Le Damier des Knauties (Euphydryas desfontainii)'),
(512, 470, 'Melitaea '),
(513, 512, 'diamina'),
(514, 513, 'Le Damier noir (Melitaea diamina)'),
(515, 512, 'phoebe'),
(516, 515, 'Le Grand Damier (Melitaea phoebe)'),
(517, 512, 'cinxia'),
(518, 517, 'La Mélitée du Plantain (Melitaea cinxia)'),
(519, 512, 'didyma'),
(520, 519, 'La Mélitée orangée (Melitaea didyma)'),
(521, 470, 'Melanargia '),
(522, 521, 'galathea'),
(523, 522, 'Le Demi-Deuil (Melanargia galathea)'),
(524, 521, 'russiae'),
(525, 524, 'L\'Echiquier de Russie (Melanargia russiae)'),
(526, 521, 'lachesis'),
(527, 526, 'L\'Echiquier d\'Ibérie (Melanargia lachesis)'),
(528, 521, 'occitanica'),
(529, 528, 'L\'Echiquier d\'Occitanie (Melanargia occitanica)'),
(530, 470, 'Libythea '),
(531, 530, 'celtis'),
(532, 531, 'L\'Echancré (Libythea celtis)'),
(533, 470, 'Minois '),
(534, 533, 'dryas'),
(535, 534, 'Le Grand Nègre des bois (Minois dryas)'),
(536, 470, 'Nymphalis '),
(537, 536, 'polychloros'),
(538, 537, 'La Grande Tortue (Nymphalis polychloros)'),
(539, 536, 'antiopa'),
(540, 539, 'Le Morio (Nymphalis antiopa)'),
(541, 470, 'Erebia '),
(542, 541, 'manto'),
(543, 542, 'Le Moiré variable (Erebia manto)'),
(544, 470, 'Maniola '),
(545, 544, 'jurtina'),
(546, 545, 'Le Myrtil (Maniola jurtina)'),
(547, 470, 'Aglais'),
(548, 547, 'io'),
(549, 548, 'Le Paon du jour (Aglais io)'),
(550, 470, 'Danaus '),
(551, 550, 'chrysippus'),
(552, 551, 'Le Petit Monarque (Danaus chrysippus)'),
(553, 470, 'Issoria '),
(554, 553, 'lathonia'),
(555, 554, 'Le Petit Nacré (Issoria lathonia)'),
(556, 470, 'Aglais '),
(557, 556, 'urticae'),
(558, 557, 'La Petite Tortue (Aglais urticae)'),
(559, 470, 'Brintesia '),
(560, 559, 'circe'),
(561, 560, 'La Silène (Brintesia circe)'),
(562, 470, 'Pararge '),
(563, 562, 'aegeria'),
(564, 563, 'Le Tircis (Pararge aegeria)'),
(565, 470, 'Aphantopus '),
(566, 565, 'hyperantus'),
(567, 566, 'Le Tristan (Aphantopus hyperantus)'),
(568, 209, 'Papilionidae'),
(569, 568, 'Papilio '),
(570, 569, 'alexanor'),
(571, 570, 'L\'Alexanor (Papilio alexanor)'),
(572, 569, 'machaon'),
(573, 572, 'Le Machaon (Papilio machaon)'),
(574, 569, 'hospiton'),
(575, 574, 'Le Porte-queue de Corse (Papilio hospiton)'),
(576, 568, 'Zerynthia '),
(577, 576, 'polyxena'),
(578, 577, 'La Diane (Zerynthia polyxena)'),
(579, 576, 'rumina'),
(580, 579, 'La Proserpine (Zerynthia rumina)'),
(581, 568, 'Iphiclides '),
(582, 581, 'podalirius'),
(583, 582, 'Le Flambé (Iphiclides podalirius)'),
(584, 568, 'Parnassius '),
(585, 584, 'mnemosyne'),
(586, 585, 'Le Semi-Apollon (Parnassius mnemosyne)'),
(587, 209, 'Pieridae'),
(588, 587, 'Anthocharis '),
(589, 588, 'cardamines'),
(590, 589, 'L\'Aurore (mâle) (Anthocharis cardamines)'),
(591, 588, 'euphenoides'),
(592, 591, 'L\'Aurore de Provence (mâle) (Anthocharis euphenoides)'),
(593, 587, 'Gonepteryx '),
(594, 593, 'cleopatra'),
(595, 594, 'Le Citron de Provence (mâle) (Gonepteryx cleopatra)'),
(596, 587, 'Aporia '),
(597, 596, 'crataegi'),
(598, 597, 'Le Gazé (Aporia crataegi)'),
(599, 209, 'Sphingidae'),
(600, 599, 'Deilephila '),
(601, 600, 'elpenor'),
(602, 601, 'Le Grand Sphinx de la Vigne (Deilephila elpenor)'),
(603, 600, 'porcellus'),
(604, 603, 'Le Petit Sphinx de la Vigne (Deilephila porcellus)'),
(605, 599, 'Macroglossum '),
(606, 605, 'stellatarum'),
(607, 606, 'Le Moro-sphinx (Macroglossum stellatarum)'),
(608, 599, 'Hemaris '),
(609, 608, 'tityus'),
(610, 609, 'Le Sphinx-Bourdon (Hemaris tityus)'),
(611, 608, 'fuciformis'),
(612, 611, 'Le Sphinx gazé (Hemaris fuciformis)'),
(613, 599, 'Hyles '),
(614, 613, 'vespertilio'),
(615, 614, 'Le Sphinx Chauve-Souris (Hyles vespertilio)'),
(616, 613, 'dahlii'),
(617, 616, 'Le Sphinx de Dahl (Hyles dahlii)'),
(618, 613, 'gallii'),
(619, 618, 'Le Sphinx de la Garance (Hyles gallii)'),
(620, 613, 'hippophaes'),
(621, 620, 'Le Sphinx de l\'Argousier (Hyles hippophaes)'),
(622, 613, 'euphorbiae'),
(623, 622, 'Le Sphinx de l\'Euphorbe (Hyles euphorbiae)'),
(624, 613, 'livornica'),
(625, 624, 'Le Sphinx livournien (Hyles livornica)'),
(626, 613, 'nicaea'),
(627, 626, 'Le Sphinx Nicéa (Hyles nicaea)'),
(628, 599, 'Proserpinus '),
(629, 628, 'proserpina'),
(630, 629, 'Le Sphinx de l\'Epilobe (Proserpinus proserpina)'),
(631, 599, 'Smerinthus '),
(632, 631, 'ocellata'),
(633, 632, 'Le Sphinx demi-paon (Smerinthus ocellata)'),
(634, 599, 'Marumba '),
(635, 634, 'quercus'),
(636, 635, 'Le Thécla du Chêne (Quercusia quercus)'),
(637, 635, 'Le Sphinx du Chêne vert (Marumba quercus)'),
(638, 599, 'Daphnis '),
(639, 638, 'nerii'),
(640, 639, 'Le Sphinx du Laurier-rose (Daphnis nerii)'),
(641, 599, 'Agrius '),
(642, 641, 'convolvuli'),
(643, 642, 'Le Sphinx du Liseron (Agrius convolvuli)'),
(644, 599, 'Laothoe '),
(645, 644, 'populi'),
(646, 645, 'Le Sphinx du Peuplier (Laothoe populi)'),
(647, 599, 'Mimas '),
(648, 647, 'tiliae'),
(649, 648, 'Le Sphinx du Tilleul (Mimas tiliae)'),
(650, 599, 'Sphinx '),
(651, 650, 'ligustri'),
(652, 651, 'Le Sphinx du Troène (Sphinx ligustri)'),
(653, 599, 'Hippotion '),
(654, 653, 'celerio'),
(655, 654, 'Le Sphinx Phoenix (Hippotion celerio)'),
(656, 599, 'Thyris '),
(657, 656, 'fenestrella'),
(658, 657, 'Le Sphinx pygmée (Thyris fenestrella)'),
(659, 209, 'Zygaenidae'),
(660, 659, 'Zygaena '),
(661, 660, 'lavandulae'),
(662, 661, 'La Zygène de la Badasse (Zygaena lavandulae)'),
(663, 660, 'rhadamanthus'),
(664, 663, 'La Zygène de l\'Esparcette (Zygaena rhadamanthus)'),
(665, 18, 'Lépidoptère'),
(666, 665, 'Lycaenidae'),
(667, 666, 'Eumedonia'),
(668, 667, 'eumedon'),
(669, 668, 'L\'Argus de la Sanguinaire (Eumedonia eumedon)'),
(670, 666, 'Lycaena '),
(671, 670, 'tityrus'),
(672, 671, 'L\'Argus myope (mâle) (Lycaena tityrus)'),
(673, 670, 'phlaeas'),
(674, 673, 'Le Cuivré commun (Lycaena phlaeas)'),
(675, 670, 'helle'),
(676, 675, 'Le Cuivré de la Bistorte (mâle) (Lycaena helle)'),
(677, 670, 'virgaureae'),
(678, 677, 'Le Cuivré de la Verge d\'or (femelle) (Lycaena virgaureae)'),
(679, 677, 'Le Cuivré de la Verge d\'or (mâle) (Lycaena virgaureae)'),
(680, 670, 'dispar'),
(681, 680, 'Le Cuivré des marais (mâle) (Lycaena dispar)'),
(682, 670, 'hippothoe'),
(683, 682, 'Le Cuivré écarlate (mâle) (Lycaena hippothoe)'),
(684, 666, 'Glaucopsyche '),
(685, 684, 'melanops'),
(686, 685, 'L\'Azuré de la Badasse (Glaucopsyche melanops)'),
(687, 684, 'alexis'),
(688, 687, 'L\'Azuré des Cytises (Glaucopsyche alexis)'),
(689, 666, 'Agriades'),
(690, 689, 'optilete'),
(691, 690, 'L\'Azuré de la Canneberge (Agriades optilete)'),
(692, 689, 'orbitulus'),
(693, 692, 'L\'Azuré de la Phaque (Agriades orbitulus)'),
(694, 689, 'glandon'),
(695, 694, 'Les Azurés de haute montagne (Alpes) (Agriades glandon)'),
(696, 666, 'Polyommatus '),
(697, 696, 'daphnis'),
(698, 697, 'L\'Azuré de l\'Orobe (Polyommatus daphnis)'),
(699, 666, 'Aricia'),
(700, 699, 'nicias'),
(701, 700, 'L\'Azuré des Géraniums (Aricias nicias)'),
(702, 666, 'Celastrina '),
(703, 702, 'argiolus'),
(704, 703, 'L\'Azuré des Nerpruns (femelle) (Celastrina argiolus)'),
(705, 666, 'Scolitantides '),
(706, 705, 'orion'),
(707, 706, 'L\'Azuré des Orpins (Scolitantides orion)'),
(708, 666, 'Maculinea'),
(709, 708, 'nausithous'),
(710, 709, 'L\'Azuré des Paluds (Maculinea nausithous)'),
(711, 666, 'Pseudophilotes '),
(712, 711, 'baton'),
(713, 712, 'L\'Azuré du Thym (femelle) (Pseudophilotes baton)'),
(714, 712, 'L\'Azuré du Thym (mâle) (Pseudophilotes baton)'),
(715, 666, 'Cupido '),
(716, 715, 'argiades'),
(717, 716, 'L\'Azuré du Trèfle (Cupido argiades)'),
(718, 666, 'Cacyreus '),
(719, 718, 'marshalli'),
(720, 719, 'Le Brun des Pélargoniums (Cacyreus marshalli)'),
(721, 666, 'Tomares '),
(722, 721, 'ballus'),
(723, 722, 'Le Faux-Cuivré smaragdin (Tomares ballus)'),
(724, 666, 'Hamearis '),
(725, 724, 'lucina'),
(726, 725, 'La Lucine (Hamearis lucina)'),
(727, 666, 'Callophrys '),
(728, 727, 'rubi'),
(729, 728, 'Le Thécla de la Ronce (Callophrys rubi)'),
(730, 666, 'Satyrium '),
(731, 730, 'w- album'),
(732, 731, 'Le Thécla de l\'Orme (Satyrium w-album)'),
(733, 730, 'spini'),
(734, 733, 'Le Thécla des Nerpruns (Satyrium spini)'),
(735, 730, 'pruni'),
(736, 735, 'Le Thécla du Prunier (Satyrium pruni)'),
(737, 666, 'Thecla '),
(738, 737, 'betulae'),
(739, 738, 'Le Thécla du Bouleau (Thecla betulae)'),
(740, 666, 'Quercusia'),
(741, 740, 'quercus'),
(742, 741, 'Le Thécla du Chêne (Quercusia quercus)'),
(743, 741, 'Le Sphinx du Chêne vert (Marumba quercus)'),
(744, 666, 'Laeosopis '),
(745, 744, 'roboris'),
(746, 745, 'Le Thécla du Frêne (Laeosopis roboris)'),
(747, 18, 'Hémiptères'),
(748, 747, 'Lygaeidae'),
(749, 748, 'Tropidothorax'),
(750, 749, 'leucopterus'),
(751, 750, 'La Lygée familière (Tropidothorax leucopterus)'),
(752, 748, 'Spilostethus'),
(753, 752, 'saxatilis'),
(754, 753, 'La Punaise à damier (Spilostethus saxatilis)'),
(755, 752, 'pandurus'),
(756, 755, 'La Viole rouge (Spilostethus pandurus)'),
(757, 747, 'Nabidae'),
(758, 757, 'Himacerus'),
(759, 758, 'mirmicoides'),
(760, 759, 'Le juvénile d\'Himacerus (Himacerus mirmicoides)'),
(761, 747, 'Pentatomidae'),
(762, 761, 'Graphosoma'),
(763, 762, 'semipunctatum'),
(764, 763, 'Le Pentatome ponctué (Graphosoma semipunctatum)'),
(765, 762, 'lineatum'),
(766, 765, 'Le Pentatome rayé (Graphosoma lineatum)'),
(767, 761, 'Ancyrosoma'),
(768, 767, 'leucogrammes'),
(769, 768, 'La Punaise Ancyrosoma (Ancyrosoma leucogrammes)'),
(770, 761, 'Eurydema'),
(771, 770, 'oleracea'),
(772, 771, 'La Punaise du Chou (Eurydema oleracea)'),
(773, 747, 'Plataspididae'),
(774, 773, 'Coptosoma'),
(775, 774, 'scutellatum'),
(776, 775, 'Le Coptosoma (Coptosoma scutellatum)'),
(777, 747, 'Pyrrhocoridae'),
(778, 777, 'Pyrrhocoris'),
(779, 778, 'apterus'),
(780, 779, 'Le Gendarme (Pyrrhocoris apterus)'),
(781, 747, 'Reduviidae'),
(782, 781, 'Phymata'),
(783, 782, 'crassipes'),
(784, 783, 'La Punaise guitare (Phymata crassipes)'),
(785, 747, 'Rhopalidae'),
(786, 785, 'Chorosoma'),
(787, 786, 'schillingii'),
(788, 787, 'Le Chorosome (Chorosoma schillingii)'),
(789, 785, 'Myrmus'),
(790, 789, 'miriformis'),
(791, 790, 'Le Myrmus (Myrmus miriformis)'),
(792, 785, 'Corizus'),
(793, 792, 'hyoscyami'),
(794, 793, 'La Punaise de la Jusquiame (Corizus hyoscyami)'),
(795, 18, 'Hyménoptères'),
(796, 795, 'Andrenidae'),
(797, 796, 'Andrena'),
(798, 797, 'albopunctata'),
(799, 798, 'L\'Andrène à points blancs (Andrena albopunctata)'),
(800, 797, 'fulva'),
(801, 800, 'L\'Andrène fauve (femelle) (Andrena fulva)'),
(802, 797, 'morio'),
(803, 802, 'L\'Andrène noire (Andrena morio)'),
(804, 802, 'L\'Anthracine morio (Hemipenthes morio)'),
(805, 797, 'marginata'),
(806, 805, 'L\'Andrène marginée (Andrena marginata)'),
(807, 797, 'florea '),
(808, 807, 'L\'Andrène des fleurs (Andrena florea)'),
(809, 795, 'Apidae'),
(810, 809, 'Epeoloides'),
(811, 810, 'coecutiens'),
(812, 811, 'L\'Abeille coucou Epeloides (femelle) (Epeoloides coecutiens)'),
(813, 809, 'Apis'),
(814, 813, 'mellifera'),
(815, 814, 'L\'Abeille mellifère (Apis mellifera)'),
(816, 809, 'Xylocopa'),
(817, 816, 'violacea'),
(818, 817, 'Le Xylocope violet (mâle) (Xylocopa violacea)'),
(819, 809, 'Ceratina'),
(820, 819, 'cucurbitina'),
(821, 820, 'L\'Abeille Ceratina noire (Ceratina cucurbitina)'),
(822, 795, 'Cephidae'),
(823, 822, 'Calameuta'),
(824, 823, 'pygmaea'),
(825, 824, 'Le Céphide à abdomen orange (Calameuta pygmaea)'),
(826, 795, 'Cimbicidae'),
(827, 826, 'Abia'),
(828, 827, 'fasciata'),
(829, 828, 'L\'Abia fasciata (Abia fasciata)'),
(830, 795, 'Megachilidae'),
(831, 830, 'Anthidium'),
(832, 831, 'punctatum'),
(833, 832, 'L\'Anthidie à points (Anthidium punctatum)'),
(834, 795, 'Mutillidae'),
(835, 834, 'Mutilla'),
(836, 835, 'europaea'),
(837, 836, 'La Mutille européenne (mâle) (Mutilla europaea)'),
(838, 795, 'Scoliidae'),
(839, 838, 'Colpa'),
(840, 839, 'sexmaculata'),
(841, 840, 'La Scolie Colpa (femelle) (Colpa sexmaculata)'),
(842, 840, 'La Scolie Colpa (mâle) (Colpa sexmaculata)'),
(843, 839, 'quinquecincta'),
(844, 843, 'La Scolie sans taches jaunes (Colpa quinquecincta)'),
(845, 838, 'Dasyscolia'),
(846, 845, 'ciliata'),
(847, 846, 'La Scolie de l\'Ophrys miroir (Dasyscolia ciliata)'),
(848, 838, 'Megascolia'),
(849, 848, 'maculata'),
(850, 849, 'La Lepture tachetée (Rutpela maculata)'),
(851, 849, 'La Scolie des jardins (femelle) (Megascolia maculata)'),
(852, 795, 'Sphecidae'),
(853, 852, 'Sceliphron'),
(854, 853, 'caementarium'),
(855, 854, 'Le Pélopée à pattes noir et jaune (Sceliphron caementarium)'),
(856, 853, 'curvatum'),
(857, 856, 'Le Pélopée à pattes roussâtres (Sceliphron curvatum)'),
(858, 852, 'Prionyx'),
(859, 858, 'kirbii'),
(860, 859, 'Le Sphégien à abdomen orange, noir et blanc (Prionyx kirbii)'),
(861, 852, 'Isodontia'),
(862, 861, 'mexicana'),
(863, 862, 'Le Sphégien noir à ailes fumées (Isodontia mexicana)'),
(864, 795, 'Tenthredinidae'),
(865, 864, 'Athalia'),
(866, 865, 'rosae'),
(867, 866, 'L\'Athalie des roses (Athalia rosae)'),
(868, 795, 'Tiphiidae'),
(869, 868, 'Tiphia'),
(870, 869, 'femorata'),
(871, 870, 'La Tiphide aux pattes rouges (Tiphia femorata)'),
(872, 795, 'Vespidae'),
(873, 872, 'Delta'),
(874, 873, 'unguiculatum'),
(875, 874, 'L\'Eumène unguiculé (Delta unguiculatum)'),
(876, 872, 'Vespa'),
(877, 876, 'velutina'),
(878, 877, 'Le Frelon asiatique (Vespa velutina)'),
(879, 877, 'L\'Anthracine velutina (Hemipenthes velutina)'),
(880, 876, 'crabro'),
(881, 880, 'Le Frelon européen (Vespa crabro)'),
(882, 872, 'Vespula'),
(883, 882, 'rufa'),
(884, 883, 'La Guêpe rousse (Vespula rufa)'),
(885, 18, 'Aranéides'),
(886, 885, 'Thomisidae'),
(887, 886, 'Synema'),
(888, 887, 'globosum'),
(889, 888, 'L\'Araignée crabe Napoléon (Synema globosum)'),
(890, 886, 'Ebrechtella'),
(891, 890, 'tricuspidata'),
(892, 891, 'L\'Araignée crabe Ebrechtella (Ebrechtella tricuspidata)'),
(893, 886, 'Runcinia'),
(894, 893, 'grammica'),
(895, 894, 'L\'Araignée crabe Runcinia (Runcinia grammica)'),
(896, 18, 'Diptères'),
(897, 896, 'Acroceridae'),
(898, 897, 'Cyrtus'),
(899, 898, 'gibbus'),
(900, 899, 'Le Cyrtus (Cyrtus gibbus)'),
(901, 896, 'Bibionidae'),
(902, 901, 'Bibio'),
(903, 902, 'hortulanus'),
(904, 903, 'Le Bibion précoce (femelle) (Bibio hortulanus)'),
(905, 896, 'Bombyliidae'),
(906, 905, 'Hemipenthes'),
(907, 906, 'maura'),
(908, 907, 'L\'Anthracine maura (Hemipenthes maura)'),
(909, 906, 'morio'),
(910, 909, 'L\'Andrène noire (Andrena morio)'),
(911, 909, 'L\'Anthracine morio (Hemipenthes morio)'),
(912, 906, 'velutina'),
(913, 912, 'Le Frelon asiatique (Vespa velutina)'),
(914, 912, 'L\'Anthracine velutina (Hemipenthes velutina)'),
(915, 905, 'Bombylella'),
(916, 915, 'atra'),
(917, 916, 'Le Bombyle Atra (Bombylella atra)'),
(918, 905, 'Eclimus'),
(919, 918, 'gracilis'),
(920, 919, 'Le Bombyle Eclimus (Eclimus gracilis)'),
(921, 896, 'Calliphoridae'),
(922, 921, 'Stomorhina'),
(923, 922, 'lunata'),
(924, 923, 'La Stomorhina (Stomorhina lunata)'),
(925, 896, 'Stratiomyidae'),
(926, 925, 'Clitellaria'),
(927, 926, 'ephippium'),
(928, 927, 'La Clitellaire (Clitellaria ephippium)'),
(929, 925, 'Stratiomys'),
(930, 929, 'singularior'),
(931, 930, 'Le Stratiome singularior (Stratiomys singularior)'),
(932, 929, 'longicornis'),
(933, 932, 'Le Stratiome terne (Stratiomys longicornis)'),
(934, 896, 'Syrphidae'),
(935, 934, 'Baccha'),
(936, 935, 'elongata'),
(937, 936, 'La Baccha allongée (Baccha elongata)'),
(938, 934, 'Chrysotoxum'),
(939, 938, 'bicinctum'),
(940, 939, 'Le Chrysotoxe à double ceinture (Chrysotoxum bicinctum)'),
(941, 934, 'Myathropa'),
(942, 941, 'florea'),
(943, 942, 'L\'Eristale des fleurs (Myathropa florea)'),
(944, 934, 'Eristalinus'),
(945, 944, 'taeniops'),
(946, 945, 'L\'Eristale taeniops (Eristalinus taeniops)'),
(947, 934, 'Ferdinandea'),
(948, 947, 'cuprea'),
(949, 948, 'La Ferdinandea cuprea (Ferdinandea cuprea)'),
(950, 934, 'Milesia'),
(951, 950, 'semiluctifera'),
(952, 951, 'La Milésie bigarée (Milesia semiluctifera)'),
(953, 950, 'crabroniformis'),
(954, 953, 'La Milésie frelon (Milesia crabroniformis)'),
(955, 934, 'Temnostoma'),
(956, 955, 'bombylans'),
(957, 956, 'La Milésie bourdon (Temnostoma bombylans)'),
(958, 955, 'vespiforme'),
(959, 958, 'La Milésie vespiforme (Temnostoma vespiforme)'),
(960, 934, 'Episyrphus'),
(961, 960, 'balteatus'),
(962, 961, 'Le Syrphe ceinturé (Episyrphus balteatus)'),
(963, 934, 'Leucozona'),
(964, 963, 'glaucia'),
(965, 964, 'Le Syrphe Leucozona (Leucozona glaucia)'),
(966, 934, 'Sphaerophoria'),
(967, 966, 'scripta'),
(968, 967, 'Le Syrphe porte-plume (mâle) (Sphaerophoria scripta)'),
(969, 934, 'Volucella'),
(970, 969, 'inflata'),
(971, 970, 'La Volucelle enflée (Volucella inflata)'),
(972, 969, 'pellucens'),
(973, 972, 'La Volucelle transparente (Volucella pellucens)'),
(974, 972, 'Le Grillon d\'Italie (Oecanthus pellucens)'),
(975, 969, 'inanis'),
(976, 975, 'La Volucelle vide (Volucella inanis)'),
(977, 969, 'zonaria'),
(978, 977, 'La Volucelle zonée (Volucella zonaria)'),
(979, 896, 'Tabanidae'),
(980, 979, 'Haematopota'),
(981, 980, 'pluvialis'),
(982, 981, 'Le Taon des pluies (Haematopota pluvialis)'),
(983, 896, 'Tachinidae'),
(984, 983, 'Tachina'),
(985, 984, 'grossa'),
(986, 985, 'La Tachinaire corpulente (Tachina grossa)'),
(987, 983, 'Trichopoda'),
(988, 987, 'pennipes'),
(989, 988, 'Le Trichopode (Trichopoda pennipes)'),
(1000, 1, 'art'),
(1001, 1000, 'cinema'),
(1002, 2, 'SQL');

-- --------------------------------------------------------

--
-- Structure de la table `skylines`
--

CREATE TABLE `skylines` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `preference` text NOT NULL,
  `idtasks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `spipoll`
--

CREATE TABLE `spipoll` (
  `id` int(11) NOT NULL DEFAULT '0',
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `spipoll`
--

INSERT INTO `spipoll` (`id`, `file`) VALUES
(1, 'http://indicia.mnhn.fr/indicia/upload/1528364634244.jpg'),
(2, 'http://indicia.mnhn.fr/indicia/upload/1528362153105.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `subartifacts`
--

CREATE TABLE `subartifacts` (
  `idChild` int(11) NOT NULL,
  `idParent` int(11) NOT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `artifact` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `body` varchar(10000) NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `timer` bigint(20) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `reward` int(11) DEFAULT NULL,
  `checker` varchar(1000) DEFAULT NULL,
  `checkermsg` varchar(1000) DEFAULT NULL,
  `ajax` tinyint(1) NOT NULL DEFAULT '0',
  `help` tinyint(1) NOT NULL DEFAULT '0',
  `arg1` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `arg2` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `task`
--

INSERT INTO `task` (`id`, `artifact`, `title`, `description`, `type`, `body`, `deadline`, `timer`, `duration`, `reward`, `checker`, `checkermsg`, `ajax`, `help`, `arg1`, `arg2`) VALUES
(4001, 1, 'ISNLP - Création de sujet', 'Proposition de requête (1/4)', 3, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><div class=\"card\"><h5 class=\"card-header\">Fonctionnement du projet</h5><div class=\"card-body\">Ce projet a pour but la création d\'un sujet d\'examen que vous pourrez utiliser afin de tester vos connaissances.<br>Afin de créer ce sujet, vous devrez participer à 4 étapes :<br><ol><li>Proposition de requêtes : l\'outil récupère les propositions de chacun</li><li>Vote des requêtes : tous les participants évaluent toutes les requêtes récupérées lors de la première étape, cette étape permet de connaître les meilleures requêtes</li><li>Écriture des corrections : tous les participants proposent une traduction SQL des meilleures requêtes</li><li>Vote des corrections : l\'étape permet de déterminer la correction adaptée pour chaque requête</li></ol>Une fois ces étapes terminées, vous pouvez vous rendre sur la page <a class=\"text-primary\" href=\"isnlp.php?mode=answers\">de ce lien</a> afin de voir le sujet final ainsi que sa correction.<hr>Cette étape permet donc de récupérer les propositions. Cette étape se terminera lorsque tous les participants auront proposé au moins une requête ou lorsque le temps sera écoulé.</div></div></p><p>On considère le schéma relationnel suivant :<br>MOVIE(<u>id_movie</u>, title, length, release_date, director)<br>ACTOR(<u>id_actor</u>, name, firstname, birthdate, nationality)<br>PLAYED_IN(<u>id_actor#, id_movie#</u>)</p><p>Veuillez donner des requêtes exploitant ce schéma <strong><u>en français</u></strong>.</p><p>Si vous souhaitez écrire plusieurs requêtes, retournez à la ligne entre chacune.</p><div><u>Exemple</u></div><div class=\"card card-body\">Tous les acteurs nés après 1960<br>Tous les films qui durent plus de 2h</div></div>', NULL, 1567085517, NULL, NULL, NULL, 'Saisie libre (multi-lignes possible)', 1, 0, 'Requête 1\nRequête 2\netc…', NULL),
(4002, 1, 'ISNLP - Création de sujet', 'Vote des requêtes (2/4)', 5, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><div class=\"card\"><h5 class=\"card-header\">But de l\'étape</h5><div class=\"card-body\">Cette étape permet de décider quelles requêtes seront utilisées dans le sujet.</div></div></p><p>Évaluez les requêtes ci-dessous en choisissant 1 des 3 options, cela permettra d\'augmenter le score de la requête selon votre choix.</p></div>', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'SELECT idq as \'id\', value, value as \'text\' FROM bdd_query ORDER BY id', 'SELECT *, \'bdd_feedback\' AS \'target_table\' FROM bdd_marks'),
(4003, 1, 'ISNLP - Création de sujet', 'Écriture des corrections (3/4)', 6, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><div class=\"card\"><h5 class=\"card-header\">But de l\'étape</h5><div class=\"card-body\">Cette étape permet de proposer les traductions SQL de chaque requête qui seront ensuite évaluées.</div></div></p><p>On rappelle que le schéma relationnel est le suivant :<br>MOVIE(<u>id_movie</u>, title, length, release_date, director)<br>ACTOR(<u>id_actor</u>, name, firstname, birthdate, nationality)<br>PLAYED_IN(<u>id_actor#, id_movie#</u>)</p><p>Donnez en SQL les requêtes suivantes :</p><div><u>Exemple</u> pour la requête \'Tous les acteurs\'</div><div class=\"card card-body\">SELECT * FROM actor</div></div>', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'SELECT idq as \'id\', value as \'text\' FROM bdd_query WHERE (total_score / voters_nb) > (SELECT AVG(total_score / voters_nb) FROM bdd_query)', NULL),
(4004, 1, 'ISNLP - Création de sujet', 'Vote des corrections (4/4)', 7, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><div class=\"card\"><h5 class=\"card-header\">But de l\'étape</h5><div class=\"card-body\">Cette étape permet de sélectionner la traduction SQL la mieux adaptée pour chaque requête.</div></div></p><p>Choisissez la meilleure requête SQL donnée pour chaque requête :</p></div>', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'SELECT idq as \'id\', value as \'text\' FROM bdd_query WHERE (total_score / voters_nb) > (SELECT AVG(total_score / voters_nb ) FROM bdd_query)', 'SELECT idq as \'id\', value, value as \'text\', help FROM bdd_proposal ORDER by id ASC'),
(4005, 1, 'ISNLP - Création de sujet', 'Décision de l\'oracle', 4, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><p>Veuillez choisir la correction adaptée à la requête parmi la liste : \"test \"lier un cours\"\"</p></div>', NULL, NULL, NULL, NULL, NULL, 'Choix unique', 0, 0, 'SELECT value, value as \'text\' FROM bdd_proposal WHERE idq=4005', NULL),
(4006, 1, 'ISNLP - Création de sujet', 'Attente de l\'oracle', 0, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><div class=\"card\"><h5 class=\"card-header\">Une action de l\'oracle est requise</h5><div class=\"card-body\">Une ou plusieurs requêtes n\'ayant pas de correction approuvée par la majorité, l\'oracle doit choisir la correction adaptée. Cette action peut prendre du temps. Merci de votre compréhension.</div></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL),
(4007, 1, 'ISNLP - Création de sujet', 'Sujet créé', 0, '<div class=\"col-12 text-left\"><h2 class=\"alert alert-info\" role=\"alert\">Mode démo</h2><div class=\"card\"><h5 class=\"card-header\">Création de sujet terminée</h5><div class=\"card-body\">Merci pour votre participation. Le sujet que vous avez créé en collaboration avec d\'autres étudiants est disponible sur <a class=\"text-primary\" href=\"isnlp.php?mode=answers\">cette page</a>.</div></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id` varchar(100) NOT NULL,
  `body` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `template`
--

INSERT INTO `template` (`id`, `body`) VALUES
('spipoll', 'Please propose a taxon for picture  $IDP <br/><center><img width=\"200px\" src=\"$FILE\"></center><select><option>Le Ptinomorphe impérial</option><option>L Acméodère de la Piloselle</option></select>\r\n<script language=\"JavaScript\">\nvar myDiv = document.getElementById(\"answer\");\ndocument.getElementById(\"mySelect\").onchange = function(){\n  myDiv.value = this.value;\n}\n</script>');

-- --------------------------------------------------------

--
-- Structure de la table `userartifact`
--

CREATE TABLE `userartifact` (
  `location` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `userartifact`
--

INSERT INTO `userartifact` (`location`, `id`) VALUES
('sdfsdf', 1),
('Somewhere', 2),
('Somewhere', 3),
('Somewhere', 4);

-- --------------------------------------------------------

--
-- Structure de la table `userprofile`
--

CREATE TABLE `userprofile` (
  `id` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `description` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `availability` set('January','February','March','June','July','August','September','October','November','December') NOT NULL,
  `wallet` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hashed_password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `hashed_password`) VALUES
(1, 'Demo', '$2y$10$.qQpNWFKm62Ja.eZkm5Y7OP5mh.je5Jerj381VjysHZyOtIT9xDoC'),
(2, 'admin', '$2y$10$xrJhB.lcgljZqpKiIKOuuu9BWyH0QzMbKwVyaC67.fIPS9rMP7X3S'),
(3, 'Demo2', '$2y$10$suHPmFVFubVQQhm5PR9QsOK7AH//XPy6ocRFrD.hyakMXXP1ZYqNW'),
(4, 'Oracle', '$2y$10$vcP/tqzkPLz419l9rONg.uB4FJYhXNOkEb0dg8R9RTcTCmxNjoura');

-- --------------------------------------------------------

--
-- Structure de la vue `matrixskyline`
--
DROP TABLE IF EXISTS `matrixskyline`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `matrixskyline`  AS  select `task`.`id` AS `idtask`,`task`.`duration` AS `duration`,`task`.`reward` AS `reward`,`sk1`.`skill` AS `skill1`,`sk2`.`skill` AS `skill2` from (((`task` join `profile` `p1` on((`task`.`id` = `p1`.`idtask`))) join `skilltree` `sk1` on((`p1`.`idskill` = `sk1`.`id`))) join (`profile` `p2` join `skilltree` `sk2` on((`p2`.`idskill` = `sk2`.`id`)))) where ((`p1`.`idtask` = `p2`.`idtask`) and (`p1`.`idskill` <> `p2`.`idskill`)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `preferences`
--
DROP TABLE IF EXISTS `preferences`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `preferences`  AS  select `skills`.`iduser` AS `iduser`,`skilltree`.`skill` AS `skill` from (`skilltree` join `skills` on((`skilltree`.`id` = `skills`.`idskill`))) order by `skills`.`level` desc ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `answer`
--
ALTER TABLE `answer`
  ADD KEY `idtask` (`idtask`);

--
-- Index pour la table `artifact`
--
ALTER TABLE `artifact`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `artifactclass`
--
ALTER TABLE `artifactclass`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `availability`
--
ALTER TABLE `availability`
  ADD KEY `iduser` (`iduser`);

--
-- Index pour la table `bdd_feedback`
--
ALTER TABLE `bdd_feedback`
  ADD PRIMARY KEY (`idf`);

--
-- Index pour la table `bdd_marks`
--
ALTER TABLE `bdd_marks`
  ADD PRIMARY KEY (`idm`);

--
-- Index pour la table `bdd_proposal`
--
ALTER TABLE `bdd_proposal`
  ADD PRIMARY KEY (`idp`);

--
-- Index pour la table `bdd_query`
--
ALTER TABLE `bdd_query`
  ADD PRIMARY KEY (`idq`);

--
-- Index pour la table `bdd_time`
--
ALTER TABLE `bdd_time`
  ADD PRIMARY KEY (`idt`);

--
-- Index pour la table `chat_post`
--
ALTER TABLE `chat_post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `id_topic` (`id_topic`);

--
-- Index pour la table `chat_topic`
--
ALTER TABLE `chat_topic`
  ADD PRIMARY KEY (`id_topic`),
  ADD KEY `forum_topic_ibfk_2` (`id_task`);

--
-- Index pour la table `cities`
--
ALTER TABLE `cities`
  ADD KEY `iduser` (`iduser`);

--
-- Index pour la table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`idfeedback`);

--
-- Index pour la table `forum_category`
--
ALTER TABLE `forum_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Index pour la table `forum_post`
--
ALTER TABLE `forum_post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `id_topic` (`id_topic`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `forum_post_thanks`
--
ALTER TABLE `forum_post_thanks`
  ADD PRIMARY KEY (`id_post`,`id_user`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `forum_topic`
--
ALTER TABLE `forum_topic`
  ADD PRIMARY KEY (`id_topic`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `creator_id` (`creator_id`),
  ADD KEY `forum_topic_ibfk_4` (`id_task`);

--
-- Index pour la table `identificationartifact`
--
ALTER TABLE `identificationartifact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `isnlpartifact`
--
ALTER TABLE `isnlpartifact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `isnlp_query`
--
ALTER TABLE `isnlp_query`
  ADD PRIMARY KEY (`idquery`);

--
-- Index pour la table `photoartifact`
--
ALTER TABLE `photoartifact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`idp`);

--
-- Index pour la table `profile`
--
ALTER TABLE `profile`
  ADD KEY `idtask` (`idtask`),
  ADD KEY `idskill` (`idskill`);

--
-- Index pour la table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`iduser`,`idskill`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `idskill` (`idskill`);

--
-- Index pour la table `skilltree`
--
ALTER TABLE `skilltree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_skill` (`parent_skill`);

--
-- Index pour la table `skylines`
--
ALTER TABLE `skylines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser` (`iduser`);

--
-- Index pour la table `spipoll`
--
ALTER TABLE `spipoll`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subartifacts`
--
ALTER TABLE `subartifacts`
  ADD PRIMARY KEY (`idChild`);

--
-- Index pour la table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `userartifact`
--
ALTER TABLE `userartifact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `userprofile`
--
ALTER TABLE `userprofile`
  ADD KEY `id` (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `artifact`
--
ALTER TABLE `artifact`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `bdd_feedback`
--
ALTER TABLE `bdd_feedback`
  MODIFY `idf` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bdd_marks`
--
ALTER TABLE `bdd_marks`
  MODIFY `idm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `bdd_proposal`
--
ALTER TABLE `bdd_proposal`
  MODIFY `idp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `bdd_query`
--
ALTER TABLE `bdd_query`
  MODIFY `idq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4009;

--
-- AUTO_INCREMENT pour la table `bdd_time`
--
ALTER TABLE `bdd_time`
  MODIFY `idt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT pour la table `chat_post`
--
ALTER TABLE `chat_post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `chat_topic`
--
ALTER TABLE `chat_topic`
  MODIFY `id_topic` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `idfeedback` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `forum_category`
--
ALTER TABLE `forum_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `forum_post`
--
ALTER TABLE `forum_post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `forum_topic`
--
ALTER TABLE `forum_topic`
  MODIFY `id_topic` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `identificationartifact`
--
ALTER TABLE `identificationartifact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `isnlpartifact`
--
ALTER TABLE `isnlpartifact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `isnlp_query`
--
ALTER TABLE `isnlp_query`
  MODIFY `idquery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT pour la table `photoartifact`
--
ALTER TABLE `photoartifact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `skilltree`
--
ALTER TABLE `skilltree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1003;

--
-- AUTO_INCREMENT pour la table `skylines`
--
ALTER TABLE `skylines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4008;

--
-- AUTO_INCREMENT pour la table `userartifact`
--
ALTER TABLE `userartifact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `chat_post`
--
ALTER TABLE `chat_post`
  ADD CONSTRAINT `chat_post_ibfk_1` FOREIGN KEY (`id_topic`) REFERENCES `chat_topic` (`id_topic`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `forum_post`
--
ALTER TABLE `forum_post`
  ADD CONSTRAINT `forum_post_ibfk_1` FOREIGN KEY (`id_topic`) REFERENCES `forum_topic` (`id_topic`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_post_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `forum_post_thanks`
--
ALTER TABLE `forum_post_thanks`
  ADD CONSTRAINT `forum_post_thanks_ibfk_1` FOREIGN KEY (`id_post`) REFERENCES `forum_post` (`id_post`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_post_thanks_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `forum_topic`
--
ALTER TABLE `forum_topic`
  ADD CONSTRAINT `forum_topic_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `forum_category` (`id_category`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `forum_topic_ibfk_3` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`idtask`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`idskill`) REFERENCES `skilltree` (`id`);

--
-- Contraintes pour la table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `skills_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `skills_ibfk_2` FOREIGN KEY (`idskill`) REFERENCES `skilltree` (`id`);

--
-- Contraintes pour la table `skilltree`
--
ALTER TABLE `skilltree`
  ADD CONSTRAINT `skilltree_ibfk_1` FOREIGN KEY (`parent_skill`) REFERENCES `skilltree` (`id`);

--
-- Contraintes pour la table `skylines`
--
ALTER TABLE `skylines`
  ADD CONSTRAINT `skylines_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `userprofile`
--
ALTER TABLE `userprofile`
  ADD CONSTRAINT `userprofile_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;