DELETE from CURRENTARTIFACT where NAME = (select TABLENAME from ARTIFACTCLASS where project = 'ISNLP');
DELETE from ARTIFACT where CLASSID = (select id from ARTIFACTCLASS where project = 'ISNLP');
DELETE from ARTIFACTCLASS where project = 'ISNLP';
DELETE from profile;
DELETE from task;
DELETE from answer;
-- DELETE from result;
ALTER TABLE ARTIFACT AUTO_INCREMENT = 1;
ALTER TABLE task AUTO_INCREMENT = 2;

drop table if exists bdd_query;
drop table if exists bdd_proposal;
drop table if exists bdd_marks;
drop table if exists bdd_time;
drop table if exists bdd_feedback;
drop table if exists bdd_participant;

DELETE FROM chat_post;
DELETE FROM chat_topic;
ALTER TABLE chat_post AUTO_INCREMENT = 1;
ALTER TABLE chat_topic AUTO_INCREMENT = 1;

drop table if exists IsnlpArtifact;
create table if not exists IsnlpArtifact (query_test varchar(100),id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into IsnlpArtifact(query_test) values ('Test Query');

insert into ARTIFACTCLASS(id,description,definition,tablename,project) values (4000,'The Isnlp Artifact','isnlp.sca', 'IsnlpArtifact', 'ISNLP');

insert into ARTIFACT(OWNERID,CLASSID,STATE,NODE,ATTRIBUTES) values ('1','4000','running',1,1);

-- insert into CURRENTARTIFACT(ID,NAME,USER) values (5,'IsnlpArtifact', 1);
-- insert into CURRENTARTIFACT(ID,NAME,USER) values (5,'IsnlpArtifact', 2);
-- insert into CURRENTARTIFACT(ID,NAME,USER) values (5,'IsnlpArtifact', 3);
-- insert into CURRENTARTIFACT(ID,NAME,USER) values (5,'IsnlpArtifact', 4);
