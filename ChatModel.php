<?php

class ChatModel {

    private $db;

    function __construct($conn) {
        $this->db = $conn;
    }

     /**
      * Returns the ID of the topic which has same values. false if not found.
      * @param int $artifactId The ID of the artifact of the topic
      * @param int $taskId The ID of the task of the topic. If NULL, the topic is at the project-level.
      */
    function getTopicIdFrom($taskId = NULL) {
            debug("here");
            $query = "SELECT id_topic FROM chat_topic WHERE id_task = :task";
            if (is_null($taskId)) {
                $query = "SELECT id_topic FROM chat_topic WHERE id_task IS NULL";
            }
            $stmt = $this->db->prepare($query);
            if (!is_null($taskId)) {
                $stmt->bindParam(':task', $taskId, PDO::PARAM_INT);
            }
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
    }

    /**
     * Retrieves all the data of the topic (not the posts)
     * @param int $topicId The targeted topic
     */
    function getTopicData($topicId) {
        try {
            $stmt = $this->db->prepare("SELECT * FROM chat_topic WHERE id_topic = :topic");
            $stmt->bindParam(':topic', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Creates a new topic whose artifact and task are those in parameter
     * @param int $artifactId The ID of the artifact of the topic
     * @param int $taskId The ID of the task of the topic. If NULL, the topic is at the project-level.
     */
    function createTopicFor($taskId) {
        try {
            $stmt = $this->db->prepare("INSERT INTO chat_topic (id_task) VALUES (:task)");
            $stmt->bindParam(':task', $taskId, PDO::PARAM_INT);
            $stmt->execute();
            return $this->db->lastInsertId();
            // return true;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    function retrieveAllTopics() {
        try {
            $stmt = $this->db->prepare("SELECT ft.*, COUNT(fp.id_post) AS 'post_number' FROM chat_topic ft, chat_post fp WHERE ft.id_topic = fp.id_topic GROUP BY ft.id_topic ORDER BY ft.id_topic ASC");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieves all posts from a topic
     * @param int $topicId The ID of the topic to get posts from
     */
    function retrievePostsFromTopic($topicId) {
        try {
            $stmt = $this->db->prepare("SELECT fp.*, u.name FROM chat_post fp, Users u WHERE id_topic = :id_topic AND fp.user_id = u.id ORDER BY date ASC");
            $stmt->bindParam(':id_topic', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Adds a new post to the database
     * @param string $content The post content written by the user
     * @param int $userId The ID of the connected user who submitted his post
     * @param int $topicId The ID of the topic owning the post
     */
    function postToTopic($content, $userId, $topicId) {
        try {
            $stmt = $this->db->prepare("INSERT INTO chat_post (content, user_id, id_topic) VALUES (:content, :user_id, :id_topic)");
            $stmt->bindParam(':content', $content, PDO::PARAM_STR);
            $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
            $stmt->bindParam(':id_topic', $topicId, PDO::PARAM_INT);
            return $stmt->execute();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

}

?>