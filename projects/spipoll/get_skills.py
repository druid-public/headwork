import requests

# 'spgp'@'locahost' spgpPASSWORD
"""
create table spipoll_headwork as
(
select s.id, s.userId, s.resourceId, f.url, s.typeId, t.value, o.nbValidation, o.nbSuggestIdent, o.taxonId, i.fr_name
from social_event as s, social_event_type as t, spipoll_observation as o, spipoll_insecte as i, Files as f
where s.typeId = t.id and s.typeId in (2,3) and s.resourceId = o.id and i.id = o.taxonId and f.id = o.imgTaxon1
);

"""


hostname = 'localhost'
username = 'headworkadmin'
password = 'HEAg2r4a3g2'
database = 'headwork'

import urllib.request
import random





def createTableTemplate( conn ):
    mycursor = conn.cursor()
    sql = "DROP TABLE IF EXISTS templateSkills"
    mycursor.execute(sql)
    mycursor = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS templateSkills ( id INT AUTO_INCREMENT PRIMARY KEY, niv_taxon varchar(150), parent varchar(255), body varchar(10000) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
    mycursor.execute(sql)

def getORDRE_SKILLS( conn ):

    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    body = ('<table style="width:50%">' + '\n' +
            '<caption>ORDER SKILLS</caption>' + '\n' +
            '<tr>' + '\n' +
            '<th>Skills</th>' + '\n'+
            '<th>Level</th>' + '\n' +
            '</tr> \n')

    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        print(id_ordre, taxon_ordre)
        body += ('<tr>'+ '\n' +
                 '<td>'+taxon_ordre+'</td> \n'+
                 '<td><input type="range" min="0" max="100" value="0" class="slider" id="myRange_'+str(id_ordre)+'" name="level['+str(id_ordre)+']" style="width:50%"> <span id="'+str(id_ordre)+'"></span></td>' + '\n' +
                 '</tr> \n');

    body += ('</table> \n ');
    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    body += '<script> \n'
    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        body += ('var slider_'+str(id_ordre)+' = document.getElementById("myRange_'+str(id_ordre)+'");' + '\n' +
                 'var output_'+str(id_ordre)+' = document.getElementById("'+str(id_ordre)+'");' + '\n' +
                 'output_'+str(id_ordre)+'.innerHTML = 0;' + '\n' +
                 'slider_'+str(id_ordre)+'.oninput = function()  {' + '\n' +
                 'output_'+str(id_ordre)+'.innerHTML = this.value;}' + '\n')



    body += '</script>';
    #return cur_list
    print()
    print(body)
    print()
    insert = conn.cursor()
    sql = "INSERT INTO templateSkills ( niv_taxon, parent, body ) VALUES (%s, %s, %s)"
    val = ('ORDRE', 'spipoll', body)
    insert.execute(sql, val)
    conn.commit()

"""
changement effectué
aricia => aricias
"""

print("Using mysql.connector…")
import mysql.connector
myConnection = mysql.connector.connect( host=hostname, user=username, passwd=password, db=database )

createTableTemplate( myConnection )
#print('---ORDRE---')
#insertTemplateORDRE( myConnection )
print('---ORDRE_SKILLS---')
getORDRE_SKILLS(myConnection)
myConnection.close()
