package model.classConstructors;

import model.Parameters;
import model.actions.Action;
import java.util.List;

public class JavaArtifactClassConstructor extends JavaClassConstructor{
    private String nameArtifact;
    private Parameters<String> attributesNameAndType;
    private List<Action> listactions;

    public JavaArtifactClassConstructor(String nameArtifact, Parameters<String> attributes, List<Action> actions,
                                        String directory, String classpackage, Parameters<Object> params){
        super(nameArtifact,directory,classpackage,"implements Artifact " , params);

        this.nameArtifact = nameArtifact;
        this.attributesNameAndType = attributes;
        this.listactions = actions;

        this.addConstructor(artifactConstructor());

        String[] imports = {"model.*", "model.actions.*", "java.util.*", "java.io.*"};
        this.addImports(imports);
        this.addAttributes(attributes.getParameters().entrySet());
        this.addAttribute("actions","Set<Action>")
                .addAttribute("state","String")
                .addAttribute("idPeer","Integer")
                .addAttribute("connection","DBConnector")
                .addAttribute("id","Integer");

        for (Action action : actions){
            this.addMethod(action.writeMethod());
        }
    }

    private String artifactConstructor(){
        /*
                Create String builder for java file
         */
        StringBuilder content = new StringBuilder("\n    public ").append(nameArtifact).
                append("(Parameters parameters,DBConnector connection){\n");
        for (String attribute : attributesNameAndType.keySet()){
            String type = attributesNameAndType.get(attribute);
            content.append("        this.").append(attribute).append(" = " +
                    "(").append(type).append(") " +
                    "parameters.get(\"").append(attribute).append("\");\n");
        }
        content.append("        this.actions = new HashSet<>();\n" +
                "        this.state = \"active\";\n" +
                "        this.id = Counter.nextValue(\"").append(nameArtifact).append("\");\n" +
                "        this.connection = connection;\n" +
                "        this.idPeer = (Integer) parameters.get(\"idPeer\");\n\n");

        /*
                Actions
         */
        content.append("\n        // Create the Actions\n");
        for (Action act : listactions) {
            String nameAction = (String) act.getInfo().get("title");
            content.append("\n        // Action ").append(nameAction).append("\n");

            content.append("        Parameters<Object> info").append(act.getId()).append(" = new Parameters<>();\n");
            for (String info : act.getInfo().keySet()) {
                Object infovalue = act.getInfo().get(info);

                // if the parameter is a Parameters (in case of a CreationAction, for the parameters of the new artifact for example)
                if (infovalue instanceof Parameters) {
                    Parameters<String> parametersInInfo = (Parameters<String>) infovalue;
                    content.append("        Parameters<String> " + info + act.getId() + " = new Parameters<>();\n");
                    for (String param : parametersInInfo.keySet()) {
                        content.append("        " + info + act.getId() + ".addParam(\"" + param + "\",\"" + parametersInInfo.get(param) + "\");\n");
                    }
                    infovalue = info + act.getId();
                } else {
                    infovalue = "\"" + infovalue + "\"";
                }
                content.append("        info").append(act.getId()).append(".addParam(\"")
                        .append(info).append("\", ").append(infovalue).append(");\n");
            }
            content.append("        Action action").append(act.getId()).append(" = new OtherAction(\"")
                    .append(act.getGuard()).append("\", info").append(act.getId()).append(");\n");//\""+act.getBody()+"\",
            content.append("        this.actions.add(action").append(act.getId()).append(");\n");
            /*
            content.append("        //update task database\n" +
                    "        connection.sendToDb(action").append(act.getId()).append(".writeTask());\n\n");

             */
        }

        /*
                Update the database

        content.append("\n        //update database\n" +
                "        connection.sendToDb(\"").append(initRequest()).append("\");\n" +
                "        connection.sendToDb(\"insert into " +
                "state(idPeer, idArtifact, classArtifact, state) " +
                "values ('\"+idPeer+\"','\"+id+\"','").append(nameArtifact).append("','\"+state+\"')\");\n");

         */

        content.append("    }\n");

        return content.toString();
    }
}

/*
    private String initRequest(){
        StringBuilder content = new StringBuilder("insert into ");
        content.append(nameArtifact).append("Artifact(id");
        for (String attribute : attributesNameAndType.keySet()){
            content.append(", ").append(attribute);
        }
        content.append(") values ('\"+id+\"'");
        for (String attribute : attributesNameAndType.keySet()){
            content.append(", '\"+").append(attribute).append("+\"'");
        }
        content.append(")");
        return content.toString();
    }

        /*
               En-tête de la fonction (attributs en argument)

        StringBuilder content = new StringBuilder("    public ").append(nameArtifact).append("(Integer idPeer,");
        for (String attribute : attributes.keySet()){
            content.append(attributes.get(attribute)).append(" ").append(attribute).append(", ");
        }
        content.append("DBConnector connection){\n");

        /*
               Instanciation des attributs

        for (String attribute : attributes.keySet()){
            content.append("        this.").append(attribute).append(" = ").append(attribute).append(";\n");
        }
        content.append("        this.actions = new HashSet<>();\n" +
                "        this.state = \"active\";\n" +
                "        this.idPeer = idPeer;\n" +
                "        this.connection = connection;\n\n");
        */
        /*
    private String contentArtifactClass(String nameArtifact, Map<String,String> attributes, List<Action> actions){
        StringBuilder content = new StringBuilder("package "+appName+";\nimport java.util.*;\nimport model.*;\n" +
                "\npublic class " + nameArtifact + " implements Artifact{\n");
        content.append(artifactAttributes(attributes));
        content.append(artifactConstructor(nameArtifact, attributes, actions));

        for (Action action : actions){
            content.append("\n").append(action.writeMethod());
        }

        content.append("\n}");
        //System.out.println(content);
        return content.toString();
    }

 */
/*
    private String artifactAttributes(Map<String,String> attributes){
        StringBuilder content = new StringBuilder("    private Set<Action> actions;\n" +
                "    private String state;\n" +
                "    private Integer idPeer;\n" +
                "    private DBConnector connection;\n" +
                "    private Integer id;\n");
        for (String attribute : attributes.keySet()){
            content.append("    private ").append(attributes.get(attribute)).append(" ").append(attribute).append(";\n");
        }
        return "\n"+content.toString();
    }

 */
            /*
    private String fctAttribute(String attribute, String type) {
        String init = "\n    private " + type + " " + attribute + ";\n";
        String getFct = "    public " + type + " get" + attribute + "(){" +
                "return this." + attribute + ";" +
                "}\n";
        String setFct = "    public void set" + attribute + "(" + type + " param){" +
                "this." + attribute + " = param;" +
                "}\n";
        return init + getFct + setFct;
    }

             */

/*
        content.append(artifactAttributes(attributes));
        content.append(artifactConstructor(nameArtifact, attributes, actions));

        for (Action action : actions){
            content.append("\n").append(action.writeMethod());
        }

        content.append("\n}");
        //System.out.println(content);
        return content.toString();
    }

 */