package model.connections;

import java.io.*;
import java.sql.*;

public class WriteSql extends ConnectDirectly {

    private FileWriter sqlFile;

    public WriteSql(String directory, Connection connection){
        super(connection);
        File initSqlFile = new File(directory + "init-workflow.sql");
        try {
            sqlFile = new FileWriter(initSqlFile, false);
        } catch (IOException e) {
            System.out.println(initSqlFile.toString());
            e.printStackTrace();
        }
    }

    public void sendToDb(String request) {
        try {
            sqlFile.write(request+"\n");
            sqlFile.flush();
            //System.out.println(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
