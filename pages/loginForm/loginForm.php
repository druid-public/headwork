<?php

class LoginForm extends Action {
    /**
     * login
     *
     * Log in the user if username/password are correct
     *
     * @param $conn : A valid database PDO connection
     *
     * @return string the mode to activate, showPages by default
     **/
    function login($conn)
    {
        // Extract the action mode in POST then GET


        if (array_key_exists("mode", $_POST)) {
            $mode=$_POST["mode"];
        } else {
            if (array_key_exists("mode", $_GET)) {
                $mode=$_GET["mode"];
            } else {
                $mode="showTasks";
            }
        }

        $_SESSION["mode"]=$mode;

        debug("Session mode set to $mode");
        // Check if we are already logged. If so, return the action mode

        if (!empty($_SESSION["login"])) {
            debug("Already logged");
            return $_SESSION["mode"];
        }

        // Check if we are trying to log in

        if ($_SESSION["mode"]=="login") {
            @debug("checking for login ".$_POST['username']." ".$_POST['password']);

            $id=$this->checkLogin($_POST['username'], $_POST["password"], $conn);
            debug("received value $id");

            // wrong username/password
            if(is_null($id)){
                $_SESSION["wrong"]=true;
                return "login";
            }

            debug("login successful");


            $_SESSION["login"]=true;
            $_SESSION['wrong']=false;
            $_SESSION["id"]=$id;
            $_SESSION["username"]=$_POST['username'];
            $_SESSION["mode"]="showTasks";

            // set a default project if necessary
            if (empty($_SESSION["project"]))
                    $_SESSION['project'] = DEFAULTPROJECT;


            return "showTasks";
        }
            // check if we are trying to create an account
        if ($_SESSION["mode"]=="register") {
            debug("Detecting registering mode");
            return "register";
        }

        if ($_SESSION["mode"]=="addUser") {
            debug("Detecting registered mode");
            return "addUser";
        }

        // we are not supposed to reach this point. Anyway:
        return "login";

    }

    /**
    * checkLogin
    *
    * Ask the bd to check if username/password are correct
    *
    * @param $username : The username of the user
    * @param $password : The password of the user
    * @param $conn : A valid database PDO connection
    *
    * @return $id : The id of the user if username/password are correct
    **/
    function checkLogin($username, $password, $conn)
    {
        global $VIEW;
        $stmt = $conn->prepare("select id,name,hashed_password from Users where name=:username");
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();
        debug("checking for $username");
        $line=$stmt->fetch();
        if ($line){
            debug("identified as ".$line['id']);
            if (password_verify($password, $line['hashed_password'])) {
                debug("password ok");
                return $line['id'];
            }
            debug("wrong password");
            return null;
        }
        debug("wrong user");
        return null;
     }

    /**
    * create_anonymous
    *
    * Create in the Db an anonymous user with full availibilities
    *
    * @param $conn : A valid database PDO connection
    **/
    function create_anonymous($conn)
    {
        $stmt = $conn->prepare("insert into users values('',:name,:pwd)");
        $stmt->bindParam(':name', $_POST["username"], PDO::PARAM_STR);
        $stmt->bindParam(':pwd', $_POST["password"], PDO::PARAM_STR);
        $stmt->execute() or die( PDO::errorInfo());
        $id=(string)@$this->checkLogin($_POST['username'], $_POST["password"], $conn);
        $stmt = $conn->prepare("select id from skilltree");
        $stmt->execute();
        for ($i=0;$i<7;$i++) {
            for ($j=0;$j<4;$j++) {
                $stmt = $conn->prepare("insert into availability values(:id,:day,:period)");
                $stmt->bindParam(':id', $id, PDO::PARAM_STR);
                $stmt->bindParam(':day', $i, PDO::PARAM_STR);
                $stmt->bindParam(':period', $j, PDO::PARAM_STR);
                $stmt->execute();
            }
        }
    }


    /**
     * login
     *
     * Show the form to log in
     *
     * @param $conn :
     *            A valid database PDO connection
     */
    function ShowloginForm($conn){
        global $VIEW;
        $template=file_get_contents("templates/loginForm.tmpl");

        $msg="";
        if (isset($_SESSION["wrong"]))
            $msg="<center><br/><p class='error'>Wrong username or password</p>";

        $template=str_replace("{PASSWORDCHECK}",$msg,$template);

        if(!isset($_COOKIE['cookies'])){
            // First time visitor
            $show_consent = True;
            $cookies = ['consent'=>0];
            $cookies_string = json_encode($cookies);
            setcookie("cookies",$cookies_string,time() + (60*60*24*30),'/','localhost',1); // Set cookie for 30 days

        } else {
            $show_consent = False; // Don't show the popup
            $cookies = json_decode($_COOKIE['cookies'],True);

            if($cookies['consent']==0){
                $cookies = ['consent'=>1];
                $cookies_string = json_encode($cookies);
                setcookie("cookies",$cookies_string,time() + (60*60*24*30),'/','localhost',1);
            }
        }

        $msg2 = "";
        if ($show_consent) {
            $msg2 = <<<EOT
<link href="css/cookies.css" rel="stylesheet" type="text/css"/>
<input type="checkbox" id="close_cookie"></input>

<div id="cookie_consent_popup" class="rounded border border-dark">
    <h1>Cookies</h1>
    <label for="close_cookie" id="close_cookie_box">X</label>
    <p>
        Headwork uses cookies to store preferences which are only used for service operation. Read more about the used cookies by clicking the button below.
    </p>
    <p>
        Headwork utilise des cookies pour stocker les préférences qui ne sont utilisées que pour le fonctionnement du service. Pour en savoir plus sur les cookies utilisés, cliquez sur le bouton.
    </p>

    <!-- Button trigger modal -->

    <button type="button" class="btn btn-outline-dark btn-sm" data-bs-toggle="modal" data-bs-target="#rgpdModal" onclick="event.stopPropagation();event.preventDefault();">
          Used Cookies / Cookies utilisés
    </button>
</div>


	<!-- Modal -->
<div class="modal fade" id="rgpdModal" tabindex="-1" aria-labelledby="rgpdModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="rgpdModalLabel" style="color:black">Vos données personnelles / Your personal data</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        {RGPD}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
EOT;
        }

        $template=str_replace("{COOKIE}",$msg2,$template);
        $template=str_replace("{RGPD}",file_get_contents(tpath("templates/rgpd",".tmpl")),$template);

        $VIEW['MAIN'].=$template;
    }

    function render(){
        $this->ShowloginForm($this->conn);
    }
}
?>
