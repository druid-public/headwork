<?php
 	/**
     * profile
     *
     * Show the form to watch and complete the user profile
     *
     * @param  $conn : A valid database PDO connection
     */
    
    function profile($conn){
        global $VIEW;
        $select="";
        $stmt = $conn->prepare("select * from UserProfile where id=:id");
        $stmt->bindParam(':id', $_SESSION["id"], PDO::PARAM_STR);
        $stmt->execute();
        $result =$stmt->setFetchMode(PDO::FETCH_ASSOC);
        $line=$stmt->fetch();
        if (!$line){
            $line=array();
            $line["availability"]=2;
        }

        if((int)$line["availability"]>250){
            $_POST["365"]="checked";
        }else if((int)@$line["availability"]>50){
            $_POST["60"]="checked";
        }else if((int)@$line["availability"]>10){
            $_POST["20"]="checked";
        }else if((int)@$line["availability"]>0){
            $_POST["2"]="checked";
        }

        $VIEW['MAIN'].=HTMLform("index.php","POST",
            HTMLh1("".$_SESSION['username'].t("'s profile")).
            HTMLtag("textarea",array("id"=>"description","name"=>"description","placeholder"=>"Description")).@$line["description"].HTMLclosingtag("textarea").HTMLbr().HTMLbr().
	    HTMLh2(t("Availability:")).HTMLp(t("Indicate how often you plan on coming on the platform:")).
            HTMLp(t("As often as possible"),array("id"=>"deb")).
            HTMLtag("input",array("type"=>"radio",isset($_POST["365"]) ? $_POST["365"] : ''=>"","name"=>"availability","value"=>"365","required"=>"")).HTMLbr().
            HTMLp(t("Once per week or so"),array("id"=>"deb")).
            HTMLtag("input",array("type"=>"radio",isset($_POST["60"]) ? $_POST["60"] : ''=>"","name"=>"availability","value"=>"60","required"=>"")).HTMLbr().
            HTMLp(t("Some times per month"),array("id"=>"deb")).
            HTMLtag("input",array("type"=>"radio",isset($_POST["20"]) ? $_POST["20"] : ''=>"","name"=>"availability","value"=>"20","required"=>"")).HTMLbr().
            HTMLp(t("Some times per year"),array("id"=>"deb")).
            HTMLtag("input",array("type"=>"radio",isset($_POST["2"]) ? $_POST["2"] : ''=>"","name"=>"availability","value"=>"2","required"=>"")).HTMLbr().HTMLbr().
            HTMLtag("div",array("class"=>"lined")).
            HTMLinput("hidden","mode","saveProfile",null,false,"").
            HTMLbr().
            file_get_contents(tpath("templates/rgpd",".tmpl")).
            HTMLbr().
            HTMLinput("submit","submit",t("Accept conditions below and save changes"),null,false,"")
            );

    }
    
    /**
     * saveProfile
     *
     * Add or update the user profile in the db
     *
     * @param  $conn : A valid database PDO connection
     */
    
    function saveProfile($conn){
        $stmt = $conn->prepare("select id from UserProfile where id=:id");
        $stmt->bindParam(':id', $_SESSION["id"], PDO::PARAM_STR);
        $stmt->execute();
        if ($stmt->rowCount()>0){
            //$stmt = $conn->prepare("UPDATE UserProfile SET age = :age,gender = :gender,description = :description,city = :city,availability = :availability WHERE id = :id");
            $stmt = $conn->prepare("UPDATE UserProfile SET description = :description, availability= :availability WHERE id = :id");
            $stmt->bindParam(':id', $_SESSION["id"], PDO::PARAM_STR);
            //$stmt->bindParam(':age', $_POST["age"], PDO::PARAM_INT);
            //$stmt->bindParam(':gender', $_POST["gender"], PDO::PARAM_STR);
            $stmt->bindParam(':description', $_POST["description"], PDO::PARAM_STR);
            //$stmt->bindParam(':city', $_POST["city"], PDO::PARAM_STR);
            $stmt->bindParam(':availability', $_POST["availability"], PDO::PARAM_INT);
            $stmt->execute() or die(mysql_error());
        }else{
            //$stmt = $conn->prepare("insert into UserProfile values(:id,:age,:gender,:description,:city,:availability,0)");
            $stmt = $conn->prepare("insert into UserProfile(id,description, availability) values(:id,:description, :availability)");
            $stmt->bindParam(':id', $_SESSION["id"], PDO::PARAM_INT);
            //$stmt->bindParam(':age', $_POST["age"], PDO::PARAM_INT);
            //$stmt->bindParam(':gender', $_POST["gender"], PDO::PARAM_STR);
            $stmt->bindParam(':description', $_POST["description"], PDO::PARAM_STR);
            //$stmt->bindParam(':city', $_POST["city"], PDO::PARAM_STR);
            $stmt->bindParam(':availability', $_POST["availability"], PDO::PARAM_INT);
            $stmt->execute() or die(mysql_error());
        }
        showTasks($conn);
    }
?>