# ISNLP Artifact setup

At first, be sure to have an updated schema of the database, if you're not sure about it, drop all the tables and reuse the `init-db-schema.sql` script.

The easiest solution is to use the `init-workflow.sql` from the `isnlp` directory.

In the file `init-workflow.sql`, comment everything below the last `ALTER TABLE`.  
Then you need to add these lines :  
```sql
INSERT INTO ARTIFACTCLASS(id, description, definition) VALUES (4000,'IsnlpArtifact','isnlp.sca'); -- or isnlp-demo.sca to test alone
INSERT INTO ARTIFACT(OWNERID, CLASSID, STATE, NODE, ATTRIBUTES) VALUES ('1', '4000', 'running', 1, 1);
INSERT INTO CURRENTARTIFACT(ID, NAME, USER) VALUES (1, 'IsnlpArtifact', 1);
INSERT INTO CURRENTARTIFACT(ID, NAME, USER) VALUES (1, 'IsnlpArtifact', 2);
INSERT INTO CURRENTARTIFACT(ID, NAME, USER) VALUES (1, 'IsnlpArtifact', 3);
INSERT INTO CURRENTARTIFACT(ID, NAME, USER) VALUES (1, 'IsnlpArtifact', 4);
-- Last 4 lines allow the first 4 users in the database to access the tasks created by the artifact.
```
Once you've done that, just hit "Restart" on the application and you're good to go.

If you are using the isnlp.sca file and you want to change the minimal user requirements (3 by default), just edit the second guard line : `SELECT COUNT(DISTINCT user) > 2 FROM answer WHERE idtask = 4000`, you only have to change the number `2` by the wished number - 1.

If you want to change the first step timer (not the enrolment), open the artifact file in any text editor and use the search function to look for `insert into bdd_time (endtime) values (UNIX_TIMESTAMP()` and replace the later value (before the `* 60`) by the number of minutes you wish.