DELETE from Artifact where classid in (select id from ArtifactClass where project = 'Headwork Interface');
DELETE from ArtifactClass where project = 'Headwork Interface';

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (11001,'Task interface capabilities','NewGUI/gui.sca', null, 'Headwork Interface',true);
