<?php

/**
 * callWirkImageAnnotator
 *
 * given an image URL, call the Wirk API for image annotation
 * @param $imageURL: the URL of the image to annotate
 * @param $artifactid: the id of the artifact issuing the call
 * @param $taskid: the id of the task corresponding to this call
 * @param $userid: the id of the user of the artifact
 * @param conn: a valid PDO connection
 */

function callWirkImageAnnotator($imageUrl,$artifactid,$taskid,$userid,$conn){
    // Setup

    $idApp = 853;
    $idQuality  = 1386;
    $title = "Headwork/Wirk image categorization ($artifactid,$taskid,$userid)";

    // Create AppProject
    $bodyAppProject = new \stdClass();
    $bodyAppProject->IdApp = WIRKAPPID;
    $bodyAppProject->Title = $title;
    $bodyAppProject->IdQuality = WIRKQUALITYID;
    $bodyAppProject->UrlNotification = "";

    debug("Create AppProject");
    $answerAppProject = CallAPI("POST", WIRKPROJECTENDPOINT, WIRKAPIUSER, WIRKAPIPASSWORD, json_encode($bodyAppProject));

    debug("Call: ".json_encode($bodyAppProject));
    debug("Reponse: ".$answerAppProject);

    $jsonAppProject = json_decode($answerAppProject);
    $IdAppProject = $jsonAppProject->{'IdAppProject'};

    // Create request
    $bodyTaskLine = new \stdClass();
    $bodyTaskLine->IdAppProject = $IdAppProject;
    $bodyTaskLine->Inputs[] = $imageUrl;

    debug("Create TaskLine");
    $answerTaskLine = CallAPI("POST", WIRKTASKLINEENDPOINT, WIRKAPIUSER, WIRKAPIPASSWORD, json_encode($bodyTaskLine));

    debug("Call: ".json_encode($bodyTaskLine));
    debug("Reponse: ".$answerTaskLine);

    $jsonTaskLine = json_decode($answerTaskLine);
    $IdTaskLine = $jsonTaskLine->{'IdTaskLine'};

    // Record call into ServiceCall table
    $conn->query("insert into ServiceCall(artifactid,taskid,tasklineid,userid,description) values ($artifactid,$taskid,$IdTaskLine,$userid,'Wirk Image Annotator')");
}

/**
 * callAPI
 * Call an external API
 * @author Wirk
 */

function CallAPI($method, $url, $username, $password, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, $username.":".$password);

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

/**
 * chekWirkImageAnnotator
 *
 * Check if the current calls to Wirk image annotator have finished. Fills the corresponding
 * Answer and Answered tables.
 * @param $conn: a valid PDO connection
 */

function checkWirkImageAnnotator($conn){
    $awaitingServices=$conn->query("select * from ServiceCall");
    foreach($awaitingServices as $awaitingService){
        debug("Get TaskLine: ".$awaitingService['tasklineid']);
        $IdTaskLine=$awaitingService['tasklineid'];
        $artifactid=$awaitingService['artifactid'];
        $taskid=$awaitingService['taskid'];
        $userid=$awaitingService['userid'];

        $answerTaskLineAnswer = CallAPI("GET", WIRKTASKLINEENDPOINT."/".$IdTaskLine, WIRKAPIUSER, WIRKAPIPASSWORD);
        debug("answer to call: ".$answerTaskLineAnswer);
        $jsonAnswer=json_decode($answerTaskLineAnswer);
        $tasks=$jsonAnswer->{'Tasks'};

        if(empty($tasks))
            debug("Not yet backed");
        else {
            $result=$tasks[0]->{'Outputs'}->{'1605'};
            $quality=$tasks[0]->{'Outputs'}->{'1606'};
            if(empty($quality))
                $quality=0;
            $conn->query("insert into Answer(idtask,user,artifact,value,mass) values ($taskid,$userid,$artifactid,'".$result."',$quality)");
            $conn->query("insert into Answered(id,artifact) values ($taskid,$artifactid)");
            $conn->query("delete from ServiceCall where artifactid=$artifactid and userid=$userid and taskid=$taskid");
        }

// example
// Thu, 29 Apr 2021 09:36:51 +0000 checkWirkImageAnnotator:answer to call: {"IdTaskLine":625941,"IdAppProject":6334,"Inputs":["https://www.rd.com/wp-content/uploads/2020/03/GettyImages-1060486568.jpg?resize=1536,1536"],"Tasks":[]}
//  Reponse: {"IdTaskLine":625967,"IdAppProject":6337,"Inputs":["https://www.rd.com/wp-content/uploads/2020/03/GettyImages-1060486568.jpg?resize=1536,1536"],"Tasks":[{"IdTask":812315,"Outputs":{"1605":"animal_panda","1606":"0.99609375"},"UpdateDate":"2021-04-29T14:39:29.927"}]}
    }
}

?>
