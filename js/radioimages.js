$(':input[name="answer"]').change(function() {
    if ($(this).is(':checked')) {
        $(this).parent().attr('class', 'selectedImage');
        $(':input[name="answer"]').not(this).parent().attr('class', 'diagramImage');
    }
});