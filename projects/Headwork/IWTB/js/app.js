
// also add the active class to the selected checkbox
let certitude = document.querySelectorAll('.certitude_container input[type=checkbox]')
let checkbox = document.querySelectorAll('.list input[type="checkbox"]')
let certitudeIsChecked = false
let pass=false

let aHistory = []
let cHistory = []
let answerHistory_dom = document.querySelector('#aHistory')
let certitudeHisory_dom = document.querySelector('#cHistory')

var answerJSON = { answers: [{name: 'Blue tit'},
	{name: 'Great tit' },
	{name: 'Coal tit'},
	{name: 'European robin'}],
	omega: true,
	empty: true };
	
// Event perform when an answer is checked
function check_answer(){
	let checkbox = document.querySelectorAll('.list input[type="checkbox"]')
	checkbox.forEach(c => {
		c.parentElement.addEventListener('click', () => {
			//let checked_checkbox = document.querySelectorAll('.list input[type="checkbox"]:checked')
				//if we unchecked an answer (emptyset or omega)
				if (c.checked) {
					c.checked = false;
					chek_omega_emptyset();
					c.parentElement.classList.remove('active');
					formValidator();
				} 
				else {
					// if omega or the emptyset is checked we pass
					if(pass){
						c.checked = false;
					}
					else{
						c.checked = true;
						checked_checkbox = document.querySelectorAll('.list input[type="checkbox"]:checked');
		        c.parentElement.classList.add('active');
		        aHistory.push(c.value);
		        answerHistory_dom.value = JSON.stringify(aHistory);
		        chek_omega_emptyset();
						// if omega or the empty set have just been check we clean the other answers
						if(pass){
							checked_checkbox.forEach((item) =>{
								if(item.value!='omega' && item.value!='emptyset'){
									item.checked=false;
									item.parentElement.classList.remove('active');
									formValidator();
								}
							})
						}
						let certitudeContainer = document.querySelector('.active_certitude');
						addToCertitude(certitudeContainer, c);
				}
			}
			formValidator();
		})
	})
}

certitude.forEach(c => {
	c.addEventListener('change', () => {
		formValidator()
	})
})

certitude.forEach((c) => {
	c.addEventListener('click', () => {
		if (c.checked) {
			certitude.forEach((cer) => {
				cer.parentElement.classList.remove('active_certitude')
			})
			let checkedOiseaux = document.querySelectorAll('.list input[type="checkbox"]:checked')
			checkedOiseaux.forEach((item) => {
				addToCertitude(c.parentElement, item)
			})
			cHistory.push(c.value);
			certitudeHisory_dom.value = JSON.stringify(cHistory);
			c.parentElement.classList.add('active_certitude');
			formValidator();
		}
	})
})

//function which verifie if omega or the emtyset are check in this case it is not allow to select anything else
function chek_omega_emptyset(){
	pass = false
	checked_checkbox = document.querySelectorAll('.list input[type="checkbox"]:checked')
	checked_checkbox.forEach((item) =>{
		if(item.value=='omega' || item.value=='emptyset'){
			pass = true;
			formValidator();
			if(certitude.length>0){
				certitude.forEach((cer) => {
					cer.checked=false
					clearCertitude(cer.parentElement)
					cer.parentElement.classList.remove('active_certitude')
				})
			}
		}        	
	})
}

//make the final answer to use
function transform_answer(){
	let answer = [[],[],[],[],[],[],[]]
	let final_answer={}
	if(pass){
	final_answer=JSON.stringify(document.querySelector('.list input[type="checkbox"]:checked').value)
	}
	else{
		certitude.forEach((c) => {		
			//console.log(c.parentElement.querySelectorAll('.certitude_oiseaux'))
			let answer_list =c.parentElement.querySelectorAll('.certitude_oiseaux')
			if(answer_list.length>0){
			answer_list.forEach((ans) =>{		
				answer[(c.value-1)].push(ans.textContent)
			})
			}
		})
		final_answer=JSON.stringify(answer)
	}
	console.log(final_answer)
}


function addToCertitude(container, oiseau) {
	//be carrefull a certainty level is selected
	if(!(container===null)){
		let append = true
		// check if the answer is not already add to the certainty level
		let answer_list =container.querySelectorAll('.certitude_oiseaux')
			if(answer_list.length>0){
			answer_list.forEach((ans) =>{		
				if(ans.textContent==oiseau.value){
				append=false
				}
			})
			}
		chek_omega_emptyset()
		// in case where not omega nor the emptyset is selected 
		if(!pass){		        	 
			let item = document.createElement('p')
			item.textContent = oiseau.value
			item.classList.add('certitude_oiseaux')
			item.addEventListener('click', () => {
				oiseau.checked = false
				oiseau.parentElement.classList.remove('active')
				item.remove()
				transform_answer()
				formValidator()
			})
			if(append){
			container.appendChild(item)
			}
			oiseau.checked = false
			oiseau.parentElement.classList.remove('active')
		}
		else{ 
			clearCertitude(container)
			checked_checkbox = document.querySelectorAll('.list input[type="checkbox"]:checked')
		    checked_checkbox.forEach((item) =>{
		    	if(item.value!='omega' && item.value!='emptyset'){
		    		item.checked=false
		    		item.parentElement.classList.remove('active')
		    		formValidator()
		    	}        	
		    })
		}   
	}
	transform_answer()
}

function clearCertitude(container) {
    let childs = [...container.children]
    childs.forEach(child => {
        if (child.classList.contains('certitude_oiseaux'))
            child.remove()
    })
}



function formValidator() {
    let checkedChebox = document.querySelectorAll('.list input[type="checkbox"]:checked')
    let certitudeCheck = document.querySelectorAll('.certitude_container input[type="checkbox"]:checked')
    
    if (certitudeCheck == 0 || checkedChebox==0) {

        document.querySelector('#btn').classList.add('disabled');
    }
    else {
        document.querySelector('#btn').classList.remove('disabled');
    }
}

function create_div(dvalue, dlabel){
	var checkbox = document.createElement('input');
	checkbox.type = 'checkbox';
	checkbox.id = '';
	checkbox.name = 'answer';
	checkbox.value = dvalue;	  
		
	var label = document.createElement('label');
	label.innerHTML = dlabel;
	
	var div = document.createElement('div');
	div.className = "input_wrapper";
	div.appendChild(checkbox);
	div.appendChild(label);
	 
	var container = document.getElementById('list');
	container.appendChild(div);
}


document.addEventListener('DOMContentLoaded', function() {
	var answers = answerJSON["answers"];
	for (var i = 0; i < answers.length; i++) {
		create_div(answers[i]['name'],answers[i]['name']);
	}
	
	//Omega creation if needed
	if(answerJSON['omega']){
		create_div('omega','I do not know');
	}
    	
	//Emptyset creation if needed
	if(answerJSON['empty']){ 
		create_div('emptyset','The good answer is not proposed');
	}
	check_answer();
}, false);

