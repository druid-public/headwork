package model.connections;

import model.Parameters;
import model.SqlCommands;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

abstract class HeadworkConnector implements PlatformConnector {

    private Connection connection;

    static final String[] CLEAN_DATABASE = {
            //"SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));",

            "DELETE from CURRENTARTIFACT;",
            "DELETE from ARTIFACT;",
            "DELETE from ARTIFACTCLASS;",
            "DELETE from profile;",
            "DELETE from task;",
            "DELETE from answer;",
            "DELETE from result;",
            "DELETE from subArtifacts;",
            "ALTER TABLE ARTIFACT AUTO_INCREMENT = 1;",
            "ALTER TABLE task AUTO_INCREMENT = 2;"
    };

    HeadworkConnector() {
        try {
            String url = "jdbc:mysql://localhost/headwork?" +
                    "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            String username = "headworkadmin";
            String password = "HEAg2r4a3g2";

            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection(
                    url,
                    username,
                    password);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    List<String> getSqlNewClass(String nameArtifact, Parameters<String> attributes){
        List<String> requests = new ArrayList<>();
        requests.add(SqlCommands.drop(nameArtifact+"Artifact"));
        requests.add(SqlCommands.create(nameArtifact+"Artifact", attributes));

        List<String> classValues = new ArrayList<>();
        classValues.add("id");
        classValues.add("description");
        classValues.add("definition");
        classValues.add("tablename");
        List<String> classAttributes = new ArrayList<>();
        classAttributes.add(""+Math.abs(nameArtifact.hashCode()));
        classAttributes.add("'The " + nameArtifact + " Artifact'");
        classAttributes.add("'" + nameArtifact + ".sca'");
        classAttributes.add("'" + nameArtifact + "Artifact'");
        requests.add(SqlCommands.insert("ARTIFACTCLASS",classValues, classAttributes));
        return requests;
    }

    Connection getConnection(){
        return connection;
    }


}
