# TODO
* HTML tag system
* Organize code
* timestamps for questions and answers
* checkers sql
* utilisateurs
* profils utilisateurs (disponibilité)
* affectation de tâches / proposition
* branchement mail
* API REST
* probabilités / croyance / flou

# LOG
* 2017-12-17 Task and participant skills, plus a simple ranking function
* 2017-12-15 Branching workflows
* 2017-12-14 Artifacts in JSON outside the code. Better results display. HTML header and footer.
* 2017-12-12 fist two-states sequential workflow
* 2017-12-11 first static workflow
* 2017-12-10 ability to answer tasks
* 2017-12-09 project start, ability to see tasks

