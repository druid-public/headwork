<?php
/**
 * showState
 *
 * Display all useful tables
 *
 * @param $conn :
 *            A valid database PDO connection
 */
function showState($conn){
    global $VIEW;
    $VIEW['MAIN'] .= HTMLh1(t("Monitor Board"));

    showQueryAnswer($conn, "Users", "select id,name,count(*)-1 as '#contribution' from Users left outer join Answer on Answer.user=Users.id group by id,name");
    showQueryAnswer($conn, "User Profile", "select * from UserProfile");
    showQueryAnswer($conn,"Human Power","select sum(availability)/365 as 'Human power' from UserProfile");
    //showQueryAnswer($conn, "User skills", "select * from Skills");
    showQueryAnswer($conn, "Activities (ArtifactClass)", "select * from ArtifactClass");
    showQueryAnswer($conn, "Running activities (Artifacts)", "select concat('<a href=\"index.php?mode=showArtifact&artifactid=',id,'\">View</a>') as detail, id, classid, ownerid, node, state, awaited, attributes from Artifact");
    showQueryAnswer($conn, "Tasks", "select * from Task");
    //showQueryAnswer($conn, "Task Profile", "select * from Profile");
    showQueryAnswer($conn, "Answered tasks", "select * from Answered");
    showQueryAnswer($conn, "Task Answers", "select * from Answer");
    //showQueryAnswer($conn, "Templates", "select * from Template");
    showTableUpload($conn, "Table Upload", "select * from Upload");
    showTableReportAbuse($conn, "Table Report Abuse", "select activity, project, count(activity) as numberOfReport from reportabuse group by activity having count(activity) >= 3");
}

?>
