package model;

import java.util.HashMap;
import java.util.Set;

/**
 * Simple wrap-up around Hashmap so that addParam (equivalent of add) returns this.
 * @param <E>
 */
public class Parameters<E> {
    private HashMap<String,E> parameters;

    public Parameters(){
        parameters = new HashMap<>();
    }

    public Parameters<E> addParam(String name, E value){
        parameters.put(name, value);
        return this;
    }
    public E get(String name){
        return parameters.get(name);
    }
    public Set<String> keySet(){
        return parameters.keySet();
    }
    public Integer size(){ return parameters.size(); }
    public HashMap<String, E> getParameters() {
        return parameters;
    }
}
