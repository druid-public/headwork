<?php

/**
 *	workflowdesign
 *
 *	Displays a custom blockly component for artifact design.
 *	$conn: database connection
 */

function workflowdesign($conn){
    global $VIEW;
    $VIEW['MAIN']=file_get_contents(tpath("templates/template-workflow-design",".tmpl"));
}


