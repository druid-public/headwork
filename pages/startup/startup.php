<?php

/**
 * initialize
 *
 * Initialize all artifacts is the projects folder, except the disabled one, and update the artfactID in startme table
 *
 * @param  $conn : A valid database PDO connection
 */
function initialize($conn)
{
    // run each init-workflow.sql in each project, except in the disabled directory

    $projectPath="projects";
    $projectlist=scandir($projectPath);
    $projectlist=array_diff($projectlist,array(".",".."));
    foreach($projectlist as $projectdir)
        if(is_dir("$projectPath/$projectdir"))
            if($projectdir!="disabled"){
               if(in_array($projectdir,STARTEDPROJECTS)){
                   debug("Loading project ".$projectdir);
                   executeSqlFile("$projectPath/$projectdir/init-skills.sql", $conn);
                   executeSqlFile("$projectPath/$projectdir/init-workflow.sql", $conn);
                   // load each .tmpl template file of each project
                   $filenames=scandir("$projectPath/$projectdir");
                   $filenames=array_diff($filenames,array(".",".."));
                   foreach($filenames as $filename){
                       debug("looking into $filename");
                       if(is_file("$projectPath/$projectdir/$filename")){
                           debug("checking $filename, with ".pathinfo($filename)['extension']);
                           if (pathinfo($filename,PATHINFO_EXTENSION)=="tmpl"){
                               $shortfilename=pathinfo($filename,PATHINFO_FILENAME);
                               $query="insert into Template(id,body) values ('$projectdir-$shortfilename',".$conn->quote((file_get_contents("$projectPath/$projectdir/$filename"))).")";
                               $conn->query($query);
                           }
                   }
               }
           }
        }

   // update all artifacts in the startme table

    $query="select ID from Artifact";

    $table=$conn->query($query);
    foreach ($table as $artifactid){
        debug("launching artifact ".$artifactid['ID']);
        update($conn, $artifactid['ID']);
    }

}

/**
 * restart
 *
 * Initialize the db
 *
 * @param  $conn : A valid database PDO connection
 */
function restart($conn)
{
    // empty the log file
    file_put_contents('log/log.txt', '');
    // empty the database
    executeSqlFile('script/init-db-schema.sql', $conn);
}

/**
 * execute the content of an SQL file
 *
 * @param $file_name : the name of a file containing SQL queries
 * param $conn : A valid database PDO connection
 */
function executeSqlFile($file_name, $conn){
    $file_content = file($file_name);
    $query = "";
    foreach ($file_content as $sql_line) {
        if (trim($sql_line) != "" && strpos($sql_line, "--") === false) {
            $query .= $sql_line;
            if (substr(rtrim($query), -1) == ';') {
                    debug("query from init-workflow");
                    debug($query);
                    $conn->query($query);
                    $query = "";
            }
        }
    }
}

/**
 * createBots
 * create an armada of bots as users
 * @param $conn: a valid PDO connection
 * @param $nbbots: the number of bots to create
 */

function  createBots($conn,$nbbots){
    $namePrefix="Allgaier";
    $conn->query("delete from Skills where iduser in (select id from Users where name like '$namePrefix%')");
    $conn->query("delete from UserProfile where id in (select id from Users where name like '$namePrefix%')");
    $conn->query("delete from Users where name like '$namePrefix%'");


    for($i=1;$i<=NBBOTS;$i++){
        $botname="$namePrefix$i";
        $pwd="bot";
        $stmt = $conn->prepare("insert into Users(name,hashed_password) values(:name,:pwd)");
        $stmt->bindParam(':name', $botname, PDO::PARAM_STR);
        $stmt->bindParam(':pwd', $pwd, PDO::PARAM_STR);
        $pwd=password_hash($pwd, PASSWORD_DEFAULT);
        $stmt->execute();
        $stmt = $conn->prepare("select * from Users where name=:username");
        $stmt->bindParam(':username', $botname, PDO::PARAM_STR);
        $stmt->execute();
        $result =$stmt->setFetchMode(PDO::FETCH_ASSOC);
        $line=$stmt->fetch();
        $id=$line['id'];
        // Any user has the "common knowledge" skill
        $conn->query("insert into Skills values ($id,1,100)");
        $conn->query("insert into UserProfile values ($id,'I am a bot and proud of it.',365,0)");
    }
}
?>

