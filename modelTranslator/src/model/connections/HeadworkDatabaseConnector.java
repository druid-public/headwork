package model.connections;

import com.mysql.cj.util.StringUtils;
import model.Parameters;
import model.actions.Action;
import model.classConstructors.JavaArtifactClassConstructor;
import model.classConstructors.JavaClassConstructor;

import java.sql.*;
import java.util.List;

public class HeadworkDatabaseConnector extends HeadworkConnector {

    private DBConnector connector;
    private String directory;
    private String appName;

    public HeadworkDatabaseConnector(){
            this.connector = new ConnectDirectly(this.getConnection());
            this.appName = "demoSpipollWoDeleg";
            this.directory = "modelTranslator/src/demo2";
    }

    @Override
    public void CreateArtifactClass(String nameArtifact, Parameters<String> attributes, List<Action> actions) {
        JavaArtifactClassConstructor newClass = new JavaArtifactClassConstructor(nameArtifact, attributes, actions,
                appName, directory, new Parameters<>());
        newClass.writeFile();
    }

    @Override
    public void instantiate(List<Parameters> BasicPeersArtifact) {

        for (String request : CLEAN_DATABASE)
            connector.sendToDb(request);

        try {
            JavaClassConstructor mainConstructor = new JavaClassConstructor("Main", directory, appName, "", new Parameters<>());
            String[] imports = {"model.*", "java.sql.*"};
            mainConstructor.addImports(imports)
                    .addClassDescription("Main to execute the app created by "+appName);
            StringBuilder mainMethod = new StringBuilder(
                    "    public static void main(String[] args){\n" +
                            "        try {\n" +
                            "            String url = \"jdbc:mysql://localhost/headwork?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC\";\n" +
                            "            String username = \"headworkadmin\";\n" +
                            "            String password = \"HEAg2r4a3g2\";\n" +
                            "\n" +
                            "            Class.forName(\"com.mysql.cj.jdbc.Driver\");\n" +
                            "            Connection connector = DriverManager.getConnection(\n" +
                            "                    url,\n" +
                            "                    username,\n" +
                            "                    password);\n" +
                            "\n" +
                            "            DBConnector con = new ConnectDirectly(connector);\n");

            //create basic artifacts
            for (Parameters artifact : BasicPeersArtifact){
                String artifactName = (String) artifact.get("artifact");
                //faire la requête bd correspondante (à qui on envoit)
                String request = (String) artifact.get("request");
                ResultSet rs = connector.getFromDb(request);
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();

                //récupérer les résultats
                while (rs.next()) { //iterate on peers

                    int idPeer = rs.getInt("idPeer");
                    //List<String> attributes = new ArrayList<>();
                    mainMethod.append("            Parameters<Object> attributes").append(idPeer).append(" = new Parameters<>();\n");
                    for (int col = 1; col <= columnCount; col++ ) {//iterate on attributes
                        //for (String attribute : artifactsAttributes.get(artifactName).keySet()) {
                        //attributes.add(rs.getString(attribute));
                        String value = rs.getString(col);
                        mainMethod.append("            attributes").append(idPeer).append(".addParam(\"").append(rsmd.getColumnLabel(col)).append("\",");
                        if (StringUtils.isStrictlyNumeric(value))
                            mainMethod.append(value);
                        else
                            mainMethod.append("\"").append(value).append("\"");
                        mainMethod.append(");\n");
                    }

                    //pour chaque résultat de la requête, instancie un artefact
                    mainMethod.append("            new ").append(artifactName).append("(attributes").append(idPeer).append(", con);\n");

                }

            }
            //lance le dbListener
            mainMethod.append(
                    "        } catch (SQLException | ClassNotFoundException e) {\n" +
                            "            e.printStackTrace();\n" +
                            "        }\n" +
                            "    }");
            mainConstructor.addMethod(mainMethod.toString());
            mainConstructor.writeFile();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DBConnector getDBConnection() {
        return connector;
    }
}
