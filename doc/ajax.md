# Asynchronous tasks

## Introduction
In order to make user experience more enjoyable, a new option has been created for task creators.  
It is now possible to make a multiple-task sequence using Ajax. To enable this option, a new field has been created in the table `task` : `ajax`.

## Usage
If you want to create an artifact whose tasks are building a sequence (one by one), you should consider using this.  
To use it, while you define your task sequence in your artifact, just set the `ajax` field to true for each task you wish.  
Please avoid using it on the last task as it will make users wait forever (for the next task while there is none).

