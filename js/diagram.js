var editor = 'https://www.draw.io/?embed=1&ui=atlas&spin=1&proto=json&ui=min';
var name = 'default';
var id = 'diagramImage';
var modalId = 'iframeModal';
var hiddenId = 'diagramData';
var iframe = null;

// Enable the edit of the image
function edit(image) {

    // We only create the iframe on the first time
    if (iframe === null) {
        iframe = $(document.createElement('iframe'));
        iframe.attr('frameborder', '0');
        iframe.attr('class', 'diagramIframe');
    }

    // Hide the modal
    var close = function () {
        window.removeEventListener('message', receive);
        $('#' + modalId).modal('hide');
    };

    // We check if the user has a draft of a diagram to propose him to work on it (in case he refreshed the page with the modal opened for example)
    var draft = localStorage.getItem('.draft-' + name);
    if (draft != null) {
        draft = JSON.parse(draft);
        if (!confirm("A version of this page from " + new Date(draft.lastModified) + " is available. Would you like to continue editing?")) {
            draft = null;
        }
    }

    // Different actions are triggered depending of the data retrieved from draw.io
    var receive = function (evt) {
        if (evt.data.length > 0) {
            var msg = JSON.parse(evt.data);
            // When we want to edit the image or create a new diagram
            if (msg.event == 'init') {
                // If the user chose to back on his draft we load it to the embed
                if (draft != null) {
                    iframe.get(0).contentWindow.postMessage(JSON.stringify({
                        action: 'load',
                        autosave: 1,
                        xml: draft.xml
                    }), '*');
                    iframe.get(0).contentWindow.postMessage(JSON.stringify({
                        action: 'status',
                        modified: true
                    }), '*');
                } else {
                    // If the user already created an image he has to edit it
                    if ($('#' + hiddenId).val().length != 0 || $('#' + id).attr('data-default') == "false") {
                        iframe.get(0).contentWindow.postMessage(JSON.stringify({
                            action: 'load',
                            autosave: 1,
                            xmlpng: $('#' + id).attr('src')
                        }), '*');
                    // On the first load, the user can choose a template among multiple possibilities
                    } else {
                        iframe.get(0).contentWindow.postMessage(JSON.stringify({
                            action: 'template',
                            autosave: 1
                        }), '*');
                    }
                }
            // This action is triggered when the user saves his diagram
            } else if (msg.event == 'export') {
                image.setAttribute('src', msg.data);
                $('#' + hiddenId).val(msg.data);
                localStorage.setItem(name, JSON.stringify({
                    lastModified: new Date(),
                    data: msg.data
                }));
                localStorage.removeItem('.draft-' + name);
                draft = null;
                close();
            // After every action in the embed
            } else if (msg.event == 'autosave') {
                localStorage.setItem('.draft-' + name, JSON.stringify({
                    lastModified: new Date(),
                    xml: msg.xml
                }));
            // When the user decides to save his diagram
            } else if (msg.event == 'save') {
                iframe.get(0).contentWindow.postMessage(JSON.stringify({
                    action: 'export',
                    format: 'xmlpng',
                    xml: msg.xml,
                    spin: 'Updating page'
                }), '*');
                localStorage.setItem('.draft-' + name, JSON.stringify({
                    lastModified: new Date(),
                    xml: msg.xml
                }));
            // When the user discard his diagram changes
            } else if (msg.event == 'exit') {
                localStorage.removeItem('.draft-' + name);
                draft = null;
                close();
            }
        }
    };

    window.addEventListener('message', receive);
    
    // Modal has to be created by JS to allow a better integration of the embed (issues were happening when it was added via PHP)
    // Only created on first edit
    if ($('#' + modalId).length == 0) {
        iframe.attr('src', editor);
        $('#content').append(''
            + '<div class="modal fade" id="' + modalId + '" tabindex="-1" role="dialog" aria-modal="true" style="display: block;">'
            + '<div class="modal-dialog modal-dialog-centered modal-xl" role="document">'
            + '<div class="modal-content rounded-0">'
            + '<div class="modal-body p-0" id="iframeContainer">'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>'
        );
        $('#iframeContainer').append(iframe);
    }

    $('#' + modalId).modal({backdrop : 'static'});
};