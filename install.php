<?php

/**
 *	HEADWORK database installer
 */


require("HWlib.php");
require("HTML.php");
require("profile.php");
require("skills.php");
require("tasks.php");
require("actions.php");
require("connection.php");
require("show-pages.php");

$conn = HWdbconnect();
restart($conn);
$VIEW = array("MAIN" => "HEADWORK database installation successful.".HTMLlink("index.php","Continue"));

require("template-main.php");
