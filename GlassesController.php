<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define( "APP_ROOT", realpath( dirname( __FILE__ ) ).'/' );

require_once APP_ROOT."lib/HWlib.php";
$sitebasepath=$_SERVER['DOCUMENT_ROOT'];
header("Content-Type: application/json; charset=UTF-8");

class GlassesController {
    private $db;

    function __construct() {
        $this->db=HWdbconnect();
    }

    function insert($PARAM){
        $msg=$PARAM['msg'];
        $msg=$this->db->quote($msg);

        $user=$this->db->quote($PARAM['user']);

        $statement=$this->db->prepare("insert into CrowdGlassesMsg(user,text) select name, :msg from Users where id=:user");
        $statement->bindParam(":msg",$msg,PDO::PARAM_STR);
        $statement->bindParam(":user",$user,PDO::PARAM_INT);
        $statement->execute();
        $answer=new StdClass();
        $answer->status = "Ok";
        return $answer;
    }

    function get($PARAM){
        if(isset($PARAM['longitude']) && isset($PARAM['latitude'])){
            $longitude=$PARAM['longitude'];
            $latitude=$PARAM['latitude'];

            @$statement=$this->db->prepare("insert into CrowdGlassesGPS(longitude,latitude) values (:long,:lat)");
            @$statement->bindParam(":long",$longitude,PDO::PARAM_STR);
            @$statement->bindParam(":lat",$latitude,PDO::PARAM_STR);
            @$statement->execute();
        }

        $myObj=new StdClass();
        $myObj->content=array();

       $table=$this->db->query("select id, user as name, text from CrowdGlassesMsg",PDO::FETCH_OBJ);
        foreach($table as $tuple)
            array_push($myObj->content,$tuple);
        return $myObj;
    }

    function getGPS($PARAM){
            $table=$this->db->query("select longitude,latitude from CrowdGlassesGPS order by id desc limit 1",PDO::FETCH_OBJ);
            $myObj=new StdClass();
            $myObj->content=$table->fetch();
            return $myObj;
        }
}


session_start();

$manager = new GlassesController();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $PARAM=$_REQUEST;
    if (isset($_SESSION['id']))
        $PARAM['user']=$_SESSION['id'];
    else
        $PARAM['user']="unknown";

    $result=json_encode($manager->$action($PARAM));
    echo $result;
}

?>
