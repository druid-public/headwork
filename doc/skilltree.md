* Skill Tree Artifact

Crowdsource the discovery of a tree of skills.

* Schema
** validskill(skill:string)
all validated skill with majority voting

** skillrelation(skill1<skill2,relation in {isa,asi,sameas},nbagree:positive int,nbvote:positive int)
the number of participants that agree with the relationship between skill1 and skill2, wrt the total number of votes

** skilltree(skill1,skill2,parent,sameas)
a materialization of the current skill tree, according to votes.

skill "any skill" is certain
please propose a skill : karate
do you think that karate is a skill ?
majority with support>3 => ok

draw:reask until majority with support>3

from the "any skill"
what is the relationship between karate and skill ?

building tree:
decide majority relations, no draw
from 'any skill':
add all that are direct isa


finish with all sameas


anyskill,karate,asi
anyskill,sport,asi
karate,isa sport

depth1:anyskill
karate->anyskill:1
sport->anyskill:1
musique->anyskill:1

karate->sport:2  ,2>1 we favor deeper relationships. Delete karate->* with lower depth

