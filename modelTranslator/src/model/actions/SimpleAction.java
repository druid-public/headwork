package model.actions;

import model.Parameters;

/**
 * A simple action is an action that can be executed in one time (update, creation, deletion)
 */
public abstract class SimpleAction extends Action {

    public SimpleAction(String guard, Parameters<Object> info) {
        super(guard, info);
    }

    public String writeFinishTask(){
        return "    },\n" +
               "    \"" + this.getId() + "\": {\n" +
               "        \"1\": {\n" +
               "            \"guard\": \"select true from dual\",\n" +
               "            \"actions\": []\n" +
               "        }\n" +
               "    },\n" +
               "    \"" + (this.getId()+1) + "\": {\n" +
               "        \"1\": {\n" +
               "            \"guard\": \"select true from dual\",\n" +
               "            \"actions\": []\n" +
               "        }\n";
    }

}
