package model.connections;

import com.mysql.cj.util.StringUtils;
import model.Parameters;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConnectDirectly implements DBConnector {

    private Connection connection;

    public ConnectDirectly(Connection connection){
        this.connection = connection;
    }

    public void sendToDb(String request) {

        try {
            Statement stm = connection.createStatement();
            stm.execute(request);
            //System.out.println(request);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getFromDb(String request) {
        try {
            Statement stm = connection.createStatement();
            //System.out.println(request);
            return stm.executeQuery(request);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Parameters> getPeers(String request){
        List<Parameters> peers = new ArrayList<>();
        try {
            // send the request
            ResultSet rs = this.getFromDb(request);
            // get how many columns compose the result of the request
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            //récupérer les résultats
            while (rs.next()) { //iterate on peers
                int idPeer = rs.getInt("idPeer");
                List<String> values = new ArrayList<>();
                List<String> attributes = new ArrayList<>();
                for (int col = 1; col <= columnCount; col++) {//iterate on attributes
                    String attribute = rsmd.getColumnLabel(col);
                    if (!attribute.equals("idPeer")) {
                        String value = rs.getString(col);
                        if (StringUtils.isStrictlyNumeric(value))
                            values.add(value);
                        else if (value.startsWith("[sql]"))
                            values.add(value.substring(5));
                        else
                            values.add("'" + value + "'");
                        attributes.add(attribute);
                    }
                }
                peers.add(new Parameters<>().addParam("idPeer",idPeer)
                        .addParam("values",values).addParam("attributes",attributes));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return peers;
    }

}
