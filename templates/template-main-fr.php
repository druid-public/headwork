<!DOCTYPE html>
<html>
    <head>
    <title>HEADWORK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://drvic10k.github.io/bootstrap-sortable/Contents/bootstrap-sortable.css" />
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://d3js.org/d3.v3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://drvic10k.github.io/bootstrap-sortable/Scripts/bootstrap-sortable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

    <!--  LeafLet -->
    <link rel="stylesheet"
        href="css/leaflet/leaflet.css"/>

    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="js/ext/leaflet/leaflet.js">
    </script>

    <script src="js/ext/cytoscape/dist/cytoscape.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.5/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/cytoscape-popper@1.0.2/cytoscape-popper.min.js"></script>

    <!-- Projet Viz stylesheet -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="https://unpkg.com/webcola/WebCola/cola.min.js"></script>
    <script src="js/cytoscape-cola.js"></script>
    <script src="js/howler.js"></script>



    <style>
        #cy {
            width: 100%;
            height: 600px;
            border: 1px solid #6c757d;
            border-radius: 5px;
            position: absolute;
            top: 4px;
            left: 0px;
        }
    </style>

    </head>

    <?php if(isset($_SESSION['mode'])&&!in_array($_SESSION['mode'],array("showTasks","answer","startArtifact","answer","insertAnswer"))):?>
        <body>
    <?php else: ?>
        <body  style="<?php echo file_get_contents("projects/".$_SESSION['project']."/background.css"); ?>">
    <?php endif;?>

	<nav class="navbar navbar-dark justify-content-between bg-dark flex-sm-row flex-column" style="background-color: black;">

	<!-- Navbar content -->

	<ul id="menu" class="nav nav-pills">
        <a class="navbar-brand" href="/">HEADWORK <span class="box">Beta<span></a>

	    <!--  if logged -->
        <?php if (isset($VIEW['ID'])): ?>
  		    <li class="nav-item dropdown">
    		    <a class="nav-link dropdown-toggle bg-dark navbar-dark" style="background-color: black;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Projets</a>
    		    <div class="dropdown-menu">
    	  		    <?php foreach($VIEW['PROJECT'] as $project): ?>
    				    <a class="dropdown-item" href="index.php?mode=showTasks&project=<?php echo $project ?>">
    				        <?php echo $project ?>
    			        </a>
    			    <?php endforeach; ?>
      		    </div>
  		    </li>
			<li class="nav-item"><a class="nav-link white" href="index.php?mode=showTasks">Contribuez !</a></li>
			<li class="nav-item"><a class="nav-link" href="index.php?mode=workflowdesign">Recrutez</a></li>
			<li class="nav-item"><a class="nav-link" href="index.php?mode=skills">Progressez</a></li>
			<li class="nav-item"><a class="nav-link" href="index.php?mode=showForum">Forum</a></li>
	       	<li class="nav-item"><a class="nav-link" href="index.php?mode=showFeedback">Retours</a></li>
            <!--    <li class="nav-item"><a class="nav-link" href="index.php?mode=showTasksViewer">Tasks viewer</a></li> -->

	    </ul>
		
        <ul id="menu-left" class="nav nav-pills ml-auto">
            <!--  if admin -->
            <?php if ($_SESSION['username']=="admin"): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle bg-dark navbar-dark" style="background-color: black;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Outils</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?mode=showState">Tableau de bord</a>
                    <a class="dropdown-item" href="index.php?mode=restart">Réinitialisation</a>
                </div>
            </li>
            <?php endif ?>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle bg-dark navbar-dark" style="background-color: black;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Connecté comme <?php echo $VIEW['ID']; ?></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?mode=profile">Edition du profil</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?mode=logout">Déconnexion</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle bg-dark navbar-dark" style="background-color: black;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">A propos</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="index.php?mode=showCredit">Crédits</a>
                    <a class="dropdown-item" href="https://headwork.irisa.fr/headwork-web">Le projet</a>
                    <a class="dropdown-item" href="https://gitlab.inria.fr/druid/headwork">Source <img src="images/gitlab-icon-rgb.png" width="30" height="30" alt=""></a>
                    <a class="dropdown-item" href="https://gitlab.inria.fr/druid-public/headwork/-/wikis/home">Wiki (EN)</a>
                    <a class="dropdown-item" href="documentation/html/files.html">Documentation</a>
                </div>
            </li>

            <li class="nav-item"><a class="nav-link" href="index.php?mode=toggleLang"><img src="images/flag-uk.png" width="30px" alt="french flag"/></a></li>
        </ul>

    <?php endif ?>
</nav>

    <main id="content" class="container" role="main">

    <!-- if logged -->
    <?php if (isset($VIEW['ID'])): ?>
        &nbsp;
        <div class="alert alert-success" id="feedbackAlert" role="alert" style="display: none;">Thank you for your feedback ! We will take it into consideration.</div>
		<button class="fixedFeedback btn btn-info" data-toggle="modal" data-target="#feedbackModal" tabindex="-1">Votre avis ?</button>
    <?php endif ?>

	<p/>

	<div class="HWmain">
        <?= $VIEW['MAIN'] ?>
    </div>

   <!-- Modal used to write down feedback -->
   <?php if (isset($VIEW['ID'])): ?>
       <div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Que devrions nous améliorer sur cette page ?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <textarea id="feedbackTextarea" class="form-control" rows="4"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-info" id="submitFeedbackBtn">Send feedback</button>
                    </div>
                </div>
            </div>
       </div>
   <?php endif ?>
   <!--
    <?php if (isset($VIEW['ID'])): ?>
	    <script src="js/feedback.js"></script>
	    <script src="js/chat.js"></script>
    <?php endif ?>
    -->
</main>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
    <script src="js/ext/bootstrap/bootstrap.js"></script>
        </body>
</html>
