-- Initialization commands for the HEADWORK database
-- The "headwork" database is supposed to exist
-- and this script is applied to it 
-- ALERT
-- ALERT
-- ALERT
-- ALERT
-- ALERT
-- ALERT     Destroys Users and Skills. Do not use in prod!
-- ALERT
-- ALERT
-- ALERT
-- ALERT
-- ALERT


-- Allow invariant dumps
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";

-- /* !40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
-- /* !40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
-- /* !40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
-- /* !40101 SET NAMES utf8mb4 */;

-- DROPing everything
DROP TABLE IF EXISTS CurrentArtifact;
DROP TABLE IF EXISTS Artifact;
DROP TABLE IF EXISTS BroadcastArtifactClass;
DROP TABLE IF EXISTS ArtifactClass;
DROP TABLE IF EXISTS Cities;
DROP TABLE IF EXISTS Skills;
DROP TABLE IF EXISTS Skylines;
DROP TABLE IF EXISTS UserProfile;
DROP TABLE IF EXISTS chat_topic;
DROP TABLE IF EXISTS forum_post_thanks;
DROP TABLE IF EXISTS forum_post;
DROP TABLE IF EXISTS forum_topic;
DROP TABLE IF EXISTS forum_category;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Template;
DROP TABLE IF EXISTS ArtifactUser;
DROP TABLE IF EXISTS TaskClass;
DROP TABLE IF EXISTS Task;
DROP TABLE IF EXISTS Answered;
DROP TABLE IF EXISTS ServiceCall;
DROP TABLE IF EXISTS Upload;

-- TODO missing other chat/forum tables, bad name format

-- User table
-- id: user id (primary key)
-- name : user name
-- hashed_password: password hashed with ?

DROP TABLE IF EXISTS Users;

CREATE TABLE IF NOT EXISTS Users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  hashed_password varchar(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Useful pre-installed Users
-- login/password
-- Demo/Demo
-- admin/admin
-- Demo2/Demo2
-- Oracle/Oracle

INSERT INTO `Users` (`id`, `name`, `hashed_password`) VALUES
(0, 'broadcast', 'qqdsd'), 
-- broadcast fake users for task dissemination -- not loggable
(2, 'admin', '$2y$10$xrJhB.lcgljZqpKiIKOuuu9BWyH0QzMbKwVyaC67.fIPS9rMP7X3S'),
(4, 'Oracle', '$2y$10$vcP/tqzkPLz419l9rONg.uB4FJYhXNOkEb0dg8R9RTcTCmxNjoura');
-- TODO WHAT ??
INSERT INTO `users`(`id`, `name`, `hashed_password`) VALUES ('35','idriss','idriss');
INSERT INTO `users`(`id`, `name`, `hashed_password`) VALUES ('36','yann','yann');
INSERT INTO `users`(`id`, `name`, `hashed_password`) VALUES ('37','safietou','safietou');
INSERT INTO `users`(`id`, `name`, `hashed_password`) VALUES ('38','jean','jean');
INSERT INTO `users`(`id`, `name`, `hashed_password`) VALUES ('39','rahima','rahima');
INSERT INTO `users`(`id`, `name`, `hashed_password`) VALUES ('40','stephane','stephane');
-- Answer table, stores all answer to a task
-- idtask: id of the answered task
-- user: the id of the user who answered
-- value: the value of the answer
-- mass: the probability or the belief of the value for the user
-- help: TODO ADRIEN
-- step: TODO One day, unify in json

-- Artifact: temporary

DROP TABLE IF EXISTS Answer;
CREATE TABLE IF NOT EXISTS Answer (
  idtask int(11) NOT NULL,
  user varchar(100) NOT NULL,
  artifact varchar(100) NOT NULL,
  value mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  mass float default 1,
  help json DEFAULT NULL,
  step int(11) DEFAULT NULL
-- TODO missing FK
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `availability`
--

-- DROP TABLE IF EXISTS availability;
-- CREATE TABLE IF NOT EXISTS availability (
--  iduser int(11) NOT NULL,
--  idday int(11) NOT NULL,
--  period int(11) NOT NULL,
--  KEY iduser (iduser)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Structure de la table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `idfeedback` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `page` varchar(128) NOT NULL,
  `text` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Structure de la table `chat_topic`
--

CREATE TABLE IF NOT EXISTS `chat_topic` (
  `id_topic` int(11) NOT NULL AUTO_INCREMENT,
  `id_task` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_topic`),
  KEY `forum_topic_ibfk_2` (`id_task`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Structure de la table `chat_post`
--

CREATE TABLE IF NOT EXISTS `chat_post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  PRIMARY KEY (`id_post`),
  KEY `id_topic` (`id_topic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

delete from chat_post;

-- --------------------------------------------------------

--
-- Structure de la table `forum_category`
--

CREATE TABLE IF NOT EXISTS `forum_category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `forum_category`
--

INSERT INTO `forum_category` (`id_category`, `title`, `description`) VALUES
(1, 'HEADWORK Platform', 'For all topics related to the platform'),
(2, 'Tasks', 'For all topics about specific tasks');
COMMIT;

-- --------------------------------------------------------

--
-- Structure de la table `forum_topic`
--

CREATE TABLE IF NOT EXISTS `forum_topic` (
  `id_topic` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) DEFAULT NULL,
  `id_task` int(11) DEFAULT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id_topic`),
  KEY `id_category` (`id_category`),
  KEY `id_task` (`id_task`),
  KEY `creator_id` (`creator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour la table `forum_topic`
--
ALTER TABLE `forum_topic`
  ADD CONSTRAINT `forum_topic_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `forum_category` (`id_category`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_topic_ibfk_2` FOREIGN KEY (`creator_id`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Structure de la table `forum_post`
--

CREATE TABLE IF NOT EXISTS `forum_post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `thanks` int(11) NOT NULL DEFAULT '0',
  `id_topic` int(11) NOT NULL,
  PRIMARY KEY (`id_post`),
  KEY `id_topic` (`id_topic`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour la table `forum_post`
--
ALTER TABLE `forum_post`
  ADD CONSTRAINT `forum_post_ibfk_1` FOREIGN KEY (`id_topic`) REFERENCES `forum_topic` (`id_topic`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_post_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Structure de la table `forum_post_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_post_thanks` (
  `id_post` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_post`,`id_user`) USING BTREE,
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour la table `forum_post_thanks`
--
ALTER TABLE `forum_post_thanks`
  ADD CONSTRAINT `forum_post_thanks_ibfk_1` FOREIGN KEY (`id_post`) REFERENCES `forum_post` (`id_post`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_post_thanks_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Structure de la table `Cities`
--

DROP TABLE IF EXISTS `Cities`;
CREATE TABLE IF NOT EXISTS `Cities` (
  `iduser` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- profile table, specifying for each task its required Skills
-- idtask: id of the task
-- idartifact: id of the corresponding artifact
-- idskill: id of the required skill
-- TODO Foreign key?

DROP TABLE IF EXISTS `Profile`;
CREATE TABLE IF NOT EXISTS `Profile` (
  `idtask` int(11) NOT NULL,
  idartifact int(11) NOT NULL,
  `idskill` int(11) NOT NULL,
  KEY `idtask` (`idtask`),
  KEY `idskill` (`idskill`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- result table
-- TODO DEPRECATED ?

-- DROP TABLE IF EXISTS `result`;
-- CREATE TABLE IF NOT EXISTS `result` (
--  `id` int(11) NOT NULL,
--  `description` varchar(100),
--  `value` varchar(500) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Skills`
--

DROP TABLE IF EXISTS `Skills`;
CREATE TABLE IF NOT EXISTS `Skills` (
  `iduser` int(11) NOT NULL,
  `idskill` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  KEY `iduser` (`iduser`),
  KEY `idskill` (`idskill`),
  PRIMARY KEY (iduser,idskill)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Skills`
--

INSERT INTO `Skills` (`iduser`, `idskill`, `level`) VALUES
(1, 1, 100),
(2, 1, 100),
(3, 1, 100),
(4, 1002, 100);

-- --------------------------------------------------------

--
-- Structure de la table `skillTree`
--

DROP TABLE IF EXISTS `SkillTree`;
CREATE TABLE IF NOT EXISTS `SkillTree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_skill` int(11) DEFAULT NULL,
  `skill` varchar(100) NOT NULL,
  `lien_esco` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_skill` (`parent_skill`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




--
-- Structure de la table `spipoll`
--

DROP TABLE IF EXISTS `spipoll`;
CREATE TABLE IF NOT EXISTS `spipoll` (
  `id` int(11) NOT NULL DEFAULT '0',
  `file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `spipoll`
--

INSERT INTO `spipoll` (`id`, `file`) VALUES
(1, 'http://indicia.mnhn.fr/indicia/upload/1528364634244.jpg'),
(2, 'http://indicia.mnhn.fr/indicia/upload/1528362153105.jpg');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS TaskClass (
       id int primary key,
       difficulty float
);

       
--
-- Structure de la table Task
--

CREATE TABLE IF NOT EXISTS Task (
  id int(11) NOT NULL,
  `artifact` int(11) NOT NULL,
   taskclass varchar(1000),
  `title` varchar(100) NOT NULL,
  `description` varchar(100),
  `type` int(11),
   modality ENUM ('crisp','belief') default 'crisp', 
  `body` varchar(10000) NOT NULL,
  `deadline` datetime,
  `timer` bigint(20),
  `duration` int(11),
  `reward` int(11),
  `applied` boolean default false,
  `checker` varchar(1000) DEFAULT NULL,
  `checkermsg` varchar(1000) DEFAULT NULL,
  `ajax` tinyint(1) NOT NULL DEFAULT '0',
  `help` tinyint(1) NOT NULL DEFAULT '0',
  `arg1` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `arg2` text,
  PRIMARY KEY (id,artifact)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `UserProfile`
-- availability: divider by 365 (how many day per year)

DROP TABLE IF EXISTS UserProfile;
CREATE TABLE IF NOT EXISTS UserProfile (
  `id` int(11) NOT NULL,
  `description` varchar(500),
  `availability` int(11),
  `wallet` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO UserProfile (`id`, `description`, `availability`, `wallet`) VALUES (2, 'admin', 0, 0);


-- Contraintes pour la table `Cities`
--
ALTER TABLE `Cities`
  ADD CONSTRAINT `Cities_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `Users` (`id`);

--
-- Contraintes pour la table `profile`
--
-- ALTER TABLE `profile`
--  ADD CONSTRAINT `profile_ibfk_01` FOREIGN KEY (`idtask`) REFERENCES `task` (`id`),
--  ADD CONSTRAINT `profile_ibfk_02` FOREIGN KEY (`idskill`) REFERENCES `skillTree` (`id`);

--
-- Contraintes pour la table `Skills`
--
-- ALTER TABLE `Skills`
--  ADD CONSTRAINT `Skills_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `Users` (`id`),
--  ADD CONSTRAINT `Skills_ibfk_2` FOREIGN KEY (`idskill`) REFERENCES `skillTree` (`id`);

--
-- Contraintes pour la table `skilltree`
--
-- ALTER TABLE `skilltree`
--  ADD CONSTRAINT `skilltree_ibfk_1` FOREIGN KEY (`parent_skill`) REFERENCES `skilltree` (`id`);

--
-- Contraintes pour la table `UserProfile`
--
-- ALTER TABLE `UserProfile`
  -- ADD CONSTRAINT `UserProfile_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Users` (`id`);

-- Template for tasks in HTML
-- id: Template id
-- body: Template body

CREATE TABLE Template (
  id varchar(100) NOT NULL PRIMARY KEY,
  body varchar(10000) DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `Template`(`id`, `body`)
VALUES(
   'spipoll',
   'Please propose an ORDER taxon for picture <br/>
<center><img width=\"200px\" src=\"$FILE\"></center>
<select id=\"myselect\" name=\"myselect\" onchange=\"myFunction()\">
<option value=\"Coleoptere\">Coleoptere</option>
<option value=\"Hymenoptere\">Hymenoptere</option>
<option value=\"Lepidoptere\">Lepidoptere</option>
<option value=\"Orthoptere\">Orthoptere</option>
<option value=\"Araneide\">Araneide</option>
<option value=\"Diptere\">Diptere</option>
<option value=\"Hemiptere\">Hemiptere</option>
</select>\r\n<script>
function myFunction(){
   var myDiv = document.getElementById("answer"); -- Conflict with SQL ";" Leave the -- for restart() function
   var select = document.getElementById("myselect").value; -- Conflict with SQL ";" Leave the -- for restart() function
   myDiv.value = select;}
</script>'
);

-- table ArtifactClass
-- Class of Artifacts
-- ID: unique Artifact ID
-- DESCRIPTION: full text description of the Artifact class
-- TABLENAME: the table related to the Artifacts of this class
-- DEFINITION: the path to a .sca file describing the Artifact automaton
-- PROJECT: the project of the Artifact class
-- autostart: is the artifact available at start for anyone (default no)

CREATE TABLE ArtifactClass(
	id varchar(100) PRIMARY KEY,
	description varchar(1000),
	tablename varchar(100),
	definition varchar(10000) NOT NULL,
	project varchar(250),
	autostart boolean default false NOT NULL
-- TODO should be a foreign key to a PROJECT table with description, authors, ...
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- table ArtifactUser
-- Specify which Artifact belongs to which user
-- id: ??
-- Artifact: the id of the considered Artifact
-- user_id: the id of the user of the Artifact
-- state: the current state of this Artifact
CREATE TABLE ArtifactUser(
	id int primary KEY AUTO_INCREMENT, -- TODO Useful Iandry ?
	Artifact int,
	user_id int,
	state varchar(10000) 
	-- TODO 10000 ?
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- table Artifact
-- ID: the unique id of the Artifact
-- CLASSID: the class ID of the Artifact
-- OWNERID: the ID of the user owning this Artifact TODO difference with ArtifactUser ?
-- NODE: the current node of the Artifact automaton
-- STATE: the current state of the Artifact (running, waiting, finished)
-- AWAITED: true if the Artifact is waited by another one (i.e. the current Artifact is a sub-Artifact) TODO right ?
-- ATTRIBUTES: ID of the tuple in the Artifact corresponding table
CREATE TABLE Artifact(
	id int PRIMARY KEY AUTO_INCREMENT,
	classid varchar(100) NOT NULL,
	ownerid int(11),
	node int NOT NULL,
	state ENUM ('running','waiting','finished') NOT NULL,
	awaited int,
	attributes int (11),
	FOREIGN KEY (classid) REFERENCES ArtifactClass(id),
	FOREIGN KEY (ownerid) REFERENCES Users(id)	
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- table SUBArtifactS, list the subArtifact relationships between Artifacts
-- idChild: the id of the child Artifact
-- idParent: the id of the parent Artifact
-- active: TODO remove, redundant with status (but used)
-- TODO: add foreign keys 
CREATE TABLE IF NOT EXISTS subArtifacts (
    idchild int(11) NOT NULL PRIMARY KEY,
    idparent int(11) NOT NULL,
     active bit NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- table CurrentArtifact, list the current selected Artifact
-- ID: the ID of the current Artifact of the user
-- NAME: table attached to the current Artifact 
-- USER: the ID of the user
-- TODO replace with an environ. variable
CREATE TABLE IF NOT EXISTS CurrentArtifact (
    id int NOT NULL,
    name varchar(100) NOT NULL,
    user int(11),
    FOREIGN KEY (id) REFERENCES Artifact(id),
	FOREIGN KEY (user) REFERENCES Users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Hack for changing of artefact TODO Beuark ! :)
-- TODO Deprecated: ALTER TABLE Task AUTO_INCREMENT = 1;


-- No longer needed ?
-- INSERT INTO task(title,description,body,checker,checkermsg) values ('Change current Artifact','Change the current Artifact','Change the current Artifact',null,'insert valid Artifact id');
-- insert into profile values (1,1);


-- table BroadcastArtifactClass, list all artifact classes that are available for instanciation for any user

CREATE TABLE BroadcastArtifactClass(
		classid varchar(100) PRIMARY KEY
--	TODO	FOREIGN KEY (classid) REFERENCES ArtifactClass (id)
);

CREATE TABLE Answered (
		id int NOT NULL, 
		artifact int NOT NULL			
);

CREATE TABLE ServiceCall (
        artifactid int NOT NULL,
        taskid int NOT NULL,
        userid int NOT NULL,
        tasklineid int NOT NULL,
        description varchar(100) NOT NULL
);

-- table Upload, list all the blobs corresponding to an audio or an image
CREATE TABLE Upload (
                        id int PRIMARY KEY auto_increment,
                        blobValue longblob,
                        name varchar(100),
                        type varchar(11)
);