<?php
    /**
    * HW library
    *
    */
    require_once("config.php");

    /**
    * If global constant DEBUG is true, echoes the parameter string. In any case, log the message.
    * @param String The debug message to print/log.
    *
    */
    function debug($msg){

        // get the calling function
        $ex = new Exception();
        $trace= $ex->getTrace();
        $calling="main";
        if (array_key_exists(1, $trace))
            if(array_key_exists('function',$trace[1]))
             $calling = $trace[1]['function'];
        
        $msg=$calling.":".$msg;
        $msgHTML=htmlentities($msg);
        // conditional web page output
//        if(DEBUG){
//            echo "<p><pre>DEBUG: $msg</pre></p>";
//            echo "<script type='text/javascript'>";
//            echo "  console.log(\"$msgHTML\");";
//            echo "</script>";
//          }
        // logfile output
        $handle = fopen(LOGFILENAME, "a+") or die('Cannot open log file '.LOGFILENAME);
        fwrite($handle, date("r").":$msg\n");
        fflush($handle);
    }

    
    
    /*
    * Convert a crowdy line into a crowd sql artifact
    * Crowdy language:
    * ask 10 "What is the height of Sandra Bullock ?"
    * take majority
    *
    * CROWDY := ASK | TAKE
    * ASK := ask INT STRING
    * TAKE := take AGGREGATE
    * AGGREGATE := majority
    * 
    */ 
   function crowdy2craft($line){
   	$token=strtok($line,' ');
	switch ($token){
	       case "ask":
			$res=readASK($line);
			break;
		default:$res="syntax error";
			break;
	}	
 }
  function readASK($line,$source,$sink,$max){
  	$next=$max+1;
   	$token=strtok($line,' ');
	if($token!="ask")
		return "first token: ask awaited";
	$count=strtok(' ');
	if (!is_numeric($count))
		return "requires a population";
	$count=intval($count);
	$question=strtok('\"');
	$artifact=array(
		  $source=>array(
            	      $next=>array(
			"guard"=>"select true from dual",
			"actions"=>[
				"insert into task(body) values ('$question')",
		          	"insert into profile (select id,'any' from task)"]),
				),			  
                  $next=>array(
                     $sink=>array(
	               "guard"=>"select count(*)>=$count from answer",
                       "actions"=>array("delete from task")
		       )
	             )
                   );
       return array($artifact,$next+1);
		 
}
    
    function HWdbconnect()
    {
        // database connection
            $conn = new PDO("mysql:host=".SERVERNAME.";dbname=".DATABASE."; charset=utf8", USERNAME, PASSWORD);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            debug("Connected successfully");
            return $conn;
    }

    function HWtestdbconnect()
    {
        // database connection
            $conn = new PDO("mysql:host=".SERVERNAME.";dbname=".TESTDATABASE."; charset=utf8", USERNAME, PASSWORD);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            debug("Connected successfully");
            return $conn;
    }

    /*
     * Given $conn, a valid PDO connection, returns the distinct list of projects from the set of artifact classes
     */
    function HWlistProjects($conn){
        $projects=array();
        $stmt = $conn->query("select distinct project from ArtifactClass");
        while($project=@$stmt->fetch()['project'])
            array_push($projects,$project);
        return $projects;
    }
?>
