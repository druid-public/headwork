-- install here your workflow

-- empty your project
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'ProjectTemplate');

-- empty your activities
DELETE from ArtifactClass where project = 'ProjectTemplate';

-- create your activities, see projectmap.txt to replace 14000
insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (14000,'Activity description','ProjectTemplate/example.sca', '<activity name>', 'ProjectTemplate',true);

-- create your local data if needed
