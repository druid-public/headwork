<?php

session_start();

require_once "lib/HWlib.php";
require "ChatModel.php";

$chat = new ChatController();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $chat->$action();
}

/**
 * As this class is designed to be used with Ajax, it is 'out' of the global engine (it's called directly, not using index.php)
 * Thus it needs its own session_start()
 * NB (if you plan to edit it) : It was not designed to be used by files other than js/chat.js
 */
class ChatController {

    private $model;

    function __construct() {
        $conn = HWdbconnect();
        $this->model = new ChatModel($conn);
    }

    /**
     * Main function of the class, creates the base HTML for the topic(s)
     */
    function getTopic() {
        $taskid = NULL;
        if (isset($_POST['task_id'])) {
            $taskid = $_POST['task_id'];
            debug("taskid $taskid");
        }
        $topicId = intval($this->model->getTopicIdFrom($taskid));
        if ($topicId == 0) {
            $topicId = intval($this->model->createTopicFor($taskid));
        }
        $topicData = $this->model->getTopicData($topicId);
        echo $this->generateHtml($topicData);
    }
    
    /**
     * Builds a JSON with all the posts from a topic whose ID is in POST request
     */
    function getPostsFromTopic() {
        if (isset($_POST['topic_id'])) {
            $topicId = $_POST['topic_id'];
            $posts = $this->model->retrievePostsFromTopic($topicId);
            echo json_encode($posts);
        }
    }

    /**
     * Adds a post to a topic whose ID is in POST request
     */
    function addPostToTopic() {
        if (isset($_POST['topic_id']) && isset($_POST['post_content'])) {
            $topicId = $_POST['topic_id'];
            $postContent = $_POST['post_content'];
            $userId = $_SESSION['id'];
            $insertDone = $this->model->postToTopic($postContent, $userId, $topicId);
            $array = array("success" => $insertDone);
            echo json_encode($array);
        }
    }


    /**
     * Function used to create the base HTML for the chat embed (displayed on the left side of the screen)
     */
    private function generateHtml($topic) {
        $html = "";
        // The button to toggle the display of the chat
        $html .= "<div id='chatContentDiv'>";
        $html .= "<div>";
        $html .= "<button id='chatBtn' class='btn btn-info' tabindex='-1' data-chat-display='false'>Chat</button>";
        // The main chat div
        $html .= "<div id='chatDiv' class='col-lg-3 col-md-4 col-sm-8 col-xs-12 chatBox text-left'>";
        // The alerts shown when the user succeeded / failed in sending a message
        $html .= "<div class='alert alert-success chatAlert' id='chatConfirmation' role='alert'>Your message was sent.</div>";
        $html .= "<div class='alert alert-danger chatAlert' id='chatFailure' role='alert'>An error has occurred.<br>Please try again later.</div>";
        // The nav menu to switch between project-level and task-level
        $html .= "<ul class='nav nav-tabs chatNavtabs' id='chatTabMenu' role='tablist'>";
        $html .= "<li class='nav-item'>";
        $html .= "<a class='nav-link active rounded-0' id='project-tab' data-toggle='tab' href='#chatProject' role='tab' aria-controls='chatProject' aria-selected='true'>Project</a>";
        $html .= "</li>";
        if (!is_null($topic['id_task'])) {
            $html .= "<li class='nav-item'>";
            $html .= "<a class='nav-link rounded-0' id='task-tab' data-toggle='tab' href='#chatTask' role='tab' aria-controls='chatTask' aria-selected='false'>Task</a>";
            $html .= "</li>";
        }
        $html .= "</ul>";
        // The chats
        $html .= "<div class='card chatCard border-top-0 tab-content'>";
        // The project chat
        $html .= "<div class='tab-pane active' id='chatProject' role='tabpanel' aria-labelledby='project-tab'>";
        $html .= "<ul class='list-group list-group-flush' id='chatProjectPostList'></ul>";
        $html .= "<ul class='list-group list-group-flush chatTextareaList'>";
        $html .= "<li class='list-group-item p-2'>";
        $html .= "<div class='input-group'>";
        $html .= "<textarea class='form-control' id='chatProjectTextarea' aria-describedby='chatSendProjectPost' rows='2' tabindex='-1'></textarea>";
        $html .= "<div class='input-group-append'><button class='btn btn-secondary' id='chatSendProjectPost'>Send</button></div>";
        $html .= "</div></li>";
        $html .= "</ul></div>";
        // The task chat
        if (!is_null($topic['id_task'])) {
            $html .= "<div class='tab-pane' id='chatTask' role='tabpanel' aria-labelledby='task-tab'>";
            $html .= "<ul class='list-group list-group-flush' id='chatTaskPostList'></ul>";
            $html .= "<ul class='list-group list-group-flush chatTextareaList'>";
            $html .= "<li class='list-group-item p-2'>";
            $html .= "<div class='input-group'>";
            $html .= "<textarea class='form-control' id='chatTaskTextarea' aria-describedby='chatSendTaskPost' rows='2' tabindex='-1'></textarea>";
            $html .= "<div class='input-group-append'><button class='btn btn-secondary' id='chatSendTaskPost'>Send</button></div>";
            $html .= "</div></li>";
            $html .= "</ul></div>";
            $html .= "</div>";
        }
        $html .= "</div>";
        $html .= "</div>";
        // Hidden inputs containing the IDs and number of posts for the Project / Task chats
        $html .= "<input type='hidden' id='chatProjectId' value='1'>";
        $html .= "<input type='hidden' id='chatProjectPostsNumber' value='0'>";
        if (!is_null($topic['id_task'])) {
            $html .= "<input type='hidden' id='chatTaskId' value='".$topic['id_topic']."'>";
            $html .= "<input type='hidden' id='chatTaskPostsNumber' value='0'>";
        }
        $html .= "</div>";
        return $html;
    }

}

?>