// The frequency used to check for a new task (in milliseconds)
var frequency = 10000;

/**
 * Submit form using Ajax
 */
$('#taskAnswerForm').on('submit.async', function(e) {
    e.preventDefault();
    var form = $(this);
    var previousId = $(':input[name="id"]').val();
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action') + "?" + $(':input[name="mode"]').val(),
        data: form.serialize(),
        dataType: "html",
        success: function(page) {
            // We must unbind the vent to avoid multiple same binds
            $('#taskAnswerForm').off('submit.async');
            var newId = $(page).find('#taskAnswerForm').children(':input[name="id"]').val();
            if (previousId != newId) {
                if (typeof newId != "undefined") {
                    var newContent = $(page).filter('#content');
                    fillInNewTask(newContent);
                } else {
                    fillWaiting();
                    checkForNextTask(previousId);
                }
            } else {
                var refreshedForm = $(page).find('#taskAnswerForm');
                $('#taskAnswerForm').empty().append(refreshedForm.html());
            }
        }
    });
});

/**
 * Replace the content of the page with the new task one
 * @param {jQuery} newContent The #content div of the new page
 */
function fillInNewTask(newContent) {
    $('#content').fadeOut().delay(400).queue(function() {
        $(this).empty().append(newContent.html());
        history.replaceState(null, null, "index.php?mode=answer&choice=" + $(':input[name="id"]').val());
        initializeChat();
        $(this).dequeue();
    }).fadeIn();
}

/**
 * Replace the content of the page with a waiting alert, calling another function
 */
function fillWaiting() {
    $('#content').fadeOut().delay(400).queue(function() {
        $(this).empty().append('<div class="row" id="waitingDiv">'
            + '&nbsp;<div class="col-12 text-center">'
            + '<div class="alert alert-primary" role="alert">'
            + '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'
            + '<span>&nbsp;Please wait while we are gathering answers from everyone</span>'
            + '</div></div></div>'
        );
        history.replaceState(null, null, "index.php?mode=answer&choice=waiting");
        initializeChat();
        $(this).dequeue();
    }).fadeIn();
}

/**
 * Continuously check for a new task ID until the retrieved ID is greater than the current one
 * @param {int} idToCheck The ID of the submitted task
 */
function checkForNextTask(idToCheck) {
    intervalId = setInterval(function() {
        $.ajax({
            type: "POST",
            url: "AjaxManager.php",
            data: {action: 'getLastestTaskId', task: idToCheck},
            success: function(data) {
                var jsonData = JSON.parse(data);
                if (jsonData['lastTask'] > idToCheck) {
                    clearInterval(intervalId);
                    retrieveTaskHtml(jsonData['lastTask']);
                }
            }
        });
    }, frequency);
}

/**
 * Retrieve the new task HTML and change the page content
 * @param {int} newTask The new task ID, retrieved from checkForNextTask
 */
function retrieveTaskHtml(newTask) {
    $.ajax({
        type: "POST",
        url: "index.php?mode=answer&choice=" + newTask,
        success: function(page) {
            var newContent = $(page).filter('#content');
            fillInNewTask(newContent);
        }
    });
}