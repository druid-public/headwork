# New features related changes

## Answers types

### Files

**`index.php`** :  
Added a new case : `insertTaskFeedback`, it enables the possibility to ask for feedback when the user choose a specific answer.

**`show-pages.php`** :  
`answer` function : contains multiple switch statements to add the custom HTML on the page with its required JavaScript if needed

**`actions.php`** :
* `insertAnswer` function : check if the received answer is a JSON (some custom answers send data through JSON) and builds an insert query string
* `insertTaskFeedback` function : insert the user feedback **for a specific answer of a task** into a custom table

**`HTML.php`** :  
New functions have been created that generate the custom HTML for new answers : `HTMLselect`, `HTMLradio`, `HTMLradiolist`, `HTMLtextarealist` and `HTMLmultiplecards` functions.

**`JS files`** :  
New JS files have been created to enable the JSON building for multiple answers types : `multiplecards.js`, `radiolist.js` and `textarealist.js`.  
Another file exist only to allow the diagram creation and edit : `diagram.js`.

**`style.css`** :  
Added classes `.diagramIframe`, `.diagramImage` and `.selectedImage` for the diagram display and selection.

### Database

New fields have been added to the `task` table :
* `type` : can be *NULL*, integer type
* `arg1` : can be *NULL*, varchar type
* `arg2` : can be *NULL*, varchar type

See `answer_types.md` for more informations.  

#### Ajax

Ajax has been introduced into the task answers to enable a smoother experience, without going back to the task selection list.

##### Files

**`index.php`** :  
`insertAnswer` case : added a new parameter for the Ajax, `false` by default.

**`actions.php`** :  
`insertAnswer` function : with the new Ajax parameter, if true, call the `answer` function to get the new task HTML, managed by the JS script.

**`show-pages.php`** :  
`answer` function : a new hidden input has been created to allow easier JS management.

**`AjaxController.php`** :  
Its role is to send the latest task ID to the JS script.  
It wasn't possible to do that in a single function in the main files as they all include `template-main.php`, thus increasing the data size to send.

**`ajaxtask.js`** :  
Manage all the interactions instead of the standard behaviour (page changes) and replace the page content with the new task.

##### Database

The `ajax` field has been added to the `task` table, it uses boolean/tinyint type. To enable it, the field must be set to `true`/`1`.

#### Help, resource linking

An introduction the resource linking has been created and can be used in the type `6`.  
If you wish to implement that for other types, please see edited files below.  
Pay attention to the all the files and the implementation example with type `6`. It should not be hard to expand it for other types as some functions already exist for it, the biggest work would be to find a proper adaptation to the existing display.

##### Files

**`index.php`** :  
`answer` case : added a new parameter for the linking, `false` by default.

**`actions.php`** :  
`insertAnswer` function : with the new parameter, if true, the optional inputs are processed and inserted into a new field of the `answer` table.

**`show-pages.php`** :  
`answer` function : a small change has been made to use the resource linking in the `case 6` of the `switch ($line['type'])` statement. A new hidden input has been created to allow easier JS management.

**`HTML.php`** :  
2 new functions have been created :
* `HTMLResourceInputs` : creates the optional inputs, called by `HTMLtextarealist` as an example
* `HTMLHelpingResource` : using previously saved values from the inputs, creates a direct link to the ressource and give the page number if there is one. Called by `HTMLmultiplecards` as an example

**`textarealist.js`** :  
Adujstements have been made to add the optional values into the submitted JSON.

##### Database

The `help` field has been added to the `task` table, it uses boolean/tinyint type. To enable it, the field must be set to `true`/`1`.  
NB : it only work with type `6` at the time of this commit.


## Timer

### Files

**`show-pages.php`** :  
`answer` function : a very small portion of code have been added to include the required HTML and to disable the submit button (auto-submit on time = 0).

**`HTML.php`** :  
A function have been created to generate the base HTML used by the JS script.

**`timer.js`** :  
Using Moment.js, the script generates a countdown. When it reaches 0, the task try to submit itself.

### Database

A new field have been added to the `task` table :  
`timer`, can be *NULL*, integer type, stores a unixtime (need to see if `deadline` and `timer` can be merged)


## Feedback

### Files

**`index.php`** :  
2 cases have been added to the switch statement :
* `submitFeedback` : call the right function to insert the feedback into the database
* `showFeedback` : call the right function to display a part of the feedback (using pagination)

**`actions.php`** :  
`submitFeedback` function : a simple SQL statement that insert the feedback into the database

**`show-pages.php`** :
* `showFeedback` function : creates a HTML table wich displays the user, the page, its feedback and the date of the sent feedback
* `generateFeedbackModal` function : generate a modal to display the whole feedback if it is too long

**`feedback.js`** :  
Manage the submit part of the feedback.

**`template-main.php`** :  
Added a link to the feedback page and a button, displaying the feedback modal, on the right side of the screen.

**`style.css`** :  
Added the class `.fixedFeedback` for the button.

### Database

A new table have been created :  
`feedback`, it contains the user id, the page, its feedback and the date of the submit


## Chat

### Files

**`ChatController.php`** :  
Creates the base HTML depending of the page the user is on and contains functions that are called using Ajax.

**`ChatModel.php`** :  
Contains all the functions to send or retrieve data with the database.

**`chat.js`** :  
Retrieves chat content when the page has been loaded and display the button when its done.  
Also updates the chat every *n* seconds or when the user submit a message.

**`style.css`** :
Added some classes for the chat : `.chatCard`, `.chatDate`, `.chatAlert`, `.chatNavtabs` and `.chatTextareaList`.  
Also added style for main chat IDs : `#chatBtn` and `#chatDiv`.

**`template-main.php`** :  
Added the script when the user is logged in.

### Database

2 new tables have been created :
* `chat_topic` : very simple, contains an auto incremented ID and the task ID (*NULL* for the project)
* `chat_post` : contains the expected informations about a post and its topic ID

### Possible improvements

For now, the entire chat is loaded with no message cap. If thousands of messages exist, it could lead to performance issues.  
Moreover, the Ajax script checks every 15 seconds if at least one message has been sent by another user for every connected user. It could be too heavy for the server to handle if many users are logged in.  
To improve this feature, a limit should be implemented and the new message loading should occur only if another user sent a message.


## Forum

### Files

**`index.php`** :  
5 new cases have been added to the switch statement :
* `showForum` : call a function to display a part of the forum, depending on the parameters
* `insertPostForum` : call a function to insert a post into a topic
* `showTopicCreation` : call a function to display the topic creation page
* `insertNewTopic` : pass the parameters from the topic creation page to create a new topic
* `searchForum` : allow the user to look for a topic, looking in the title and its content

**`show-pages.php`** :  
* `showForum` : call the controller to generate the HTML depending on the parameters
* `showTopicCreation` : call the controller to display the topic creation page
* `searchForum` : call the controller to search for the user string

**`actions.php`** :
* `insertPostForum` : call the controller to insert a new post in a topic
* `insertNewTopic` : call the controller to insert the new topic

**`ForumController.php`** :  
Contains all the functions that interact with the database and generate the HTML to include in the template.

**`ForumModel.php`** :  
Contains all the functions to send or retrieve data with the database.

**`forum.js`** :  
Initialize the TinyMCE textarea (WYSIWYG editor) and manage the display of task list on the topic creation page.

**`template-main.php`** :  
Added a link to the forum in the navigation bar.

### Possible improvements

* Implement a in-topic search engine that will only look into posts from the topic
* Adding a report feature to alert admins about an unwelcomed post / topic
* Adding the possibility for admins to delete posts and topics
* Creating a permalink for each post of a topic, thus implementing a post-only view in a topic
* Find a better way for the only Ajax call to have access to the user id (`session_start()` in `managePostThanks()` of `ForumController.php`)


## ISNLP

Some content exist only for ISNLP artifacts.

### Files

**`isnlp.php`** :  
File that allows the user to see the final subject and the feedback some user wrote for him.  
It also generates downloadable LaTeX and HTML files from the subject.  
Available pages : `isnlp.php?mode=feedback` and `isnlp.php?mode=answers`.

### Database

A 'permanent' table exists that is not reset on restart :  
`isnlp_query`, it contains every query that have been chosen and its correction