<?php

// TODO here convert into drawArtifact function
require_once("lib/artifacts.php");

//Fonction récursive pour convertir un tableau d'artifact en ligne sugarfree
function recursiveConvertSugarFree(array $data, array &$convertedData = [],$conn){
    foreach ($data as $key=>$val){
        if(is_array( $val)){
            $convertedData[$key] = [];
            recursiveConvertSugarFree($val, $convertedData[$key],$conn);
        }else{
            $convertedData[$key] = sugarFree($val,$conn,true);
        }
    }
    return $convertedData;
}

function drawArtifact($conn,$artifactid){
    global $VIEW;
    $stmt_artifact = $conn->query("select definition,node from Artifact, ArtifactClass where Artifact.id=$artifactid and Artifact.classid=ArtifactClass.id")->fetch();
    $currentNode=$stmt_artifact['node'];
    $artifact_file = $stmt_artifact['definition'];
    $loadedArtifact = load_artifact($artifact_file);
    $jsonString=json_encode($loadedArtifact);

    $init=[];
    $jsonStringSugarFree = json_encode(recursiveConvertSugarFree($loadedArtifact,$init,$conn));

    $VIEW['MAIN'].=<<<EOF
<div class="container">
    <div class="workflow-tittle">
        <div class="row">
            <div class="col bg-secondary text-white">
                <div id="hw-artefact-info">placeholder
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col bg-secondary text-white">
                <div id="hw-artefact-author">placeholder
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div id="cy"></div>
            <div>
                <label class="legend-label">Current node</label><div class="legend-curent"></div>
                <label class="legend-label">Visited node</label><div class="legend-visited"></div>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <input type="checkbox" id="toggleId" checked data-toggle="toggle" data-on="Sugar" data-off="Sugar free" data-onstyle="success" data-offstyle="danger">
            </div>
            <div id="info" style="position: relative;top: 5px; left: 20px;">(move your mouse on nodes and arrows for infos)</div>
        </div>
    </div>
</div>
<script type="text/javascript">


EOF;
$VIEW['MAIN'].="var jsonString=`".$jsonString."`;";
$VIEW['MAIN'].="var jsonStringSugarFree=`".$jsonStringSugarFree."`;";
$VIEW['MAIN'].=<<<'EOF'

var obj;
try {
    obj = JSON.parse(jsonString);
    objSugarFree = JSON.parse(jsonStringSugarFree);
    } catch(e) {
        console.log("error"+jsonString);
        alert(e); // error in the above string (in this case, yes)!
    }

var cy = cytoscape({
  container: document.getElementById('cy'),
  elements: [
],
    style: [
        {
            selector: 'node',
            style: {
                shape: 'ellipse',
                'background-color': 'black',
                label: 'data(id)',
                'text-wrap': 'wrap',
                height:'40px',
                width:'40px',
                'text-halign': "center",
                'text-valign': "center",
                'color':"white"
            }
        },
         {
      selector: ':selected',
      css: {
        'background-color': '#17a2b8',
        'line-color': '#17a2b8',
        'target-arrow-color': '#17a2b8',
        'source-arrow-color': '#17a2b8'
      }
    },
        {
            selector: 'node[id="node1"]',
            style: {
                'background-color': 'green'
            }
        },
        {
            selector: 'edge',
            style: {
                'curve-style':'straight',
                'line-color': 'black',
                'target-arrow-shape': 'vee',
                'target-arrow-fill':'filled',
                'target-arrow-color':'black',
                width:3
            }
        },
    ],
    layout: { name: 'grid'}
});

for (var i in obj){
    if (i!="author" && i!="doc" && i!="nodetip" && i!="result" && i!="adminview"){
        cy.add({
            data: { 
                id: i,
                nodetip: obj[i]["nodetip"]
            }
        });
    }
}

for (var i in obj){
    if (i!="author" && i!="doc" && i!="nodetip" && i!="result" && i!="adminview"){
        for (var j in obj[i]){
            if(j!="nodetip" ){
                cy.add({
                    data: {
                        id: i + '-' +j,
                        source: i,
                        target: j,
                        guardtip: obj[i][j]["guardtip"],
                        actiontip: obj[i][j]["actiontip"]
                    }
                }); 
            }
        };
    }
}


cy.layout({
    name: 'breadthfirst',
    directed: 'true',
    padding: 10,
    avoidOverlap: true
}).run();

document.getElementById("hw-artefact-info").innerHTML = obj['doc'];
document.getElementById("hw-artefact-author").innerHTML = 'Author: '+obj['author'];


// Popover sur edge et node ---------------------------------------------------------------
	function bindPopper(target) {
		let tooltipId = `popper-target-${target.id()}`;
		let existingTarget = document.getElementById(tooltipId);
		if (existingTarget && existingTarget.length !== 0) {
			existingTarget.remove();
		}

		let popper = target.popper({
			content: () => {
				// create div container
				let tooltip = document.createElement('div');
				
				// adding id for easier JavaScript control
				tooltip.id = tooltipId;

				// adding class for easier CSS control
				tooltip.classList.add('target-popper');

				// create actual table
				let table = document.createElement('table');

				// append table to div container
				tooltip.append(table);
				let targetData = target.data();

				// loop through target data
				for (let prop in targetData) {
					if (!targetData.hasOwnProperty(prop)) continue;

					let targetValue = targetData[prop];
					// no recursive or reduce support
					if (typeof targetValue === "object") continue;
					
					if(prop !== "nodetip" && prop !== "guardtip" && prop !== "actiontip") continue;

					let tr = table.insertRow();

					//let tdTitle = tr.insertCell();
					let tdValue = tr.insertCell();

					//tdTitle.innerText = prop;
					tdValue.innerText = targetValue;
				}

				document.body.appendChild(tooltip);

				return tooltip;
			}
		});

		target.on('position', () => {
			popper.scheduleUpdate();
		});

		target.cy().on('pan zoom resize', () => {
			popper.scheduleUpdate();
		});

		target.on('mouseover', () => {
			if (document.getElementById(tooltipId)) {
				document.getElementById(tooltipId).classList.add('active');
			}
		}).on('mouseout', () => {
			if (document.getElementById(tooltipId)) {
				document.getElementById(tooltipId).classList.remove('active');
			}
		})
	}
	
	cy.filter('node, edge').forEach(t => {
	    this.bindPopper(t);
    });
//-----------------------------------------------------------------------

//--modifcation de couleur vers bleu---------------------------------------------------------------------

let tab = ['1'];
//let visitedNode = ['1','2']; //vert clair pour visité
let visitedNode = ['1']; //vert clair pour visité

EOF;
$VIEW['MAIN'].="let currentNode='".$currentNode."';";
$VIEW['MAIN'].=<<<'EOF'


// let currentNode = '3'; //vert foncé pour noeud courant


function toRed(){
    visitedNode.forEach(element =>{
      cy.$('#' + element).animate({
       style: {
      'background-color': '#90EE90',
        'line-color': '#90EE90',
        'target-arrow-color': '#90EE90',
        'source-arrow-color': '#90EE90',
    }
  });   
    });
  cy.$('#' + currentNode).animate({
       style: {
      'background-color': '#006400',
        'line-color': '#006400',
        'target-arrow-color': '#006400',
        'source-arrow-color': '#006400',
    }
  });   
}

toRed();

cy.bind('click', 'node', function(node) {

toRed();

 /*node.target.predecessors().nodes().animate({
    style: {
      'background-color': '#17a2b8',
        'line-color': '#17a2b8',
        'target-arrow-color': '#17a2b8',
        'source-arrow-color': '#17a2b8',
    }
  });*/
  
  /*node.target.predecessors().edges().animate({
    style: {
      lineColor: "#17a2b8",
      'target-arrow-color':'#17a2b8',
    }
  });*/
   
   node.target.successors().edges().animate({
    style: {
      lineColor: "black",
      'target-arrow-color':'black',
    }
  });
  
});

//----------------------------------------------------------------------

// Set min and max zoom-------------------------------------------------
    cy.minZoom( 0.5 );
    cy.maxZoom( 1.5 );
//----------------------------------------------------------------------

// make sugar-toggle appear-------------------------------------------------
function sugaryToggleFunction() {
    document.getElementsByClassName("toggle")[0].setAttribute("style", "display:inline-block;width:120px;margin-left:20px;top:4px;margin-bottom:3px;");
    // document.getElementsByClassName("toggle")[0].className += "custom-toggle";
  }
//----------------------------------------------------------------------
cy.on('unselect', 'edge', function(evt) {

evt.target.animate({
     style: {
                'curve-style':'straight',
                'line-color': 'black',
                'target-arrow-shape': 'vee',
                'target-arrow-fill':'filled',
                'target-arrow-color':'black',
                width:3
            }
  });

});


var toggleElement = document.getElementById('toggleId');


cy.on('click', 'edge', function(evt){
  sugaryToggleFunction();
  evt.target.animate({
     style: {
                'curve-style':'straight',
                'line-color': '#17a2b8',
                'target-arrow-shape': 'vee',
                'target-arrow-fill':'filled',
                'target-arrow-color':'#17a2b8',
                width:3
            }
  });
  
  var edge = evt.target;
  split = edge.id().split("-");
  source=split[0];
  target=split[1];

  actionString="";
  actionStringSugarFree="";
  
  var actions=[];
  var actionsSugarFree=[];
  try {
    actions=obj[source][target]['actions'];
    actionsSugarFree=objSugarFree[source][target]['actions'];
  }
  catch(e) {
    actions=[];
    actionsSugarFree=[];
  }
  
  for(var i=0;i<actions.length;i++){
        if (actions[i].startsWith("--")){
            actionString+=actions[i].substring(2);
            actionStringSugarFree+=actionsSugarFree[i].substring(2);
        }
        else{
            actionString+='<div class ="alert alert-info div-artifact-info"><p class="info-style">'+actions[i]+'</p></div>';
            actionStringSugarFree+='<div class ="alert alert-info div-artifact-info"><p class="info-style">'+actionsSugarFree[i]+'</p></div>';
        }
      }
      
      var isSugarHidden = '';
      var isSugarFreeHidden = 'sugar-hidden';
      
      if (!toggleElement.checked) {
      isSugarHidden = 'sugar-hidden';
      isSugarFreeHidden = '';
      }
      
      document.getElementById("info").innerHTML = ` 
      
      <div class="artifact-infos ${isSugarHidden}" id="info-sugar">
      <div class ="alert alert-secondary div-artifact-title"><h1 class="tittle-style">State ${source} <i class="fa fa-arrow-right fa-xs"></i> State ${target}</h1></div>
      <div class ="alert alert-secondary div-artifact-title"><h2 class="tittle-style">Guard</h2></div>
      <div class ="alert alert-info div-artifact-info"><p class="info-style">${obj[source][target]['guard']}</p></div>
      <div class ="alert alert-secondary div-artifact-title"><h2 class="tittle-style">Actions</h2></div>
      ${actionString} </div>
      
      <div class="artifact-infos ${isSugarFreeHidden}" id="info-sugarfree">
      <div class ="alert alert-secondary div-artifact-title"><h1 class="tittle-style">State ${source} <i class="fa fa-arrow-right fa-xs"></i> State ${target}</h1></div>
      <div class ="alert alert-secondary div-artifact-title"><h2 class="tittle-style">Guard</h2></div>
      <div class ="alert alert-info div-artifact-info"><p class="info-style">${objSugarFree[source][target]['guard']}</p></div>
      <div class ="alert alert-secondary div-artifact-title"><h2 class="tittle-style">Actions</h2></div>
      ${actionStringSugarFree} </div>
      `;

});

toggleElement.onchange = function() {
    
    if (this.checked) {
        document.getElementById('info-sugarfree').classList.add('sugar-hidden'); 
        setTimeout('document.getElementById("info-sugarfree").style.display=\'none\'', 100); 
        
        document.getElementById('info-sugar').classList.remove('sugar-hidden'); 
        setTimeout('document.getElementById("info-sugar").style.display=\'block\'', 100); 
        
    }
    else {
        document.getElementById('info-sugar').classList.add('sugar-hidden'); 
        setTimeout('document.getElementById("info-sugar").style.display=\'none\'', 100); 
        
        document.getElementById('info-sugarfree').classList.remove('sugar-hidden'); 
        setTimeout('document.getElementById("info-sugarfree").style.display=\'block\'', 100);
        
    }
}

</script>
EOF;

}


/**
 * showArtifact
 *
 * Set the content of the HTML page with the content returned by the artifact
 *
 * @param $conn: a valid PDO connection
 * @param $artifactid: the id of a running artifact
 */


function showArtifact($conn,$artifactid)
{

    global $VIEW;

	// aller chercher le nom du fichier .sca
    $stmt_artifact = $conn->query("select definition from Artifact, ArtifactClass where Artifact.id=$artifactid and Artifact.classid=ArtifactClass.id")->fetch();
    $artifact_file = $stmt_artifact['definition'];

    // lire le fichier, l'encoder en JSON avec mise en forme, et affichage dans une balise PRE

    $artifact=load_artifact($artifact_file);
    $jsonString=htmlentities(json_encode($artifact,JSON_PRETTY_PRINT));
    $VIEW['MAIN'].='</p></p></p><a class="btn btn-dark" role="button" href="http://localhost:8888/headwork-master/index.php?mode=drawArtifact&artifactid='.$artifactid.'">View automaton</a><p/>';
    $VIEW['MAIN'] .= HTMLpre($jsonString);
//  TODO sugar-free version
// $actions=$artifact["1"]["2"]["actions"];
//    foreach($actions as $action)
//        $VIEW['MAIN'] .= HTMLpre(sugarFree($action));

}
