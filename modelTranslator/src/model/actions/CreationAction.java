package model.actions;

import model.Parameters;

/**
 * Action that creates an other artifact
 */
public class CreationAction extends SimpleAction {
    private String newArtifact;
    private Parameters<String> newArtAttributes;

    public CreationAction(String guard, Parameters<Object> parameters) {
        super(guard, parameters);
        this.newArtifact = (String) parameters.get("newArtifact");
        this.newArtAttributes = (Parameters<String>) parameters.get("newArtAttributes");
        super.info.addParam("action",this.writeAction());
    }

    public String writeMethod(){
        StringBuilder fct = new StringBuilder("    public "+ newArtifact +" create"+newArtifact+"(");
        for (String attr : newArtAttributes.keySet()){
            fct.append(newArtAttributes.get(attr)).append(" ").append(attr).append(", ");
        }
        fct.append("DBConnector connection){\n" +
                "        Parameters<Object> parameters = new Parameters<>();\n");
        for (String attr : newArtAttributes.keySet()){
            fct.append("        parameters.addParam(\"").append(attr).append("\",").append(attr).append(");\n");
        }
        fct.append("        parameters.addParam(\"idPeer\",idPeer);\n");
        fct.append("        return new ").append(newArtifact).append("(parameters, connection);\n    }\n");
        return fct.toString();
    }

    /**
     * The actions are :
     *   - insert the new artifact in the table of its class of artifact
     *   - insert the new artifact in the table ARTIFACT
     * @return
     */
    public String writeAction() {
        String idtask = "(select id from task where title='"+this.info.get("title")+"' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER))";

        //insert the new artifact in the table of its class of artifact
        StringBuilder insertNameArtifact = new StringBuilder("insert into "+newArtifact+"Artifact(");

        /*
        List<String> values = new ArrayList<>();
        values.add("select value from answer where idtask=" + idtask + ")");
        for (int i=1; i<newArtAttributes.size();i++){
            values.add("null");
        }
        //SqlCommands.insert(newArtifact+"Artifact",newArtAttributes.keySet(),values);
         */

        Boolean first = true;
        for (String attribute:newArtAttributes.keySet()) {
            if (!first)
                insertNameArtifact.append(", ");
            first = false;
            insertNameArtifact.append(attribute);
        }
        insertNameArtifact.append(") values ((select value from answer where idtask=").append(idtask).append(")");
        for (int i=1; i<newArtAttributes.size();i++){
            insertNameArtifact.append(",null");
        }
        insertNameArtifact.append(");");

        //insert the new artifact in the table ARTIFACT
        String insertArtifact = "insert into ARTIFACT(classid,ownerid,node,state,awaited, attributes) values (" +
                Math.abs(this.newArtifact.hashCode()) + "," +
                "(select user from answer where idtask=" + idtask + ")," +
                "1," +
                "'running'," +
                "null," +
                "(select max(id) from " + newArtifact + "Artifact));";

        return insertNameArtifact.toString() + insertArtifact;
    }

}
