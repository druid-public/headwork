<?php

require_once("lib/Action.php");

class Credits extends Action {

  function render(){
    $this->showCredit();
  }

  function showCredit(){
    global $VIEW;
    $VIEW['MAIN'] = file_get_contents(tpath("templates/credit",".html"));
  }
}
?>
