-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: headwork
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `templateSkills`
--

DROP TABLE IF EXISTS `templateSkills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templateSkills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `niv_taxon` varchar(150) DEFAULT NULL,
  `parent` varchar(255) DEFAULT NULL,
  `body` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templateSkills`
--

LOCK TABLES `templateSkills` WRITE;
/*!40000 ALTER TABLE `templateSkills` DISABLE KEYS */;
INSERT INTO `templateSkills` VALUES (1,'ORDRE','spipoll','<table style=\"width:50%\">\n<caption>ORDRE SKILLS</caption>\n<tr>\n<th>Skills</th>\n<th>Level</th>\n</tr> \n<tr>\n<td>Orthoptères</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_19\" name=\"level[19]\" style=\"width:50%\"> <span id=\"19\"></span></td>\n</tr> \n<tr>\n<td>Coléoptères</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_25\" name=\"level[25]\" style=\"width:50%\"> <span id=\"25\"></span></td>\n</tr> \n<tr>\n<td>Lépidoptères</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_209\" name=\"level[209]\" style=\"width:50%\"> <span id=\"209\"></span></td>\n</tr> \n<tr>\n<td>Hémiptères</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_747\" name=\"level[747]\" style=\"width:50%\"> <span id=\"747\"></span></td>\n</tr> \n<tr>\n<td>Hyménoptères</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_795\" name=\"level[795]\" style=\"width:50%\"> <span id=\"795\"></span></td>\n</tr> \n<tr>\n<td>Aranéides</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_885\" name=\"level[885]\" style=\"width:50%\"> <span id=\"885\"></span></td>\n</tr> \n<tr>\n<td>Diptères</td> \n<td><input type=\"range\" min=\"0\" max=\"100\" value=\"0\" class=\"slider\" id=\"myRange_896\" name=\"level[896]\" style=\"width:50%\"> <span id=\"896\"></span></td>\n</tr> \n</table> \n <script> \nvar slider_19 = document.getElementById(\"myRange_19\");\nvar output_19 = document.getElementById(\"19\");\noutput_19.innerHTML = 0;\nslider_19.oninput = function()  {\noutput_19.innerHTML = this.value;}\nvar slider_25 = document.getElementById(\"myRange_25\");\nvar output_25 = document.getElementById(\"25\");\noutput_25.innerHTML = 0;\nslider_25.oninput = function()  {\noutput_25.innerHTML = this.value;}\nvar slider_209 = document.getElementById(\"myRange_209\");\nvar output_209 = document.getElementById(\"209\");\noutput_209.innerHTML = 0;\nslider_209.oninput = function()  {\noutput_209.innerHTML = this.value;}\nvar slider_747 = document.getElementById(\"myRange_747\");\nvar output_747 = document.getElementById(\"747\");\noutput_747.innerHTML = 0;\nslider_747.oninput = function()  {\noutput_747.innerHTML = this.value;}\nvar slider_795 = document.getElementById(\"myRange_795\");\nvar output_795 = document.getElementById(\"795\");\noutput_795.innerHTML = 0;\nslider_795.oninput = function()  {\noutput_795.innerHTML = this.value;}\nvar slider_885 = document.getElementById(\"myRange_885\");\nvar output_885 = document.getElementById(\"885\");\noutput_885.innerHTML = 0;\nslider_885.oninput = function()  {\noutput_885.innerHTML = this.value;}\nvar slider_896 = document.getElementById(\"myRange_896\");\nvar output_896 = document.getElementById(\"896\");\noutput_896.innerHTML = 0;\nslider_896.oninput = function()  {\noutput_896.innerHTML = this.value;}\n</script>');
/*!40000 ALTER TABLE `templateSkills` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-14 13:54:24
