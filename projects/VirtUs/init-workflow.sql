
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'VirtUs');
DELETE from ArtifactClass where project = 'VirtUs';

insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (9000,'Indentify realistic bodies','VirtUs/virtus.sca', 'Artifact', 'VirtUs',true);

drop table if exists VirtUsVideo;
create table VirtUsVideo(url text);
insert into VirtUsVideo values
            ("TST_big_Walk_skinny0001-0063.mp4"),
            ("TST_male_Throw_female0001-0120.mp4");
