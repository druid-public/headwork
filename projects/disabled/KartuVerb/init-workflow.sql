
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'KartuVerb');
DELETE from ArtifactClass where project = 'KartuVerb';

DROP TABLE IF EXISTS AnswerButton;
CREATE TABLE AnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO AnswerButton values (1,'Mauvaise','danger');
INSERT INTO AnswerButton values (2,'À revoir','warning');
INSERT INTO AnswerButton values (3,'Parfaite','success');


insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (8000,'Masdar','KartuVerb/masdar.sca', 'Artifact', 'KartuVerb',true);
