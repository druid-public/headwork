<?php

/**
 * Store the user feedback in database
 * @param PDO $conn a valid PDO connection to database
 * @param string $page the path of the affected page
 * @param string $feedbackText the feedback given by the user
 */
function submitFeedback($conn, $page, $feedbackText)
{
    $user=$_SESSION['id'];
    debug("feedback : $user $page $feedbackText");
    $stmt = $conn->prepare("INSERT INTO feedback (iduser, page, text) VALUES (:iduser, :page, :text)");
    $stmt->execute(array(
        ":iduser" => $user,
        ":page" => $page,
        ":text" => $feedbackText
    )); // or die(mysql_error());
}

/**
 * Store the user question/task feedback in a specific table in database
 * @param PDO $conn a valid PDO connection to database
 * @param int $idtask the task id targeted by the feedback
 * @param string $feedbackText the feedback
 * @param string $targetTable the name of the database table which contains the fedback
 */
function insertTaskFeedback($conn, $idtask, $feedbackText, $targetTable)
{
    $user=$_SESSION['id'];
    $stmt = $conn->prepare("INSERT INTO `$targetTable` (idtask, feedback, iduser) VALUES (:idtask, :text, :iduser)");
    $stmt->execute(array(
        "idtask" => $idtask,
        "text" => $feedbackText,
        "iduser" => $user
        ));
}

/**
 * Display a table which contains every user feedback, with pagination (see limit if you want to increase the number of row displayed)
 *
 * @param PDO $conn
 *            a valid PDO connection
 * @param int $page
 *            the page to display
 */
function showFeedback($conn, $pageNo)
{
    // Creating the base table with headers
    global $VIEW;
    $page = HTMLh1("User feedback");
    $page .= "<table class='table table-hover table-bordered'>";
    $page .= "<thead class='thead-dark'><tr><th scope='col'>User</th><th>Page</th><th>Feedback</th><th>Date (Y/m/d)</th></tr></thead>";
    $page .= "<tbody>";

    // Retrieving the feedback in range
    $limit = 10; // The number of rows we want on one page
    $start = ($pageNo - 1) * $limit;
    $stmt = $conn->prepare("select u.name, f.idfeedback, f.page, f.text, f.date from feedback f, Users u where u.id = f.iduser order by f.idfeedback asc limit :limit offset :start");
    $stmt->bindParam("limit", $limit, PDO::PARAM_INT);
    $stmt->bindParam("start", $start, PDO::PARAM_INT);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Retrieving the total number of feedback for the pagination
    $stmt2 = $conn->prepare("SELECT COUNT(*) AS 'total_feedback' FROM feedback");
    $stmt2->execute();
    $nbTotal = $stmt2->fetchColumn();

    // Filling the table & creating modals for text too long
    $modalArray = array();
    foreach ($results as $row) {
        $fbText = $row['text'];
        $displayedText = "";
        if (strlen($fbText) > 54) {
            $displayedText = HTMLtagblock("a", array(
                "href" => "#",
                "style" => "color: black;",
                "data-toggle" => "modal",
                "data-target" => "#moreInfoFeedback" . $row['idfeedback']
            ), substr($fbText, 0, 42) . "… (click to see more)");
            array_push($modalArray, generateFeedbackModal($row));
        } else {
            $displayedText = $fbText;
        }
        $page .= "<tr><td>" . $row['name'] . "</td><td>" . htmlLinkNewPage($row['page'], $row['page']) . "</td><td>" . $displayedText . "</td><td>" . $row['date'] . "</tr>";
    }
    $page .= "</tbody></table>";
    foreach ($modalArray as $modal) {
        $page .= $modal;
    }

    // Creation of pagination links
    $leftBtn = HTMLtagblock("a", array(
        "class" => ($pageNo == 1 ? "btn btn-dark disabled" : "btn btn-dark"),
        "href" => ($pageNo == 1 ? "#" : "index.php?mode=showFeedback&page=" . ($pageNo - 1))
    ), "&lt;");
    $middleBtn = HTMLtagblock("button", array(
        "class" => "btn btn-dark"
    ), $pageNo);
    $rightBtn = HTMLtagblock("a", array(
        "class" => ($pageNo * $limit >= $nbTotal ? "btn btn-dark disabled" : "btn btn-dark"),
        "href" => ($pageNo * $limit >= $nbTotal ? "#" : "index.php?mode=showFeedback&page=" . ($pageNo + 1))
    ), "&gt;");
    $page .= HTMLdiv($leftBtn . $middleBtn . $rightBtn, array(
        "class" => "btn-group",
        "role" => "group"
    ));

    $VIEW['MAIN'] .= $page;
    $conn = null;
}

/**
 * Generates a modal which contains the whole text if it is too long
 */
function generateFeedbackModal($queryRow)
{
    return '<div class="modal fade" id="moreInfoFeedback' . $queryRow['idfeedback'] . '" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5>' . $queryRow['name'] . '\'s suggestion for "' . $queryRow['page'] . '"</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
                <p>' . $queryRow['text'] . '</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>';
}

?>