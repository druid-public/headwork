const ID = require('../models/id.model')
const EVENT = require('../models/event.model')
const con = require('../models/connection.model')
const parameters = require('../config/parameters')
const Bowser = require('bowser')
var moment = require('moment')
const UserData = require('../UserData')
const IDs = require('../ID.json')
module.exports = {

    firstime:  (req,res) => {
        if(req.cookies.finished == undefined)
            res.render('index')
        else
            res.render('error',{message : "Vous ne pouvez réaliser l'expérience qu'une seule fois."})
        
    }
    ,
    /**
     * Displays the index if the number of user is not achieve, if it's not the case it will displays an error page. 
     */

    index :  (req,res) => {
        ID.countUser(con, moment(new Date()).format("YYYY-MM-DD HH:mm:ss") , (error, rows )=> {
            if(error){
                console.error(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " " + error )
                console.log(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " Could not count users")
                res.render('error',{message:error})        
            }else if(rows.length != 1){
                console.log(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " Unexpected line count")
                res.render('error',{message:"Unexpected line count"})        
            }else if(rows[0].numberOfUsers >= parameters.maxUser){
                res.render('error',{message:"The maximum number of paid participants has been reached. Some of the current participants may not finish the experiment, therefore some slots may be available if you check again later."})
                console.log(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " Max users reached (" + rows[0].numberOfUsers + "), user rejected")       
            }else{
                console.log(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " Currently " + rows[0].numberOfUsers + " users, new user allowed.")
                let data = {}
                data.loginf8 = req.body.login
                if(Object.values(IDs).includes(data.loginf8)){
                    res.render('error',{message:"Vous avez déjà réalisé cette expérience, où une expérience similaire vous ne pouvez réaliser celle-ci."})
                }
                data.user = (Math.random()*2147483647) | 0
                ID.insert(con, data,  (error) => {
                    if(error){
                        console.log(error)
                        // if the user id is already taken
                        if(error.code = "ER_DUP_ENTRY"){
                            data.user = (Math.random()*2147483647) | 0
                            ID.insert(con, data,  (err) => { if(err) throw err
                                else{
                                         EVENT.insert(con, data.user, moment(new Date()).format("YYYY-MM-DD HH:mm:ss"), "start", (error)=>{
                                             if(error){
                                                 if(error.code == "ER_DUP_ENTRY"){
                                                     UserData.restoreStatus(data.user,res)
                                                 }
                                                console.error(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " " + error);
                                                console.log(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " Could not log start");
                                                res.render('error',{message:error})
                                             }else{
                                                res.render('instruction',{user : data.user})
                                             }
                                         })
                                }
                            })
                        }
                    }else{
                        EVENT.insert(con, data.user, moment(new Date()).format("YYYY-MM-DD HH:mm:ss"), 'start', (error)=>{
                            if(error){
                                if(error.code == "ER_DUP_ENTRY"){
                                    UserData.restoreStatus(data.user,res)
                                }
                               console.error(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " " + error);
                               console.log(moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + " Could not log start");
                               res.render('error',{message:error})
                            }else{
                               res.render('instruction',{user : data.user})
                            }
                        })
                    }
                })
            }
        })
    }
}