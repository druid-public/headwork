<?php

/**
 * showTasks
 *
 * Produce the HTML view of the list of available activities and tasks for the current user, ranked according to skill and profile matching
 *
 * @param $conn : A valid database PDO connection
 * @param $displayAlert : display a confirmation if the user has submitted a task
 */
function showTasks($conn, $displayAlert = false)
{
    global $VIEW;

    $id = $_SESSION["id"];

    if (isset($_GET['project'])) {
        $_SESSION['project'] = $_GET['project'];
        $_SESSION['activity_desc'] = null;
    } else if (!isset($_SESSION['project']))
        $_SESSION['project'] = DEFAULTPROJECT;
    if (isset($_GET['activitydesc']))
        $_SESSION['activity_desc'] = $_GET['activitydesc'];
    else if (!isset($_SESSION['activity_desc']))
        $_SESSION['activity_desc'] = null;
    if (isset($_GET['activityid'])) {
        $_SESSION['activity_id'] = $_GET['activityid'];
        update($conn, $_SESSION['activity_id']);
    } else if (!isset($_SESSION['activity_id']))
        $_SESSION['activity_id'] = null;

    $VIEW["MAIN"] .= "<div id='container'>";

    $presentationFilename = "projects/" . $_SESSION['project'] . tpath("/presentation",".html");
    if (file_exists($presentationFilename))
        $VIEW["MAIN"] .= file_get_contents($presentationFilename);

    $VIEW["MAIN"] .= "<div id='activity-list'>";
    if ($_SESSION['project'] != null) {
        startAllArtifact($conn, $_SESSION['id']);
        showRunningActivities($conn);
    }
    $VIEW["MAIN"] .= "</div>"; // for activity-list

    $VIEW["MAIN"] .= "<div id='task-list'>";

    if ($_SESSION['activity_desc'] != null) {

        $query = "SELECT Task.id, Task.description, 20 as level From Task,Artifact,ArtifactClass
                 where
                    Artifact.classid=ArtifactClass.id
                    and ArtifactClass.description='" . $_SESSION['activity_desc'] . "'
                    and Task.artifact=Artifact.id
                    and Artifact.id=" . $_SESSION['activity_id'] . "
                    and Artifact.state='running'
                    and Artifact.ownerid=" . $_SESSION['id'] . " order by Artifact.classid,Artifact.id";

        $query = "SELECT Task.id, Task.description,sum(level) From Task,Artifact,ArtifactClass, Profile,Skills
                 where
                    Artifact.classid=ArtifactClass.id
                    and ArtifactClass.description='" . $_SESSION['activity_desc'] . "'
                    and Task.artifact=Artifact.id
                    and Artifact.id=" . $_SESSION['activity_id'] . "
                    and Artifact.state='running'
                    and Artifact.ownerid=" . $_SESSION['id'] . "
                    and Skills.iduser=" . $_SESSION['id'] . "
                    and Profile.idtask=Task.id and Profile.idartifact=Artifact.id and Profile.idskill=Skills.idskill
                    group by Task.id, Task.description
                    order by Artifact.classid,Artifact.id desc";

        showTaskList($conn, t("Then Contribute!"), $query, $displayAlert);
    }

    $VIEW["MAIN"] .= "</div>"; // for task

    $VIEW["MAIN"] .= "</div>"; // for container
}

/**
 * showRunningActivities
 *
 * Showing running artifacts of the user
 *
 * @param $conn : A valid database PDO connection
 *
 */
function showRunningActivities($conn)
{
    global $VIEW;
    $table = $conn->query("select description, Artifact.id, concat('index.php?mode=showTasks&activitydesc=',description,'&activityid=',Artifact.id),'button' from Artifact,ArtifactClass where project='" . $_SESSION['project'] . "' and Artifact.classid=ArtifactClass.id and Artifact.state='running' and Artifact.ownerid=" . $_SESSION['id'] . " order by Artifact.classid, Artifact.id");
    $mylist = $table->fetchAll(PDO::FETCH_NUM);

    $query = "select tablename from ArtifactClass where ArtifactClass.description='" . $_SESSION['activity_desc'] . "'";

    $table = $conn->query($query);
    if ($table->rowcount() > 0)
        $tablename = $table->fetch()['tablename'];
    else {
        debug("No tablename");
        $tablename = null;
    }

    if (!$tablename) {
        debug("No tablename given, switching to Default");
        $tablename = "Artifact"; // by default, show the content of the Artifact table
    }

    $VIEW["MAIN"] .= HTMLh1(t("Choose your activity..."));
    if ($mylist)
        $VIEW["MAIN"] .= BSArtifactList($mylist, $_SESSION['activity_id'], $conn);
    else
        $VIEW["MAIN"] .= "<table class='table table-striped table-bordered'><tr><td>Sorry, no activity available for now.</td></tr></table>";
    $VIEW["MAIN"] .= HTMLbr();
}

/**
 * showTaskList
 *
 * Display the task list returned by the query
 *
 * @param $conn :
 *            A valid database PDO connection
 * @param $query :
 *            A table of
 *            query selecting the tasks to show

 * @param $displayThanks :
 *            boolean enabling the display of a thanks alert or not
 */
function showTaskList($conn, $title, $query, $displayThanks = false)
{
    $query_pertinence = "select Min(pertinence_min) as pertinence_min 
        from(
        select tb.skill_user,tb.skill_requis,Min(pertinence) as pertinence_min
            From
            (SELECT Skills.idskill as skill_user,Profile.idskill as skill_requis,
                --  Min(MGM(Profile.idskill,Skills.idskill)) as pertinence
                1 as pertinence
                    From Task,Artifact,ArtifactClass,Profile,Skills
                        where Artifact.id=" . $_SESSION['activity_id'] . "
			    		and Artifact.classid=ArtifactClass.id
				    	and Task.artifact=Artifact.id
                        and Artifact.state='running'
                        and Artifact.ownerid=" . $_SESSION['id'] . "
					    and Skills.iduser=" . $_SESSION['id'] . "
                        and Profile.idtask=:id_task 
                        and Profile.idartifact=Artifact.id 

            Group by Skills.idskill,Profile.idskill) as tb
            where pertinence is not null
            group by tb.skill_user, tb.skill_requis) as ush limit 1;";
    global $VIEW;
    $res = "";

    //if ($displayThanks) {
    //    $res .= "&nbsp;<h2 class=\"alert alert-success\" role=\"alert\">Thanks for your answers</h2>";
    //}
    $res .= HTMLh1($title);

    $header = HTMLtr(array(
        HTMLth(""),
        HTMLth(t("Task Description")),
        HTMLth(t("Relevance Score for You"))
    ));

    $stmt = $conn->prepare($query);
    $stmt->execute();

    $stmt_pertinence = $conn->prepare($query_pertinence);


    $form = HTMLinput("hidden", "mode", "answer");
    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);


    $rowcount = 0;

    while ($line = ($stmt->fetch())) {
        $rowcount++;
        $row = "";
        $pertinence=[];
        foreach ($line as $k => $v) {
            if ($k == "id") {
                $stmt_pertinence->execute(array('id_task'=>$v));

                $pertinence= $stmt_pertinence->setFetchMode(PDO::FETCH_ASSOC);

                $temp = "<button type='submit' name='choice' value='$v' class='btn btn-dark'>" . t("Start!") . "</button>";
            } else {
                $temp = $v;

            }
            $row .= HTMLtd($temp);
        }

/* progress bar
        while($f = ($stmt_pertinence->fetch()))
            if(is_null($f['pertinence_min'])){
                $row.=HTMLtd(t("Missing"));
            }else{
                $row.=HTMLtd("<div class='progress'> <div class='progress-bar bg-info' role='progressbar'
                               style='width: ".((1-$f['pertinence_min'])*100)."%'"." aria-valuenow='".(1-$f['pertinence_min'])."' aria-valuemin='0'
                               aria-valuemax='1'>".$f['pertinence_min']."</div></div>");
               // $row.=HTMLtd($f['pertinence_min']);
            }
*/
        $form .= HTMLtr(array(
            $row
        ));

    }

    $finalform = "<table class='table table-striped table-bordered'><tr><td>Sorry, no task today.</td></tr></table>";

    if ($rowcount > 0) {
        $finalform = HTMLtable(array(
            $header,
            $form
        ), array(
            "class" => "table table-fixed table-striped"
        ));
    }
    $res .= HTMLform("index.php", "GET", $finalform);

    $VIEW['MAIN'] .= HTMLdiv($res);
    $VIEW['MAIN'] .= HTMLtagblock("script", array(
        "src" => "js/taskrefresh.js"
    ), "");
}

/**
 * searchTotalTasks
 *
 * Function to search through a JSON the number of total tasks from a project
 *
 * @param $array :
 *              The JSON in which we search for the total tasks
 */
function searchTotalTasks($array): int
{
    static $counter = 0;
    foreach($array as $val) {
        if (is_array($val)) {
            searchTotalTasks($val);
        } else {
            if (strpos($val,"insert into Task") !== false || strpos($val,"install task") !== false) {
                $counter ++;
            }
        }
    }
    return $counter;
}

/**
 * createProgress
 *
 * Display a progress bar to tell the user about the task completion
 *
 * @param $totalTime :
 *            The total time remaining to complete the task
 * @param $id :
 *            The id of the current task
 * @param $totalTasks :
 *            The number of tasks to be completed
 */
function createProgress($totalTime, $id, $totalTasks) {
    $res="";
    $res.= "<link href='css/progressBar.css' rel='stylesheet' type='text/css'>";

    $totalTime -= $id;
    $res.="
<div class='text_a'>Step : $id / $totalTasks</div>
<div class='text_b'>Remaining : $totalTime min</div>
<div class='barbox_a'></div>
<div class='bar blank'></div>
<div class='per'>0%</div>
";
    return $res;
}

/**
 * answer
 *
 * Display the answer form
 *
 * @param $id :
 *            The id of the task to display
 * @param $error :
 *            if true, print the checker message (typically, wrong input type on previous answer)
 */
function answer($conn, $id, $error = false)
{
    global $VIEW;

    $_SESSION['taskStart']=microtime(true);

    $stmt = $conn->prepare("select * from Task where id=:id and artifact=:artifactid");
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':artifactid', $_SESSION['activity_id'], PDO::PARAM_INT);
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $line = $stmt->fetch();

    $str = HTMLinput("hidden", "id", $id);

    if ($line['ajax']) {
        $str .= HTMLinput("hidden", "ajax", "true");
    }
    if ($line['help']) {
        $str .= HTMLinput("hidden", "help", "true");
    }
    debug("id $id");

    /*    $str .= "
    <nav aria-lbel='breadcrumb'>
      <ol class='breadcrumb'>
        <li class='breadcrumb-item'>" . $_SESSION['project'] . "</li>
        <li class='breadcrumb-item' aria-current='page'>" . $_SESSION['activity_desc'] . "</li>
      </ol>
    </nav>";
    */

    //--------------------Progress Bar Section--------------------
    $stmt = $conn->prepare("select definition from ArtifactClass, Artifact, Task
        where Task.artifact = Artifact.id
        and Artifact.classid = ArtifactClass.id
        and Task.id=:id and Task.artifact=:artifactid");
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':artifactid', $_SESSION['activity_id'], PDO::PARAM_INT);
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $line3 = $stmt->fetch();

    $json = file_get_contents('projects/'.$line3['definition']);
    $json_data = json_decode($json,true);

    if (isset($line3['definition'])) {


        $totalTasks = searchTotalTasks($json_data);
        $totalTime = $totalTasks;


        /**
         * updateProgress
         *
         * It will generate a new div to overlay the current ones
         *
         * @param $percent :
         *                 The percent of the progress bar
         */
        function updateProgress($percent) {
            $res="";
            $res.= "<div class='per'>$percent %</div>\n";
            $res.="<div class='bar' style='width: ".($percent * 3)."px'></div>\n";
            return $res;
        }

//        $str .= createProgress($totalTime, $id, $totalTasks);
//        $str .= updateProgress(round(($id/$totalTasks) * 100));
    }
    //--------------------End of Progress Bar Section--------------------

    //--------------------Redirect "Browser Button Back" when answering a task Section--------------------
    $str .= <<<EOT
<script type="text/javascript">
(function(window, location) {
    history.replaceState(null, document.title, location.pathname+"#!/noHistory");
    history.pushState(null, document.title, location.pathname);

    window.addEventListener("popstate", function() {
      if(location.hash === "#!/noHistory") {
            history.replaceState(null, document.title, location.pathname);
            setTimeout(function(){
              location.replace("index.php?mode=showTasks");
            },0);
      }
    }, false);
}(window, location));
</script>
EOT;
    //--------------------End of the "redirect" Section--------------------

    $str .= HTMLh1($line['description']);

    // if the task begins with file:, show the file content
    // deprecated: better use the Template table
    $nbmatch = preg_match('/^file:(.*)$/', $line['body'], $matches);
    if ($nbmatch == 0)
        $str .= HTMLh3($line['body']);
    else // $nbmatch<>0
        $str .= file_get_contents($matches[1]);

    // We need to do different action depending of chosen type
    switch ($line['type']) {
        case null:
            // echo ' type null </br>';
            $str .= HTMLp(HTMLtextarea("answer", "60", "3", "", ""));
            break;

        default:
            //echo ' type default </br>';
            $placeholder = ($line['arg1'] !== null ? $line['arg1'] : "");
            $str .= HTMLp(HTMLtextarea("answer", "60", "3", "", $placeholder));
            break;
        case 0: // hidden 'answer' input, used to display text
            $str .= HTMLtag("input", array(
                "type" => "hidden",
                "name" => "answer"
            ));
            break;
        case 1: // select HTML element
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLp(HTMLselect("answer", $result2));
            break;
        case 2: // radio buttons HTML elements
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLp(HTMLradio("answer", $result2, false));
            break;
        case 4: // radio buttons HTML elements with custom answer enabled
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLp(HTMLradio("answer", $result2, true));
            break;
        case 5: // HTML list containing a text and radio buttons as choices
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $stmt3 = $conn->prepare($line['arg2']);
            $stmt3->execute();
            $result3 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLdiv(HTMLradiolist("answer", $result2, $result3), array(
                "class" => "col-12"
            ));
            break;
        case 6: // HTML clickable list displaying a textarea bound to each list element
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLtextarealist("answer", $result2, $line['help']);
            break;
        case 7: // Cards displaying multiple possible answers
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $stmt3 = $conn->prepare($line['arg2']);
            $stmt3->execute();
            $result3 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLmultiplecards("answer", $result2, $result3);
            break;
        case 8: // Embedded draw.io, displaying an image which, on click, displays the draw.io editor
            $result2 = $line['arg1'];
            $str .= HTMLdrawio($result2);
            break;
        case 9: // Multi-choice among images
            $stmt2 = $conn->prepare($line['arg1']);
            $stmt2->execute();
            $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $str .= HTMLimagechoice("answer", $result2);
            break;
        case 10: // Wirk users validation code
            $str .= "Votre code de validation Wirk est 666.";
            $str .= HTMLtag("input", array(
                "type" => "hidden",
                "name" => "answer"
            ));
            break;
        case 11: // JSON generic answer
            break;
        case 12:
            // Sound Record template
            $str .= file_get_contents('templates/soundRecord.tmpl');
            break;
    }

    $str .= "<center>";

    if ($error) {
        $str .= "<div class='alert alert-danger' role='alert'>" . $line['checkermsg'] . "</div>";
    } else {
        $str .= "<tr><td>" . HTMLp($line['checkermsg']) . "</td></tr>";
    }
    $str .= "</center>";

    // If the task allows uncertain answers, display the input modality
    if ($line['modality'] == 'belief') {
        $str .= <<<EOT
        <center>
        <br/><br/>
        <div class="alert alert-info" role="alert">
        This task allows to qualify your certainty about your answer. Do not hesitate do be uncertain if so (I will not alter your contribution, on the contrary, it will be more precise)  </div>

        I am: <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.05">
          <label class="form-check-label" for="inlineRadio1">totally<br/>uncertain</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.20">
          <label class="form-check-label" for="inlineRadio2">uncertain</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.35">
          <label class="form-check-label" for="inlineRadio3">slightly <br/>uncertain</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.5">
          <label class="form-check-label" for="inlineRadio3">neither certain<br/>nor uncertain</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.65">
          <label class="form-check-label" for="inlineRadio3">slightly<br/>certain</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.80">
          <label class="form-check-label" for="inlineRadio3">certain</label>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="mass" id="mass" value="0.95" checked>
          <label class="form-check-label" for="inlineRadio3">totally<br/>certain</label>
        </div>
        <br/>
        <br/>
        </center>
EOT;
    }

    // Disabling the submit button for JSON tasks or or timed tasks
    if ($line['timer'] === null && $line['type'] != 11 && $line['type'] != 12) {
        $str .= HTMLcenter(HTMLinput("submit", "name", "submit", null, false, 'btn btn-dark'));
    }

    //---------------Add "Ask a friend!" button for each task---------------
    //Used to send a mail with the project's name, activity's name and step's name to somebody
    //If the user need someone to help him answer a question
    $selectMailTo = $conn->prepare("select ArtifactClass.project, ArtifactClass.description, Task.description as etape
from Task, Artifact, ArtifactClass
where Task.artifact = Artifact.id
and Artifact.classid = ArtifactClass.id
and Task.id = " . $id . "
and Task.artifact = "  . $_SESSION['activity_id'] . ";");
    $selectMailTo->execute();

    foreach ($selectMailTo as $row) {
        $str .=  <<<EOT
<div class="d-grid gap-2 d-md-flex justify-content-md-end">
<button class="btn btn-info btn-lg me-md-2" onclick="event.stopPropagation();
    event.preventDefault(); 
    window.location.href='mailto:?subject=Help%20on%20Headwork%20project!&body=Hello!%0D%0A%0D%0APlease%2C%20could%20you%20help%20me%20to%20answer%20this%20question%3F%0D%0A%0D%0AHere%20is%20a%20link%20to%20the%20website%20%3A%20headwork.irisa.fr%0D%0A%0D%0AInformation%20about%20the%20question%20%3A%0D%0A%0D%0AProject%20%3A%20{$row['project']}%0D%0A%0D%0AActivity%20name%20%3A%20{$row['description']}%0D%0A%0D%0AStep%20name%20%3A%20{$row['etape']}';">
Ask a friend!
</button>
<!-- Button trigger modal for the report abuse-->
<button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Report abuse
</button>
</div>
EOT;
    }
    //---------------End of "Ask a friend!" button section---------------

    //---------------"Report abuse" button section---------------
    $selectReportAbuse = $conn->prepare("select ArtifactClass.project, ArtifactClass.description, Task.description as etape
from Task, Artifact, ArtifactClass
where Task.artifact = Artifact.id
and Artifact.classid = ArtifactClass.id
and Task.id = " . $id . "
and Task.artifact = "  . $_SESSION['activity_id'] . ";");
    $selectReportAbuse->execute();

    foreach ($selectReportAbuse  as $row) {
        $str .= <<<EOT

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Report abuse</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      
        <div id="formReport">
            <div class="row mb-3">
                <label for="inputProject" class="col-sm-2 col-form-label">Project</label>
                <div class="col-sm-10">
                    <input class="form-control-plaintext" id="project" type="text" name="project" value="{$row['project']}" readonly>
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputActivity" class="col-sm-2 col-form-label">Activity</label>
                <div class="col-sm-10">
                    <input class="form-control-plaintext" id="activity" type="text" name="activity" value="{$row['description']}" readonly>
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputStep" class="col-sm-2 col-form-label">Step</label>
                <div class="col-sm-10">
                    <input class="form-control-plaintext" id="etape" type="text" name="etape" value="{$row['etape']}" readonly>
                </div>
            </div>
            <div class="row mb-3">
                <select class="form-select bs-tooltip-auto" id="category" name="category">
                    <option selected>Select the category</option>
                    <option value="Spam">Spam</option>
                    <option value="Malware">Malware</option>
                    <option value="Phishing">Phishing</option>
                    <option value="Violence">Violence</option>
                    <option value="Hate Speech">Hate Speech</option>
                    <option value="Terrorist Content">Terrorist Content</option>
                    <option value="Harassment, Bullying, and Threats">Harassment, Bullying, and Threats</option>
                    <option value="Sexually Explicit Material">Sexually Explicit Material</option>
                    <option value="Impersonation and Misrepresentation">Impersonation and Misrepresentation</option>
                    <option value="Personal and Confidential Information">Personal and Confidential Information</option>
                    <option value="Illegal Activities">Illegal Activities</option>
                    <option value="Copyright Infringement">Copyright Infringement</option>
                    <option value="Child Endangerment">Child Endangerment</option>
                </select>
            </div>
  
            <div class="mb-3 form-floating">
                <textarea class="form-control" id="comments" style="height: 350px" placeholder="comment" name="comments"></textarea>
                <label for="comments" class="form-label">Comments</label>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <input id="submitReportAbuse" type="button" class="btn btn-primary" value="Send the report">
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#submitReportAbuse").click(function() {
            var project = $("#project").val();
            var activity = $("#activity").val();
            var etape = $("#etape").val();
            var category = $("#category").val();
            var comments = $("#comments").val();
            var dataString = 'project1='+ project + '&activity1='+ activity + '&etape1='+ etape + '&category1='+ category + '&comments1='+ comments;
            if(project==''||activity==''||etape==''||category==''||comments=='') {
                alert("Please Fill All Fields");
            } else {
                $.ajax({
                    type: "POST",
                    url: "index.php?mode=insertReportAbuse",
                    data: dataString,
                    cache: false,
                    success: function(){
                        alert("Form Submitted Succesfully");
                    }
                });
            }
            return false;
        });
    });
</script>
EOT;
    }
    //---------------End of "Report abuse" button section---------------

    // When we allow the user to answer multiple tasks at once, we must include a script to send JSON data to PHP
    // We do not put the script location in src attribute as it causes jQuery issues when retrieving the page through Ajax
    switch ($line['type']) {
        case 5:
            $str .= HTMLtagblock("script", array(), "var url='js/radiolist.js';$.getScript(url);");
            break;
        case 6:
            $str .= HTMLtagblock("script", array(), "var url='js/textarealist.js';$.getScript(url);");
            break;
        case 7:
            $str .= HTMLtagblock("script", array(), "var url='js/multiplecards.js';$.getScript(url);");
            break;
        case 8:
            $str .= HTMLtagblock("script", array(), "var url='js/diagram.js';$.getScript(url);");
            break;
        case 9:
            $str .= HTMLtagblock("script", array(), "var url='js/radioimages.js';$.getScript(url);");
            break;
        default:
            break;
    }

    // If the task has Ajax enabled, we must include the JS script
    if ($line['ajax']) {
        $str .= HTMLtagblock("script", array(), "setTimeout(function() { var url='js/ajaxtask.js';$.getScript(url); }, 500);");
    }

    // Adds a timer to complete the task if the user takes too long
    if ($line['timer'] !== null and $line['timer'] !== 0) {
        $str .= HTMLtimer($line['timer'] - time());
        $str .= HTMLtagblock("script", array(), "var url='js/timer.js';$.getScript(url);");
    }


    if ($line['type'] == 12) {
        $str .= HTMLinput("hidden", "mode", "insertUpload");
        $VIEW['MAIN'] .= HTMLtagblock("form", array(
            "action" => "index.php?mode=insertUpload",
            "method" => "POST",
            "id" => "taskAnswerForm",
            "enctype" => "multipart/form-data"
        ), $str);
    } else {
        $str .= HTMLinput("hidden", "mode", "insertAnswer");
        $VIEW['MAIN'] .= HTMLtagblock("form", array(
            "action" => "index.php?mode=showTasks",
            "method" => "POST",
            "id" => "taskAnswerForm"
        ), $str);
    }


}

/**
 * insertReportAbuse
 *
 * Insert in the db a report
 *
 * @param $conn : A valid database PDO connection
 * @param $project : The project in which the user is answering
 * @param $activity : The activity of the project
 * @param $step : The step of the activity
 * @param $category : The category of the report
 * @param $comments : The comments of the report
 */
function insertReportAbuse($conn, $project, $activity, $step, $category, $comments) {
    $insert = $conn->query("INSERT into reportAbuse (project,activity,step,category,comments) VALUES ('" . $project . "','" . $activity . "','" . $step . "','" . $category . "','" . $comments . "')");
}

/**
 * insertUpload
 *
 * Insert in the db a blob corresponding to an audio file or an image
 *
 * @param  $conn : A valid database PDO connection
 * @param  $id : The id of the task
 * @param  $activity_id : The id of the artifact of the task
 * @param  $mass : the mass of the answer for uncertain ones
 */
function insertUpload($conn, $id, $activity_id, $mass)
{

    $typeanswer = 0;
    $answer = "file_upload";
    $ajax = (isset($_POST['ajax']) and $_POST['ajax'] == true) ? true : false;
    $help = (isset($_POST['help']) and $_POST['help'] == true) ? true : false;

    if (!empty($_FILES["audio"]["name"])) {
        // Get file information
        $fileName = basename($_FILES["audio"]["name"]);
        $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

        // Allow below file formats
        $allowTypes = array('mp3', 'ogx');
        if (in_array($fileType, $allowTypes)) {
            $audio = $_FILES['audio']['tmp_name'];
            $audiofile = addslashes(file_get_contents($audio));

            // Insert audio into database
            $insert = $conn->query("INSERT into Upload (blobValue,name,type) VALUES ('" . $audiofile . "','" . $fileName . "','" . $fileType . "')");
            $answer = $conn->lastInsertId();
        }
    }

    if (!empty($_FILES["image"]["name"])) {
        // Get file information
        $fileName = basename($_FILES["image"]["name"]);
        $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

        // Allow below file formats
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');
        if (in_array($fileType, $allowTypes)) {
            $image = $_FILES['image']['tmp_name'];
            $imgfile = addslashes(file_get_contents($image));

            // Insert image into database
            $insert = $conn->query("INSERT into Upload (blobValue,name,type) VALUES ('" . $imgfile . "','" . $fileName . "','" . $fileType . "')");
            $answer = $conn->lastInsertId();
        }
    }

    insertAnswer($conn, $id, $activity_id, $answer, $mass, $ajax, $help, $typeanswer);
}

/**
 * insertAnswer
 *
 * Insert in the db a user answer & call update
 *
 * @param  $conn : A valid database PDO connection
 * @param  $id : The id of the task
 * @param  $artifactid : The id of the artifact of the task
 * @param  $value : The user answer(s)
 * @param  $mass : the mass of the answer for uncertain ones
 * @param boolean $ajax true if you want to make the user go directly to the next task of the artifact
 */
function insertAnswer($conn, $id, $artifactid, $value, $mass, $isAjax = false, $help = false, $typeanswer = 0){

    $_SESSION['taskStop']=microtime(true);

    $duration=10000000; // canari for none defined duration
    if(array_key_exists('taskStart',$_SESSION)){
        $duration=$_SESSION['taskStop']-$_SESSION['taskStart'];
        $_SESSION['taskStart']=0;
    }


    // secure the value
    $value = addslashes($value);
    $emptyAnswer = false;
    $user = $_SESSION['id'];
    $stmt = $conn->query("select * from Task where id=$id and artifact=$artifactid");
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $checker = $result['checker'];
    $type = $result['type'];
    $artifact = $result['artifact'];
    $realMass = 1; // by default, answers are certain (crisp)
    // checking if this is an uncertain answer
    if ($result['modality'] == 'belief')
        $realMass = $mass;
    // Checking if whether or not the answer is a JSON object
    $isJson = false;
    if (($type >= 5 and $type <= 7) or ($type == 11)) {
        json_decode($value);
        $isJson = (json_last_error() == JSON_ERROR_NONE);
    }
    if (!empty($checker) && !$isJson) {
        $stmt = $conn->query("select '$value' $checker as result from dual");
        $checked = $stmt->fetch()['result'];
    } else {
        $checked = true;
    }

    // Checking if the answer is valid depending of its type
    if ((($checked and $type != 3) or ($checked and $type == 3 and strlen(preg_replace("/\r|\n/", "", $value)) > 0)) and $value !== null) {


        $query = "insert into Answer(idtask, artifact, user, mass, duration, value" . ($help ? ", help" : "") . ") values ";

        if ($type == 3 || (($type == 5 || $type == 6 || $type == 7 || $type == 11) && $isJson)) { // Multi-line answer or JSON answer
            $values = ""; // default input in case of empty answer, plus space for last substr
            if ($type == 3) {
                foreach (explode(PHP_EOL, $value) as $val) {
                    $val=trim($val); // no empty lines nor useless spaces
                    $val = html_entity_decode($val, ENT_QUOTES); // We need to decode in order to avoid double encoding issues
                    if($val!="") $values .= "(:id, :artifact, :user, $realMass, $duration, " . $conn->quote($val) . "),";
                }
            } else {
                foreach (json_decode($value) as $jsonObject) {
                    $val = html_entity_decode($jsonObject->value, ENT_QUOTES); // We need to decode in order to avoid double encoding issues
                    $values .= "(" . $jsonObject->idtask . ",:artifact, :user, $realMass,$duration," . $conn->quote($val) . ($help ? ", " . (strlen($jsonObject->help->link) > 0 ? $conn->quote(json_encode($jsonObject->help)) : "null") : "") . "),";
                }
            }
            $values = substr($values, 0, strlen($values) - 1);
            $query .= $values;
            if (!$values)
                $emptyAnswer = true;
        } else {
            $query .= "(:id, :artifact,:user, $realMass, $duration, :value)";
        }

        $stmt = $conn->prepare($query);

        if (!$isJson) {
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            if ($type != 3) {
                $stmt->bindParam(':value', $value, PDO::PARAM_STR);
            }
        }

        $stmt->bindParam(':user', $user, PDO::PARAM_INT);
        $stmt->bindParam(':artifact', $artifact, PDO::PARAM_STR);

        if (!$emptyAnswer)
            $stmt->execute();

        // indicated that the task has been answered
        $conn->query("insert into Answered(id,artifact) values ($id,$artifact)");


        update($conn, $artifact);
        if (!$isAjax) {

            $nextTaskId = nextTaskFromArtifact($conn, $user, $artifact);
            if ($nextTaskId)
                answer($conn, $nextTaskId);
            else
                showTasks($conn, true);

        } else {
            $taskId = nextTaskFromArtifact($conn, $user, $_REQUEST['choice']);
            // We only send the new task html if its id is different than the previous one (Ajax takes care of the 'else' part)
            if ($taskId != $id) {
                answer($conn, $taskId);
            }
        }
    } else {
        answer($conn, $id, true);
    }

}

/**
 * insertJsonAnswer
 *
 * Insert in the db a user answer & call update
 *
 * @param  $conn : A valid database PDO connection
 * @param  $id : The id of the task
 * @param  $activity_id : The id of the artifact of the task
 * @param  $answer : The user answer(s)
 * @param  $mass : the mass of the answer for uncertain ones
 */

function insertJsonAnswer($conn, $id, $activity_id, $answer, $mass)
{


    $typeanswer = 0;
    $ajax = (isset($_POST['ajax']) and $_POST['ajax'] == true) ? true : false;
    $help = (isset($_POST['help']) and $_POST['help'] == true) ? true : false;

    if (isset($_POST['name']) && isset($_POST['MyRadio'])) {

        $radioVal = $_POST["MyRadio"];

        if ($radioVal == "q1") {
            $_POST['answer'] = $_POST["select_" . $radioVal];
            $typeanswer = 1;
        } elseif ($radioVal == "q2") {
            $_POST['answer'] = $_POST["select_" . $radioVal];
            $typeanswer = 2;
        } elseif ($radioVal == "qPrecis") {
            $_POST['answer'] = $_POST["select_" . $radioVal];
            $typeanswer = 3;
        }

        $_POST['answer'] = $_POST["select_" . $radioVal];
    }

    if (isset($_POST["level"]) && $_POST["level"]) {
        $_POST['answer'] = $_POST["level"];
        $typeanswer = 0;
    }

    insertAnswer($conn, $id, $activity_id, $answer, $mass, $ajax, $help, $typeanswer);
}


/**
 * showQueryAnswer
 *
 * Display the result of a query in a table titled $title
 *
 * @param $conn :
 *            A valid database PDO connection
 * @param $title :
 *            A title
 * @param $query :
 *            A valid query
 */
function showQueryAnswer($conn, $title, $query, $exception = null)
{
    global $VIEW;
    if ($exception == null) :
        $exception = array();
    endif;

    $VIEW['MAIN'] .= "\n<div><table class='table table-striped'><caption>$title</caption>";
    try {
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $first = true;
        while ($line = ($stmt->fetch())) {
            if ($first) {
                $VIEW['MAIN'] .= "\n<tr>";
                foreach ($line as $k => $v) {
                    if (!in_array($k, $exception)) :
                        $VIEW['MAIN'] .= "<th>$k</th>";
                    endif;

                }
                $VIEW['MAIN'] .= "</tr>";
                $first = false;
            }
            $VIEW['MAIN'] .= "\n<tr>";
            foreach ($line as $k => $v) {
                if (!in_array($k, $exception)) :
                    $VIEW['MAIN'] .= "<td>" . showValue($v) . "</td>";
                endif;

            }
            $VIEW['MAIN'] .= "</tr>";
        }
    } catch (PDOException $e) {
        $VIEW['MAIN'] .= "Error: " . $e->getMessage();
    }
    $conn = null;
    $VIEW['MAIN'] .= "</table></div>";
    // debug($query);
}

/**
 * showQueryAnswerAsString
 *
 * return the result of a query in a table titled $title as a string
 *
 * @param $conn :
 *            A valid database PDO connection
 * @param $title :
 *            A title
 * @param $query :
 *            A valid query
 */
function showQueryAnswerAsString($conn, $title, $query, $exception = null)
{
    if ($exception == null) :
        $exception = array();
    endif;

    if (!$query) return "";

    $res = "\n<div><table class='table table-striped'><caption>$title</caption>";
    try {
        $stmt = $conn->prepare($query);
        debug("1022".$query);
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $first = true;
        while ($line = ($stmt->fetch())) {
            if ($first) {
                $res .= "\n<tr>";
                foreach ($line as $k => $v) {
                    if (!in_array($k, $exception)) :
                        $res .= "<th class=''>$k</th>";
                    endif;

                }
                $res .= "</tr>";
                $first = false;
            }
            $res .= "\n<tr>";
            foreach ($line as $k => $v) {
                if (!in_array($k, $exception)) :
                    $res .= "<td class='text-break text-wrap'>" . showValue($v) . "</td>";
                endif;

            }
            $res .= "</tr>";
        }
    } catch (PDOException $e) {
        $res .= "Error: " . $e->getMessage();
    }
    $res .= "</table></div>";
    return $res;
}


/**
 * showValue : return an image if it is an image and a text otherwise
 */
function showValue($v)
{
    if (is_string($v)) :
        if (substr($v, -3) === 'jpg') :
            return '<img src=' . $v . ' alt=' . $v . ' width=200></img>';
        endif;
    endif;


    return $v;
}

/**
 * showTableUpload
 *
 * Display the audio and/or the images from the table Upload
 *
 * @param $conn :
 *            A valid database PDO connection
 * @param $title :
 *            A title
 * @param $query :
 *            A valid query
 */
function showTableUpload($conn, $title, $query, $exception = null)
{
    global $VIEW;
    if ($exception == null) :
        $exception = array();
    endif;

    $VIEW['MAIN'] .= "\n<div><table class='table table-striped'><caption>$title</caption>";
    try {
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $VIEW['MAIN'] .= "\n<tr>
            <td style='border: 1px solid #333'><b>id</b></td>
            <td style='border: 1px solid #333'><b>blobValue</b></td>
            <td style='border: 1px solid #333'><b>name</b></td>
            <td style='border: 1px solid #333'><b>type</b></td>";

        foreach ($stmt as $row) {
            $VIEW['MAIN'] .= "\n<tr>";
            $VIEW['MAIN'] .= "<td style='border: 1px solid #333'>" . $row['id'] . "</td>";

            if ($row['type'] == 'mp3') {
                $VIEW['MAIN'] .= "<td style='border: 1px solid #333'> <audio controls>
                        <source src='data:audio/mp3;base64," . base64_encode($row['blobValue']) . "'/>
                        </audio></td>";
            } elseif ($row['type'] == 'png' || $row['type'] == 'jpg') {
                "<td style='border: 1px solid #333'>
                <img src='data:image/jpg;charset=utf8;base64," . base64_encode($row['blobValue']) . "'/>
                </td>";
            }

            $VIEW['MAIN'] .= "<td style='border: 1px solid #333'>" . $row['name'] . "</td>";
            $VIEW['MAIN'] .= "<td style='border: 1px solid #333'>" . $row['type'] . "</td>";
            $VIEW['MAIN'] .= "</tr>";
        }
    } catch (PDOException $e) {
        $VIEW['MAIN'] .= "Error: " . $e->getMessage();
    }
    $conn = null;
    $VIEW['MAIN'] .= "</table></div>";
    // debug($query);
}

/**
 * showTableReportAbuse
 *
 * Display the projects with at least 3 reports abuse
 *
 * @param $conn :
 *            A valid database PDO connection
 * @param $title :
 *            A title
 * @param $query :
 *            A valid query
 */
function showTableReportAbuse($conn, $title, $query, $exception = null)
{
    global $VIEW;
    if ($exception == null) :
        $exception = array();
    endif;

    $VIEW['MAIN'] .= "\n<div><table class='table table-striped'><caption>$title</caption>";
    try {
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $VIEW['MAIN'] .= "\n<tr>
            <td style='border: 1px solid #333'><b>Activity</b></td>
            <td style='border: 1px solid #333'><b>Project</b></td>
            <td style='border: 1px solid #333'><b>Number of report abuse</b></td>";

        foreach ($stmt as $row) {
            $VIEW['MAIN'] .= "\n<tr>";
            $VIEW['MAIN'] .= "<td style='border: 1px solid #333'>" . $row['activity'] . "</td>";
            $VIEW['MAIN'] .= "<td style='border: 1px solid #333'>" . $row['project'] . "</td>";
            $VIEW['MAIN'] .= "<td style='border: 1px solid #333'>" . $row['numberOfReport'] . "</td>";
            $VIEW['MAIN'] .= "</tr>";
        }
    } catch (PDOException $e) {
        $VIEW['MAIN'] .= "Error: " . $e->getMessage();
    }
    $conn = null;
    $VIEW['MAIN'] .= "</table></div>";
    // debug($query);
}

function showHome()
{
    global $VIEW;
    $VIEW['MAIN'] = file_get_contents("templates/home.html");
}

?>

