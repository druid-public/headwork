
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'Cool-in-v2');
DELETE from ArtifactClass where project = 'Cool-in-v2';
insert into ArtifactClass(id,description, definition, tablename, project, autostart)
values(9000, 'Collectons vos tâches !', 'Cool-in-v2/coolin.sca', 'Artifact', 'Cool-in-v2', true);

DROP TABLE IF EXISTS CoolInAnswerButton;
CREATE TABLE CoolInAnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO CoolInAnswerButton values (1,'Personnelle','danger');
INSERT INTO CoolInAnswerButton values (2,'Profesionnelle','warning');
INSERT INTO CoolInAnswerButton values (3,'Familiale','success');
INSERT INTO CoolInAnswerButton values (4,'1 - aucun stress, voire relaxant','');
INSERT INTO CoolInAnswerButton values (5,'2 - aucun stress, neutre','');
INSERT INTO CoolInAnswerButton values (6,'3 - stress léger, motivant','');
INSERT INTO CoolInAnswerButton values (7,'4 - stress, désagréable, fatiguant','');
INSERT INTO CoolInAnswerButton values (8,'5 - stress très élevé, ne peut durer trop longtemps','');
INSERT INTO CoolInAnswerButton values (9,'30mn ou moins','');
INSERT INTO CoolInAnswerButton values (10,'1h','');
INSERT INTO CoolInAnswerButton values (11,'2h','');
INSERT INTO CoolInAnswerButton values (12,'1 demi-journée','');
INSERT INTO CoolInAnswerButton values (13,'1 jour','');
INSERT INTO CoolInAnswerButton values (14,'2 jours','');
INSERT INTO CoolInAnswerButton values (15,'3 jours','');
INSERT INTO CoolInAnswerButton values (16,'1 semaine','');
INSERT INTO CoolInAnswerButton values (17,'Plus de 1 semaine','');

DROP TABLE IF EXISTS CoolInTask;
DROP TABLE IF EXISTS CoolInTaskDecomposition;
DROP TABLE IF EXISTS CoolInTaskDecompositionTarget;

CREATE TABLE IF NOT EXISTS CoolInTask (
    idtask  int,
    user    int,
    artifact    int,
    idtodo  int auto_increment primary key,
    description varchar(2000),
    duration    int,
    stress      int,
    stresslimit    int
);

CREATE TABLE IF NOT EXISTS CoolInTaskDecomposition (
        idtask  int,
        user    int,
        artifact    int,
        idtodo  int,
        description varchar(2000),
        parentid int,
        position int,
        primary key (idtask,user,artifact,idtodo)
    );

CREATE TABLE IF NOT EXISTS CoolInTaskDecompositionTarget (
        idtask  int,
        user    int,
        artifact    int,
        idtarget  int
);

DROP TABLE IF EXISTS CoolInToDecompose;

CREATE TABLE CoolInToDecompose (
    id int auto_increment primary key,
    task varchar(100)
);

DROP TABLE IF EXISTS CoolInToDecompose;
 CREATE TABLE CoolInToDecompose (
    id int auto_increment primary key,
    task varchar(100)
);
insert into CoolInToDecompose(task) values
("Organiser un anniversaire"),
("Réviser une matière pour un contrôle"),
("Préparer l’arrivée d’un nouveau-né"),
("Préparer ses vacances au camping"),
("Préparer ses vacances à l’étranger"),
("Préparer ses vacances au ski"),
("Mettre en place un projet web"),
("Faire un rapport"),
("Faire un potager"),
("Préparer l’arrivée d’un chiot"),
("Organiser un mariage"),
("Déménager"),
("Trouver un stage/une alternance"),
("Organiser une sortie de groupe au bowling avec son entreprise"),
("Acheter une maison"),
("Vendre une maison"),
("Préparer la rentrée des enfants"),
("Ecrire un roman"),
("Se mettre à un sport"),
("Organiser un pique-nique"),
("Mettre en vente ses vêtements en ligne"),
("Acheter une voiture"),
("Vendre une voiture");
