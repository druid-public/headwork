package model.actions;

//import java.sql.*;

import model.Parameters;

public class OtherAction extends Action {

    public OtherAction(String guard, Parameters<Object> info){//, String body
        super(guard, info);
    }

    public String writeMethod(){
        return "";
    }

    public String writeAction() {
        return (String) getInfo().get("action");
    }

    public String writeFinishTask(){
        return "";
    }
}
