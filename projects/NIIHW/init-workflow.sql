-- install here your workflow

-- empty your project
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'NIIHW');

-- empty your activities
DELETE from ArtifactClass where project = 'NIIHW';

-- create your activities, see projectmap.txt to replace 14000
insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (15000,'Annotate images','NIIHW/annotate.sca', null, 'NIIHW',true);

-- create your local data if needed
