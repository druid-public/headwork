<?php

/**
 * addUser
 *
 * Add a new user in the db
 *
 * @param  $conn : A valid database PDO connection
 */
function addUser($conn)
{
    $stmt = $conn->prepare("select id from Users where name=:username");
    debug("addUser:username:".$_POST["username"]." !");
    $stmt->bindParam(':username', $_POST["username"], PDO::PARAM_STR);
    $stmt->execute();
    if ($stmt->rowCount()>0 && $_POST["username"]!="anonymous") {
        (new Register($conn))->registerUser($conn, true);
    } else {
        $stmt = $conn->prepare("insert into Users(name,hashed_password) values(:name,:pwd)");
        $stmt->bindParam(':name', $_POST["username"], PDO::PARAM_STR);
        $stmt->bindParam(':pwd', $pwd, PDO::PARAM_STR);
        $pwd=password_hash($_POST["password"], PASSWORD_DEFAULT);
        $stmt->execute() or die(mysql_error());
        $stmt = $conn->prepare("select * from Users where name=:username AND hashed_password=:pwd");
        $stmt->bindParam(':username', $_POST["username"], PDO::PARAM_STR);
        $stmt->bindParam(':pwd', $pwd, PDO::PARAM_STR);
        $stmt->execute();
        $result =$stmt->setFetchMode(PDO::FETCH_ASSOC);
        $line=$stmt->fetch();
        $_SESSION["login"]=true;
        $_SESSION["id"]=$line['id'];
        // Any user has the "common knowledge" skill
        $conn->query("insert into Skills values (".$_SESSION["id"].",1,100)");
        $_SESSION['username']=$line['name'];
        if ($_POST["username"]!="anonymous") {
            profile($conn);
        } else {
            showTasks($conn);
        }
    }
}
?>
