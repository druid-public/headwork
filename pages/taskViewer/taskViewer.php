<?php
/**
 * Display Tasks Viewer page.
 *
 * @see tasks-viewer.php
 */
function showTasksViewer()
{
    global $VIEW;
    $VIEW['MAIN'] = file_get_contents("templates/taskViewer.tmpl");
}
?>
