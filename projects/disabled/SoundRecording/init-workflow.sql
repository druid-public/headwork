DROP TABLE IF EXISTS SoundRecording;
DELETE from Artifact where classid in (select id from ArtifactClass where project = 'SoundRecording');
DELETE from ArtifactClass where project = 'SoundRecording';

DROP TABLE IF EXISTS AnswerButton;
CREATE TABLE AnswerButton(id int,text varchar(100),class varchar(100));
INSERT INTO AnswerButton values (1,'Mauvaise','danger');
INSERT INTO AnswerButton values (2,'À revoir','warning');
INSERT INTO AnswerButton values (3,'Parfaite','success');


insert into ArtifactClass(id,description,definition,tablename,project,autostart) values (12000,'SoundRecording basics','SoundRecording/SoundRecording.sca', 'Artifact', 'SoundRecording',true);