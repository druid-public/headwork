<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\ApiBulkOperationApi();
$isInScheme = isInScheme_example; // String | The unique identifier of the requested concept scheme.
$language = language_example; // String | The default language of the returned response. Overwrites the Accept-Language header.
$offset = 56; // Integer | The offset of the returned resources in the response. Supports paging where the 'offset' specifies the page number
$limit = 56; // Integer | The maximum number of returned resources in the response.
$selectedVersion = selectedVersion_example; // String | The selected ESCO dataset version.
$viewObsolete = true; // Boolean | If set to 'true', the obsoleted concepts will be returned
$acceptLanguage = acceptLanguage_example; // String | The default language of the returned response. Optional and might be overwritten by the language request parameter.

try {
    $result = $api_instance->getConceptByConceptScheme($isInScheme, $language, $offset, $limit, $selectedVersion, $viewObsolete, $acceptLanguage);
    print_r($result);
    print_r('test');
} catch (Exception $e) {
    echo 'Exception when calling BulkOperationApi->getConceptByConceptScheme: ', $e->getMessage(), PHP_EOL;
}
?>
