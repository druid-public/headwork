package model.connections;

import model.Parameters;
import model.actions.Action;

import java.util.List;

public interface PlatformConnector {

    //void prepareDB();
    void CreateArtifactClass(String nameArtifact, Parameters<String> attributes, List<Action> actions);
    void instantiate(List<Parameters> BasicPeersArtifact);

    DBConnector getDBConnection();
}
