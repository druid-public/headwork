$(document).ready(function() {

    // Used on topic pages
    $(':button[id^="forumQuotePost"]').click(function() {
        var idPost = $(this).attr('data-post-id');
        var quotedUser = $('#forumPostHeader-' + idPost + ' > strong').text();
        var quotedDate = $('#forumPostHeader-' + idPost + ' > small').text();
        var quotedContent = $('#forumPost-' + idPost).html();
        var currentContent = tinymce.activeEditor.getContent();
        tinymce.activeEditor.setContent(currentContent + "<blockquote><p><strong>" + quotedUser + "</strong>, <small>" + quotedDate + "</small> said :</p>" + quotedContent + "</blockquote>");
    });

    $(':button[id^="forumThanksPost"]').click(function() {
        var idPost = $(this).attr('data-post-id');
        $.ajax({
            type: "POST",
            url: "ForumController.php?action=managePostThanks",
            data: {post_id: idPost},
            success: function(data) {
                var jsonData = JSON.parse(data);
                if (jsonData['success']) {
                    var postNumber = $('#forumThanksPostNumber-' + idPost);
                    if (jsonData['action'] == "increase") {
                        postNumber.fadeOut(function() {
                            $(this).text(parseInt($(this).text()) + 1).fadeIn();
                        })
                    } else {
                        postNumber.fadeOut(function() {
                            $(this).text(parseInt($(this).text()) - 1).fadeIn();
                        })
                    }
                }
            }
        });
    });

    manageTaskSelect();

});

// Binding the event
$('#topicCategory').change(manageTaskSelect);

/**
 * Manage the display and properties of the 'task' select.
 * Only used on topic creation page.
 */
function manageTaskSelect() {
    var taskSelect = $('#taskSelect');
    if ($('#topicCategory').val() == 2) { // 2 => task category ID in database
        taskSelect.parent().fadeIn();
        taskSelect.prop("required", true);
    } else {
        taskSelect.parent().fadeOut();
        taskSelect.prop("required", false);
    }
}