package model.classConstructors;

import model.SqlCommands;
import model.actions.Action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class JsonClassArtifact {

    public JsonClassArtifact(String nameArtifact, List<Action> actions){

        String automaton = jsonContent(actions);
        FileOutputStream scaFile;
        File newFile = new File("artifactclasses/"+nameArtifact + ".sca");
        try {
            scaFile = new FileOutputStream(newFile);
            scaFile.write(automaton.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String jsonContent(List<Action> listactions){
        /*
            automaton: StringBuilder for the .sca file describing the artifact automata
         */
        StringBuilder automaton = new StringBuilder(
                    "{\n");
        automaton.append(
                    "    \"1\": {\n");

        /* pour l'instant dans init-workflow.sql, met je garde au cas où on met dans state "0" (à la place de "init"
        content.append("                \"    \\\"init\\\": [\\n\" +\n" +
                "                \"        \\\"").append(initRequest()).append("\\\",\\n\" +\n");
        for (Action act : listactions) {
            content.append("                \"        \\\"").append(act.writeTask().replaceAll("\"","\\\\\\\\\"")).append("\\\",\\n\" +\n");
        }
        content.append("                \"        \\\"insert into ARTIFACT(id, ownerid, classid, state)\" +\n" +
                "                \" values (\"+id+\",'\"+idPeer+\"',")
                .append(nameArtifact.getId()).append(",'\"+state+\"')\\\"\\n\" +\n" +
                "                \"    ],\\n\" +\n"
        */

        /*
                Actions
         */
        AtomicReference<Boolean> firstAction = new AtomicReference<>(true);
        for (Action act : listactions){
            String nameAction = (String) act.getInfo().get("title");
            if (!firstAction.get())
                automaton.append(
                    "        },\n");
            firstAction.set(false);

            String selectIdTask = "select id from task where title='" + nameAction +"' and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)";

            automaton.append(
                    "        \"").append(act.getId()).append("\": {\n" +
                    "            \"guard\": " +
                                    "\"select if(" +
                                        "(select count(*)>0 from ARTIFACT where ID=(select ID from CURRENTARTIFACT " +
                                            "where USER=SESSION_USER) and state='running')," +
                                        "if(" +
                                            "(select count(*)<1 from task where title='").append(nameAction).append("' " +
                                                "and artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)), " +
                                            "("+act.getGuard()+")" +
                                            ", 0)" +
                                        ", 0)" +
                                    "\",\n" +
                    "            \"actions\": [\n" +
                    "                \"").append(act.writeTask()).append("\",\n")
                .append(
                    "                \"insert into profile values ((").append(selectIdTask).append("),1)\"\n" +
                    "            ]\n" +
                    "        },\n");
            automaton.append(
                    "        \"").append(act.getId()+1).append("\": {\n" +
                    "            \"guard\": " +
                                    "\"select count(*)>=1 from answer where " +
                                        "idtask=(").append(selectIdTask).append(")\",\n" +
                    "            \"actions\": [\n");
            String[] actionActions = act.writeAction().split(";");
            for (String action :actionActions)
                automaton.append(
                    "                \"").append(action).append("\",\n");
            automaton.append(
                    "                \"delete from answer where " +
                                        "idtask=(").append(selectIdTask).append(")\"\n" +
                    "            ]\n");
        }
        automaton.append(
                    "        },\n");
        //              "        }\n");
        /*
                Change of artifact in the automaton
        */
        automaton.append(
        //            "        \"2\": {\n" +
        //                        WRITE_TASK_CHANGE_ARTIFACT +
        //            "        },\n" +
                    "        \"3\": {\n" +
                                WRITE_ACTION_CHANGE_ARTIFACT +
                    "        }\n");


        /*
                Retour à l'état initial
         */
        for (Action act : listactions){
            automaton.append(act.writeFinishTask());
        }
        /*
        automaton.append(
                    "    },\n" +
                    "    \"2\": {\n" +
                    "        \"1\": {\n" +
                    "            \"guard\": \"select true from dual\",\n" +
                    "            \"actions\": []\n" +
                    "        }\n");
         */
        automaton.append(
                    "    },\n" +
                    "    \"3\": {\n" +
                    "        \"1\": {\n" +
                    "            \"guard\": \"select true from dual\",\n" +
                    "            \"actions\": []\n" +
                    "        }\n" +
                    "    }\n" +
                    "}");
        return automaton.toString();
    }

    public static String WRITE_TASK_CHANGE_ARTIFACT =
                "            \"guard\": " +
                                "\"select if((select count(*)<1 from task where title='Change current artifact')," +
                                    "(select count(*)>1 from ARTIFACT " +
                                    "where state!='finished' " +
                                    "AND OWNERID=(select OWNERID from ARTIFACT where ID=" +
                                        "(select ID from CURRENTARTIFACT where USER=SESSION_USER))" +
                                    "),0) as changeArt;\",\n" +
                "            \"actions\": [\n" +
                "                \"insert into task(title,description,body,checker,checkermsg) " +
                                    "values('Change current artifact'," +
                                    "'Change the current artifact'," +
                                    "'Change the current Artifact'," +
                                    "null," +
                                    "'insert valid artifact id')" + "\",\n" +
                "                \"insert into profile values " +
                                    "((select id from task where title='Change current artifact'),1)\"\n" +
                "            ]\n";


    private static String idNewArtifact = "(select value from answer where idtask=1)";// +
                                            //"(select id from task where title='Change current artifact'))";
    public static String WRITE_ACTION_CHANGE_ARTIFACT =
                "            \"guard\": " +
                                "\"select count(*)>=1 from answer " +
                                    "where idtask=1\",\n" +//where idtask=(select id from task where title='Change current artifact')
                "            \"actions\": [\n" +
                "                \"" + SqlCommands.update("CURRENTARTIFACT", "ID", idNewArtifact,  "USER=SESSION_USER") + "\",\n" +
                "                \"" + SqlCommands.update("CURRENTARTIFACT", "NAME",
                                            "(select tablename from ARTIFACTCLASS " +
                                                    "where ID=(select classid from ARTIFACT " +
                                                    "where id=" + idNewArtifact + "))",
                                         "USER=SESSION_USER") + "\",\n" +
                "                \"delete from answer where idtask=1\",\n" +
                                    //" idtask=(select id from task where title='Change current artifact')\",\n" +
                "                \"delete from profile where idtask in (select id from task where artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER))\",\n" +
                "                \"delete from task where artifact=(select ID from CURRENTARTIFACT where user=SESSION_USER)\"\n" +
                "            ]\n";

}
