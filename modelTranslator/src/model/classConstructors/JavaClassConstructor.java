package model.classConstructors;

import model.Parameters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public class JavaClassConstructor {
    FileOutputStream file;
    StringBuilder imports = new StringBuilder();
    StringBuilder classDescription = new StringBuilder();
    StringBuilder attributes = new StringBuilder();
    StringBuilder methods = new StringBuilder();
    StringBuilder constructor = new StringBuilder();
    private String classPackage;
    private String extension;
    private String name;

    /**
     *
     * @param name the name of the class to construct
     * @param directory the directory in which the file should be situated
     * @param classpackage the package in which the class belongs
     * @param extension if the class extends or implements a class or interface ("" if not)
     * @param params Can contain:
     *              * needed imports ("imports" : Iterable<String>),
     *              * a description of the class ("classDescription":String),
     *              * a constructor method ("constructor":String)
     */
    public JavaClassConstructor(String name, String directory, String classpackage, String extension, Parameters<Object> params){
        this.file = openNewFile(name, directory);
        this.classPackage = classpackage;
        this.extension = extension;
        this.name = name;
        if (params.get("imports") != null)
            this.addImports((Iterable<String>) params.get("imports"));
        if (params.get("classDescription") != null)
            this.addClassDescription((String) params.get("classDescription"));
        if (params.get("constructor") != null)
            this.addImports((String) params.get("constructor"));
    }

    public void writeFile(){
        try {
            file.write(("package " + classPackage + ";\n\n").getBytes());
            file.write(imports.append("\n").toString().getBytes());
            file.write(("/**\n"+classDescription.toString()+"\n */\n").getBytes());
            file.write(("public class " + name + " " + extension + "{\n").getBytes());
            file.write(attributes.append("\n").toString().getBytes());
            file.write(constructor.append("\n").toString().getBytes());
            file.write(methods.toString().getBytes());
            file.write("\n}".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public FileOutputStream getFile(){
        return file;
    }

    public JavaClassConstructor addImports(String anImport){
        this.imports.append("import ").append(anImport).append(";\n");
        return this;
    }
    protected JavaClassConstructor addImports(Iterable<String> severalImports){
        for (String anImport : severalImports)
            addImports(anImport);
        return this;
    }
    public JavaClassConstructor addImports(String[] severalImports){
        for (String anImport : severalImports)
            addImports(anImport);
        return this;
    }

    public JavaClassConstructor addClassDescription(String description){
        String[] perLineDescription = description.split("\n");
        for (String line : perLineDescription)
            this.classDescription.append(" * ").append(line);
        return this;
    }

    public JavaClassConstructor addMethod(String method){
        this.methods.append("\n").append(method).append("\n");
        return this;
    }

    public JavaClassConstructor addAttribute(String attribute, String type){
        this.attributes.append("\tprivate ").append(type).append(" ").append(attribute).append(";\n");
        return this;
    }
    public JavaClassConstructor addAttribute(String attribute){
        this.attributes.append("\t").append(attribute);
        if (attribute.endsWith(";"))
            attributes.append("\n");
        else if (!attribute.endsWith("\n"))
            attributes.append(";\n");
        return this;
    }
    public JavaClassConstructor addAttributes(String[] attributes){
        for (String attribute:attributes)
            this.addAttribute(attribute);
        return this;
    }
    protected JavaClassConstructor addAttributes(Iterable<Map.Entry<String,String>> severalImports){
        for (Map.Entry<String,String> anImport : severalImports)
            addAttribute(anImport.getKey(), anImport.getValue());
        return this;
    }

    public JavaClassConstructor addConstructor(String constructor){
        this.constructor.append("\t").append(constructor);
        return this;
    }

    private FileOutputStream openNewFile(String nameClass, String directory){
        FileOutputStream newStream;
        //create directory if not exist
        new File(directory).mkdirs();
        File newFile = new File(directory, nameClass.concat(".java"));
        try {
            newStream = new FileOutputStream(newFile);
            return newStream;
        } catch (FileNotFoundException e) {
            // shouldn't happen since we created the file just before
            // except maybe if the directory name is wrong ? or the artifact name to weird ?
            System.out.println(newFile.toString());
            e.printStackTrace();
        }
        return null;
    }



}
