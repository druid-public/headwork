$('#taskAnswerForm').on('submit.textarealist', function(e) {
    // We prevent the default action of the from in order to add a custom answer input
    e.preventDefault();
    answer = [];
    $(':input[id^="answer-"]').each(function() {
        var id = $(this).attr('data-task-id');
        if ($(':input[name="help"]').length == 0) {
            answer.push({"idtask": id, "value": $(this).val() });
        } else {
            answer.push({
                "idtask": id,
                "value": $(this).val(),
                "help": {
                    "link" : $(':input[name="help-link-' + id + '"]').val(),
                    "page" : $(':input[name="help-page-' + id + '"]').val()
                }
            });
        }
    });
    // Creation of a hidden input containing the JSON as string
    let toAdd = $("<input/>").attr("type", "hidden").attr("name", "answer").val(JSON.stringify(answer));
    $(this).append(toAdd);
    // We unbind this event handler to avoid infinite loop
    $(this).unbind("submit.textarealist");
    // We submit the form ONLY is no ajax is involved, as it would cause double submit with it
    if ($(':input[name="ajax"]').length == 0) {
        $(this).submit();
    }
})