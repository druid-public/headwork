<?php

class ForumModel {

    private $db;

    function __construct($conn) {
        $this->db = $conn;
    }


    /**
     * Retrieve all the content of `table_category` table
     */
    function getCategories() {
        try {
            $stmt = $this->db->prepare("SELECT * FROM forum_category");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve all the content of `table_category` table and the date of the last post for each category
     */
    function getCategoriesWithEdit() {
        try {
            $stmt = $this->db->prepare("SELECT fc.*, IFNULL( ( SELECT MAX(fp.date) FROM forum_post fp, forum_topic ft WHERE fp.id_topic = ft.id_topic AND ft.id_category = fc.id_category GROUP BY fc.id_category ), ( SELECT MAX(ft.creation_date) FROM forum_topic ft WHERE ft.id_category = fc.id_category GROUP BY fc.id_category ) ) AS 'last_modification' FROM forum_category fc");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the category record whose id is the parameter
     * @param int $category the id of the category to look for
     */
    function getCategory($category) {
        try {
            $stmt = $this->db->prepare("SELECT * FROM forum_category WHERE id_category = :category");
            $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetch(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve all the content of `task` table
     */
    function getTasks() {
        try {
            $stmt = $this->db->prepare("SELECT * FROM task");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    function getTask($task) {
        try {
            $stmt = $this->db->prepare("SELECT * FROM task WHERE id = :task");
            $stmt->bindParam(':task', $task, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve n topics from a given category
     * @param int $category the category to retrieve topics from
     * @param int $limit the number of topics to retrieve
     * @param int $start the offset for the query, used for the pagination
     */
    function getTopicsFromCateg($category, $limit, $start) {
        try {
            $stmt = $this->db->prepare("SELECT ft.*, u.name AS 'creator_name', IFNULL((SELECT MAX(fp.date) FROM forum_post fp WHERE ft.id_topic = fp.id_topic), ft.creation_date) AS 'last_modification' FROM forum_topic ft, forum_post fp, users u WHERE ft.id_category = :category AND ft.creator_id = u.id GROUP BY ft.id_topic ORDER BY last_modification DESC LIMIT :limit OFFSET :offset");
            $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $start, PDO::PARAM_INT);
            $stmt->execute();            
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the total number of topics in the forum
     */
    function getTotalTopics() {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) AS 'total' FROM forum_topic");
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the total number of topics for a given category
     * @param int $category the id of the targeted category
     */
    function getTotalTopicsFromCateg($category) {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) AS 'total' FROM forum_topic WHERE id_category = :category");
            $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the topic record whose id is the parameter
     * @param int $topicId the id of the topic to look for
     */
    function getTopic($topicId) {
        try {
            $stmt = $this->db->prepare("SELECT ft.*, u.name FROM forum_topic ft, users u WHERE ft.id_topic = :topic AND ft.creator_id = u.id");
            $stmt->bindParam(':topic', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetch(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve n posts from a given topic
     * @param int $topicId the topic to retrieve posts from
     * @param int $limit the number of posts to retrieve
     * @param int $start the offset for the query, used for the pagination
     */
    function getPostsFromTopic($topicId, $limit, $start) {
        try {
            $stmt = $this->db->prepare("SELECT fp.*, u.name FROM forum_post fp, users u WHERE fp.id_topic = :topic AND fp.user_id = u.id ORDER BY fp.date ASC LIMIT :limit OFFSET :offset");
            $stmt->bindParam(':topic', $topicId, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $start, PDO::PARAM_INT);
            $stmt->execute();            
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the total number of posts for a given topic
     * @param int $topicId the id of the targeted topic
     */
    function getTotalPostsFromTopic($topicId) {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) AS 'total' FROM forum_post WHERE id_topic = :topic");
            $stmt->bindParam(':topic', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Add a post to a given topic
     * @param int $topicId the id of the parent topic
     * @param string $content the content of the post
     * @param int $userId the user id
     */
    function addPostToTopic($topicId, $content, $userId) {
        try {
            $stmt = $this->db->prepare("INSERT INTO forum_post (content, user_id, id_topic) VALUES (:content, :user, :topic)");
            $stmt->bindParam(':content', $content, PDO::PARAM_STR);
            $stmt->bindParam(':user', $userId, PDO::PARAM_INT);
            $stmt->bindParam(':topic', $topicId, PDO::PARAM_INT);
            return $stmt->execute();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Create a new topic with values submitted by the user
     * @param string $title the topic title
     * @param int $category the category id to belong to
     * @param string $content the content of the topic
     * @param int $userId the user id
     */
    function createNewTopic($title, $category, $content, $userId) {
        try {
            $stmt = $this->db->prepare("INSERT INTO forum_topic (title, content, creator_id, id_category) VALUES (:title, :content, :creator, :category)");
            $stmt->bindParam(':title', $title, PDO::PARAM_STR);
            $stmt->bindParam(':content', $content, PDO::PARAM_STR);
            $stmt->bindParam(':creator', $userId, PDO::PARAM_INT);
            $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            $stmt->execute();
            return $this->db->lastInsertId();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Create a new topic affected to a specific task
     * @param string $title the topic title
     * @param int $category the category id to belong to
     * @param int $task the task id
     * @param string $content the content of the topic
     * @param int $userId the user id
     */
    function createNewTopicTask($title, $category, $task, $content, $userId) {
        try {
            $stmt = $this->db->prepare("INSERT INTO forum_topic (title, content, creator_id, id_task, id_category) VALUES (:title, :content, :creator, :task, :category)");
            $stmt->bindParam(':title', $title, PDO::PARAM_STR);
            $stmt->bindParam(':content', $content, PDO::PARAM_STR);
            $stmt->bindParam(':creator', $userId, PDO::PARAM_INT);
            $stmt->bindParam(':task', $task, PDO::PARAM_INT);
            $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            $stmt->execute();
            return $this->db->lastInsertId();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Search for topic whose title or content contains the $toSearch string
     * @param string $toSearch the string to look for in topics (title or content)
     * @param int $limit the number of topics to retrieve
     * @param int $start the offset for the query, used for the pagination
     */
    function searchTopics($toSearch, $limit, $start) {
        try {
            $stmt = $this->db->prepare("SELECT ft.*, u.name AS 'creator_name', IFNULL((SELECT MAX(fp.date) FROM forum_post fp WHERE ft.id_topic = fp.id_topic), ft.creation_date) AS 'last_modification' FROM forum_topic ft, users u WHERE (title LIKE :toSearch OR content LIKE :toSearch) AND ft.creator_id = u.id ORDER BY last_modification DESC LIMIT :limit OFFSET :offset");
            $stmt->bindValue(':toSearch', '%'.$toSearch.'%', PDO::PARAM_STR);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $start, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

/**
     * Search for topic whose title or content contains the $toSearch string in the given category
     * @param string $toSearch the string to look for in topics (title or content)
     * @param int $category the category id
     * @param int $limit the number of topics to retrieve
     * @param int $start the offset for the query, used for the pagination
     */
    function searchTopicsFromCateg($toSearch, $category, $limit, $start) {
        try {
            $stmt = $this->db->prepare("SELECT ft.*, u.name AS 'creator_name', IFNULL((SELECT MAX(fp.date) FROM forum_post fp WHERE ft.id_topic = fp.id_topic), ft.creation_date) AS 'last_modification' FROM forum_topic ft, users u WHERE (title LIKE :toSearch OR content LIKE :toSearch) AND ft.creator_id = u.id AND ft.id_category = :category ORDER BY last_modification DESC LIMIT :limit OFFSET :offset");
            $stmt->bindValue(':toSearch', '%'.$toSearch.'%', PDO::PARAM_STR);
            $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $start, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the total number of topics found by the search
     * @param string $toSearch the string used to count topics
     * @param int $category the category to look in
     */
    function getTotalFromSearch($toSearch, $category = NULL) {
        try {
            $query = "SELECT COUNT(*) FROM forum_topic WHERE (title LIKE :toSearch OR content LIKE :toSearch)";
            if (!is_null($category)) {
                $query .= " AND id_category = :category";
            }
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':toSearch', '%'.$toSearch.'%', PDO::PARAM_STR);
            if (!is_null($category)) {
                $stmt->bindParam(':category', $category, PDO::PARAM_INT);
            }
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve the number of thanks the user gave to a post (either 1 or 0)
     * @param int $postId the post to check the thanks from
     * @param int $userId the user to check
     */
    function getPostThanksFromUser($postId, $userId) {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM forum_post_thanks WHERE id_post = :post AND id_user = :user");
            $stmt->bindParam(':post', $postId, PDO::PARAM_INT);
            $stmt->bindParam(':user', $userId, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Add a record with the user and the post to track the user thanks
     * @param int $postId the post to add a record of
     * @param int $userId the user to add a record of
     */
    function addThanksRecord($postId, $userId) {
        try {
            $stmt = $this->db->prepare("INSERT INTO forum_post_thanks (id_post, id_user) VALUES (:post, :user)");
            $stmt->bindParam(':post', $postId, PDO::PARAM_INT);
            $stmt->bindParam(':user', $userId, PDO::PARAM_INT);
            return $stmt->execute();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Increase the number of thanks of a given post
     * @param int $postId the post to increase the thanks number
     */
    function increasePostThanks($postId) {
        try {
            $stmt = $this->db->prepare("UPDATE forum_post SET thanks = thanks + 1 WHERE id_post = :post");
            $stmt->bindParam(':post', $postId, PDO::PARAM_INT);
            return $stmt->execute();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Remove the record with the user and the post
     * @param int $postId the post to remove the record
     * @param int $userId the user to remove a record
     */
    function removeThanksRecord($postId, $userId) {
        try {
            $stmt = $this->db->prepare("DELETE FROM forum_post_thanks WHERE id_post = :post AND id_user = :user");
            $stmt->bindParam(':post', $postId, PDO::PARAM_INT);
            $stmt->bindParam(':user', $userId, PDO::PARAM_INT);
            return $stmt->execute();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Decrease the number of thanks of a given post
     * @param int $postId the post to decrease the thanks number
     */
    function decreasePostThanks($postId) {
        try {
            $stmt = $this->db->prepare("UPDATE forum_post SET thanks = thanks - 1 WHERE id_post = :post");
            $stmt->bindParam(':post', $postId, PDO::PARAM_INT);
            return $stmt->execute();
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

    /**
     * Retrieve all the thanked posts by the user from a given topic
     * @param int $topicId the topic to check posts from
     * @param int $userId the user to check for
     */
    function retrieveUserThankedPosts($topicId, $userId) {
        try {
            $stmt = $this->db->prepare("SELECT id_post FROM forum_post_thanks WHERE id_user = :user AND id_post IN ( SELECT id_post FROM forum_post WHERE id_topic = :topic )");
            $stmt->bindParam(':user', $userId, PDO::PARAM_INT);
            $stmt->bindParam(':topic', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_COLUMN);
            return $result;
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }

}

?>