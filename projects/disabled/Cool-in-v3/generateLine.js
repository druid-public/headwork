// function generateLine: add a input line to the mytable HTML table
// parameter    i: number of line
//              eng: English text
//              fr: French text
// author: David Gross-Amblard

function generateLine(i,eng,fr){
    table=document.getElementById("mytable");

    table.innerHTML +=`
      <tr>
    <td>
        <input type="hidden" name="text_task_${i}" value="${fr}"/>${fr}
    </td>
    <td>
        <input type="hidden" name="text_en_task_${i}" value="${eng}" />${eng}
    </td>
    <td>
        <select name="duration_task_${i}" id="duration_task_${i}" required>
            <option value="">non renseigné</option>
            <option value="1">30mn ou moins</option>
            <option value="2">1 heure</option>
            <option value="3">2 heures</option>
            <option value="4">1 demi-journée</option>
            <option value="5">1 journée</option>
            <option value="6">2 jours</option>
            <option value="7">3 jours</option>
            <option value="8">1 journée</option>
            <option value="9">2 jours</option>
            <option value="10">3 jours</option>
            <option value="11">1 semaine</option>
            <option value="12">plus de 1 semaine</option>
        </select>
    </td>
    <td>
        <select name="stress_task_${i}" id="stress_task_${i}" required>
            <option value="">non renseigné</option>
            <option value="1">aucun stress, relaxant</option>
            <option value="2">aucun stress, neutre</option>
            <option value="3">stress leger, motivant</option>
            <option value="4">stress désagreable, fatiguant</option>
            <option value="5">stress très élevé, ne doit durer trop longtemps</option>
        </select>
    </td>
    <td>
        <select name="stress_limit_task_${i}" id="stress_limit_task_${i}" required>
            <option value="">non renseigné</option>
            <option value="1">aucun stress, voire relaxant</option>
            <option value="2">aucun stress, neutre</option>
            <option value="3">stress leger, motivant</option>
            <option value="4">stress désagreable, fatiguant</option>
            <option value="5">stress très élevé, ne doit durer trop longtemps</option>
        </select>
    </td>
</tr>
 `;
}
