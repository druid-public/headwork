Before to be able to access to the SPIPOLL project, you should launch 3 python (v3) scripts :

- get_headwork_skill.py : it manage to create a table to optimize the hierarchy from the SPIPOLL taxonomy in 'skillTree' table.
- get_taxon_tree_headwork.py : it create all answer template for different step in task resolution
- get_skills.py : it create the template for skills tasks, only for ORDER level from now


==> to launch (cmd or ide):
1) go to the main directory of the project '~/headwork/"
2) launch the script one by one with command "python3 project/spipoll/[script].py" > should change [script] to the name of python file.