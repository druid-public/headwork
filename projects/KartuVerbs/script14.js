/*const json1 =
{
    "emulated":true,
    "masdars": [
        "გავაამხანაგებ;future;1;sg;გა;ვა;ამხანაგ;ებ;-;-;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "გააამხანაგებ;future;2;sg;გა;ა;ამხანაგ;ებ;-;-;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "გააამხანაგებს;future;3;sg;გა;ა;ამხანაგ;ებ;-;ს;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "ვაამხანაგე;aorist;1;sg;-;ვა;ამხანაგ;-;-;ე;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "გავაამხანაგე;aorist;1;sg;გა;ვა;ამხანაგ;-;-;ე;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "აამხანაგე;aorist;2;sg;-;ა;ამხანაგ;-;-;ე;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "გამიამხანაგებია;perfect;1;sg;გა;მი;ამხანაგ;ებ;-;ია;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "გაგიამხანაგებია;perfect;2;sg;გა;გი;ამხანაგ;ებ;-;ია;T1;active;39-8;39;*ამხანაგება;გაამხანაგება",
        "გაუამხანაგებია;perfect;3;sg;გა;უ;ამხანაგ;ებ;-;ია;T1;active;39-8;39;*ამხანაგება;გაამხანაგება"]
};
const json2 =
{
    "emulated":true,
    "masdars": [
        "მივაახლოებ;future;1;sg;მი;ვა;ახლო;ებ;-;-;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "მიაახლოებ;future;2;sg;მი;ა;ახლო;ებ;-;-;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "მიაახლოებს;future;3;sg;მი;ა;ახლო;ებ;-;ს;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "ვაახლოე;aorist;1;sg;-;ვა;ახლო;-;-;ე;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "მივაახლოე;aorist;1;sg;მი;ვა;ახლო;-;-;ე;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "აახლოე;aorist;2;sg;-;ა;ახლო;-;-;ე;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "მიმიახლოებია;perfect;1;sg;მი;მი;ახლო;ებ;-;ია;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "მიგიახლოებია;perfect;2;sg;მი;გი;ახლო;ებ;-;ია;T1;active;71-2;71;*ახლოვება;მიახლოვება",
        "მიუახლოებია;perfect;3;sg;მი;უ;ახლო;ებ;-;ია;T1;active;71-2;71;*ახლოვება;მიახლოვება"]
};
const json3 =
{
    "emulated":true,
    "masdars": [
        "მოაახლოებს;future;3;sg;მო;ა;ახლო;ებ;-;ს;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "მოვაახლოებთ;future;1;pl;მო;ვა;ახლო;ებ;-;თ;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "მოაახლოებთ;future;2;pl;მო;ა;ახლო;ებ;-;თ;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "ვაახლოე;aorist;1;sg;-;ვა;ახლო;-;-;ე;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "მოვაახლოე;aorist;1;sg;მო;ვა;ახლო;-;-;ე;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "აახლოე;aorist;2;sg;-;ა;ახლო;-;-;ე;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "აახლოეთ;aorist;2;pl;-;ა;ახლო;-;-;ეთ;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "მომიახლოებია;perfect;1;sg;მო;მი;ახლო;ებ;-;ია;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "მოგიახლოებია;perfect;2;sg;მო;გი;ახლო;ებ;-;ია;T1;active;71-3;71;*ახლოვება;მოახლოვება",
        "მოუახლოებია;perfect;3;sg;მო;უ;ახლო;ებ;-;ია;T1;active;71-3;71;*ახლოვება;მოახლოვება"]
};
const json4 =
{
    "emulated":true,
    "masdars": [
        "მივიახლოვებ;future;1;sg;მი;ვი;ახლო;-;-;ვებ;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "მოვიახლოვებ;future;1;sg;-;მოვი;ახლო;-;-;ვებ;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "მიიახლოვებ;future;2;sg;მი;ი;ახლო;-;-;ვებ;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "მივიახლოვე;aorist;1;sg;მი;ვი;ახლო;-;-;ვე;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "მივიახლოვე;aorist;1;sg;მი;ვი;ახლოვ;-;-;ე;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "ვიახლოვე;aorist;1;sg;-;ვი;ახლო;-;-;ვე;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "მიმიახლოვებია;perfect;1;sg;მი;მი;ახლო;-;-;ვებია;T2;active;71-5;71;*ახლოვება;მიახლოვება",
        "მომიახლოვებია;perfect;1;sg;-;მომი;ახლო;-;-;ვებია;T2;active;71-5;71;*ახლოვება;მიახლოვება"]
};
const json5 =
{
    "emulated":true,
    "masdars": [
        "აახლოებინა;aorist;3;sg;-;ა;ახლო;ებ;-;ინა;KT;causative;71-17;71;*ახლოვებინება;დაახლოვებინება",
        "დააახლოებია;aorist;3;sg;და;ა;ახლო;ებ;-;ია;KT;causative;71-17;71;*ახლოვებინება;დაახლოვებინება",
        "დააახლოებინა;aorist;3;sg;და;ა;ახლო;ებ;-;ინა;KT;causative;71-17;71;*ახლოვებინება;დაახლოვებინება",
        "დამიახლოებიებია;perfect;1;sg;და;მი;ახლო;ებ;-;იებია;KT;causative;71-17;71;*ახლოვებინება;დაახლოვებინება",
        "დამიახლოებინებია;perfect;1;sg;და;მი;ახლო;ებ;-;ინებია;KT;causative;71-17;71;*ახლოვებინება;დაახლოვებინება",
        "დაგიახლოებიებია;perfect;2;sg;და;გი;ახლო;ებ;-;იებია;KT;causative;71-17;71;*ახლოვებინება;დაახლოვებინება"]
};
const json6 =
{
    "emulated":true,
    "masdars": [
        "დავიბარებ;future;1;sg;და;ვი;ბარ;ებ;-;-;T2;active;101-3;101;*ბარება;დაბარება",
        "დაიბარებ;future;2;sg;და;ი;ბარ;ებ;-;-;T2;active;101-3;101;*ბარება;დაბარება",
        "დაიბარებს;future;3;sg;და;ი;ბარ;ებ;-;ს;T2;active;101-3;101;*ბარება;დაბარება",
        "იბარე;aorist;2;sg;-;ი;ბარ;-;-;ე;T2;active;101-3;101;*ბარება;დაბარება",
        "დაიბარე;aorist;2;sg;და;ი;ბარ;-;-;ე;T2;active;101-3;101;*ბარება;დაბარება",
        "იბარა;aorist;3;sg;-;ი;ბარ;-;-;ა;T2;active;101-3;101;*ბარება;დაბარება",
        "დამიბარებია;perfect;1;sg;და;მი;ბარ;ებ;-;ია;T2;active;101-3;101;*ბარება;დაბარება",
        "დაგიბარებია;perfect;2;sg;და;გი;ბარ;ებ;-;ია;T2;active;101-3;101;*ბარება;დაბარება",
        "დაუბარებია;perfect;3;sg;და;უ;ბარ;ებ;-;ია;T2;active;101-3;101;*ბარება;დაბარება"]
};
const json7 =
{
    "emulated":true,
    "masdars": [
        "მიიბარებთ;future;2;pl;მი;ი;ბარ;ებ;-;თ;T2;active;101-4;101;*ბარება;მიბარება",
        "მიიბარებენ;future;3;pl;მი;ი;ბარ;ებ;-;ენ;T2;active;101-4;101;*ბარება;მიბარება",
        "ვიბარე;aorist;1;sg;-;ვი;ბარ;-;-;ე;T2;active;101-4;101;*ბარება;მიბარება",
        "მივიბარე;aorist;1;sg;მი;ვი;ბარ;-;-;ე;T2;active;101-4;101;*ბარება;მიბარება",
        "იბარე;aorist;2;sg;-;ი;ბარ;-;-;ე;T2;active;101-4;101;*ბარება;მიბარება",
        "მიმიბარებია;perfect;1;sg;მი;მი;ბარ;ებ;-;ია;T2;active;101-4;101;*ბარება;მიბარება",
        "მიგიბარებია;perfect;2;sg;მი;გი;ბარ;ებ;-;ია;T2;active;101-4;101;*ბარება;მიბარება",
        "მიუბარებია;perfect;3;sg;მი;უ;ბარ;ებ;-;ია;T2;active;101-4;101;*ბარება;მიბარება"]
};
const json8 =
{
    "emulated":true,
    "masdars": [
        "ჩავიბარებ;future;1;sg;ჩა;ვი;ბარ;ებ;-;-;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაიბარებ;future;2;sg;ჩა;ი;ბარ;ებ;-;-;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაიბარებს;future;3;sg;ჩა;ი;ბარ;ებ;-;ს;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩავიბარე;aorist;1;sg;ჩა;ვი;ბარ;-;-;ე;T2;active;101-5;101;*ბარება;ჩაბარება",
        "იბარე;aorist;2;sg;-;ი;ბარ;-;-;ე;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაიბარე;aorist;2;sg;ჩა;ი;ბარ;-;-;ე;T2;active;101-5;101;*ბარება;ჩაბარება",
        "იბარა;aorist;3;sg;-;ი;ბარ;-;-;ა;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაიბარა;aorist;3;sg;ჩა;ი;ბარ;-;-;ა;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩამიბარებია;perfect;1;sg;ჩა;მი;ბარ;ებ;-;ია;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაგიბარებია;perfect;2;sg;ჩა;გი;ბარ;ებ;-;ია;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაგვიბარებია;perfect;1;pl;ჩა;გვი;ბარ;ებ;-;ია;T2;active;101-5;101;*ბარება;ჩაბარება",
        "ჩაგიბარებიათ;perfect;2;pl;ჩა;გი;ბარ;ებ;-;იათ;T2;active;101-5;101;*ბარება;ჩაბარება"]
};
const json9 =
{
    "emulated":true,
    "masdars": [
        "გადააბარა;aorist;3;sg;გადა;ა;ბარ;-;-;ა;T5;active;101-8;101;*ბარება;გადაბარება",
        "ვაბარეთ;aorist;1;pl;-;ვა;ბარ;-;-;ეთ;T5;active;101-8;101;*ბარება;გადაბარება",
        "გადავაბარეთ;aorist;1;pl;გადა;ვა;ბარ;-;-;ეთ;T5;active;101-8;101;*ბარება;გადაბარება",
        "გადაგიბარებია;perfect;2;sg;გადა;გი;ბარ;ებ;-;ია;T5;active;101-8;101;*ბარება;გადაბარება",
        "გადაუბარებია;perfect;3;sg;გადა;უ;ბარ;ებ;-;ია;T5;active;101-8;101;*ბარება;გადაბარება",
        "გადაგვიბარებია;perfect;1;pl;გადა;გვი;ბარ;ებ;-;ია;T5;active;101-8;101;*ბარება;გადაბარება"]
};
const json10 =
{
    "emulated":true,
    "masdars": [
        "დავაბარებ;future;1;sg;და;ვა;ბარ;ებ;-;-;T5;active;101-9;101;*ბარება;დაბარება",
        "დააბარებ;future;2;sg;და;ა;ბარ;ებ;-;-;T5;active;101-9;101;*ბარება;დაბარება",
        "დააბარებს;future;3;sg;და;ა;ბარ;ებ;-;ს;T5;active;101-9;101;*ბარება;დაბარება",
        "დავაბარე;aorist;1;sg;და;ვა;ბარ;-;-;ე;T5;active;101-9;101;*ბარება;დაბარება",
        "აბარე;aorist;2;sg;-;ა;ბარ;-;-;ე;T5;active;101-9;101;*ბარება;დაბარება",
        "დამიბარებია;perfect;1;sg;და;მი;ბარ;ებ;-;ია;T5;active;101-9;101;*ბარება;დაბარება",
        "დაგიბარებია;perfect;2;sg;და;გი;ბარ;ებ;-;ია;T5;active;101-9;101;*ბარება;დაბარება",
        "დაუბარებია;perfect;3;sg;და;უ;ბარ;ებ;-;ია;T5;active;101-9;101;*ბარება;დაბარება"]
};
const json11 =
{
    "emulated":true,
    "masdars": [
        "მივაბარებ;future;1;sg;მი;ვა;ბარ;ებ;-;-;T5;active;101-10;101;*ბარება;მიბარება",
        "მიაბარებ;future;2;sg;მი;ა;ბარ;ებ;-;-;T5;active;101-10;101;*ბარება;მიბარება",
        "მიაბარებს;future;3;sg;მი;ა;ბარ;ებ;-;ს;T5;active;101-10;101;*ბარება;მიბარება",
        "მიაბარა;aorist;3;sg;მი;ა;ბარ;-;-;ა;T5;active;101-10;101;*ბარება;მიბარება",
        "ვაბარეთ;aorist;1;pl;-;ვა;ბარ;-;-;ეთ;T5;active;101-10;101;*ბარება;მიბარება",
        "მივაბარეთ;aorist;1;pl;მი;ვა;ბარ;-;-;ეთ;T5;active;101-10;101;*ბარება;მიბარება",
        "მიმიბარებია;perfect;1;sg;მი;მი;ბარ;ებ;-;ია;T5;active;101-10;101;*ბარება;მიბარება",
        "მომიბარებია;perfect;1;sg;-;მომი;ბარ;ებ;-;ია;T5;active;101-10;101;*ბარება;მიბარება",
        "მიგიბარებია;perfect;2;sg;მი;გი;ბარ;ებ;-;ია;T5;active;101-10;101;*ბარება;მიბარება"]
};
const json12 =
{
    "emulated":true,
    "masdars": [
        "მოვაბარებინებ;future;1;sg;მო;ვა;ბარ;ებ;-;ინებ;KT;causative;101-21;101;*ბარებინება;მობარებინება",
        "მოაბარებიებ;future;2;sg;მო;ა;ბარ;ებ;-;იებ;KT;causative;101-21;101;*ბარებინება;მობარებინება",
        "მოაბარებინებ;future;2;sg;მო;ა;ბარ;ებ;-;ინებ;KT;causative;101-21;101;*ბარებინება;მობარებინება",
        "ვაბარებიე;aorist;1;sg;-;ვა;ბარ;ებ;-;იე;KT;causative;101-21;101;*ბარებინება;მობარებინება",
        "ვაბარებინე;aorist;1;sg;-;ვა;ბარ;ებ;-;ინე;KT;causative;101-21;101;*ბარებინება;მობარებინება",
        "მოვაბარებიე;aorist;1;sg;მო;ვა;ბარ;ებ;-;იე;KT;causative;101-21;101;*ბარებინება;მობარებინება"]
};

*/
var vote = "";
const times = ['future', 'aorist', 'perfect'];

// console.log(`json is\r\n${json}`);
// document.getElementsByTagName("body")[0].setAttribute("onload", "Load()");

function GetMasdar(masdars) {
    // WITHOUT CHECK!!!
    // console.log(masdars);
    return masdars[0].split(';')[15];
}
function GetMasdarForm(masdars, time, number, all) {
    let data = masdars.filter(x => x.split(';')[1].toLowerCase() == time && x.split(';')[3].toLowerCase() == number);
    if (data) {
        if (all) {
            return data;
        }
        else {
            console.log(`Return data = ${data[0]}`);
            return data[0].split(';')[0];
        }
    }
    return "";
}
function GetMasdarForm_FutureSingleFirst(masdars) {
    return GetMasdarForm(masdars, 'future', 'sg', false);
}
function GetMasdarForm_AoristSingleFirst(masdars) {
    return GetMasdarForm(masdars, 'aorist', 'sg', false);
}
function GetMasdarForm_PerfectSingleFirst(masdars) {
    return GetMasdarForm(masdars, 'perfect', 'sg', false);
}
function GetMasdarForms_FutureSingle(masdars) {
    return GetMasdarForm(masdars, 'future', 'sg', true);
}
function GetMasdarForms_AoristSingle(masdars) {
    return GetMasdarForm(masdars, 'aorist', 'sg', true);
}
function GetMasdarForms_PerfectSingle(masdars) {
    return GetMasdarForm(masdars, 'perfect', 'sg', true);
}
function GetMasdarForms_FuturePlural(masdars) {
    return GetMasdarForm(masdars, 'future', 'pl', true);
}
function GetMasdarForms_AoristPlural(masdars) {
    return GetMasdarForm(masdars, 'aorist', 'pl', true);
}
function GetMasdarForms_PerfectPlural(masdars) {
    return GetMasdarForm(masdars, 'perfect', 'pl', true);
}

function CreateDiv(classes = [""], id = "", hidden = false) {
    const div = document.createElement("div");
    if (id != "") {
        div.id = id;
    }
    for (let i = 0; i < classes.length; i++) {
        if (classes[i] != "") {
            div.classList.add(classes[i]);
        }
    }
    if (hidden) {
        div.hidden = hidden;
    }
    return div;
}
function CreateLabel(value = "", id = "", styles = [""]) {
    const label = document.createElement("label");
    if (value != "") {
        label.innerHTML = value;
    }
    if (id != "") {
        label.id = id;
    }
    for (let i = 0; i < styles.length; i++) {
        if (styles[i] != "") {
            label.style[styles[i].split(":")[0]] = styles[i].split(":")[1];
        }
    }
    return label;
}
function CreateInput(value = "", type = "", disabled = false, hidden = false, styles = [""], id = "") {
    const input = document.createElement("input");
    if (value != "") {
        input.value = value;
    }
    if (type != "") {
        input.type = type;
    }
    if (disabled) {
        input.disabled = disabled;
    }
    if (hidden) {
        input.hidden = hidden;
    }
    for (let i = 0; i < styles.length; i++) {
        if (styles[i] != "") {
            input.style[styles[i].split(":")[0]] = styles[i].split(":")[1];
        }
    }
    if (id != "") {
        input.id = id;
    }
    return input;
}
function CreateButton(classes = [""], type = "", text = "", id = "", onclick = "", styles = [""]){
    const button = document.createElement("button");
    if (type != "") {
        button.type = type;
    }
    if (text != "") {
        button.innerHTML = text;
    }
    if (id != "") {
        button.id = id;
    }
    for (let i = 0; i < classes.length; i++) {
        if (classes[i] != "") {
            button.classList.add(classes[i]);
        }
    }
    if (onclick != "") {
        button.setAttribute("onclick", onclick);
    }
    for (let i = 0; i < styles.length; i++) {
        if (styles[i] != "") {
            button.style[styles[i].split(":")[0]] = styles[i].split(":")[1];
        }
    }
    // button.classList.add("btn");
    return button;
}
function CreateLegend(value = "") {
    const legend = document.createElement("legend");
    if (value != "") {
        legend.innerHTML = value;
    }
    return legend;
}
function CreateFieldset(classes = [""], legend = "") {
    const fieldset = document.createElement("fieldset");
    for (let i = 0; i < classes.length; i++) {
        if (classes[i] != "") {
            fieldset.classList.add(classes[i]);
        }
    }
    if (legend != "") {
        fieldset.appendChild(CreateLegend(legend));
    }
    return fieldset;
}
function CreateThead() {
    const thead = document.createElement("thead");
    thead.appendChild(CreateTr());
    return thead;
}
function CreateTbody() {
    const tbody = document.createElement("tbody");
    return tbody;
}
function CreateTh(child = "") {
    const th = document.createElement("th");
    if (child != "") {
        th.appendChild(child);
    }
    return th;
}
function CreateTr() {
    const tr = document.createElement("tr");
    return tr;
}
function CreateTd(value = "") {
    const td = document.createElement("td");
    if (value != "") {
        td.innerHTML = value;
    }
    return td;
}
function CreateTable() {
    const table = document.createElement("table");
    return table;
}
function CreateTextarea(rows = 0, cols = 0, placeholder = "", id ="", hidden = false) {
    const textarea = document.createElement("textarea");
    if (rows > 0) {
        textarea.setAttribute("rows", rows);
    }
    if (cols > 0) {
        textarea.setAttribute("cols", cols);
    }
    textarea.setAttribute("placeholder", placeholder);
    if (id != "") {
        textarea.id = id;
    }
    if (hidden) {
        textarea.hidden = hidden;
    }
    return textarea;
}

function cb(data) {
    console.log(data)
}

function Load(data,reloaded = false) {
    //var data = json3;
    console.log("Loading...");
    if (data["emulated"]) {
        console.log(`THIS DATA IS EMULATED!!!`);
    }
    console.log(`JSON data is\r\n`);
    console.log(data);
    //
    const masdarBox = document.getElementById("masdarBox");
    const div1 = CreateDiv(["rendered"]);
    const div2 = CreateDiv(["rendered"]);
    const divMore = CreateDiv(["rendered"], "divMore");
    const divSkip = CreateDiv(["rendered"], "divSkip");
    const divMasdar = CreateDiv(["rendered"], "divMasdar", false);
    const divComment = CreateDiv(["rendered"], "divComment", false);
    const divSubmit = CreateDiv(["rendered"], "divSubmit", true);
    const fieldSingle = CreateFieldset(["rendered"], "Serie I");
    const fieldPlural = CreateFieldset(["rendered"], "Plural");
    const tableSingle = CreateTable();
    const tablePlural = CreateTable();
    //
    div1.appendChild(CreateLabel("Consider the following verbal lemma:"));
    div1.appendChild(CreateInput(GetMasdar(data["masdars"]), "text", true, false, ["margin:10px"], "inputMasdar"));
    div2.appendChild(CreateLabel("Is it a valid lemma for the folowing inflected form?", "", ["padding-right:150px"]));
    div2.appendChild(CreateButton(["btn", "btn-outline-success"], "button", "Yes", "buttonYes", "Yes()", ["width:64px"]));
    div2.appendChild(CreateButton(["btn", "btn-outline-danger"], "button", "No", "buttonNo", "No()", ["width:64px"]));
    div2.appendChild(CreateButton(["btn", "btn-outline-info"], "button", "???", "buttonDontKnow", "DontKnow()", ["width:64px"]));
        divMore.appendChild(CreateButton(["btn", "btn-secondary"], "button", "Show all tenses", "buttonMore", "ShowAllTenses()"));
    //divSkip.appendChild(CreateButton(["btn", "btn-primary"], "button", "Skip this one and try another masdar", "buttonSkip", "SkipMasdar()"));
    divMasdar.appendChild(CreateTextarea(1, 40, "Please, propose the correct lemma.", "textareaMasdar", false));
    divComment.appendChild(CreateTextarea(3, 40, "Please, leave a comment.", "textareaComment", false));
    //divSubmit.appendChild(CreateButton(["btn", "btn-primary"], "button", "Submit", "buttonSubmit", "SubmitMasdar()"));
    //
    tableSingle.appendChild(CreateThead());
    tablePlural.appendChild(CreateThead());
    tableSingle.appendChild(CreateTbody());
    tablePlural.appendChild(CreateTbody());
    for (let i = 0; i < times.length; i++) {
        tableSingle.childNodes[0].childNodes[0].appendChild(CreateTh(CreateLabel(times[i].charAt(0).toUpperCase() + times[i].slice(1))));
        tablePlural.childNodes[0].childNodes[0].appendChild(CreateTh(CreateLabel(times[i].charAt(0).toUpperCase() + times[i].slice(1))));
    }
    let futureSingle = GetMasdarForms_FutureSingle(data["masdars"]);
    let aoristSingle = GetMasdarForms_AoristSingle(data["masdars"]);
    let perfectSingle = GetMasdarForms_PerfectSingle(data["masdars"]);
    let maxSingle = Math.max(futureSingle.length, aoristSingle.length, perfectSingle.length);
    console.log(`max = ${maxSingle}`)
    for (let i = 0; i < maxSingle; i++) {
        tableSingle.childNodes[1].appendChild(CreateTr());
        tableSingle.childNodes[1].childNodes[i].appendChild(CreateTd());

        if (futureSingle[i]) {
            tableSingle.childNodes[1].childNodes[i].childNodes[0].appendChild(CreateInput(futureSingle[i].split(';')[0], "text", true));
        }
        else
            tableSingle.childNodes[1].childNodes[i].childNodes[0].appendChild(CreateInput("", "text", true));
        tableSingle.childNodes[1].childNodes[i].appendChild(CreateTd());

        if (aoristSingle[i]) {
            tableSingle.childNodes[1].childNodes[i].childNodes[1].appendChild(CreateInput(aoristSingle[i].split(';')[0], "text", true));
        }
        else
            tableSingle.childNodes[1].childNodes[i].childNodes[1].appendChild(CreateInput("", "text", true));
        tableSingle.childNodes[1].childNodes[i].appendChild(CreateTd());

        if (perfectSingle[i]) {
            tableSingle.childNodes[1].childNodes[i].childNodes[2].appendChild(CreateInput(perfectSingle[i].split(';')[0], "text", true));
        }
        else
            tableSingle.childNodes[1].childNodes[i].childNodes[2].appendChild(CreateInput("", "text", true));

        if (i != 0) {
            tableSingle.childNodes[1].childNodes[i].hidden = true;
        }
    }
    let futurePlural = GetMasdarForms_FuturePlural(data["masdars"]);
    let aoristPlural = GetMasdarForms_AoristPlural(data["masdars"]);
    let perfectPlural = GetMasdarForms_PerfectPlural(data["masdars"]);
    let maxPlural = Math.max(futurePlural.length, aoristPlural.length, perfectPlural.length);
    console.log(`max = ${maxPlural}`)
    for (let i = 0; i < maxPlural; i++) {
        tablePlural.childNodes[1].appendChild(CreateTr());
        tablePlural.childNodes[1].childNodes[i].appendChild(CreateTd());
        if (futurePlural[i]) {
            tablePlural.childNodes[1].childNodes[i].childNodes[0].appendChild(CreateInput(futurePlural[i].split(';')[0], "text", true));
        }
        tablePlural.childNodes[1].childNodes[i].appendChild(CreateTd());
        if (aoristPlural[i]) {
            tablePlural.childNodes[1].childNodes[i].childNodes[1].appendChild(CreateInput(aoristPlural[i].split(';')[0], "text", true));
        }
        tablePlural.childNodes[1].childNodes[i].appendChild(CreateTd());
        if (perfectPlural[i]) {
            tablePlural.childNodes[1].childNodes[i].childNodes[2].appendChild(CreateInput(perfectPlural[i].split(';')[0], "text", true));
        }
    }
    //
    fieldSingle.appendChild(tableSingle);
    fieldPlural.appendChild(tablePlural);
    //
    masdarBox.appendChild(div1);
    masdarBox.appendChild(div2);
    masdarBox.appendChild(fieldSingle);
    if (maxPlural > 0) {
        fieldPlural.hidden = true;
        masdarBox.appendChild(fieldPlural);
    }
    masdarBox.appendChild(divMore);
    masdarBox.appendChild(divSkip);
    masdarBox.appendChild(divMasdar);
    masdarBox.appendChild(divComment);
    masdarBox.appendChild(divSubmit);
    //
    document.getElementsByTagName("body")[0].removeAttribute("onload");
    console.log("Loaded!");
}

function Yes() {
    console.log("Yes");
    vote = "Yes";
    document.getElementById("buttonYes").classList.remove("btn-outline-success");
    document.getElementById("buttonYes").classList.add("btn-success");
    document.getElementById("buttonNo").classList.add("btn-outline-danger");
    document.getElementById("buttonNo").classList.remove("btn-danger");
    //
    //document.getElementById("textareaMasdar").hidden = true;
    //document.getElementById("textareaComment").hidden = false;
    document.getElementById("textareaComment").scrollIntoView();
    divSubmit.hidden = false;
}

function No() {
    console.log("No");
    vote = "No";
    document.getElementById("buttonNo").classList.remove("btn-outline-danger");
    document.getElementById("buttonNo").classList.add("btn-danger");
    document.getElementById("buttonYes").classList.add("btn-outline-success");
    document.getElementById("buttonYes").classList.remove("btn-success");
    document.getElementById("buttonDontKnow").classList.add("btn-outline-info");
    document.getElementById("buttonDontKnow").classList.remove("btn-info");
        //
    //document.getElementById("textareaMasdar").hidden = false;
    //document.getElementById("textareaComment").hidden = false;
    document.getElementById("textareaMasdar").scrollIntoView();
    divSubmit.hidden = false;
}

function DontKnow() {
    console.log("DontKnow");
    vote = "Comment";
    document.getElementById("buttonDontKnow").classList.remove("btn-outline-info");
    document.getElementById("buttonDontKnow").classList.add("btn-info");
    document.getElementById("buttonNo").classList.add("btn-outline-danger");
    document.getElementById("buttonNo").classList.remove("btn-danger");
    document.getElementById("buttonNo").classList.add("btn-outline-success");
    document.getElementById("buttonNo").classList.remove("btn-success");
    //
    //document.getElementById("textareaMasdar").hidden = true;
    //document.getElementById("textareaComment").hidden = false;
    document.getElementById("textareaComment").scrollIntoView();
    divSubmit.hidden = false;
}

function ShowAllTenses() {
    let tables = document.getElementsByTagName("table");
    for (let i = 0; i < tables[0].children[1].children.length; i++) {
        tables[0].children[1].children[i].removeAttribute("hidden");
    }
    let fields = document.getElementsByTagName("fieldset");
    if (fields[1]) {
        fields[1].removeAttribute("hidden");
    }
    document.getElementById("divMore").remove();
}

async function SkipJSON() {
    const response = await fetch("http://[::1]/headwork/skip.json");
    const jsonData = await response.json();
    console.log(jsonData);
  }

async function SkipMasdar() {
    Array.from(document.getElementsByClassName("rendered")).forEach(element => element.remove());
    console.log("It must say to back to give another JSON and then re-render the page");
    //Load(true);
    const h2 = document.createElement("h2");
    h2.innerHTML = "Not ready yet!";
    document.getElementById("masdarBox").appendChild(h2);

    await SkipJSON();

}

function SubmitMasdar() {

    // to be implemented! 20230608-1615
    let answer = {
        "ID":`${document.getElementById("inputMasdar").value}`,
        "proposedMasdar":`${document.getElementById("textareaMasdar").value}`,
        "vote":`${vote}`,
        "comment":`${document.getElementById("textareaComment").value}`
    };

    console.log(answer);

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
    console.log(xhr.status);
    console.log(xhr.responseText);
    }};

    xhr.send(answer);
}

