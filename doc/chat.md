# HEADWORK's chat

## Chat access
The chat is reachable from anywhere on the platform while you're logged in.  
A button is fixed to the left on the screen which displays/hide the chat as you click it.

## New database schema
2 new tables have been created in the database : `chat_post` and `chat_topic`.  
Those tables are automatically managed so you don't have anything to do with them.  
However, feel free to erase their content in `init-workflow.sql` as a chat doesn't need its previous 'life' content. A forum have been created for that.

## Chat update
The system uses Ajax to update its content every 15 seconds.  
Sending a message also trigger an instant update.

## Chat categories
The chat is splitted between 2 categories : `Project` and `Task`.

### `Project` category
This category is available wherever the chat is accessible.  
The discussion is opened to anything related to HEADWORK.

### `Task` category
This category is only available on a task page.  
Each task has its own chat which in not shared between all tasks.  
The discussion must be about the task you're in.
