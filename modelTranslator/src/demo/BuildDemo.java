package demo;

import model.*;
import model.actions.*;
import model.connections.*;

import java.util.ArrayList;
import java.util.List;

public class BuildDemo {

    public static void main(String[] args) {

        PlatformConnector connector = new HeadworkPlatformConnector();
        //connector.prepareDB();
        //    Définir les attributs des artefacts ---------------------------------------------------------------

        // artifact User : attributs
        Parameters<String> userAttributes = new Parameters<>();
        userAttributes.addParam("location", "varchar(100)");

        // artifact Photo : attributs
        Parameters<String> photoAttributes = new Parameters<>();
        photoAttributes.addParam("image", "varchar(500)")
                .addParam("taxon", "varchar(100)");

        //subArtifact Identification : attributs
        Parameters<String> identificationAttributes = new Parameters<>();
        identificationAttributes.addParam("image", "varchar(500)")
                .addParam("taxon", "varchar(100)");

        //    Définir les actions des artefacts ---------------------------------------------------------------

        //artefact User : actions
        List<Action> userActions = new ArrayList<>();

        // fonction de création de la photo
        Parameters<Object> creationParams = new Parameters<>();
        creationParams.addParam("title", "Upload a photo")
                .addParam("newArtifact", "Photo")
                .addParam("newArtAttributes", photoAttributes)
                .addParam("description", "Add a new photo to your collection")
                .addParam("body","Upload here a new photo to add to your collection.")
                .addParam("checker",null)
                .addParam("checkermsg","please, enter an filename");
        userActions.add(new CreationAction("select true from dual", creationParams));

        // fonction de changement de localisation
        Parameters<Object> updateParams = new Parameters<>();
        updateParams.addParam("title", "Change your location")
                .addParam("attribute", "location")
                .addParam("artifact", "User")
                .addParam("type", "varchar(100)")
                .addParam("description", "Where do you take your pictures ?")
                .addParam("body","Please indicate here where you take your picture.")
                .addParam("checker",null).
                addParam("checkermsg","free input");
        userActions.add(new UpdateAction("select true from dual", updateParams));

        // artefact photo : actions
        List<Action> photoActions = new ArrayList<>();

        // fonction d'identification
        Parameters<Object> params2 = new Parameters<>();
        params2.addParam("title", "identify")
                .addParam("attribute", "taxon")
                .addParam("artifact", "Photo")
                .addParam("type", "varchar(50)")
                .addParam("description", "Identify the insect on the photo")
                .addParam("body", "Identification")
                .addParam("checker", null)
                .addParam("checkermsg", "insert a taxon");
        photoActions.add(new UpdateAction("select true from dual", params2));

        //TODO : active subartifacts//TODO : déplacer une partie dans Delegation (where id=...)
        String goodSubArtId = "where id in (select ATTRIBUTES from ARTIFACT " +
                                    "where ID in (select idChild from subArtifacts " +
                                        "where idParent=(select id from CURRENTARTIFACT " +
                                            "where USER=SESSION_USER" +
                                        ")" +
                                    ")" +
                                ")";
        String selectAndCountAnswers = "select taxon, count(*) as count from IdentificationArtifact " +
                                            goodSubArtId +
                                            " group by taxon";
        // artefact photo : demande d'identification ------------ DÉLÉGATION  ----------------------
        Parameters<Object> paramsDelegation = new Parameters<>();
        paramsDelegation.addParam("title", "Ask Identification")
                .addParam("parentArtifact", "Photo")
                .addParam("deep", 3)
                .addParam("description", "Ask other users to identify the insect")
                .addParam("body", "")
                .addParam("checker", null)
                .addParam("checkermsg", "nothing to enter, just click below")
                .addParam("monitoringMessage","(select CONCAT(" +
                        "(select count(taxon) from IdentificationArtifact "+goodSubArtId+" )," +
                        "'/2 have answered'))");

        Parameters<String> map = new Parameters<>();
        map.addParam("Identification",
                "select users.id as idPeer, '[sql](select image from PhotoArtifact " +
                "where id=(select ATTRIBUTES from ARTIFACT where ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER)))' as image from users;");
            //"select users.id as idPeer from users");

        photoActions.add(new Delegation(
                "select true from dual",
                map,
                "select count(*)>1 from IdentificationArtifact where taxon IS NOT NULL",
                "update PhotoArtifact set taxon=( " +
                        "(select taxon from (select taxon, max(count) from (" +
                        selectAndCountAnswers + ") as countedTaxon where taxon is not null group by taxon limit 1) as maxTaxon)" +
                        ") where id=(select ATTRIBUTES from ARTIFACT where ID=(select ID from CURRENTARTIFACT where USER=SESSION_USER))",
                paramsDelegation,
                connector.getDBConnection()));

        // artefact identification : actions
        List<Action> identificationActions = new ArrayList<>();

        // fonction d'identification
        Parameters<Object> params3 = new Parameters<>();
        params3.addParam("title", "identify")
                .addParam("attribute", "taxon")
                .addParam("artifact", "Identification")
                .addParam("type", "varchar(50)")
                .addParam("description", "Identify the insect on the photo")
                .addParam("body", "Identification")
                .addParam("checker", null).
                addParam("checkermsg", "insert a taxon");
        identificationActions.add(new UpdateAction("select true from dual", params3));

        /*
        // artefact identification : demande d'identification !!!!!!!!!!!!!! DÉLÉGATION  !!!!!!!!!!!!!!!!!!!!!!
        Parameters<Object> paramsDelegation2 = new Parameters<>();
        paramsDelegation2.addParam("title", "askIdentification")
                .addParam("parentArtifact", "Identification")
                .addParam("deep", 2)
                .addParam("description", "Ask other users to identify the insect")
                .addParam("body", "")
                .addParam("checker", null).
                addParam("checkermsg", "nothing to enter, just click below");

        Parameters<String> map2 = new Parameters<>();
        map2.addParam("Identification","select users.id as idPeer from users");

        identificationActions.add(new Delegation(
                "select true from dual",
                map2,
                "select count(*)>1 from IdentificationArtifact where taxon IS NOT NULL",
                "insert into result values (0, " +
                        "'identification of others for the picture', " +
                        "(select taxon from (select taxon, max(count) from " +
                        "(select taxon, count(*) as count from IdentificationArtifact " +
                        "where id in (select ATTRIBUTES from ARTIFACT " +
                        "where ID in (select idChild from subArtifacts " +
                        "where idParent=(select id from CURRENTARTIFACT where USER=SESSION_USER)" +
                        ")" +
                        ")" +
                        ") as countedTaxon ) as maxTaxon)" +
                        ")",
                paramsDelegation2,
                connector.getDBConnection()));

         */

        connector.CreateArtifactClass("User", userAttributes, userActions);
        connector.CreateArtifactClass("Photo", photoAttributes, photoActions);
        connector.CreateArtifactClass("Identification", photoAttributes, identificationActions);

        // définir ce qui est là dès le début ----------------------------------------------------------------------
        List<Parameters> listArtifacts = new ArrayList<>();
        Parameters<Object> firstArtifacts = new Parameters<>();
        firstArtifacts.addParam("artifact", "User")
                .addParam("request",
                        "select users.id as idPeer, 'Somewhere' as location from users");

        listArtifacts.add(firstArtifacts);

        connector.instantiate(listArtifacts);
    }
}

