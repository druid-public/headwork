package model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Counter for the identifier in all the application
 */
public class Counter {

    private static final AtomicInteger counter = new AtomicInteger(1);
    private static final Map<String, AtomicInteger> counters = new HashMap();

    /**
     * General counter
     * @return 1 or 1 more than the return of the previous call
     */
    public static int nextValue() {
        return counter.getAndIncrement();
    }

    /**
     * Counter specific to a key
     * @param counterName {String} the key
     * @return 1 or 1 more to than the previous call with the same key
     */
    public static int nextValue(String counterName) {
        AtomicInteger c = counters.get(counterName);
        if (c != null)
            return c.getAndIncrement();
        c = new AtomicInteger(1);
        counters.put(counterName, c);
        return c.getAndIncrement();
    }
}
