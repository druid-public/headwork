import requests

# 'spgp'@'locahost' spgpPASSWORD
"""
create table spipoll_headwork as 
(
select s.id, s.userId, s.resourceId, f.url, s.typeId, t.value, o.nbValidation, o.nbSuggestIdent, o.taxonId, i.fr_name 
from social_event as s, social_event_type as t, spipoll_observation as o, spipoll_insecte as i, Files as f 
where s.typeId = t.id and s.typeId in (2,3) and s.resourceId = o.id and i.id = o.taxonId and f.id = o.imgTaxon1
);

"""


hostname = 'localhost'
username = 'headworkadmin'
password = 'HEAg2r4a3g2'
database = 'headwork'

import urllib.request
import random


def downloader(image_url, id_, name):
    #file_name = random.randrange(1,10000)
    full_file_name = str(id_) +'_'+name
    urllib.request.urlretrieve(image_url,'spipoll_headwork/'+full_file_name)



a = 0
# Simple routine to run a query on a database and print the results:
def doQuery( conn , c, a) :
    cur = conn.cursor()

    cur.execute( "SELECT id, url FROM spipoll_headwork where id > "+str(c) )

    for id_, url in cur.fetchall() :
        
        try : 
            print(' n ', a, ' => ', id_, url)
            print(url.split('/')[len(url.split('/'))-1])
            img_url = "https://api.spgp.pw" + url
            downloader(img_url, id_, url.split('/')[len(url.split('/'))-1])
            
        except:
            print("------------------- BREAK ", c, " - ", a, "-----------------------")
            #doQuery( conn , c, a)
            break
        a = a + 1
        c = id_



def createTable( conn ):

    mycursor = conn.cursor()
    sql = "DROP TABLE IF EXISTS taxonHeadwork"
    mycursor.execute(sql)
    mycursor = conn.cursor()
    sql = "CREATE TABLE taxonHeadwork ( id INT AUTO_INCREMENT PRIMARY KEY, id_ordre int, id_famille int, id_genre int, id_type int, taxon varchar(255) ) "
    mycursor.execute(sql)


def doTaxonomy( conn ) :
    a = 0
    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try : 

            #print(" ORDRE ")
            #print(id_ordre, ' - ', parent_ordre, ' - ', taxon_ordre)            
            #print()

            cur_famille = conn.cursor()
            cur_famille.execute( " select id as id_famille, parent_skill as parent_ordre, skill as taxon_famille " +
                                 " from skillTree " + 
                                 " where parent_skill = "+ str(id_ordre) )
            for id_famille, parent_ordre, taxon_famille in cur_famille.fetchall() :
                try:
                    #print(" FAMILLE ")
                    #print(parent_ordre, ' ', taxon_ordre, ' ', id_famille, ' ', taxon_famille) 
                    
                    cur_genre = conn.cursor()
                    cur_genre.execute( " select id as id_genre, parent_skill as parent_famille, skill as taxon_genre " +
                                 " from skillTree " + 
                                 " where parent_skill = "+ str(id_famille) )
                    for id_genre, parent_famille, taxon_genre in cur_genre.fetchall() :
                        try:
                            #print(" GENRE ")
                            #print(parent_ordre, ' ', taxon_ordre, ' ', parent_famille, ' ', taxon_famille, ' ', id_genre, ' ', taxon_genre) 

                            cur_type = conn.cursor()
                            cur_type.execute( " select id as id_type, parent_skill as parent_genre, skill as taxon_type " +
                                 " from skillTree " + 
                                 " where parent_skill = "+ str(id_genre) )
                            for id_type, parent_genre, taxon_type in cur_type.fetchall() :
                                try:
                                    #print(" TYPE ")
                                    #print(parent_ordre, ' ', taxon_ordre, ' ', parent_famille, ' ', taxon_famille, ' ', id_genre, ' ', taxon_genre, ' ', id_type, ' ', taxon_type) 
                                    cur_taxon = conn.cursor()
                                    cur_taxon.execute( " select id as id_taxon, parent_skill as parent_type, skill as taxon " +
                                                      " from skillTree " + 
                                                      " where parent_skill = "+ str(id_type) )
                                    for id_taxon, parent_type, taxon in cur_taxon.fetchall() :
                                        try:
                                            print(str(a), ' ', parent_ordre, ' ', taxon_ordre, ' ', parent_famille, ' ', taxon_famille, ' ', parent_genre, ' ', taxon_genre, ' ', parent_type, ' ', taxon_type, ' ', id_taxon, ' ', taxon) 
                                            a = a + 1
                                            insert = conn.cursor()

                                            sql = "INSERT INTO taxonHeadwork ( id_ordre, id_famille, id_genre, id_type, taxon ) VALUES (%s, %s, %s, %s, %s)"
                                            val = (parent_ordre, parent_famille, parent_genre, parent_type, taxon)
                                            insert.execute(sql, val)
                                            conn.commit()
                                        except:
                                            break



                                except:
                                    break


                    
                        except:
                            break

                    #print()

                except:
                    break
            #print()



        except:
            #print("------------------- BREAK ", c, " - ", a, "-----------------------")
            #doQuery( conn , c, a)
            break

print("Using mysql.connector…")
import mysql.connector
myConnection = mysql.connector.connect( host=hostname, user=username, passwd=password, db=database )
#doQuery( myConnection , 32977, 2675)
createTable( myConnection )
doTaxonomy( myConnection )

myConnection.close()



