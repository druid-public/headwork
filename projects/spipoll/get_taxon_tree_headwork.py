import requests

# 'spgp'@'locahost' spgpPASSWORD
"""
create table spipoll_headwork as
(
select s.id, s.userId, s.resourceId, f.url, s.typeId, t.value, o.nbValidation, o.nbSuggestIdent, o.taxonId, i.fr_name
from social_event as s, social_event_type as t, spipoll_observation as o, spipoll_insecte as i, Files as f
where s.typeId = t.id and s.typeId in (2,3) and s.resourceId = o.id and i.id = o.taxonId and f.id = o.imgTaxon1
);

"""


hostname = 'localhost'
username = 'headworkadmin'
password = 'HEAg2r4a3g2'
database = 'headwork'

import urllib.request
import random





def createTableTemplate( conn ):
    mycursor = conn.cursor()
    sql = "DROP TABLE IF EXISTS templateTable"
    mycursor.execute(sql)
    mycursor = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS templateTable ( id INT AUTO_INCREMENT PRIMARY KEY, niv_taxon varchar(150), parent varchar(255), nbchild int, body varchar(10000) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
    mycursor.execute(sql)

def getORDRE_FAMILLE_LIST( conn ):
    cur_list = conn.cursor()
    cur_list.execute(" select * from skillTree ")
    return cur_list


def insertTemplateORDRE( conn ):
    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    #body = "Proposer un ORDRE taxon appartenant l'insecte sur la photo <br/>" + '\n'
    body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
            '<input type="radio" name="MyRadio" value="q1" id="r1" onclick="Checkradiobutton()" checked> ORDRE' + '\n' +
            '<select id=\"myselect\" name=\"select_q1\" onchange=\"myFunction()\"> ' + '\n')


    body += '<option value="-">-</option>' + '\n'

    ct = 0
    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try :
            body += '<option value=\"'+taxon_ordre+'\">'+taxon_ordre+'</option>' + '\n'
            ct = ct+1
            #print(body)
            #print()
            #print()
        except TypeError as e:
            print(e)
            #return None
            break
    body += ('</select>\r' + '\n' +
             '<br>' + '\n')

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    body += ('<input type="radio" name="MyRadio" value="q2" id="r2" onclick="Checkradiobutton()"> FAMILLE' + '\n' +
             '<select id=\"myselect2\" name=\"select_q2\" onchange=\"myFunction()\" disabled> ' + '\n')
    body += '<option value="-">-</option>' + '\n'

    #ct = 0
    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try :
            #body += '<option value=\"'+taxon_ordre+'\">'+taxon_ordre+'</option>' + '\n'
            body += '<optgroup label=\"'+taxon_ordre+'\">' + '\n'
            #ct = ct+1

            cur_famille = conn.cursor()
            cur_famille.execute( " select id as id_famille, parent_skill as parent_ordre, skill as taxon_famille " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_ordre) )
            #body += '<option value="-">-</option>' + '\n'
            #ct = 0
            for id_famille, parent_ordre, taxon_famille in cur_famille.fetchall() :
                body += '<option value=\"'+taxon_famille+'\">'+taxon_famille+'</option>' + '\n'
                #ct = ct+1
            body += '</optgroup>'


        except TypeError as e:
            print(e)
            #return None
            break
    body += ('</select>\r' + '\n' +
             '<br>' + '\n')

    body += ('<input type="radio" name="MyRadio" value="qPrecis" id="r3" onclick="Checkradiobutton()"> Taxon précis' + '\n' +
             '<input type="text" name="select_qPrecis" id="select_qPrecis" data-provide="select_qPrecis" disabled> <br>')

    body += ('<script> '  + '\n' +
             'function myFunction(){ ' + '\n' +
             'var myDiv = document.getElementById("answer"); ' + '\n' +
             'var select = document.getElementById("select_q1").value; ' + '\n' +
             'myDiv.value = select;}' + '\n' +
             'function Checkradiobutton(){ ' + '\n' +
                 'if(document.getElementById("r1").checked)' + '\n' +
                   '{' + '\n' +
                    ' document.getElementById("select_qPrecis").disabled=true;' + '\n' +
                    ' document.getElementById("myselect").disabled = false;' + '\n' +
                    ' document.getElementById("myselect2").disabled = true;' + '\n' +
                   '}else if(document.getElementById("r2").checked){' + '\n' +
                     'document.getElementById("select_qPrecis").disabled = true;' + '\n' +
                     'document.getElementById("myselect").disabled = true;' + '\n' +
                     'document.getElementById("myselect2").disabled = false;' + '\n' +
                   '}else{' + '\n' +
                     'document.getElementById("select_qPrecis").disabled = false;' + '\n' +
                     'document.getElementById("myselect").disabled = true;' + '\n' +
                     'document.getElementById("myselect2").disabled = true;' + '\n' +
                   '}' + '\n' +
               '}' + '\n' +
               '</script>')

    #print(body)

    insert = conn.cursor()
    sql = "INSERT INTO templateTable ( niv_taxon, parent, nbchild, body ) VALUES (%s, %s, %s, %s)"
    val = ('ORDRE', 'spipoll', str(ct), body)
    insert.execute(sql, val)
    conn.commit()


def insertTemplateORDRE_FAMILLE( conn ):

    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")



    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try :
            #body = "Proposer une FAMILLE taxon appartenant l'insecte sur la photo <br/>" + '\n'
            #body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
            #    '<select id=\"myselect\" name=\"myselect\" onchange=\"myFunction()\"> ' + '\n')
            body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
            '<input type="radio" name="MyRadio" value="q2" id="r2" onclick="Checkradiobutton()" checked> FAMILLE' + '\n' +
            '<select id=\"myselect\" name=\"select_q2\" onchange=\"myFunction()\"> ' + '\n')

            cur_famille = conn.cursor()
            cur_famille.execute( " select id as id_famille, parent_skill as parent_ordre, skill as taxon_famille " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_ordre) )
            body += '<option value="-">-</option>' + '\n'
            ct = 0
            for id_famille, parent_ordre, taxon_famille in cur_famille.fetchall() :
                body += '<option value=\"'+taxon_famille+'\">'+taxon_famille+'</option>' + '\n'
                ct = ct+1


            #body += '<option value=\"'+taxon_ordre+'\">'+taxon_ordre+'</option>' + '\n'

            #print()
        except TypeError as e:
            print(e)
            #return None
            break

        #body += ('</select>\r\n<script> function myFunction(){ ' + '\n' +
        #        'var myDiv = document.getElementById("answer"); ' + '\n' +
        #        'var select = document.getElementById("myselect").value; ' + '\n' +
        #        'myDiv.value = select;} </script>')

        body += ('</select>\r' + '\n' +
             '<br>' + '\n' +
             '<input type="radio" name="MyRadio" value="qPrecis" id="r3" onclick="Checkradiobutton()"> Taxon précis' + '\n' +
             '<input type="text" name="select_qPrecis" id="select_qPrecis" data-provide="select_qPrecis" disabled> <br>' + '\n'
            )

        body += ('<script> '  + '\n' +
             'function myFunction(){ ' + '\n' +
             'var myDiv = document.getElementById("answer"); ' + '\n' +
             'var select = document.getElementById("select_q1").value; ' + '\n' +
             'myDiv.value = select;}' + '\n' +
             'function Checkradiobutton(){ ' + '\n' +
                 'if(document.getElementById("r1").checked)' + '\n' +
                   '{' + '\n' +
                    ' document.getElementById("select_qPrecis").disabled=true;' + '\n' +
                    ' document.getElementById("myselect").disabled = false;' + '\n' +
                   '}else{' + '\n' +
                     'document.getElementById("select_qPrecis").disabled = false;' + '\n' +
                     'document.getElementById("myselect").disabled = true;' + '\n' +
                   '}' + '\n' +
               '}' + '\n' +
               '</script>')

        #print(body)
        #print()

        insert = conn.cursor()
        sql = "INSERT INTO templateTable ( niv_taxon, parent, nbchild, body ) VALUES (%s, %s, %s, %s)"
        val = ('FAMILLE', taxon_ordre, str(ct), body)
        insert.execute(sql, val)
        conn.commit()

def insertTemplateORDRE_FAMILLE_GENRE( conn ):

    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try :


            cur_famille = conn.cursor()
            cur_famille.execute( " select id as id_famille, parent_skill as parent_ordre, skill as taxon_famille " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_ordre) )
            for id_famille, parent_ordre, taxon_famille in cur_famille.fetchall() :
                try:
                    #body = "Proposer un GENRE taxon appartenant l'insecte sur la photo <br/>" + '\n'
                    #body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
                    #         '<select id=\"myselect\" name=\"myselect\" onchange=\"myFunction()\"> ' + '\n')
                    body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
                            '<input type="radio" name="MyRadio" value="q1" id="r1" onclick="Checkradiobutton()" checked> GENRE' + '\n' +
                            '<select id=\"myselect\" name=\"select_q1\" onchange=\"myFunction()\"> ' + '\n')

                    cur_genre = conn.cursor()
                    cur_genre.execute( " select id as id_genre, parent_skill as parent_famille, skill as taxon_genre " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_famille) )
                    body += '<option value="">-</option>' + '\n'
                    ct = 0
                    for id_genre, parent_famille, taxon_genre in cur_genre.fetchall() :

                        body += '<option value=\"'+taxon_genre+'\">'+taxon_genre+'</option>' + '\n'
                        ct = ct + 1


                    """body += ('</select>\r\n<script> function myFunction(){ ' + '\n' +
                             'var myDiv = document.getElementById("answer"); ' + '\n' +
                             'var select = document.getElementById("myselect").value; ' + '\n' +
                             'myDiv.value = select;} </script>')"""
                    body += ('</select>\r' + '\n' +
                             '<br>' + '\n' +
                             '<input type="radio" name="MyRadio" value="qPrecis" id="r2" onclick="Checkradiobutton()"> Taxon précis' + '\n' +
                             '<input type="text" name="select_qPrecis" id="select_qPrecis" data-provide="select_qPrecis" disabled> <br>' + '\n'
                             )

                    body += ('<script> '  + '\n' +
                            'function myFunction(){ ' + '\n' +
                            'var myDiv = document.getElementById("answer"); ' + '\n' +
                            'var select = document.getElementById("select_q1").value; ' + '\n' +
                            'myDiv.value = select;}' + '\n' +
                            'function Checkradiobutton(){ ' + '\n' +
                            'if(document.getElementById("r1").checked)' + '\n' +
                            '{' + '\n' +
                            ' document.getElementById("select_qPrecis").disabled=true;' + '\n' +
                            ' document.getElementById("myselect").disabled = false;' + '\n' +
                            '}else{' + '\n' +
                            'document.getElementById("select_qPrecis").disabled = false;' + '\n' +
                            'document.getElementById("myselect").disabled = true;' + '\n' +
                            '}' + '\n' +
                            '}' + '\n' +
                            '</script>')

                    #print(body)
                    #print()
                except TypeError as e:
                    print(e)
                    break
                insert = conn.cursor()
                sql = "INSERT INTO templateTable ( niv_taxon, parent, nbchild, body ) VALUES (%s, %s, %s, %s)"
                val = ('GENRE', taxon_famille, str(ct), body)
                insert.execute(sql, val)
                conn.commit()
        except TypeError as e:
            print(e)
            #return None
            break


def insertTemplateORDRE_FAMILLE_GENRE_ESPECE( conn ):

    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try :
            cur_famille = conn.cursor()
            cur_famille.execute( " select id as id_famille, parent_skill as parent_ordre, skill as taxon_famille " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_ordre) )
            for id_famille, parent_ordre, taxon_famille in cur_famille.fetchall() :
                try:
                    cur_genre = conn.cursor()
                    cur_genre.execute( " select id as id_genre, parent_skill as parent_famille, skill as taxon_genre " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_famille) )

                    for id_genre, parent_famille, taxon_genre in cur_genre.fetchall() :
                        try:
                            #body = "Proposer une ESPECE taxon appartenant l'insecte sur la photo <br/>" + '\n'
                            #body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
                            #         '<select id=\"myselect\" name=\"myselect\" onchange=\"myFunction()\"> ' + '\n')
                            body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
                                    '<input type="radio" name="MyRadio" value="q1" id="r1" onclick="Checkradiobutton()" checked> ESPECE' + '\n' +
                                    '<select id=\"myselect\" name=\"select_q1\" onchange=\"myFunction()\"> ' + '\n')
                            cur_type = conn.cursor()
                            cur_type.execute( " select id as id_espece, parent_skill as parent_genre, skill as taxon_type " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_genre) )
                            ct = 0
                            body += '<option value="">-</option>' + '\n'
                            for id_espece, parent_genre, taxon_type in cur_type.fetchall() :
                                body += '<option value=\"'+taxon_type+'\">'+taxon_type+'</option>' + '\n'
                                ct = ct + 1

                            """body += ('</select>\r\n<script> function myFunction(){ ' + '\n' +
                                     'var myDiv = document.getElementById("answer"); ' + '\n' +
                                     'var select = document.getElementById("myselect").value; ' + '\n' +
                                     'myDiv.value = select;} </script>')"""


                            body += ('</select>\r' + '\n' +
                                     '<br>' + '\n' +
                                     '<input type="radio" name="MyRadio" value="qPrecis" id="r2" onclick="Checkradiobutton()"> Taxon précis' + '\n' +
                                     '<input type="text" name="select_qPrecis" id="select_qPrecis" data-provide="select_qPrecis" disabled> <br>' + '\n'
                                    )

                            body += ('<script> '  + '\n' +
                                     'function myFunction(){ ' + '\n' +
                                     'var myDiv = document.getElementById("answer"); ' + '\n' +
                                     'var select = document.getElementById("select_q1").value; ' + '\n' +
                                     'myDiv.value = select;}' + '\n' +
                                     'function Checkradiobutton(){ ' + '\n' +
                                     'if(document.getElementById("r1").checked)' + '\n' +
                                     '{' + '\n' +
                                     ' document.getElementById("select_qPrecis").disabled=true;' + '\n' +
                                     ' document.getElementById("myselect").disabled = false;' + '\n' +
                                     '}else{' + '\n' +
                                     'document.getElementById("select_qPrecis").disabled = false;' + '\n' +
                                     'document.getElementById("myselect").disabled = true;' + '\n' +
                                     '}' + '\n' +
                                     '}' + '\n' +
                                     '</script>')
                            #print(body)
                            #print()

                        except:
                            break

                        insert = conn.cursor()
                        sql = "INSERT INTO templateTable ( niv_taxon, parent, nbchild, body ) VALUES (%s, %s, %s, %s)"
                        val = ('ESPECE', taxon_genre, str(ct), body)
                        insert.execute(sql, val)
                        conn.commit()

                except TypeError as e:
                    print(e)
                    break

        except TypeError as e:
            print(e)
            #return None
            break


def insertTemplateORDRE_FAMILLE_GENRE_ESPECE_TAXON( conn ):

    cur_ordre = conn.cursor()

    cur_ordre.execute( "select id as id_ordre, parent_skill as parent_ordre, skill as taxon_ordre " +
                 "from skillTree " +
                 "where skill in ('Orthopteres', 'Coleopteres', 'Lepidopteres', 'Hemipteres', 'Hymenopteres', 'Araneides', 'Dipteres')")

    for id_ordre, parent_ordre, taxon_ordre in cur_ordre.fetchall() :
        try :
            cur_famille = conn.cursor()
            cur_famille.execute( " select id as id_famille, parent_skill as parent_ordre, skill as taxon_famille " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_ordre) )
            for id_famille, parent_ordre, taxon_famille in cur_famille.fetchall() :
                try:
                    cur_genre = conn.cursor()
                    cur_genre.execute( " select id as id_genre, parent_skill as parent_famille, skill as taxon_genre " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_famille) )

                    for id_genre, parent_famille, taxon_genre in cur_genre.fetchall() :
                        try:

                            cur_type = conn.cursor()
                            cur_type.execute( " select id as id_espece, parent_skill as parent_genre, skill as taxon_espece " +
                                 " from skillTree " +
                                 " where parent_skill = "+ str(id_genre) )



                            for id_espece, parent_genre, taxon_espece in cur_type.fetchall() :
                                try:
                                    #body = "Proposer un TAXON appartenant l'insecte sur la photo <br/>" + '\n'
                                    #body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
                                    #         '<select id=\"myselect\" name=\"myselect\" onchange=\"myFunction()\"> ' + '\n')
                                    body = ('<center><img width=\"200px\" src=\"$FILE\"></center>' + '\n' +
                                            '<input type="radio" name="MyRadio" value="qPrecis" id="r3" onclick="Checkradiobutton()" checked> ESPECE' + '\n' +
                                            '<select id=\"myselect\" name=\"select_qPrecis\" onchange=\"myFunction()\"> ' + '\n')
                                    #print(" TYPE ")
                                    #print(parent_ordre, ' ', taxon_ordre, ' ', parent_famille, ' ', taxon_famille, ' ', id_genre, ' ', taxon_genre, ' ', id_type, ' ', taxon_type)
                                    cur_taxon = conn.cursor()
                                    cur_taxon.execute( " select id as id_taxon, parent_skill as parent_type, skill as taxon " +
                                                      " from skillTree " +
                                                      " where parent_skill = "+ str(id_espece) )
                                    body += '<option value="">-</option>' + '\n'
                                    ct = 0
                                    for id_taxon, parent_type, taxon in cur_taxon.fetchall() :
                                        str_check = '('+taxon_genre.rstrip().lstrip()+' '+taxon_espece.rstrip().lstrip()+')'
                                        #print(str_check, ' ---- ', taxon)
                                        if str_check.replace('  ',' ') in taxon:
                                            """print(taxon_ordre)
                                            print(taxon_famille)
                                            print(taxon_genre+' '+taxon_espece)
                                            print()"""
                                            body += '<option value=\"'+taxon+'\">'+taxon+'</option>' + '\n'
                                            ct = ct + 1

                                    body += ('</select>\r\n<script> function myFunction(){ ' + '\n' +
                                             'var myDiv = document.getElementById("answer"); ' + '\n' +
                                             'var select = document.getElementById("myselect").value; ' + '\n' +
                                             'myDiv.value = select;} </script>')

                                    if ct == 0:
                                        print(" problème = 0 ")
                                        cur_taxon.execute( " select id as id_taxon, parent_skill as parent_type, skill as taxon " +
                                                      " from skillTree " +
                                                      " where parent_skill = "+ str(id_espece) )
                                        #body += '<option value="">-</option>' + '\n'

                                        for id_taxon, parent_type, taxon in cur_taxon.fetchall() :
                                            str_check = taxon_genre+' '+taxon_espece
                                            print(str_check.replace('  ',' '), ' ---- ', taxon)
                                        print()

                                    if ct > 1:
                                        print(" problème > 1 ")
                                        cur_taxon.execute( " select id as id_taxon, parent_skill as parent_type, skill as taxon " +
                                                      " from skillTree " +
                                                      " where parent_skill = "+ str(id_espece) )
                                        #body += '<option value="">-</option>' + '\n'

                                        for id_taxon, parent_type, taxon in cur_taxon.fetchall() :
                                            str_check = taxon_genre+' '+taxon_espece
                                            print(str_check.replace('  ',' '), ' ---- ', taxon)
                                        print()
                                    #print("----------------------------------")


                                    """print(body)
                                    print()"""

                                except:
                                    break

                                insert = conn.cursor()
                                sql = "INSERT INTO templateTable ( niv_taxon, parent, nbchild, body ) VALUES (%s, %s, %s, %s)"
                                #val = ('TAXON', taxon_espece, str(ct), body)
                                val = ('TAXON', taxon_famille, str(ct), body)
                                insert.execute(sql, val)
                                conn.commit()


                        except:
                            break

                except TypeError as e:
                    print(e)
                    break

        except TypeError as e:
            print(e)
            #return None
            break



"""
changement effectué
aricia => aricias

"""
print("Using mysql.connector…")
import mysql.connector
myConnection = mysql.connector.connect( host=hostname, user=username, passwd=password, db=database )

createTableTemplate( myConnection )
print('---ORDRE---')
insertTemplateORDRE( myConnection )
print('---ORDRE_FAMILLE---')
insertTemplateORDRE_FAMILLE( myConnection )
"""
print('---ORDRE_FAMILLE_GENRE---')
insertTemplateORDRE_FAMILLE_GENRE( myConnection )
print('---ORDRE_FAMILLE_GENRE_ESPECE---')
insertTemplateORDRE_FAMILLE_GENRE_ESPECE( myConnection )
print('---ORDRE_FAMILLE_GENRE_ESPECE_TAXON---')
"""
print('---ORDRE_FAMILLE_TAXON---')
insertTemplateORDRE_FAMILLE_GENRE_ESPECE_TAXON( myConnection )

myConnection.close()
