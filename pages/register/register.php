<?php

/**
 * register
 *
 * Show the form to Sign up
 *
 * @param $conn :
 *            A valid database PDO connection
 * @param $bool :
 *            A bool to true if the user tried a username already used else false
 */

 class Register extends Action {
    protected $userNameAlreadyUsed;

    function setUserNameAlreadyUsed($in){
        $this->userNameAlreadyUsed=$in;
    }

    function registerUser($conn, $bool){
        global $VIEW;
        $template=file_get_contents("templates/register.tmpl");

        $msg="";
        if ($bool)
            $msg="<br/><p class='error'>Username already used</p>";

        $template=str_replace("{CHECKALREADYEXISTS}",$msg,$template);

        $VIEW['MAIN'].=$template;
    }

    function render(){
        $this->registerUser($this->conn,$this->userNameAlreadyUsed);
    }
}

?>
