<?php

/*
 * An abstract class to model any action in the website. To use, override the render method.
*/

abstract class Action {
  protected $conn;

  function __construct($conn){
    $this->conn=$conn;
  }

  abstract function render();
}

?>

