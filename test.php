<?php

require_once("config.php");
require_once("pages/profile/profile.php");
require_once("lib/HWlib.php");
require_once("lib/HTML.php");
require_once("pages/skills/skills.php");
require_once("lib/artifacts.php");
require_once("pages/startup/startup.php");
require_once("pages/workflowdesign/workflowdesign.php");

// security test: do not enable in prod
if (!ENABLETESTING)
    die("Testing is not enabled");

/**
 * check
 *
 * Display the text and result of a boolean test
 *
 * @param  $string: The test message to display
 * @param  $test: The boolean test result
 * @return bool: the boolean test result
 */

function check($string,$test){
    echo "<li>$string ";
    if($test)
        echo "PASS";
    else
        echo "<span style='color:red;'>FAIL</span>";
    return $test;
}

function testArtifact($conn){
    $tuple=$conn->query("select node from Artifact where ownerid=2 and classid=6000");
    $node=$tuple->fetch()['node'];
    return check("At initialization, Headwork artifact should reach node 2",$node==2);
}

session_start();

$conn = HWdbconnect();

// login as admin

$_SESSION["login"]=true;
$_SESSION['wrong']=false;
$_SESSION["id"]=2; // admin
$_SESSION["username"]="admin";

echo "<ul>";
// reset

restart($conn);
initialize($conn);
startAllArtifact($conn,$_SESSION["id"]);

$allPass=true;

$allPass = $allPass && testArtifact($conn);

echo "</ul>";

if ($allPass)
    echo "ALL TESTS SUCCESSFUL";
else
    echo "TEST FAILURE";

?>
