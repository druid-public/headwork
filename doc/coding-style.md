# Coding styles

## Tentative

* ocaml function name convention (ocamlStyle)
* table names start with an uppercase
* each page should have its own directory, which name is the mode
  * Mode X
  * Directory pages X
  * file X.php
  * in it, a X function, taking $conn as input
* limited HTML in code
* No direct use of PDO->query, only prepared statements
