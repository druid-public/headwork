//For the first use of this test page, you must first have Node.js (https://nodejs.org/en/).
//Once installed, you must type this command :
//npm install --global mocha 
//Then you just have to launch the tests with the command :
//mocha testblockly.js 
//at the root of the headwork project.

const { Workspace, Block, Toolbox, ToolboxCategory } = require('blockly');
var Blockly = require('blockly');
var num = 0;

     
Blockly.Blocks['answers'] = {
  init: function() {
    this.appendValueInput("commande")
        .setCheck(null)
        .appendField("Do")
        .appendField(new Blockly.FieldTextInput("commande"), "commande")
        .appendField("awaiting")
        .appendField(new Blockly.FieldNumber(0), "answers")
        .appendField("answers");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
num+=1;

Blockly.Blocks['do_commande'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(" Do");
    this.appendStatementInput("com")
        .setCheck(null)
        .appendField("Awaiting")
        .appendField(new Blockly.FieldNumber(0), "reponse")
        .appendField("answers");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
num+=1;

Blockly.JavaScript['answers'] = function(block) {
  var text_commande = block.getFieldValue('commande');
  var number_answers = block.getFieldValue('answers');
  var value_commande = Blockly.JavaScript.valueToCode(block, 'commande', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var json = "Do " + text_commande + " awaiting " + number_answers + " answers "
  var code = JSON.stringify(json, null, '\t');
  return code;
};

Blockly.JavaScript['do_commande'] = function(block) {
  var number_reponse = block.getFieldValue('reponse');
  var statements_com = Blockly.JavaScript.statementToCode(block, 'com');
  if(statements_com != null) {
    var res = statements_com.replaceAll("\"","'")
  }
  // TODO: Assemble JavaScript into code variable.
  var json = " Do Awaiting " + number_reponse + " answers " + res 
  var code = JSON.stringify(json, null, '\t');
  return code;
};

Blockly.Blocks['ask_dropdown'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Ask to anyone ")
        .appendField(new Blockly.FieldTextInput("question"), "NAME")
        .appendField(new Blockly.FieldDropdown([["yes/no","yes/no"], ["free input","free input"], ["list","list"], ["integer","integer"], ["picture","picture"], ["real","real"], ["URL","URL"] ]), "DROP");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(20);
this.setTooltip("");
this.setHelpUrl("");
  }
};
num+=1;

Blockly.JavaScript['ask_dropdown'] = function(block) {
  var text_name = block.getFieldValue('NAME');
  var dropdown_name = block.getFieldValue('DROP');
  // TODO: Assemble JavaScript into code variable.
  var json ="Ask to anyone " + text_name + " " + dropdown_name
  var code = JSON.stringify(json, null, '\t');
  return code;
};

Blockly.Blocks['take_majority'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Take majority");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
this.setTooltip("");
this.setHelpUrl("");
  }
};
num+=1;

Blockly.JavaScript['take_majority'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var json = "Take majority"
  var code = JSON.stringify(json, null, '\t');
  return code;
};  

Blockly.Blocks['take_average'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Take average");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
num+=1;

Blockly.JavaScript['take_average'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var json = "Take average"
  var code = JSON.stringify(json, null, '\t');
  return code;
};

Blockly.Blocks['task_has_reached'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Do");
    this.appendStatementInput("NAME")
        .setCheck(null)
        .appendField("Task has reached absolute majority out of")
        .appendField(new Blockly.FieldNumber(0), "NAME");

    this.setColour(20);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};
num+=1;

Blockly.JavaScript['task_has_reached'] = function(block) {
  var number_name = block.getFieldValue('NAME');
  var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
  // TODO: Assemble JavaScript into code variable.
  if(statements_name != null) {
  var res = statements_name.replaceAll("\"","'")
  }
  var json = " Do Task has reached absolute majority out of " + number_name + " " + res
  var code = JSON.stringify(json, null, '\t');
  return code;
};

Blockly.Blocks['task_has_consensus'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Do");
    this.appendStatementInput("NAME")
        .setCheck(null)
        .appendField("Task has reached consensus out of")
        .appendField(new Blockly.FieldNumber(0), "NAME");
    this.setColour(290);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};
num+=1;


Blockly.JavaScript['task_has_consensus'] = function(block) {
  var number_name = block.getFieldValue('NAME');
  var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
  // TODO: Assemble JavaScript into code variable.
  if(statements_name != null) {
  var res = statements_name.replaceAll("\"","'")
  }
  var json = "Do Task has reached consensus out of " + number_name + " " + res
  var code = JSON.stringify(json, null, '\t');
  return code;
};
var workspace = new Workspace();

var bloc1 = new Block(workspace,'ask_dropdown');
var bloc2 = new Block(workspace,'answers');
var bloc3 = new Block(workspace,'do_commande');
var bloc4 = new Block(workspace,'task_has_consensus');
var bloc5 = new Block(workspace,'take_majority');

var toolbox = new Toolbox(workspace);
var cat = new ToolboxCategory('Blocs customs',toolbox)
var name = cat.toolboxItemDef_;
bloc2.setFieldValue('une commande','commande');
bloc2.setFieldValue('une réponse','answers');
var code = bloc2.getFieldValue();

var test = require('unit.js');
describe('Learning by the example', function(){
  //Test that verifies the implementation of Blockly
  it('Blockly display', function(){
    test.object(workspace).hasProperty('id');
  });
  //Test that verifies the creation of blocks
  it('Blocs display', function(){
    test.object(bloc1).hasProperty('id');
    test.object(bloc2).hasProperty('id');
    test.object(bloc3).hasProperty('id');
    test.object(bloc4).hasProperty('id');
    test.object(bloc5).hasProperty('id');
  });
  it('Blocs code', function(){
    test.string(code).contains('Do');
  });
  it('Number of block', function(){
    test.number(num).is(7);
  });
  //Test that verifies the correct number of statement inputs on the blocks
  it('Blocs fit', function(){
    test.number(bloc1.statementInputCount).is(0);
    test.number(bloc2.statementInputCount).is(0);
    test.number(bloc3.statementInputCount).is(1);
  });
  it('Category create', function(){
    test.object(cat).hasProperty('id_');
  });
  it('Category name', function(){
    test.string(name).contains('Blocs customs');
  });
});